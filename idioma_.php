<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--><html class="no-js" lang="es"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title>Museo Km/h Guanajuato</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="estilo.css">
    <link rel="stylesheet" href="css/flexslider.css">
    <link rel="stylesheet" href="css/menu.min.css">    
    <link rel="stylesheet" href="bower_components/animate.css/animate.min.css">
    <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
</head>
<body id="top">
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v3.1';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<section class="mod-principal">
    <section class="navigation">
        <div class="menu-usuario">
            <div class="idioma">
                <div id="dd" class="wrapper-dropdown-3" tabindex="1">
                	<img class="ico-idioma" src="img/idioma.svg" alt="Idioma"><span>Idioma</span>
                    <ul class="dropdown">
                    	<li><a href="#">Español</a></li>
                        <li><a href="#">Inglés</a></li>
                    </ul>
                </div>
            </div>
				<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
                <script type="text/javascript">
                    
                    function DropDown(el) {
                        this.dd = el;
                        this.placeholder = this.dd.children('span');
                        this.opts = this.dd.find('ul.dropdown > li');
                        this.val = '';
                        this.index = -1;
                        this.initEvents();
                    }
                    DropDown.prototype = {
                        initEvents : function() {
                            var obj = this;
        
                            obj.dd.on('click', function(event){
                                $(this).toggleClass('active');
                                return false;
                            });
        
                            obj.opts.on('click',function(){
                                var opt = $(this);
                                obj.val = opt.text();
                                obj.index = opt.index();
                                obj.placeholder.text(obj.val);
                            });
                        },
                        getValue : function() {
                            return this.val;
                        },
                        getIndex : function() {
                            return this.index;
                        }
                    }
        
                    $(function() {
        
                        var dd = new DropDown( $('#dd') );
        
                        $(document).click(function() {
                            // all dropdowns
                            $('.wrapper-dropdown-3').removeClass('active');
                        });
        
                    });
        
                </script>
            <!--idioma-->
        
            <div class="redes">
                <a class="facebook" href="#"></a>
                <a class="twitter" href="#"></a>
                <a class="youtube" href="#"></a>
                <a class="instagram" href="#"></a>
            </div>
            <!--redes-sociales-->
        </div>
        
        <div class="logo">
        	<div class="logo-prin">
	        	<img class="logo-km-h" src="img/museo-km-h-gto.svg" alt="Museo Km/h">
            </div>
             <!--Museo km/h-->
             
             <div class="busqueda">
             </div>
             <!--búsqueda-->
        </div>
        <!--Museo km/h-búsqueda-->
        
        <div class="menu-principal">
        	<div class="contenido">
                <div class="ve-menu">
                  <ul class="ve-menu-pc">
                    <li><a href="index.php">Inicio</a></li>
                    <li class="item-menu-1"><a href="acerca-del-museo.php">Acerca del Museo</a></li>
                    <li><a href="#">Salas de Exhibición</a>
                      <ul>
                        <li><a href="#">Evolución e Historia</a></li>
                        <li><a href="sala-2.php">Anatomía del Automóvil</a></li>
                        <li><a href="#">Cadena de ensamblaje</a></li>
                        <li><a href="sala-4.php">Galería de Autos</a></li>
                        <li><a href="#">El futuro</a></li>
                      </ul>
                    </li>
                    <li><a href="eventos.php">Eventos</a></li>
                    <li class="item-menu-2"><a href="#">Información para el visitante</a>
                      <ul>
                        <li><a href="como-llegar.php">Como llegar</a></li>
                        <li><a href="costos.php">Costos</a></li>
                        <li><a href="mapa-del-museo.php">Mapa del Museo</a></li>
                        <li><a href="planifica-tu-visita.php">Planifica tu visita</a></li>
                        <li><a href="grupos-excursiones.php">Grupos y excursiones</a></li>
                      </ul>
                    </li>
                  </ul>
                </div>       
            </div>
        </div>
        <!--menú-principal-->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/fastclick/1.0.6/fastclick.min.js"></script>
        <script src="js/menu.min.js"></script>
        <script>
            $('.ve-menu').menu({
                fontSize: 17,
                fontColor: '#222',
                bgColor: '#fff',
                hoverFontColor: '#0694a7',
                hoverBgColor: '#fff',
                itemSpace: 5,
                subFontSize: 17,
				itemWidth: 70,
                animate: 'slide',
                speed: 500,
            });
        </script>
    </section>
</section>
<!--Módulo-principal-->

<main id="content">
	<div id="slide-principal">
    	<div id="my-slider">
        	<img data-lazy-src="img/slide/cadillac.jpg" />
            <img data-lazy-src="img/slide/sala-2.jpg" />
            <img data-lazy-src="img/slide/sala-4.jpg" />
        </div>
        <script src="https://code.jquery.com/jquery-3.2.0.min.js"></script>
        <script src="js/jquery.devrama.slider.js"></script>
        <script>
		$('#my-slider').DrSlider({transition: 'random',navigationType: 'circle',positionNavigation: 'in-center-bottom', progressColor: '#0694a7'});
		</script>
    </div>
    <!--slide-principal-->
    
	<div class="contenido">
    	<div class="col-2">
        	<div class="automovil">
            	<img class="auto-km" src="img/museo-del-auto-gto.png" alt="Museo del  Automóvil Guanajuato">
            </div>
            <!--ilustra-km/h-->
        	
        	<div class="flow-d">
                <div class="planifica-home">
                    <a href="planifica-tu-visita.php">
                    <img class="log-planea" src="img/planifica-visita.svg" alt="Planifica tu visita al Km/h"></a>
                </div>
                <!--planifica-tu-visita-km/h-->
                
                <div class="compartir-home">
                    <div class="social">
                        <div class="fb-share-button" data-href="https://developers.facebook.com/docs/plugins/" data-layout="button_count" 
                        data-size="small" data-mobile-iframe="true">
                        <a target="_blank" href=
                        "https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fdevelopers.facebook.com%2Fdocs%2Fplugins%2F&amp;src=sdkpreparse"
                        class="fb-xfbml-parse-ignore">Compartir</a></div>
                    </div>
                    <!--facebook-->
                    
                    <div class="social">
                        <a href="https://twitter.com/share?ref_src=twsrc%5Etfw" class="twitter-share-button" data-show-count="false">Tweet</a>
                        <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
                    </div>
                    <!--twitter-->
                </div>
                <!--compartir-web-km/h-->
            </div>
        </div>
        <!--ilustra-redes-sociales-->

    	<div class="col-3">
        	<h1>Eventos</h1>
            <table class="tabla">
              <tbody>
                <tr>
                  <td class="celda"><a class="link-eventos-1" href="#">10:30 A.M.</a></td>
                  <td class="celda"><a class="link-eventos-2" href="#">Ensamblando una Gran Exposición</a></td>
                  <td class="celda"><a class="link-eventos-1" href="#">JUL. 18</a></td>
                </tr>
                <tr>
                  <td class="celda"><a class="link-eventos-1" href="#">11:00 A.M.</a></td>
                  <td class="celda"><a class="link-eventos-2" href="#">Ensamblando una Gran Exposición</a></td>
                  <td class="celda"><a class="link-eventos-1" href="#">JUL. 20</a></td>
                </tr>
                <tr>
                  <td class="celda"><a class="link-eventos-1" href="#">04:30 P.M.</a></td>
                  <td class="celda"><a class="link-eventos-2" href="#">Ensamblando una Gran Exposición</a></td>
                  <td class="celda"><a class="link-eventos-1" href="#">JUL. 23</a></td>
                </tr>
                <tr>
                  <td class="celda"><a class="link-eventos-1" href="#">06:00 P.M.</a></td>
                  <td class="celda"><a class="link-eventos-2" href="#">Ensamblando una Gran Exposición</a></td>
                  <td class="celda"><a class="link-eventos-1" href="#">JUL. 25</a></td>
                </tr>
                <tr>
                  <td class="celda"><a class="link-eventos-1" href="#">11:30 A.M.</a></td>
                  <td class="celda"><a class="link-eventos-2" href="#">Ensamblando una Gran Exposición</a></td>
                  <td class="celda"><a class="link-eventos-1" href="#">JUL. 27</a></td>
                </tr>
              </tbody>
            </table>
        </div>
        <!--eventos-->
    </div>
    <!--eventos-social-media-->
    
	<div id="ubicacion">
        <div class="contenido">
	    	<h1>Nuestra ubicación</h1>
        </div>
        <!--título-mapa-->
        
        <div class="mapa">
        	<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3725.889904313068!2d-101.36213768558427!3d20.956935995603498!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x842b9d9bfe557e1d%3A0xc90f71de2ba65558!2sParque+Guanajuato+Bicentenario!5e0!3m2!1ses!2smx!4v1532569039518" width="100%" 
                height="310" frameborder="0" style="border:0" allowfullscreen></iframe>
		</div> 
        <!--Mapa Museo Km/hr-->
        
        <div class="contenido">
            <div class="col-1">
            	<img class="ico" src="img/address.svg">
                Carretera de Cuota Silao-Gto. Km 3.8, Los Rodríguez, 36270 Silao, Gto.
            </div>
            <!--Dirección-->
            
            <div class="col-1">
            	<img class="ico" src="img/phone.svg">
                <a class="link-ubicacion" href="#">01 472 + 0 000 000</a>
            </div>
            <!--Teléfono-->
        </div>
        <!--Ubicacción Museo Km/h-->
    </div>
     <!--ubicación-->
     
     <div class="experiencia-museo">
     	<div class="contenido">
        	<h3>Tu experiencia en el Museo</h3>
            <div class="flexslider">
            	<ul class="slides">
                	<li>
                    	<h4>"Si no has estado en el KM/h por un tiempo, puede ser el momento de pasar y ver qué hay de nuevo. 
                        ¡Te sorprenderás! ". </h4>
                        <p class="author">Juanita Castro</p>
                    </li>
                	<li>
                    	<h4>"Experiencia satisfactoria ir a visitar el Museo del Auto"</4>
                        <p class="author">Manuel Rangel</p>
                    </li>
                	<li>
                    	<h4>"La parte buena es que es muy interactivo, hay mucha diversión para los niños."</h2>
                        <p class="author">Juan Pablo Ponce</p>
                    </li>
                </ul>
            </div>
            <!--slider-testimonios-->
        </div>
     </div>
     <!--Tu experiencia en el Museo Km/h-->
     
     <footer>
     	<div class="contenido">
        	<div class="km-h">
            	<img src="img/km-h-museo.svg" alt="Museo Km/h">
            </div>
            <!--Logotipo Km/h-->
            
            <div class="col-4">
            	<h2>Salas de Exhibición</h2>
                <ul class="submenu">
                	<li><a href="#">Autocinema</a></li>
                    <li><a href="#">Anatomía del  automóvil</a></li>
                    <li><a href="#">Cadena de producción</a></li>
                    <li><a href="#">Galería de autos</a></li>
                    <li><a href="#">El futuro</a></li>
                </ul>
            </div>
            <!--submenú-1 Km/h-->

            <div class="col-4">
            	<h2>Información para el Visitante</h2>
                <ul class="submenu">
                	<li><a href="como-llegar.php">Como llegar</a></li>
                    <li><a href="costos.php">Costos</a></li>
                    <li><a href="mapa-del-museo.php">Mapa del Museo</a></li>
                    <li><a href="planifica-tu-visita.php">Planifica tu visita</a></li>
                    <li><a href="grupos-excursiones.php">Grupos y excursiones</a></li>
                </ul>
            </div>
            <!--submenú-2 Km/h-->

            <div class="col-4">
            	<h2>Ubicación y horarios</h2>
                <div class="dato">
                	<img class="ico-dato" src="img/address-f.svg"> Carretera de Cuota Silao-Gto. Km 3.8, Los Rodríguez, 36270 Silao, Gto.
                </div>
                
                <div class="dato">
                	<img class="ico-dato" src="img/phone-f.svg"> <a class="link-footer" href="#">01 472 + 0 000 000</a>
                </div>
                
                <div class="dato">
                    Abierto: 10:00 a 19:00 hrs.<br>
                    Martes a Domingo                
                </div>
            </div>
            <!--dirceción Km/h-->
        </div>
     </footer>
     <!--Datos del Museo Km/h-->
     
	<div class="derechos"> 
    	Copyright ©  <?php include ("copy-date.php")?> <img class="logo-museo" src="img/km-h-gto.svg" alt="Museo Km/h">
        
        <section class="to-top">
        	<div class="row">
            	<div class="to-top-wrap">
                	<a href="#top" class="top"><i class="fa fa-angle-up"></i></a>
                </div>
            </div>
        </section>
    </div>
    <!--copyright © 2018 Km/h-->     
    
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.min.js"><\/script>')</script>
    <script src="js/jquery.fancybox.pack.js"></script>
    <script src="js/vendor/bootstrap.min.js"></script>
    <script src="js/scripts.js"></script>
    <script src="js/jquery.flexslider-min.js"></script>
    <script src="bower_components/classie/classie.js"></script>
    <script src="bower_components/jquery-waypoints/lib/jquery.waypoints.min.js"></script>
</body>
</html>
