
<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="es"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title>Museo Km/h Guanajuato</title>
     <base href="http://masviral.mx/ftpmasviral/web/"; />
   <!-- <base href="http://localhost/museoautomovil/"; />-->
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	
    <link rel="stylesheet" type="text/css" href="bower_components/dist/jquery.fancybox.min.css">

    <link rel="stylesheet" href="estilo.css">
    <link rel="stylesheet" href="css/flexslider.css">
    <link rel="stylesheet" href="css/menu.min.css">    
    <link rel="stylesheet" href="css/queries.css">
    <link rel="stylesheet" href="bower_components/animate.css/animate.min.css">
    <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
<style type="text/css">
body,td,th {
	font-family: Roboto-Light, Sans-Serif;
}
</style>
</head>
<body id="top">
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v3.1';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<section class="mod-principal"><? include_once("header.php");?></section>
<!--Módulo-principal-->

<main id="content">
	<? include_once("modulos/banners/banners_principal.php");?>
    <!--slide-principal-->
    
	<div class="contenido">
    	<div class="col-2">
        	<div class="automovil">
            	<img class="auto-km" src="img/museo-del-auto-gto.png" alt="Museo del  Automóvil Guanajuato">
            </div>
            <!--ilustra-km/h-->
        	
        	<div class="flow-d">
                <div class="planifica-home">
                    <a href="seccion.php?s=4">
                    <img class="log-planea" src="img/planifica-visita.svg" alt="Planifica tu visita al Km/h"></a>
                </div>
                <!--planifica-tu-visita-km/h-->
                
                <div class="compartir-home">
                    <div class="social">
                        <div class="fb-share-button" data-href="https://developers.facebook.com/docs/plugins/" data-layout="button_count" 
                        data-size="small" data-mobile-iframe="true">
                        <a target="_blank" href=
                        "https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fdevelopers.facebook.com%2Fdocs%2Fplugins%2F&amp;src=sdkpreparse"
                        class="fb-xfbml-parse-ignore">Compartir</a></div>
                    </div>
                    <!--facebook-->
                    
                    <div class="social">
                        <a href="https://twitter.com/share?ref_src=twsrc%5Etfw" class="twitter-share-button" data-show-count="false">Tweet</a>
                        <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
                    </div>
                    <!--twitter-->
                </div>
                <!--compartir-web-km/h-->
            </div>
        </div>
        <!--ilustra-redes-sociales-->
		<? include_once("modulos/eventos/eventos_principal.php");?>
    	
        <!--eventos-->
    </div>
    <!--eventos-social-media-->
    
	<div id="ubicacion">
        <div class="contenido">
	    	<h1><? echo titulotraduccion($idIdioma, 'Nuestra ubicación');?></h1>
        </div>
        <!--título-mapa-->
        
        <div class="mapa">
        	<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3725.889904313068!2d-101.36213768558427!3d20.956935995603498!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x842b9d9bfe557e1d%3A0xc90f71de2ba65558!2sParque+Guanajuato+Bicentenario!5e0!3m2!1ses!2smx!4v1532569039518" width="100%" 
                height="310" frameborder="0" style="border:0" allowfullscreen></iframe>
		</div> 
        <!--Mapa Museo Km/hr-->
        
        <div class="contenido">
            <div class="col-1">
            	<img class="ico" src="img/address.svg">
                <? echo $direccion;?><!-- Carretera de Cuota Silao-Gto. Km 3.8, Los Rodríguez, 36270 Silao, Gto.-->
            </div>
            <!--Dirección-->
            
            <div class="col-1">
            	<img class="ico" src="img/phone.svg">
                 <a href="tel:<? echo $telefono;?>" class="link-ubicacion"><? echo $telefono;?></a>
            </div>
            <!--Teléfono-->
        </div>
        <!--Ubicacción Museo Km/h-->
    </div>
     <!--ubicación-->
     
     
            <? include_once("modulos/testimonios/testimonios_principal.php");?>
          
     <!--Tu experiencia en el Museo Km/h-->
     <!-- footer--->
   <?php include_once("footer.php");?>
     <!--Datos del Museo Km/h-->
   <?php include_once("derechos.php");?>
	
    <!--copyright © 2018 Km/h-->     
    

    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.min.js"><\/script>')</script>
    <script src="bower_components/dist/jquery.fancybox.min.js"></script>
    <script src="js/vendor/bootstrap.min.js"></script>
    <script src="js/scripts.js"></script>
    <script src="js/jquery.flexslider-min.js"></script>
    <script src="bower_components/classie/classie.js"></script>
    <script src="bower_components/jquery-waypoints/lib/jquery.waypoints.min.js"></script>
    <script src="js/jquery.mobile.custom.js"></script>
</body>
</html>
