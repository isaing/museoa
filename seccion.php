<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="es"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title>Museo Km/h Guanajuato</title>
  <base href="http://masviral.mx/ftpmasviral/web/"; />
    <!-- <base href="http://localhost/museoautomovil/"; />-->
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	
   <link rel="stylesheet" type="text/css" href="bower_components/dist/jquery.fancybox.min.css">
   <link rel="stylesheet" href="estilo.css">
    <link rel="stylesheet" href="css/menu.min.css">    
    <link rel="stylesheet" href="bower_components/animate.css/animate.min.css">
    <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
</head>
<body id="top">
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v3.1';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<section class="mod-principal">
    <? include_once("header.php");?>
</section>
<!--Módulo-principal-->

<!-- datos secciones parte común-->
<? $idseccion= intval($_GET["s"]);
	$sentencias = "CALL paPaginaSecciones('$idIdioma','2','$idseccion')";
	//echo $sentencias;
	$resultados = consulta($sentencias);
	$cuantoss=$resultados->num_rows;
	if($cuantoss>0){
		while ($rows = $resultados->fetch_array(MYSQLI_ASSOC)){
		$numeroseccion=$rows["numeroseccion"];
		$titulo=$rows["seccion"];
		$descripcionseccion=$rows["descripcionseccion"];
		$subtitulo=$rows["subtitulo"];
		$iconoseccion=$rows["icono"];
		
		}
	}
		

?>
<? if($cuantoss>0){?>	
    <div class="encabezado-info-visitante" style="background: url(modulos/img/seccionesfondos/seccionesfondos-<? echo $numeroseccion;?>.jpg) center center;background-repeat: no-repeat; background-color: #2e2d2c;
    background-size: cover; background-position: center; background-position-y: initial;">
        <div class="contenido">
            <h1 class="tit-info-visit"><? echo $titulo;?></h1>
            <div class="txt-info-visit">
               <? echo $descripcionseccion;?>
            </div>
            <!--tarifas Kh/h-->
        </div>
    </div>
    <!--Información para el Visitante-->
    
    <div class="contenido">
        <div class="sub-tit-visit">
        <? 
		$clase="ico-info-visit";
		/* si es seccion de planifica, se cambia la clase para que no sea tan pequeño el ícono*/
		if($idseccion==4)
		$clase="ico-planifica-visit";
		?>
        
            <img class="<? echo $clase;?>" src="modulos/img/seccionesiconos/<? echo $iconoseccion;?>" alt="<? echo $titulo;?>">
            <? echo $subtitulo;?>
        </div>
        
     <? if($idseccion==1){ include_once("modulos/secciones/comollegar.php"); }?>
     <? if($idseccion==2){ include_once("modulos/secciones/costos.php"); }?>
     <? if($idseccion==3){ include_once("modulos/secciones/mapamuseo.php"); }?>
     <? if($idseccion==4){ include_once("modulos/secciones/planifica.php"); }?>
     <? if($idseccion==5){ include_once("modulos/secciones/gruposexcursiones.php"); }?>
      
      
      
      
    </div>
    <!--Tarifa Km/h-->
<? }?>
     <?php include_once("footer.php");?>
     <!--Datos del Museo Km/h-->
     
	 <?php include_once("derechos.php");?>
	 <!--copyright © 2018 Km/h-->        
    

    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.min.js"><\/script>')</script>
    <script src="bower_components/dist/jquery.fancybox.min.js"></script>
    <script src="js/vendor/bootstrap.min.js"></script>
    <script src="js/scripts.js"></script>
    <script src="js/jquery.flexslider-min.js"></script>
    <script src="bower_components/classie/classie.js"></script>
    <script src="bower_components/jquery-waypoints/lib/jquery.waypoints.min.js"></script>
</body>
</html>
