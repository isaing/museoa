<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="es"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title>Museo Km/h Guanajuato</title>
    <base href="http://masviral.mx/ftpmasviral/web/"; />
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="estilo.css">
    <link rel="stylesheet" type="text/css" href="bower_components/dist/jquery.fancybox.min.css">
    <link rel="stylesheet" href="css/menu.min.css">    
    <link rel="stylesheet" href="bower_components/animate.css/animate.min.css">
    <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
</head>
<body id="top">
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v3.1';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<section class="mod-principal">
    <section class="navigation">
        <div class="menu-usuario">
            <div class="idioma">
            </div>
            <!--idioma-->
        
            <div class="redes">
                <a class="facebook" href="#"></a>
                <a class="twitter" href="#"></a>
                <a class="youtube" href="#"></a>
                <a class="instagram" href="#"></a>
            </div>
            <!--redes-sociales-->
        </div>
        
        <div class="logo">
        	<div class="logo-prin">
	        	<a href="index.php"> <img class="logo-km-h" src="img/museo-km-h-gto.svg" alt="Museo Km/h"></a>
            </div>
             <!--Museo km/h-->
             
             <div class="busqueda">
             </div>
             <!--búsqueda-->
        </div>
        <!--Museo km/h-búsqueda-->
        
        <div class="menu-principal">
        	<div class="contenido">
                <div class="ve-menu">
                  <ul class="ve-menu-pc">
                    <li><a href="index.php">Inicio</a></li>
                    <li><a href="acerca-del-museo.php">Acerca del Museo</a></li>
                    <li><a href="#">Salas de Exhibición</a>
                      <ul>
                        <li><a href="#">Evolución e Historia del Automóvil</a></li>
                        <li><a href="sala-2.php">Anatomía del Automóvil</a></li>
                        <li><a href="#">Cadena de ensamblaje</a></li>
                        <li><a href="sala-4.php">Galería de Autos</a></li>
                        <li><a href="#">El futuro</a></li>
                      </ul>
                    </li>
                    <li><a href="eventos.php">Eventos</a></li>
                    <li><a href="#">Información para el visitante</a>
                      <ul>
                        <li><a href="como-llegar.php">Como llegar</a></li>
                        <li><a href="costos.php">Costos</a></li>
                        <li><a href="mapa-del-museo.php">Mapa del Museo</a></li>
                        <li><a href="planifica-tu-visita.php">Planifica tu visita</a></li>
                        <li><a href="grupos-excursiones.php">Grupos y excursiones</a></li>
                      </ul>
                    </li>
                  </ul>
                </div>       
            </div>
        </div>
        <!--menú-principal-->
		<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/fastclick/1.0.6/fastclick.min.js"></script>
        <script src="js/menu.min.js"></script>
        <script>
            $('.ve-menu').menu({
                fontSize: 17,
                fontColor: '#222',
                bgColor: '#fff',
                hoverFontColor: '#0694a7',
                hoverBgColor: '#fff',
                itemSpace: 5,
                subFontSize: 17,
                itemWidth: 70,
                animate: 'slide',
                speed: 500,
            });
        </script>
    </section>
</section>
<!--Módulo-principal-->

<div class="encabezado-info-visitante">
	<div class="contenido">
    	<h1 class="tit-info-visit">Grupos y Excursiones</h1>
        <div class="txt-info-visit">
        	¿Cómo Traer un gran grupo al Km/h?
        </div>
        <!--tarifas Km/h-->
    </div>
</div>
<!--Información para el Visitante-->

<div class="contenido">
	<div class="sub-tit-visit">
    	<img class="ico-info-visit" src="img/group-excursiones.svg" alt="Grupos y Excursiones Km/h">
        Los grupos de 20 o más visitantes reciben un descuento al reservar con al menos dos semanas de anticipación.
    </div>
    
    <div class="flow-a">
    	<div class="perfil-grupo">
        	<img class="img-grupo" src="img/excursiones-escolares.jpg" alt="Excursiones escolares"><br>
            <span class="tiket">Excursiones escolares</span><br>
            <div class="txt-perfil">
                Los paseos escolares dirigidos por maestros inspiran a los estudiantes a hacer preguntas y comprender el impacto de la industria en 
                nuestras  vidas.
            </div>
            
            <div class="tit-descarga-perfil">
                <a class="link-descarga" href="#" target="_blank">
                <img class="ico-perfil" src="img/pdf.svg" alt="Descarga el mapa"> Descarga: Formato de reserva (70KB)</a>
            </div>
        </div>
        <!--grupo 1-->

    	<div class="perfil-grupo">
        	<img class="img-grupo" src="img/operadores-turisticos.jpg" alt="Operadores turisticos"><br>
            <span class="tiket">Operadores turísticos</span><br>
            <div class="txt-perfil">
            	Los operadores turísticos profesionales reciben descuentos para traer experiencias memorables a sus grupos.
            </div>
            <div class="tit-descarga-perfil">
                <a class="link-descarga" href="#" target="_blank">
                <img class="ico-perfil" src="img/pdf.svg" alt="Descarga el mapa"> Descarga: Formato de reserva (70KB)</a>
            </div>
        </div>
        <!--grupo 2-->

    	<div class="perfil-grupo">
        	<img class="img-grupo" src="img/planificadores-de-grupo.jpg" alt="Planificadores de grupo"><br>
            <span class="tiket">Planificadores de grupo</span><br>
            <div class="txt-perfil">
           	 Cualquier grupo de 15 o más-scouts, grupos sociales, aficionados, incluso grandes familias puede recibir descuentos y reservar tours.
            </div>
            <div class="tit-descarga-perfil">
                <a class="link-descarga" href="#" target="_blank">
                <img class="ico-perfil" src="img/pdf.svg" alt="Descarga el mapa"> Descarga: Formato de reserva (70KB)</a>
            </div>
        </div>
        <!--grupo 3-->
    </div>
</div>
<!--Grupos y Excursiones Km/h-->

     <footer>
     	<div class="contenido">
        	<div class="km-h">
            	<img src="img/km-h-museo.svg" alt="Museo Km/h">
            </div>
            <!--Logotipo Km/h-->
            
            <div class="col-4">
            	<h2>Salas de Exhibición</h2>
                <ul class="submenu">
                	<li><a href="#">Autocinema</a></li>
                    <li><a href="#">Anatomía del  automóvil</a></li>
                    <li><a href="#">Cadena de producción</a></li>
                    <li><a href="#">Galería de autos</a></li>
                    <li><a href="#">El futuro</a></li>
                </ul>
            </div>
            <!--submenú-1 Km/h-->

            <div class="col-4">
            	<h2>Información para el Visitante</h2>
                <ul class="submenu">
                	<li><a href="como-llegar.php">Como llegar</a></li>
                    <li><a href="costos.php">Costos</a></li>
                    <li><a href="mapa-del-museo.php">Mapa del Museo</a></li>
                    <li><a href="planifica-tu-visita.php">Planifica tu visita</a></li>
                    <li><a href="grupos-excursiones.php">Grupos y excursiones</a></li>
                </ul>
            </div>
            <!--submenú-2 Km/h-->

            <div class="col-4">
            	<h2>Ubicación y horarios</h2>
                <div class="dato">
                	<img class="ico-dato" src="img/address-f.svg"> Carretera de Cuota Silao-Gto. Km 3.8, Los Rodríguez, 36270 Silao, Gto.
                </div>
                
                <div class="dato">
                	<img class="ico-dato" src="img/phone-f.svg"> <a class="link-footer" href="#">01 472 + 0 000 000</a>
                </div>
                
                <div class="dato">
                    Abierto: 10:00 a 19:00 hrs.<br>
                    Martes a Domingo                
                </div>
            </div>
            <!--dirceción Km/h-->
        </div>
     </footer>
     <!--Datos del Museo Km/h-->
     
	<div class="derechos"> 
    	Copyright ©  <?php include ("copy-date.php")?> <img class="logo-museo" src="img/km-h-gto.svg" alt="Museo Km/h">
        <section class="to-top">
            <div class="row">
                <div class="to-top-wrap">
                    <a href="#top" class="top"><i class="fa fa-angle-up"></i></a>
                </div>
            </div>
        </section>
    </div>
    <!--copyright © 2018 Km/h-->     
    

    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.min.js"><\/script>')</script>
    <script src="bower_components/dist/jquery.fancybox.min.js"></script>
    <script src="js/vendor/bootstrap.min.js"></script>
    <script src="js/scripts.js"></script>
    <script src="js/jquery.flexslider-min.js"></script>
    <script src="bower_components/classie/classie.js"></script>
    <script src="bower_components/jquery-waypoints/lib/jquery.waypoints.min.js"></script>
</body>
</html>
