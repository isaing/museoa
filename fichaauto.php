<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="es"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title>Museo Km/h Guanajuato</title>
  <base href="http://masviral.mx/ftpmasviral/web/"; />
    
     <!--<base href="http://localhost/museoautomovil/"; />-->
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="estilo.css">
    <link rel="stylesheet" type="text/css" href="bower_components/dist/jquery.fancybox.min.css">
    <link rel="stylesheet" href="css/menu.min.css">    
    <link rel="stylesheet" href="bower_components/animate.css/animate.min.css">
    <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
    <link rel="stylesheet" type="text/css" href="css/base.css" />
	<script>document.documentElement.className="js";var supportsCssVars=function(){var e,t=document.createElement("style");
	return t.innerHTML="root: { --tmp-var: bold; }",document.head.appendChild(t),e=!!
	(window.CSS&&window.CSS.supports&&window.CSS.supports("font-weight","var(--tmp-var)")),t.parentNode.removeChild(t),e};supportsCssVars()||
	alert("Please view this demo in a modern browser that supports CSS Variables.");</script>  
</head>
<body id="top">
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v3.1';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<section class="mod-principal">
     <? include_once("header.php");?>
</section>

<? 
	$sentencias = "CALL paPaginaSalas('$idIdioma','4','0')";
	//echo $sentencias;
	$resultados = consulta($sentencias);
	$cuantoss=$resultados->num_rows;
	if($cuantoss>0){
		while ($rows = $resultados->fetch_array(MYSQLI_ASSOC)){
		//$idsala=$rows["idsala"];
		$idsalaidioma=$rows["idsala"];
		//echo $idsalaidioma;
		$numerosala=$rows["numerosala"];
		$nombresala=$rows["nombresala"];
		
		}
	}
		$numerosala=titulotraduccion($idIdioma, 'Sala'). " " .str_pad ( $numerosala , 2 ,"0" , STR_PAD_LEFT  );
	$resultados->close();

?>
<div class="contenido">
    	<h1 class="sala-detalle"><? echo $numerosala;?> <span class="acento"><? echo $nombresala;?></span></h2>
        
         <? 
		 $idauto= intval($_GET["s"]);
		 $sentenciaa = "CALL paPaginaAutos('$idIdioma','2','$idauto')";
           // echo $sentenciaa;
                $resultadoa = consulta($sentenciaa);
                $cuantosa=$resultadoa->num_rows;
				
                if($cuantosa>0){
		?>
           		
                <? while ($rowa = $resultadoa->fetch_array(MYSQLI_ASSOC)){
                    $id_auto=$rowa["idauto"];
					$idmarca=$rowa["idmarca"];
                    $marca=$rowa["marca"];
                    $modelo=$rowa["modelo"];
                    $anio=$rowa["anio"];
					$idpais=$rowa["idpais"];
					$pais=$rowa["pais"];
					$bandera=$rowa["bandera"];
					$descripcion=$rowa["descripcion"];
					$datosvehiculo=$rowa["datosvehiculo"];
					
				?>
                <div class="auto-detalle-1">
                        <div class="flow-a">
                            <img class="movil-detalle" src="modulos/img/modelos/modelos-<? echo $id_auto;?>.jpg" alt="<? echo $modelo;?>">
                            <!--automóvil-km/h-->
                            
                            <div class="planifica-home">
                                <a href="seccion/4">
                                <img class="planea-detalle" src="img/planifica-visita.svg" alt="Planifica tu visita al Km/h"></a>
                            </div>
                            <!--planifica-tu-visita-km/h-->
                            
                            <div class="compartir-home">
                                <div class="social">
                                    <div class="fb-share-button" data-href="https://developers.facebook.com/docs/plugins/" data-layout="button_count" 
                                    data-size="small" data-mobile-iframe="true">
                                    <a target="_blank" href=
                                    "https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fdevelopers.facebook.com%2Fdocs%2Fplugins%2F&amp;src=sdkpreparse"
                                    class="fb-xfbml-parse-ignore">Compartir</a></div>
                                </div>
                                <!--facebook-->
                                
                                <div class="social">
                                    <a href="https://twitter.com/share?ref_src=twsrc%5Etfw" class="twitter-share-button" data-show-count="false">Tweet</a>
                                    <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
                                </div>
                                <!--twitter-->
                            </div>
                            <!--compartir-web-km/h-->
                        </div>
                    </div>
                    <!--fotografía-automóvil-->
            
                    <div class="auto-detalle-2">
                        <div class="tit-detalle"><span class="marca"><? echo strtoupper($marca);?></span> <? echo strtoupper( $modelo);?></div>
                        <div class="txt-detalle">
                           <? echo nl2br($descripcion);?>
                        </div>
                        <div class="txt-origen">
                            <img class="bandera-detalle" src="modulos/img/paises/<? echo $bandera;?>" alt="<? echo $pais;?>"> <? echo titulotraduccion($idIdioma, 'País de origen');?>:<? echo $pais;?>
                        </div>
                        <div class="txt-origen">
                            <? echo titulotraduccion($idIdioma, 'Año');?>:<? echo $anio;?>
                        </div>
                    </div>
                    <!--datos-automóvil-->
                </div>
                <!--información-gral.-automóvil-->
            
                <main>
                    <div class="grid">
                            <div class="grid__item theme-9">
                                <div class="grid__item-content">
                                    <span class="grid__item-meta"><? echo titulotraduccion($idIdioma, 'Datos del Vehículo');?>:</span>
                                    <p class="grid__item-text">
                                        <? echo nl2br($datosvehiculo);?>                        
                                    </p>
                                </div>
                            </div>
                            <div class="grid__item grid__item--bg theme-10">
                             <?  $sentenciai = "CALL paPaginaAutos('$idIdioma','3','$idauto')";
									// echo $sentenciai;
										$resultadoi = consulta($sentenciai);
										$cuantosi=$resultadoi->num_rows;
										
										if($cuantosi>0){
								?>
                                <div class="grid__item-img" data-displacement="img/displacement/10.jpg" data-intensity="0.7" data-speedIn="1" 
                                data-speedOut="0.5" data-easing="Power2.easeOut">
                                <? while ($rowi = $resultadoi->fetch_array(MYSQLI_ASSOC)){
                  						  $idimagenauto=$rowi["idimagenauto"];?>
                                    <img src="modulos/img/modelosfotos/modelosfotos-<? echo $idimagenauto;?>.jpg" alt="<? echo $marca . " ". $modelo;?>"/>
                                 <? }?>
                                   <!-- <img src="img/autos/toyota-2000-gt-interiores-2.jpg" alt="TOYOTA 2000GT"/>-->
                                </div>
                                <? }
								
								$resultadoi->close();
								?>
                                <div class="grid__item-content">
                                </div>
                            </div>
                    </div>
                </main>
                
                <? } ?>
                <!--Maqueta-Región-Dorada-->
                <script src="js/imagesloaded.pkgd.min.js"></script>
                <script src="js/three.min.js"></script>
                <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/1.20.3/TweenMax.min.js"></script>
                <script src="js/hover.js"></script>
                    <script>
                        Array.from(document.querySelectorAll('.grid__item-img')).forEach((el) => {
                            const imgs = Array.from(el.querySelectorAll('img'));
                            new hoverEffect({
                                parent: el,
                                intensity: el.dataset.intensity || undefined,
                                speedIn: el.dataset.speedin || undefined,
                                speedOut: el.dataset.speedout || undefined,
                                easing: el.dataset.easing || undefined,
                                hover: el.dataset.hover || undefined,
                                image1: imgs[0].getAttribute('src'),
                                image2: imgs[1].getAttribute('src'),
                                displacementImage: el.dataset.displacement
                            });
                        });
                    </script>
		<? } $resultadoa->close();?>
      <?php include_once("footer.php");?>
     <!--Datos del Museo Km/h-->
     
	 <?php include_once("derechos.php");?>
	 <!--copyright © 2018 Km/h-->       
    

    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.min.js"><\/script>')</script>
    <script src="bower_components/dist/jquery.fancybox.min.js"></script>
    <script src="js/vendor/bootstrap.min.js"></script>
    <script src="js/scripts.js"></script>
    <script src="js/jquery.flexslider-min.js"></script>
    <script src="bower_components/classie/classie.js"></script>
    <script src="bower_components/jquery-waypoints/lib/jquery.waypoints.min.js"></script>
</body>
</html>
