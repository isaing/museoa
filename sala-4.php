<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="es"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title>Museo Km/h Guanajuato</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="estilo.css">
    <link rel="stylesheet" href="css/menu.min.css">    
    <link rel="stylesheet" href="bower_components/animate.css/animate.min.css">
    <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
    <link rel="stylesheet" type="text/css" href="css/base.css" />
	<script>document.documentElement.className="js";var supportsCssVars=function(){var e,t=document.createElement("style");
	return t.innerHTML="root: { --tmp-var: bold; }",document.head.appendChild(t),e=!!
	(window.CSS&&window.CSS.supports&&window.CSS.supports("font-weight","var(--tmp-var)")),t.parentNode.removeChild(t),e};supportsCssVars()||
	alert("Please view this demo in a modern browser that supports CSS Variables.");</script>    
</head>
<body id="top">
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v3.1';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<section class="mod-principal">
    <section class="navigation">
        <div class="menu-usuario">
            <div class="idioma">
            </div>
            <!--idioma-->
        
            <div class="redes">
                <a class="facebook" href="#"></a>
                <a class="twitter" href="#"></a>
                <a class="youtube" href="#"></a>
                <a class="instagram" href="#"></a>
            </div>
            <!--redes-sociales-->
        </div>
        
        <div class="logo">
        	<div class="logo-prin">
	        	<img class="logo-km-h" src="img/museo-km-h-gto.svg" alt="Museo Km/h">
            </div>
             <!--Museo km/h-->
             
             <div class="busqueda">
             </div>
             <!--búsqueda-->
        </div>
        <!--Museo km/h-búsqueda-->
        
        <div class="menu-principal">
        	<div class="contenido">
                <div class="ve-menu">
                  <ul class="ve-menu-pc">
                    <li><a href="index.php">Inicio</a></li>
                    <li><a href="acerca-del-museo.php">Acerca del Museo</a></li>
                    <li><a href="#">Salas de Exhibición</a>
                      <ul>
                        <li><a href="#">Evolución e Historia del Automóvil</a></li>
                        <li><a href="sala-2.php">Anatomía del Automóvil</a></li>
                        <li><a href="#">Cadena de ensamblaje</a></li>
                        <li><a href="sala-4.php">Galería de Autos</a></li>
                        <li><a href="#">El futuro</a></li>
                      </ul>
                    </li>
                    <li><a href="eventos.php">Eventos</a></li>
                    <li><a href="#">Información para el visitante</a>
                      <ul>
                        <li><a href="#">Como llegar</a></li>
                        <li><a href="#">Costos</a></li>
                        <li><a href="#">Mapa del Museo</a></li>
                        <li><a href="#">Planifica tu visita</a></li>
                        <li><a href="#">Grupos y excursiones</a></li>
                      </ul>
                    </li>
                  </ul>
                </div>       
            </div>
        </div>
        <!--menú-principal-->
		<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/fastclick/1.0.6/fastclick.min.js"></script>
        <script src="js/menu.min.js"></script>
        <script>
            $('.ve-menu').menu({
                fontSize: 17,
                fontColor: '#222',
                bgColor: '#fff',
                hoverFontColor: '#0694a7',
                hoverBgColor: '#fff',
                itemSpace: 5,
                subFontSize: 17,
                itemWidth: 70,
                animate: 'slide',
                speed: 500,
            });
        </script>
    </section>
</section>
<!--Módulo-principal-->

	<div class="encabezado-sala">
    	<div class="contenido">
        	<div class="sala-titulo">
            	<h1 class="sala">Sala 04</h1>
                <h2 class="tit-sala">Galería de autos</h2>
            </div>
            <!--Sala-de Exhibición-->
            
        	<div class="frase">
            	“Los interiores son una fiesta de la excelencia” <br><br>
                
                <div class="personaje">Maserati</div>
            </div>
            <!--Sala-de Exhibición-->
            
            <div class="descripcion-sala">
                La industria automotriz es una de las más dinámicas y competitivas en México. Se ha consolidado a nivel global y ha llamado la 
                atención debido al crecimiento sostenido en la producción de vehículos y autopartes, así como a la fortaleza y las perspectivas de 
                crecimiento del mercado interno.<br><br>
                
                Con el arribo de empresas nacionales y extranjeras, Guanajuato es una de las regiones más dinámicas de América Latina, ya que 
                importantes firmas y corporativos han decidido consolidar sus proyectos en la entidad. <br><br>
                
                Actualmente, el estado de Guanajuato es el segundo lugar a nivel nacional en producción de automóviles con 470 mil unidades anuales, 
                y para el año 2020 se espera que producirá 950 mil, que será el máximo a nivel nacional.<br><br>                         
            </div>
            <!--Descripción-de-la-Sala-de-Exhibición-->
            
            <div class="elemento">
            	<a href="#"><img class="ico-video" src="img/video.svg" alt="Video Km/h"></a>
            </div>
            <!--video-->

            <div class="elemento">
            	<a href="#"><img class="ico-planifica" src="img/planifica-visita.svg" alt="Planifica tu visita al Km/h"></a>
            </div>
            <!--planifica-tu-visita-->

            <div class="elemento">
                    <div class="social">
                        <div class="fb-share-button" data-href="https://developers.facebook.com/docs/plugins/" data-layout="button_count" 
                        data-size="small" data-mobile-iframe="true">
                        <a target="_blank" href=
                        "https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fdevelopers.facebook.com%2Fdocs%2Fplugins%2F&amp;src=sdkpreparse"
                        class="fb-xfbml-parse-ignore">Compartir</a></div>
                    </div>
                    <!--facebook-->
                    
                    <div class="social">
                        <a href="https://twitter.com/share?ref_src=twsrc%5Etfw" class="twitter-share-button" data-show-count="false">Tweet</a>
                        <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
                    </div>
            </div>
            <!--compartir-redes-sociales-->
        </div>
    </div>
	<!--Sala-4-->

    <main>
    	<div class="grid">
        	<div class="grid__item grid__item--bg theme-3">
            	<div class="grid__item-img" data-displacement="img/displacement/4.png" data-intensity="0.2" data-speedIn="1.6" data-speedOut="1.6">
                	<img src="img/maqueta-region-dorada.jpg" alt="Maqueta Región Dorada"/>
                    <img src="img/maqueta-region-dorada-gto.jpg" alt="Guanajuato Región Dorada"/>
                </div>
            </div>
            <div class="grid__item theme-4">
            	<div class="grid__item-content">
                	<h2 class="grid__item-title galeria">Maqueta</h2>
                    <h2 class="grid__item-title grid__item-title--small galeria">Región Dorada</h2>
                	<p class="grid__item-text">
                    	Lorem Ipsum is simply dummy text of the printing  and typesetting industry. Lorem Ipsum has been the industry's standard 
                        dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.
                        <br><br>
                        <a class="grid__item-link" href="#">ver más</a>
                    </p>
                    
                </div>
            </div>
        </div>
    </main>
    <!--Maqueta-Región-Dorada-->
    <script src="js/imagesloaded.pkgd.min.js"></script>
    <script src="js/three.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/1.20.3/TweenMax.min.js"></script>
	<script src="js/hover.js"></script>
		<script>
			Array.from(document.querySelectorAll('.grid__item-img')).forEach((el) => {
				const imgs = Array.from(el.querySelectorAll('img'));
				new hoverEffect({
					parent: el,
					intensity: el.dataset.intensity || undefined,
					speedIn: el.dataset.speedin || undefined,
					speedOut: el.dataset.speedout || undefined,
					easing: el.dataset.easing || undefined,
					hover: el.dataset.hover || undefined,
					image1: imgs[0].getAttribute('src'),
					image2: imgs[1].getAttribute('src'),
					displacementImage: el.dataset.displacement
				});
			});
		</script>
        
    <div class="galeria-de-autos">
    	<div class="contenido">
        	<h1>Galería de autos</h1>
        	<div class="auto">
            	<a href="ficha-auto.php">
                <img class="automovil" src="img/autos/toyota-2000-gt.jpg" alt="TOYOTA 2000GT"></a>
            	<div class="modelo"><span class="marca">TOYOTA</span> 2000GT</div> 
                <div class="origen-edit"><img class="bandera" src="img/banderas/japon.svg" alt="Japón">País de origen: Japón</div>
                <div class="origen-edit">Año: 1966</div>
            </div>
            <!--Auto 1- Km/h-->

        	<div class="auto">
            	<a href="#">
                <img class="automovil" src="img/autos/mustang.jpg" alt="Ford Mustang"></a>
            	<div class="modelo"><span class="marca">Ford</span> Mustang</div> 
                <div class="origen-edit"><img class="bandera" src="img/banderas/estados-unidos.svg" alt="USA">País de origen: USA</div>
                <div class="origen-edit">Año: 1967</div>
            </div>
            <!--Auto 2- Km/h-->
            
        	<div class="auto">
            	<a href="#">
                <img class="automovil" src="img/autos/chevrolet-corvette.jpg" alt="CHEVROLET CORVETTE"></a>
            	<div class="modelo"><span class="marca">CHEVROLET</span> CORVETTE</div> 
                <div class="origen-edit"><img class="bandera" src="img/banderas/estados-unidos.svg" alt="USA">País de origen: USA</div>
                <div class="origen-edit">Año: 1966</div>
            </div>
            <!--Auto 3- Km/h-->
        </div>
    </div>        
    <!--Galería de Autos Km/h-->

     <footer>
     	<div class="contenido">
        	<div class="km-h">
            	<img src="img/km-h-museo.svg" alt="Museo Km/h">
            </div>
            <!--Logotipo Km/h-->
            
            <div class="col-4">
            	<h2>Salas de Exhibición</h2>
                <ul class="submenu">
                	<li><a href="#">Autocinema</a></li>
                    <li><a href="#">Anatomía del  automóvil</a></li>
                    <li><a href="#">Cadena de producción</a></li>
                    <li><a href="#">Galería de autos</a></li>
                    <li><a href="#">El futuro</a></li>
                </ul>
            </div>
            <!--submenú-1 Km/h-->

            <div class="col-4">
            	<h2>Información para el Visitante</h2>
                <ul class="submenu">
                	<li><a href="#">Como llegar</a></li>
                    <li><a href="#">Costos</a></li>
                    <li><a href="#">Mapa del Museo</a></li>
                    <li><a href="#">Planifica tu visita</a></li>
                    <li><a href="#">Grupos y excursiones</a></li>
                </ul>
            </div>
            <!--submenú-2 Km/h-->

            <div class="col-4">
            	<h2>Ubicación y horarios</h2>
                <div class="dato">
                	<img class="ico-dato" src="img/address-f.svg"> Carretera de Cuota Silao-Gto. Km 3.8, Los Rodríguez, 36270 Silao, Gto.
                </div>
                
                <div class="dato">
                	<img class="ico-dato" src="img/phone-f.svg"> <a class="link-footer" href="#">01 472 + 0 000 000</a>
                </div>
                
                <div class="dato">
                    Abierto: 10:00 a 19:00 hrs.<br>
                    Martes a Domingo                
                </div>
            </div>
            <!--dirceción Km/h-->
        </div>
     </footer>
     <!--Datos del Museo Km/h-->
     
	<div class="derechos"> 
    	Copyright ©  <?php include ("copy-date.php")?> <img class="logo-museo" src="img/km-h-gto.svg" alt="Museo Km/h">
    </div>
    <!--copyright © 2018 Km/h-->     
    
    <section class="to-top">
            <div class="row">
                <div class="to-top-wrap">
                    <a href="#top" class="top"><span class="arriba">&#710;</span></i></a>
                </div>
            </div>
        </div>
    </section>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.min.js"><\/script>')</script>
    <script src="js/jquery.fancybox.pack.js"></script>
    <script src="js/vendor/bootstrap.min.js"></script>
    <script src="js/scripts.js"></script>
    <script src="js/jquery.flexslider-min.js"></script>
    <script src="bower_components/classie/classie.js"></script>
    <script src="bower_components/jquery-waypoints/lib/jquery.waypoints.min.js"></script>
</body>
</html>
