 <link rel="stylesheet" type="text/css" href="css/base.css" />
	<script>document.documentElement.className="js";var supportsCssVars=function(){var e,t=document.createElement("style");
	return t.innerHTML="root: { --tmp-var: bold; }",document.head.appendChild(t),e=!!
	(window.CSS&&window.CSS.supports&&window.CSS.supports("font-weight","var(--tmp-var)")),t.parentNode.removeChild(t),e};supportsCssVars()||
	alert("Please view this demo in a modern browser that supports CSS Variables.");</script>    
  <main>
  
   <? $sentenciam = "CALL paPaginaMaqueta('$idIdioma')";
            // echo $sentenciam;
                $resultadom = consulta($sentenciam);
                $cuantosm=$resultadom->num_rows;
				$vermas= titulotraduccion($idIdioma, 'Ver más');
                if($cuantosm>0){
                    while ($rowm = $resultadom->fetch_array(MYSQLI_ASSOC)){
                    $idmaqueta=$rowm["idmaqueta"];
					$urlmaqueta=$rowm["urlmaqueta"];
                    $targetmaqueta=$rowm["targetmaqueta"];
                    $titulomaqueta=$rowm["titulomaqueta"];
                    $subtitulomaqueta=$rowm["subtitulomaqueta"];
					$descripcionmaqueta=$rowm["descripcionmaqueta"];
                    ?>
					
                   
    	<div class="grid">
        	<div class="grid__item grid__item--bg theme-3">
            	<div class="grid__item-img" data-displacement="img/displacement/4.png" data-intensity="0.2" data-speedIn="1.6" data-speedOut="1.6">
                	<img src="modulos/img/maqueta/maqueta_a-<? echo $idmaqueta;?>.jpg" alt="<? echo $titulomaqueta . " ".$subtitulomaqueta;?>"/>
                    <img src="modulos/img/maqueta/maqueta_b-<? echo $idmaqueta;?>.jpg" alt="<? echo $titulomaqueta . " ".$subtitulomaqueta;?>"/>
                </div>
            </div>
            <div class="grid__item theme-4">
            	<div class="grid__item-content">
                	<h2 class="grid__item-title galeria"><? echo $titulomaqueta;?></h2>
                    <h2 class="grid__item-title grid__item-title--small galeria"><? echo $subtitulomaqueta;?></h2>
                	<p class="grid__item-text">
                    	<? echo nl2br($descripcionmaqueta);?>
                        <br><br>
                        <a class="grid__item-link" href="<? echo $urlmaqueta;?>" target="<? echo $targetmaqueta;?>"><? echo $vermas;?></a>
                    </p>
                    
                </div>
            </div>
        </div>
       <?  }
	}
	$resultadom->close();
	?>
    </main>
    <!--Maqueta-Región-Dorada-->
    <script src="js/imagesloaded.pkgd.min.js"></script>
    <script src="js/three.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/1.20.3/TweenMax.min.js"></script>
	<script src="js/hover.js"></script>
		<script>
			Array.from(document.querySelectorAll('.grid__item-img')).forEach((el) => {
				const imgs = Array.from(el.querySelectorAll('img'));
				new hoverEffect({
					parent: el,
					intensity: el.dataset.intensity || undefined,
					speedIn: el.dataset.speedin || undefined,
					speedOut: el.dataset.speedout || undefined,
					easing: el.dataset.easing || undefined,
					hover: el.dataset.hover || undefined,
					image1: imgs[0].getAttribute('src'),
					image2: imgs[1].getAttribute('src'),
					displacementImage: el.dataset.displacement
				});
			});
		</script>
         <? 
		 $sentenciaa = "CALL paPaginaAutos('$idIdioma','1','0')";
            // echo $sentenciam;
                $resultadoa = consulta($sentenciaa);
                $cuantosa=$resultadoa->num_rows;
				
                if($cuantosa>0){
                    
                    ?>
        
         <div class="galeria-de-autos">
    	<div class="contenido">
        	<h1><? echo titulotraduccion($idIdioma, 'Galería de autos');?></h1>
            
           <? while ($rowa = $resultadoa->fetch_array(MYSQLI_ASSOC)){
                    $idauto=$rowa["idauto"];
					$idmarca=$rowa["idmarca"];
                    $marca=$rowa["marca"];
                    $modelo=$rowa["modelo"];
                    $anio=$rowa["anio"];
					$idpais=$rowa["idpais"];
					$bandera=$rowa["bandera"];
					$pais=$rowa["pais"];?>
        	<div class="auto">
            	<a href="fichaauto/<? echo $idauto;?>">
                <img class="automovil" src="modulos/img/modelos/modelos-<? echo $idauto;?>.jpg" alt="<? echo $modelo;?>"></a>
            	<div class="modelo"><span class="marca"><? echo strtoupper($marca);?></span> <? echo strtoupper( $modelo);?></div> 
                <div class="origen-edit"><img class="bandera" src="modulos/img/paises/<? echo $bandera;?>" alt="<? echo $pais;?>"><? echo titulotraduccion($idIdioma, 'País de origen');?>: <? echo $pais;?></div>
                <div class="origen-edit"><? echo titulotraduccion($idIdioma, 'Año');?>: <? echo $anio;?></div>
            </div>
             <? }?>
            <!--Auto 1- Km/h-->

        	
        </div>
    </div>        
   	 <?
		}
		
		$resultadoa->close();
	?>
    <!--Galería de Autos Km/h-->
   