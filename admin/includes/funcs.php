﻿<?

function c($valor)
{
 return str_replace("'","''",$valor);
}

function consulta($sentencia)
{
	include 'vars.php';
	$mysqli = new mysqli($vservidor,$vusuario, $vpwd, $vbd);
	$mysqli->set_charset("utf8");
	if (mysqli_connect_errno())
	{
		printf("Connection failed: %s\n", mysqli_connect_error());
		exit();
	}
	$SQL = $sentencia;
	if ( ($result = $mysqli->query($SQL))=== false )
	{
		printf($sentencia);
		printf("Error en la BD");
		exit();
	}
	return $result;
	$mysqli->close();
}
function semaforoestatus($es){
	$res = "";
	if ($es == '1') {
		$res = "<i class='fa fa-circle ico-centrado verde'></i>";
	}else if ($es == '0'){
		$res = "<i class='fa fa-circle ico-centrado rojo'></i>";
	}
	return $res;
}
function agregacombodestinosurl($default = "_blank", $nombre = "cmbdestino"){
	echo "<select id='" . $nombre. "' class='control-combo'>";
  if ($default == '_top'){
    echo "<option value='_blank' selected>Nueva ventana</option>";
    echo "<option value='_top'>Misma ventana</option>";
  }else{
    echo "<option value='_blank'>Nueva ventana</option>";
    echo "<option value='_top' selected>Misma ventana</option>";
  }
  echo "</select>";
}

function agregacomboestatus(){
	echo "<select id='cmbestatus' class='control-combo'><option value='1'>ALTA</option><option value='0'>BAJA</option></select>";
}

function agregacombo($catalogo, $idExtra = 0, $default = '')
{
	
	$strSQL = "CALL paCargaDatosCombo('".$catalogo."','".$idExtra."')";
	/*echo $strSQL;
	exit;*/
	$resultado = consulta($strSQL);
	echo "<select class='control-combo' style='width:100%' id='cmb".strtolower($catalogo)."' name='cmb".strtolower($catalogo)."'>";
	while ($row = $resultado->fetch_array(MYSQLI_ASSOC)){
		$seleccionado = "";
		if ($row["valor"] == $default) {
			$seleccionado = "selected";
		}
		echo "<option value='".$row["id"]."' ".$seleccionado.">".$row["valor"]."</option>";
	}
	echo "</select>";
	$resultado->close();
}
function agregacombowebinterna(){
	echo "<select id='cmbwebinterna' class='control-combo'><option value='0'>NO</option><option value='1'>SI</option></select>";
}
function agregacombosexo(){
	echo "<select id='cmbsexo' class='control-combo'><option value='M'>MUJER</option><option value='H'>HOMBRE</option></select>";
}

function reemplazacomilla($texto){
	$textolimpio = str_replace("'", '\\\'', $texto );
	return $textolimpio;
}
function reemplazaespacios($texto){
	$textolimpio = str_replace(" ", '', $texto );
	return $textolimpio;
}


?>
