<?
require 'vs.php';
require_once '../includes/funcs.php';
?><head>
<link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Raleway" />
</head>


<div class="contenedor">

<div class="row">
<div class="col11"></div>
<div class="col1">
			<a title="Agregar imagen" href="#" class="mostrar-detalle" data-indice="-1">
			<div class="btn-flotante">
				<i class="fa fa-plus btn-flotante-texto"></i>
			</div>
			</a>
</div>
</div>

<section>
<table id="tabla-principal" class="tabla-datos">
<thead>
<tr>
<th class="coltabla0 noexcel"></th>
<th class="col-iconos texto-centrado noexcel"></th>
<th class="col-iconos texto-centrado noexcel"></th>
<th class="coltabla4 texto-derecha">Modelo</th>
<th class="coltabla3 texto-derecha">Año</th>
<th class="coltabla3 texto-derecha">Marca</th>


</tr>
</thead>
<tbody>
<?
	$filtro = $_POST['filtro'];
	cargaDatos($filtro);
?>
</tbody>
</table>


</section>

<section>
<div id="tabladetalle" style="background-color:#fff;z-index:99001" class="modalcapa modalcapa1">
	<div id="tabs" class="titulomodal">
		<ul>
    	<li id="tab-datos"><a href="#tabs-1">Datos del modelo</a></li>
  		 <li id="tab-detalle"><a href="#tabs-2">Agregar Detalle</a></li>
        <li id="tab-detalles"><a href="#tabs-3">Detalles</a></li>
        <li id="tab-agregar"><a href="#tabs-4">Agregar imagen</a></li>
  		<li id="tab-imagenes"><a href="#tabs-5">Imágenes</a></li>
  	</ul>
		<div id="tabs-1">
			<table id="modelo" class="tabla-datos">
				<tbody class="contenidomodal">
					<tr id="fila-idioma">
                   	   <td class="col3 texto-derecha">*Marca: </td>
                   	   <td class="col9 texto-izquierda"><? agregacombo('marcas','0');?></td>
    				 </tr>
					  <tr>
					   <td class="col3 texto-derecha">*Modelo
				         <input id="lblid" type="hidden" />
				         <input  id="txtID" type="hidden" /></td>
					   <td class="col9 texto-izquierda"><input class="control" id="txtmodelo" required></td>
					  
			      </tr>
                  <tr>
                        <td class="col3 texto-derecha">*Año:</td>
                        <td class="col2 texto-izquierda"><span class="col9 texto-izquierda">
                          <input class="control" id="txtanio" required="required" type="number" />
                        </span></td>
                  </tr>
                  <tr>
                  <td class="col3 texto-derecha">*Imagen Principal:</td>
                    <td class="col9 texto-izquierda"><form id="form1" runat="server">
	        JPG (900 x 265px)<br /><img id="imgPreview" width="50%"  src="#" alt="Da clic para seleccionar la imagen " />
					<br>
					<input id="archivoImagen" type='file' accept=".JPG, .jpg, image/jpeg" onchange="readURL(this);" style="display:none" />
	    </form></td>
                   
                  </tr>
                  <tr id='fila-estado'>
						<td class="col3 texto-derecha">Estado:</td>
						<td class="col2 texto-izquierda"><? agregacomboestatus(); ?></td>
					</tr>
				</tbody>
			</table>
		</div>
		
      <div id="tabs-2">
			<table id="detalle" class="tabla-datos">
				<tbody class="contenidomodal">
                <tr id="fila-idioma">
                   	   <td class="col3 texto-derecha">*Idioma: </td>
                   	   <td class="col4 texto-izquierda"><div id="comboidiomas"><? agregacombo('idiomas','2');?></div></td>
    				 </tr>
                     <tr>
						<td class="col3 texto-derecha">*Descripción Breve:
						  <input id="lblid" type="hidden"></td>
						<td class="col9 texto-izquierda"><textarea id="txtdescripcionbreve" class="control-area"></textarea></td>
					</tr>
					<tr>
						<td class="col3 texto-derecha">*Datos del vehículo:
						  <input id="lblid" type="hidden"></td>
						<td class="col9 texto-izquierda"><textarea id="txtdatosvehiculo" class="control-area"></textarea></td>
					</tr>
                  <!-- <tr id='fila-estado'>
						<td class="col3 texto-derecha">Estado:</td>
						<td class="col2 texto-izquierda"><? agregacomboestatus(); ?></td>
					</tr>-->
					<tr>
						<td class="col3 texto-derecha"></td>
						<td class="col9 texto-izquierda">
							<div class="row">
								
								<div class="col4"><br>
									<input type="button" id="agregar-detalle" name="enviar" title="Agregar detalle" class="btn-formulario" value="Agregar detalle">
								</div>
							</div>
						</td>
					</tr>
				
					
				 
				</tbody>
			</table>

		</div>
        
        
        <div id="tabs-3" class='alto-fijo-modal2'>
			<div class="row">
				<div class="col2 texto-derecha">Idioma:</div>
				<div class="col9"><div id="div-combo-detalles"> </div></div>
			</div>
			<div class="row" id="fila-datos-detalle">
				<div class="col12" >
					<div id="datos-detalle-modelo" class='alto-fijo-modal2'>

					</div>
				</div>
				
			</div>
		</div>
        
        <div id="tabs-4">
			<table id="detalle" class="tabla-datos">
				<tbody class="contenidomodal">
					<tr>
						<td class="col3 texto-derecha">*Descripción:
						  <input id="lblid" type="hidden"></td>
						<td class="col5 texto-izquierda"><input class="control" id="txtdescripcion" required></td>
					</tr>
                   
					<tr>
						<td class="col3 texto-derecha">*Imagen</td>
						<td class="col9 texto-izquierda">
							<div class="row">
								<div class="col8">JPG <br />
									<form id="form2" runat="server">
										<a href="#" id ="buscaArchivo2">Seleccionar imagen</a>
										<input id="archivoImagen2" type='file' accept=".JPG, .jpg, image/jpeg" onchange="readURL2(this);" style="display:none" />
									</form>
									<img src='' id="imgPreview2" width="50%" >
								</div>
								<div class="col4">
									<br><br><br>
								</div>
							</div>
						</td>
					</tr>
				
					<!--<tr id='fila-estado'>
						<td class="col3 texto-derecha">Estado:</td>
						<td class="col2 texto-izquierda"><? agregacomboestatus(); ?></td>
					</tr>-->
					<tr>
					  <td class="col3 texto-derecha">&nbsp;</td>
					  <td class="col9 texto-izquierda"><span class="col4">
					    <input type="button" id="agregar-imagenes" name="agregar-imagenes" title="Agregar imagen" class="btn-formulario" value="Agregar imagen" />
					  </span></td>
				  </tr>
				 
				</tbody>
			</table>

		</div>
        <!--- imagenes del modelo--->
      <div id="tabs-5" class='alto-fijo-modal2'>
			<div class="row">
				<div class="col2 texto-derecha">Imagen:</div>
				<div class="col9"><div id="div-combo-imagenes"> </div></div>
			</div>
			<div class="row" id="fila-datos-imagen">
				
					<div id="datos-imagen" class='alto-fijo-modal'>

					</div>
				
			</div>
		</div>
        
        
        
        
        
      <!-- terminan divs--->
		<div style="margin-top:15px; padding-bottom:10px; width:100%; padding-left:550px;" id="espera"></div>
		<div class="row" id="areabotones" style="padding:10px;">
		<div class="col6"></div>
		<div class="col2 texto-centrado">
			<input type="button" id="guardar" name="enviar" title="Guardar" class="btn-formulario" value="Guardar">
			<img id="espera2" src='img/loading.gif' class="icono-espera">
		</div>
          <div class="col2 texto-centrado"  id="colborrarelemento">
        <input type="button" id="borrar" name="enviar" title="Borrar" class="btn-formulario" value="Borrar">
			
		</div>
		<div class="col2">
			<input type="button" id="cerrar" name="cerrar" title="Cerrar" class="btn-formulario modalcerrar" value="Cerrar">
		</div>
	</div>
	</div>
	<div id="dialog-confirm" title="Borrar elemento">
      <p><span class="ui-icon ui-icon-alert" style="float:left; margin:12px 12px 20px 0;"></span><label id='texto-confirmacion'></label>
      </p>
    </div>
</div>
</section>

<?
function cargaDatos($filtro){
	$strSQL = "CALL paCatalogoModelos('%".$filtro."%')";
	$resultado = consulta($strSQL);
	while ($row = $resultado->fetch_array(MYSQLI_ASSOC)){
		$indice = $row["id_modelo"];
		echo "<tr id=".$indice.">";
		echo "<td style='display:none' class='noexcel'><label id='lblestatus".$indice."'>".$row["estatus"]."</label><label id='lblid".$indice."'>".$indice."</label><label id='lblidmarca".$indice."'>".$row["idmarca"]."</label></td>";
		echo "<td class='texto-centrado noexcel'><a title='Editar' href='#' id='editar' class='mostrar-detalle' data-indice='".$indice."'><i class='fa fa-edit icono-tablas' aria-hidden='true'></i></a></td>";
		echo "<td class='texto-centrado noexcel'><label>".semaforoestatus($row["estatus"])."</label></td>";
		echo "<td class='texto-centrado'><label id='lblmodelo".$indice."'>".$row["modelo"]."</label></td>";
		echo "<td class='texto-centrado'><label id='lblanio".$indice."'>".$row["anio"]."</label></td>";
		echo "<td class='texto-centrado'><label id='lblmarca".$indice."'>".$row["marca"]."</label></td>";


		
		echo "</tr>";
	}
	$resultado->close();
}
?>

<script>
	$(document).ready(function() {
		$("#dialog-confirm").hide();
		$("#tabs").tabs();
		$(".modalcapa1").draggable({ cursor: "move", handle: ".titulomodal", opacity: 0.5 });
		$("#espera2").hide();
		$("#tabladetalle").hide();
		Crearlisteners();
	});

	                
	$(".mostrar-detalle").unbind("click").bind('click', function () {
		mostrarDetalle($(this).data("indice"));
  });


	function abrirmodalc1(width, height) {
		Ajustatamanocapa(".modalcapa1", width, height);
		$(".modalgeneralcapa1").fadeIn();
		$(".modalcapa1").fadeIn();
		return;
	}

	function Ajustatamanocapa(Control, width, height) {
		$(Control).css('width', width);
		$(Control).css('left', 'calc(50% - ' + width + '/2)');
		$(Control).css('top', 'calc(40% - ' + height + '/2)');

		$(Control + ' .contenidomodal').css('height', 'calc(' + height + ' - 140px)');

		return;
	}
	function cerrarmodalc1() {
		$(".modalgeneralcapa1").fadeOut();
		$('.modalcapa1').fadeOut();
	}

	function Crearlisteners(){
		$('.modalcerrar').click(function(){
			cerrarmodalc1();
			return false;
		});
	}

	$("#guardar").click(function() {
		imgsrc=0;
		 if($("#imgPreview").attr('src')!=" ")
		 {
			 imgsrc=1;
			
		}
		
		if ($('#txtmodelo').val() != '' &&  $('#txtanio').val() != '' && $("#cmbmarcas").val()!=null && ($("#archivoImagen").val() != "" || imgsrc==1)){
			var dataObject = { id_modelo: $("#lblid").text(),
			modelo: $("#txtmodelo").val(),
			anio: $("#txtanio").val(),
			marca: $("#cmbmarcas").val(),
			estatus: $("#cmbestatus").val()};
			var idOk = "0";
			$.ajax({
				data:  dataObject,
				url:   'ajax/ajax_guarda_modelo.php',
        type:  'post',
				async: false,
	      beforeSend: function () {
					$("#guardar").hide();
          $("#espera2").show();
        },
        success:  function (response) {
					var id = $("#lblid").text();
					cerrarmodalc1();
					$("#espera2").hide();
					$("#guardar").show();
					var pos = response.indexOf("IDOK");
					idOk = response.substr(pos + 4);
					response = response.replace("IDOK" + idOk,"");
					if (response.substring(0,3) == "<tr" || response.substring(0,3) == "<td"){
						if (id == 0) {
							if ($('#tabla-principal > tbody > tr').length == 0){
								$('#tabla-principal > tbody ').html(response);
							}else{
								$('#tabla-principal > tbody > tr').eq(0).before(response);
							}
						}else{
							$("#"+id).html(response);
						}
						subeFoto(idOk);
						/*$("#imgPreview").attr('src','../modulos/img/imagenes/imagenes-' + id + ".jpg");*/
						
						$("#imgPreview").attr('src','../modulos/img/modelos/modelos-' + id + ".jpg?' + Math.random()");
					}else{
						abrirmodalavisos('Modelos',response, '800px', '450px');
					}

					$(".mostrar-detalle").unbind("click").bind('click', function () {
						mostrarDetalle($(this).data("indice"));
				  });
				}

       		});
		}else{
			abrirmodalavisos("Modelos", "Debe capturar la marca, el modelo, año  y la foto de la imagen que desea guardar", '750px', '500px');
		}
	});

function subeFoto(idC){
	
	var file_data = $("#archivoImagen").prop("files")[0];
	var form_data = new FormData();

	form_data.append("file", file_data);
	$.ajax({
		data: form_data,
    url:   'ajax/ajax_sube_foto.php?num=' + idC + "&tipo=modelos",
    type:  'post',
		cache: false,
		async: false,
		contentType: false,
		processData: false,
		success:  function (response) {
			//alert(response);
			$("#imgPreview").attr('src','../modulos/img/modelos/modelos-' + idC + ".jpg?' + Math.random()");
		}
	});
}

	function mostrarDetalle(i) {
		$('#imgPreview').attr('src', ' ');
		$('#imgPreview2').attr('src', ' ');
		$("#espera2").hide();
		$( "#tabs" ).tabs( "option", "active", 0 );
		if (i != "-1"){
			$("#lblid").html($('#lblid' + i).html());
			$("#txtID").val(i);
			$("#colborrarelemento").show();
			$("#txtmodelo").val($('#lblmodelo' + i).html());
			$("#txtanio").val($('#lblanio' + i).html());
			$("#cmbmarcas").val($('#lblidmarca' + i).html());
			$("#cmbestatus").val($("#lblestatus" + i).html())
			//alert(id);
			if ($("#lblimagen" + i).html() != ""){
				$("#imgPreview").attr('src','../modulos/img/modelos/modelos-' + $("#lblid" + i ).html()+ ".jpg?" + Math.random());
			$("#tab-detalle").show();
			$("#tab-detalles").show();
			$("#tab-agregar").show();
			$("#tab-imagenes").show();
			cargaFotos($('#lblid' + i).html());
			cargaDetalles($('#lblid' + i).html());
			}
		}else{
			
			$("#lblid").html("0");
			$("#colborrarelemento").hide();
			$("#txtmodelo").val("");
			$("#txtanio").val("");
			$("#cmbmarcas").val(1);
			$("#cmbestatus").val(1);
			$("#tab-detalle").hide();
			$("#tab-detalles").hide();
			$("#tab-agregar").hide();
			$("#tab-imagenes").hide();
		}
		abrirmodalc1('850px', '500px');

		return false;
	}

	function readURL(input) {
		var imgPath = $("#archivoImagen").val();
		var extn = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
		if (extn == "gif" || extn == "png" || extn == "jpg" || extn == "jpeg") {
    	if (input.files && input.files[0]) {
     		var reader = new FileReader();
	      reader.onload = function (e) {
			  
	      	$('#imgPreview').attr('src', e.target.result);
					
	      }
	      reader.readAsDataURL(input.files[0]);
	   	}
		}else{
			abrirmodalavisos("Archivo seleccionado", "Debe seleccionar un archivo de imagen válido.<br>(Extensión JPG)", '750px', '450px');
		}
	}
	$("#agregar-detalle").click(function() {
		if ($('#txtdescripcionbreve').val() != '' && $('#txtdatosvehiculo').val() != '' && $("#cmbidiomas").val()!=''){
			
			var idDetalle = 0;
			var datos = {id_detalle:0,
					id_modelo:$("#lblid").text(),
					idioma: $("#cmbidiomas").val(),
					descripcion:$("#txtdescripcionbreve").val(),
					datosvehiculo:$("#txtdatosvehiculo").val()};
			$.ajax({
				data: datos,
		    url:   'ajax/ajax_guarda_detalle_modelo.php',
		    type:  'post',
				async: false,
				success:  function (response) {
					idDetalle = response;
					
					if(response=='NO')
					{
						abrirmodalavisos("Agregar detalle","El detalle capturado del modelo por idioma ya existe en la base de datos.");
						
					}
					else{
						//alert(idDetalle);
			
					cargaDetalles($("#lblid").text());
					
						abrirmodalavisos("Agregar detalle","Detalle subido con éxito.");
					}
					
					},error : function(jqXHR, textStatus, errorThrown){
					alert(errorThrown);
				}
				
			});
			$("#txtdescripcionbreve").val("");
			$("#txtdatosvehiculo").val("");
			$("#cmbidiomas").val(1);
		}else{
			abrirmodalavisos("Agregar detalle","Debe capturar el idioma, la descripción breve y los Datos del Vehículo.");
		}
	})
	function cargaDetalles(id){
		//debugger;
		var dataObject = { catalogo: "idiomasdetallesmodelo",
		id_extra: $("#txtID").val()}
		$.ajax({
				data:  dataObject,
        url:   'ajax/ajax_carga_combo.php',
        type:  'post',
      	success:  function (response) {
					//alert(response);
					$("#div-combo-detalles").html(response);
					$( "#cmbidiomasdetallesmodelo" ).change(function() {
						
						CargaDatosDetalle($("#cmbidiomasdetallesmodelo").val());
					});
					if ($("#cmbidiomasdetallesmodelo").val() != null){
						$("#fila-datos-detalle").show();
						CargaDatosDetalle($("#cmbidiomasdetallesmodelo").val());
					}else{
						$("#cmbidiomasdetallesmodelo").hide();
						$("#div-combo-detalles").html("<strong>No existen detalles para el modelo</strong>");
						$("#fila-datos-detalle").hide();
					}/**/
				}
			});
	}

	function CargaDatosDetalle(idD){
		var dataObject = { id_detalle: idD}
		$.ajax({
				data:  dataObject,
        url:   'ajax/ajax_carga_datos_detalle_modelo.php',
        type:  'post',
      	success:  function (response) {
					
					$("#datos-detalle-modelo").html(response);
					$("#borra-detalle").unbind("click").bind('click', function () {
						borrarDetalle();
					});
					$("#guardarediciondetalle").click(function(){
						cambiarDatosDetalle();
					})
				}
		});
	}
	function cambiarDatosDetalle(){
		if ( $("#txtdatosvehiculoedicion").val() != '' && $("#txtdescripcionbreveedicion").val() != '' ){
		
			
			var idDetalle = $("#cmbidiomasdetallesmodelo").val();
			var datos = {id_detalle:idDetalle,
					id_modelo:0,
					idioma:0,
					descripcion:$("#txtdescripcionbreveedicion").val(),
					datosvehiculo:$("#txtdatosvehiculoedicion").val()};
			$.ajax({
				data: datos,
		    url:   'ajax/ajax_guarda_detalle_modelo.php',
		    type:  'post',
				async: false,
				success:  function (response) {
					idDetalle = response;
					
					cargaDetalles(idDetalle);
				},error : function(jqXHR, textStatus, errorThrown){
					alert(errorThrown);
				}
			});

			abrirmodalavisos("Detalle","Cambios guardados con éxito.", 500, 200);
		}else{
			abrirmodalavisos("Detalle","Debe capturar los datos del vehículo.", 500, 200);
		}
	}
/*	function borrarDetalle(){
		

		var dataObject = { id_detalle: $("#cmbidiomasdetallesmodelo").val()}
		$.ajax({
				data:  dataObject,
        url:   'ajax/ajax_borra_detalle_modelo.php',
        type:  'post',
      	success:  function (response) {
				//alert(response);
				 $("#comboidiomas").html(response);
					cargaDetalles();
				}
		});
	}*/
	function borrarDetalle(){
		//alert("aqui");
		//debugger;
		$("#texto-confirmacion").html("¿Está seguro que desea borrar el detalle de " + $("#cmbidiomasdetallesmodelo option:selected").text().toUpperCase() + '?');
		
		$( "#dialog-confirm" ).dialog({
			resizable: false,
		    height: "auto",
		    width: 400,
		    modal: true,
		    buttons: {
		        "Aceptar": function() {
		          	$( this ).dialog( "close" );
				
		          	var dataObject = { id_elemento: $("#cmbidiomasdetallesmodelo").val(),
					//nombreimagen: $("#nombreimagen").val(),
					catalogo: 'traduccionautos'
					}
					$.ajax({
						data:  dataObject,
				      //  url:   'ajax/ajax_borra_tema_sala.php',
				  		url:   'ajax/ajax_borrado_logico.php',
						type:  'post',
				      	success:  function (response) {
							//alert(response);
							
							if (jQuery.trim(response) == "OK"){
								abrirmodalavisos("Eliminar Detalle","Detalle Eliminado con éxito.", 500, 200);
								cargaDetalles($("#lblid").text());
							}				
						}
					});
		    	},
		        "Cancelar": function() {
		          $( this ).dialog( "close" );
		          return false;
	           }
		    }
	    });		
	}

	$("#imgPreview").click(function() {
    $("input[id='archivoImagen']").click();
	});
	
	
	/* todo sobre subir fotos*/
	$("#agregar-imagenes").click(function() {
		if ($('#txtdescripcion').val() != '' ){
		
			var idDetalle = 0;
			var datos = {id_detalle:0,
					id_modelo:$("#lblid").text(),
					descripcion:$("#txtdescripcion").val()};
			$.ajax({
				data: datos,
		    url:   'ajax/ajax_guarda_imagen_modelo.php',
		    type:  'post',
				async: false,
				success:  function (response) {
					//alert(response);
					idDetalle = response;
					
					},error : function(jqXHR, textStatus, errorThrown){
					alert(errorThrown);
				}
			});
			//alert(idDetalle);
			SubeFoto2(idDetalle);
			cargaFotos($("#lblid").text());
			
			$("#txtdescripcion").val("");
			$('#imgPreview2').attr('src','');
			
			
			
			abrirmodalavisos("Agregar imagen","Imagen subida con éxito.");
		}else{
			abrirmodalavisos("Agregar imagen","Debe capturar al menos una descripción y seleccionar una imagen válida.");
		}
	})
	function readURL2(input) {
		var imgPath = $("#archivoImagen2").val();
		var extn = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
		if (extn == "jpg" || extn == "jpeg") {
    	if (input.files && input.files[0]) {
     		var reader = new FileReader();
	      reader.onload = function (e) {
	      	$('#imgPreview2').attr('src', e.target.result);
	      }
	      reader.readAsDataURL(input.files[0]);
	   	}
		}else{
			abrirmodalavisos("Archivo seleccionado", "Debe seleccionar un archivo de imagen válido.<br>(Extensión JPG)", '750px', '450px');
		}
	}
	$("#buscaArchivo2").click(function() {
    $("input[id='archivoImagen2']").click();
	});
	
	function SubeFoto2(idC){
	var file_data = $("#archivoImagen2").prop("files")[0];
	var form_data = new FormData();

	form_data.append("file", file_data);
	$.ajax({
		data: form_data,
    url:   'ajax/ajax_sube_foto.php?num=' + idC + "&tipo=modelosfotos",
    type:  'post',
		cache: false,
		async: false,
		contentType: false,
		processData: false,
		success:  function (response) {

		}
	});
	
	}
	function cargaFotos(){
		var dataObject = { catalogo: "imagenesmodelo",
		id_extra: $("#txtID").val()}
		$.ajax({
				data:  dataObject,
        url:   'ajax/ajax_carga_combo.php',
        type:  'post',
      	success:  function (response) {
				//alert(response);
					$("#div-combo-imagenes").html(response);
					$( "#cmbimagenesmodelo" ).change(function() {
						//debugger
						CargaDatosFoto($("#cmbimagenesmodelo").val());
					});
					if ($("#cmbimagenesmodelo").val() != null){
						$("#fila-datos-imagen").show();
						CargaDatosFoto($("#cmbimagenesmodelo").val());
					}else{
						$("#fila-datos-imagen").hide();
						$("#div-combo-imagenes").html("<strong>No existen imagenes para el banner.</strong>");
					}
				}
			});
	}

	function CargaDatosFoto(idD){
		var dataObject = { id_detalle: idD}
		$.ajax({
				data:  dataObject,
        url:   'ajax/ajax_carga_datos_imagen_modelo.php',
        type:  'post',
      	success:  function (response) {
					//alert(response);
					$("#datos-imagen").html(response);
					/*$("#imgLista").prop("src","../modulos/img/modelosfotos/modelosfotos-" + idD + ".jpg");*/
					$("#borra-imagen").unbind("click").bind('click', function () {
						borrarImagen();
					});
					$("#guardaredicion").click(function(){
						cambiarDatosImagen();
					})
				}
		});
	}


	function cambiarDatosImagen(){
		if ( $("#txtdescripcionedicion").val() != '' ){
			
			var idDetalle = $("#cmbimagenesmodelo").val();
			var datos = {id_detalle:idDetalle,
					id_modelo:0,
					descripcion:$("#txtdescripcionedicion").val()};
			$.ajax({
				data: datos,
		    url:   'ajax/ajax_guarda_imagen_modelo.php',
		    type:  'post',
				async: false,
				success:  function (response) {
					idDetalle = response;
					subeFotoModeloEdicion(idDetalle);
					
					cargaFotos();
				},error : function(jqXHR, textStatus, errorThrown){
					alert(errorThrown);
				}
			});

			abrirmodalavisos("Imagen Modelo","Cambios guardados con éxito.", 500, 200);
		}else{
			abrirmodalavisos("Imagen Modelo","Debe capturar al menos una descripción y seleccionar una imagen válida.", 500, 200);
		}
	}


	function borrarImagen(){
		

		var dataObject = { id_detalle: $("#cmbimagenesmodelo").val()}
		$.ajax({
				data:  dataObject,
        url:   'ajax/ajax_borra_imagen_modelo.php',
        type:  'post',
      	success:  function (response) {
					cargaFotos();
				}
		});
	}
	function readURLModeloEdicion(input) {
		var imgPath = $("#archivoImagenModeloEdicion").val();
		var extn = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
		if (extn == "gif" || extn == "png" || extn == "jpg" || extn == "jpeg") {
    	if (input.files && input.files[0]) {
     		var reader = new FileReader();
	      reader.onload = function (e) {
			  
	      	$('#imgLista').attr('src', e.target.result);
					
	      }
	      reader.readAsDataURL(input.files[0]);
	   	}
		}else{
			abrirmodalavisos("Archivo seleccionado", "Debe seleccionar un archivo de imagen válido.<br>(Extensión JPG, GIF, PNG)", '750px', '450px');
		}
	}
	function subeFotoModeloEdicion(idC){
	//debugger;
	var file_data = $("#archivoImagenModeloEdicion").prop("files")[0];
	var form_data = new FormData();

	form_data.append("file", file_data);
	$.ajax({
		data: form_data,
    url:   'ajax/ajax_sube_foto.php?num=' + idC + "&tipo=modelosfotos",
    type:  'post',
		cache: false,
		async: false,
		contentType: false,
		processData: false,
		success:  function (response) {
			//alert(response);
			$("#imgPreview").attr('src','../modulos/img/modelosfotos/modelosfotos-' + idC + ".jpg?' + Math.random()");
		}
	});
}
$("#borrar").unbind("click").bind('click', function () {
		//alert("aqui");
		$("#texto-confirmacion").html("¿Está seguro que desea borrar el modelo " + $("#txtmodelo").val() + '?');
		$( "#dialog-confirm" ).dialog({
			resizable: false,
		    height: "auto",
		    width: 400,
		    modal: true,
		    buttons: {
		        "Aceptar": function() {
		          	$( this ).dialog( "close" );
		          	var dataObject = { id_elemento: $("#lblid").text(),
					//nombreimagen: $("#nombreimagen").val(),
					catalogo: 'autosgenerales'
					}
					$.ajax({
						data:  dataObject,
				      //  url:   'ajax/ajax_borra_tema_sala.php',
				  		url:   'ajax/ajax_borrado_logico.php',
						type:  'post',
				      	success:  function (response) {
							//alert(response);
							if (jQuery.trim(response) == "OK"){
								abrirmodalavisos("Eliminar modelo","Modelo Eliminado con éxito.", 500, 200);
								location.reload();
							}				
						}
					});
		    	},
		        "Cancelar": function() {
		          $( this ).dialog( "close" );
		          return false;
	           }
		    }
	    });
  });
	
</script>
<?
include '../inputs.php';
?>
