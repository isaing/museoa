<?
require 'vs.php';
require_once '../includes/funcs.php';
session_start();
$idPerfil = $_SESSION['IDP'];


?><head>
<link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Raleway" />
</head>
<link rel="stylesheet" href="trumbowyg/dist/ui/trumbowyg.css">
<!-- Import jQuery -->

<!--<script src="//ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="js/vendor/jquery-3.2.1.min.js"><\/script>')</script>-->
<!-- Import Trumbowyg -->
<script src="trumbowyg/dist/trumbowyg.js"></script>
<script src="trumbowyg/dist/langs/es.min.js"></script>




<div class="contenedor">

<div class="row">
<div class="col11"></div>
<div class="col1">
			<a title="Agregar imagen" href="#" class="mostrar-detalle" data-indice="-1">
			<div class="btn-flotante">
				<i class="fa fa-plus btn-flotante-texto"></i>
			</div>
			</a>
</div>
</div>

<section>
<table id="tabla-principal" class="tabla-datos">
<thead>
<tr>
<th class="coltabla0 noexcel"></th>
<th class="col-iconos texto-centrado noexcel"></th>
<th class="col-iconos texto-centrado noexcel"></th>
<th class="coltabla1 texto-derecha">Id Artículo</th>
<th class="coltabla3 texto-derecha">Título</th>
<th class="coltabla1 texto-derecha">Etiqueta</th>
<th class="coltabla2 texto-derecha">Tipo articulo</th>
<th class="coltabla2 texto-derecha">Sala</th>
<th class="coltabla1 texto-derecha">Idioma</th>
<th class="coltabla3 texto-derecha">Enlace</th>


</tr>
</thead>
<tbody>
<?
	$filtro = $_POST['filtro'];
	cargaDatos($filtro);
?>
</tbody>
</table>


</section>

<section>
<div id="tabladetalle" style="background-color:#fff;z-index:99001" class="modalcapa modalcapa1">
	<div class="titulomodal titulonotif" style="font-size:20px !important; font-weight:bold">Artículo</div>
	<div id="tabs" >
		<ul>
    		<li id="tab-datos"><a href="#tabs-1">Datos del artículo</a></li>
            <li id="tab-datos2"><a href="#tabs-2">Datos del Bloque 2</a></li>
  			<li id="tab-foto"><a href="#tabs-3">Imagen</a></li>
        	<!--<li id="tab-archivo"><a href="#tabs-4">Archivo Pdf</a></li>-->
  		</ul>
		<div id="tabs-1">
			<table id="detalle" class="tabla-datos">
				<tbody class="contenidomodal">
               
    
   					 <tr id="fila-idioma">
                      	<td class="col3 texto-derecha">*Idioma: </td>
                      	<td class="col3 texto-izquierda"><? agregacombo('idiomas','2');?></td>
                         
    				 </tr>
					
                     <tr> 
                     <td class="col3 texto-derecha">*Tipo de Artículo: </td>
                       <td class="col9 texto-izquierda"><? agregacombo('tipoarticulo');?></td>
                       </tr>
                       <tr  id="cmbnumsalas">
                     <td class="col3 texto-derecha">**Sala:</td>
                      <td class="col2 texto-izquierda"><? agregacombo('numerosalas');?></td>
                   		
                    </tr>
    
					<tr>
						<td class="col3 texto-derecha">*Título:<input id="lblid" type="hidden"></td>
						<td class="col4 texto-izquierda"><input class="control" id="txttitulo" required></td>
                         <td class="col2 texto-derecha">*Etiqueta</td>
					   <td class="col3 texto-izquierda"><input class="control" id="txtetiqueta" required></td>
					</tr>
                    
					  <tr>
					   <td class="col3 texto-derecha">*Frase Intro</td>
					   <td class="col9 texto-izquierda"><input class="control" id="txtfraseintro" required></td>
					  
			      </tr>
					<tr>
						<td class="col3 texto-derecha">*Bloque 1 de texto del artículo:</td>
						<td class="col9 texto-izquierda">
                        <div id="txtcontenido1">
                        
                        </div>
                        <!--<textarea name="txtcontenido"  id="txtcontenido" ></textarea>--></td>
					</tr>
					<tr id='fila-estado'>
						<td class="col3 texto-derecha">Estado:</td>
						<td class="col2 texto-izquierda"><? agregacomboestatus(); ?></td>
					</tr>
				</tbody>
			</table>
		</div>
        <div id="tabs-2">
			<table id="detalle" class="tabla-datos">
				<tbody class="contenidomodal">
               
   					 <tr>
						<td class="col3 texto-derecha">Bloque 2 de texto del artículo:</td>
						<td class="col9 texto-izquierda">
                        <div id="txtcontenido2">
                        
                        </div>
                        <!--<textarea name="txtcontenido"  id="txtcontenido" ></textarea>--></td>
					</tr>
                    
					  <tr>
					   <td class="col3 texto-derecha">Frase Cierre</td>
					   <td class="col9 texto-izquierda"><input class="control" id="txtfrasecierre" required></td>
					  
			      </tr>
					
					
				</tbody>
			</table>
		</div>
		<div id="tabs-3">
        	
			<form id="form1" runat="server">
	       
                    
                    <table id="detalle" class="tabla-datos">
				<tbody class="contenidomodal">
                <tr>
						<td class="col12 texto-izquierda"> <img id="imgPreview" width="60%" src="#" alt="Da clic para seleccionar la imagen " />
            <input id="lblimagenarchivo" type="hidden" />
					<br>
					<input id="archivoImagen" type='file' accept=".JPG, .jpg, image/jpeg" onchange="readURL(this);" style="display:none" /></td>
						
					</tr>
               <!-- <tr>
						<td class="col9 texto-derecha"></td>
						<td class="col3 texto-izquierda"><input type="button" id="borra-imagen" name="borrar" title="Borrar" class="btn-formulario borra-imagen" value="Borrar imagen"></td>
					</tr>-->
                </tbody>
             </table> 
                    
	    </form>
        
        	
		</div>
      <!--  <div id="tabs-4">
        <table id="detalle" class="tabla-datos">
				<tbody class="contenidomodal">
                <tr>
						<td class="col3 texto-derecha">Texto vínculo pdf:<input id="lblid" type="hidden"></td>
						<td class="col9 texto-izquierda"><input class="control" id="txttextopdf" ></td>
					</tr>
               <tr>
               <td class="col3 texto-izquierda">
               </td>
                <td class="col9 texto-izquierda">
		
      				 <div id='div-agregar-anexo'>				
								<input id="archivo-anexo" type="file" class="inputfile" accept=".PDF, .pdf, application/pdf">
								<label for="archivo-anexo" id="lbl-archivo-anexo">
									<i class="fa fa-file-pdf-o" style="padding-top:0px"></i>&nbsp;Seleccione el archivo
								</label><br /><br />
                               <a href="#" id="pdfPreviewArchivo" target="_blank" ><i class="fa fa-file-pdf-o" style="padding-top:0px"></i>&nbsp;Ver Archivo</a> 
                                <input type="hidden"  id='anexoanterior'/>
							</div>
							<div id='div-ver-anexo'></div>
        </td>
        </tr>
        <tr>
        <td class="col9 texto-izquierda">
        </td>
        <td class="col3 texto-izquierda">
        <input type="button" id="borra-pdf" name="borrar" title="Borrar" class="btn-formulario borra-pdf" value="Borrar pdf">
        </td>
        </tr>
        </tbody>
             </table>
		</div>-->
		<div style="margin-top:15px; padding-bottom:10px; width:100%; padding-left:550px;" id="espera"></div>
		<div class="row" id="areabotones" style="padding:10px;">
		<div class="col8"></div>
		<div class="col2 texto-centrado">
			<input type="button" id="guardar" name="enviar" title="Guardar" class="btn-formulario" value="Guardar">
			<img id="espera2" src='img/loading.gif' class="icono-espera">
		</div>
		<div class="col2">
			<input type="button" id="cerrar" name="cerrar" title="Cerrar" class="btn-formulario modalcerrar" value="Cerrar">
		</div>
	</div>
    
	</div>

</div>
</section>

<?
function cargaDatos($filtro){
	$strSQL = "CALL paCatalogoArticulos('%".$filtro."%')";
	$resultado = consulta($strSQL);
	while ($row = $resultado->fetch_array(MYSQLI_ASSOC)){
		$indice = $row["id_articulo"];
		echo "<tr id=".$indice.">";
		echo "<td style='display:none' class='noexcel'><label id='lblididioma".$indice."'>".$row["ididioma"]."</label>
		<label id='lblidtipoarticulo".$indice."'>".$row["idtipoarticulo"]."</label><label id='lblidnumsala".$indice."'>".$row["idnumsala"]."</label>
		<label id='lblestatus".$indice."'>".$row["estatus"]."</label><label id='lblid".$indice."'>".$indice."</label><label id='lblfraseintro".$indice."'>".$row["fraseintro"]."</label><label id='lblcontenido1".$indice."'>".$row["contenido1"]."</label><label id='lblcontenido2".$indice."'>".$row["contenido2"]."</label><label id='lblfrasecierre".$indice."'>".$row["frasecierre"]."</label><label id='lblimagen".$indice."'>".$row["imagen"]."</label><label id='lbltextopdf".$indice."'>".$row["textopdf"]."</label><label id='lblpdf".$indice."'>".$row["pdf"]."</label></td>";
		echo "<td class='texto-centrado noexcel'><a title='Editar' href='#' id='editar' class='mostrar-detalle' data-indice='".$indice."'><i class='fa fa-edit icono-tablas' aria-hidden='true'></i></a></td>";
		echo "<td class='texto-centrado noexcel'><label>".semaforoestatus($row["estatus"])."</label></td>";
		echo "<td class='texto-centrado'><label id='lblidimagen".$indice."'>".$row["id_articulo"]."</label></td>";
		echo "<td class='texto-izquierda'><label id='lbltitulo".$indice."'>".$row["titulo"]."</label></td>";
		echo "<td class='texto-izquierda'><label id='lbletiqueta".$indice."'>".$row["etiqueta"]."</label></td>";
		echo "<td class='texto-izquierda'><label id='lbltipoarticulo".$indice."'>".$row["tipoarticulo"]."</label></td>";
		echo "<td class='texto-izquierda'><label id='lblnombresala".$indice."'>".$row["nombresala"]."</label></td>";
		echo "<td class='texto-izquierda'><label id='lblidioma".$indice."'>".$row["idioma"]."</label></td>";
		echo "<td class='texto-izquierda'><label id='lblvinculo".$indice."'>".$row["vinculo"]."</label></td>";
		
		
		echo "</tr>";
	}
	$resultado->close();
}
?>

<script>
	$(document).ready(function() {
		$("#tabs").tabs();
		$(".modalcapa1").draggable({ cursor: "move", handle: ".titulomodal", opacity: 0.5 });
		$("#espera2").hide();
		$("#tabladetalle").hide();
		Crearlisteners();
		/*$('#txtcontenido').trumbowyg();*/
		$('#txtcontenido1').trumbowyg({
   			 btns: [['viewHTML'],
        		['undo', 'redo'], // Only supported in Blink browsers
		        ['formatting'],
		        ['strong', 'em'],
		        ['link'],
		        ['justifyLeft', 'justifyCenter', 'justifyRight', 'justifyFull'],
        		['unorderedList', 'orderedList'],
		        ['horizontalRule'],
		        ['removeformat']],
				lang: 'es',
				svgPath : 'trumbowyg/dist/ui/icons.svg',
				removeformatPasted: true
		});
		/*$('#txtcontenido').trumbowyg();*/
		$('#txtcontenido2').trumbowyg({
   			 btns: [['viewHTML'],
        		['undo', 'redo'], // Only supported in Blink browsers
		        ['formatting'],
		        ['strong', 'em'],
		        ['link'],
		        ['justifyLeft', 'justifyCenter', 'justifyRight', 'justifyFull'],
        		['unorderedList', 'orderedList'],
		        ['horizontalRule'],
		        ['removeformat']],
				lang: 'es',
				svgPath : 'trumbowyg/dist/ui/icons.svg',
				removeformatPasted: true
		});
		//$.trumbowyg.svgPath = '/assets/my-custom-path/icons.svg';
		/*$('#txtcontenido').trumbowyg({
   			 btns: [['viewHTML'],
        ['undo', 'redo'], // Only supported in Blink browsers
        ['formatting'],
        ['strong', 'em', 'del'],
        ['superscript', 'subscript'],
        ['link'],
        ['insertImage'],
        ['justifyLeft', 'justifyCenter', 'justifyRight', 'justifyFull'],
        ['unorderedList', 'orderedList'],
        ['horizontalRule'],
        ['removeformat'],
        ['fullscreen']]
		});*/
	});

	                
	$(".mostrar-detalle").unbind("click").bind('click', function () {
		mostrarDetalle($(this).data("indice"));
  });
		$("#borra-imagen").unbind("click").bind('click', function () {
						borrarImagen();
	});
	$("#borra-pdf").unbind("click").bind('click', function () {
						borrarPdf();
	});

	function abrirmodalc1(width, height) {
		Ajustatamanocapa(".modalcapa1", width, height);
		$(".modalgeneralcapa1").fadeIn();
		$(".modalcapa1").fadeIn();
		return;
	}

	function Ajustatamanocapa(Control, width, height) {
		$(Control).css('width', width);
		$(Control).css('left', 'calc(50% - ' + width + '/2)');
		$(Control).css('top', 'calc(30% - ' + height + '/2)');

		$(Control + ' .contenidomodal').css('height', 'calc(' + height + ' - 140px)');

		return;
	}
	function cerrarmodalc1() {
		$(".modalgeneralcapa1").fadeOut();
		$('.modalcapa1').fadeOut();
	}

	function Crearlisteners(){
		$('.modalcerrar').click(function(){
			cerrarmodalc1();
			return false;
		});
	}
	$("#cmbtipoarticulo").change(function() {
                var valor = this.value;
				//alert(valor);
				if(valor==3)
				{
					$("#cmbnumsalas").show();
				}
				else{
					$("#cmbnumsalas").hide();
				}
    });

	$("#guardar").click(function() {
		imgsrc=0;
		 if($("#imgPreview").attr('src')!=" ")
		 {
			 imgsrc=1;
			
		}
		/*alert($("#imgPreview").attr('src'));
		alert($("#archivoImagen").val());
		debugger;*/
		if (($('#txttitulo').val() != '') && $('#txtetiqueta').val() != '' && $('#cmbtipoarticulo').val() != null && $("#cmbidiomas").val()!=null && 
		$('#txtfraseintro').val() != '' && $('#txtcontenido1').html() != '' && ($("#archivoImagen").val() != "" || imgsrc==1)){
			/* si selecciona tipo salas y no ha seleccionado el número de salas*/
			if($('#cmbtipoarticulo').val() == 3 && $('#cmbnumerosalas').val() == null)
			{
				
			abrirmodalavisos("Artículos", "Debe capturar el número de sala si selecciona el tipo sala ", '750px', '450px');
			}
			else{
				id=$("#lblid").text();
				if(id!=0)
				{
					//alert($("#archivo-anexo").val());
					/*if($("#archivo-anexo").val()!="")
					{
						if($("#txttextopdf").val()=="")
						{
							abrirmodalavisos("Artículos", "Debe capturar el título del vínculo pdf si selecciona un archivo pdf", '750px', '450px');
							return false;
						}
						else{
							subePdf(id);
						}
					}*/
					
					if($("#archivoImagen").prop("files")[0]!="undefined")
					{
					subeFoto(id);
					//$("#lblimagenarchivo").text()
					}
				}
				
			
				//textopdf: $("#txttextopdf").val(),
				//alert($("#lblimagenarchivo").text());
				//alert($("#txttextopdf").val());
				/*if($("#archivo-anexo").val()==""){
					pdf='';
				}
				else{
					pdf=$("#lbl-archivo-anexo").text();
				}*/
				if($("#cmbtipoarticulo").val()!=3){
					numsala=""
				}
				else
				{
					numsala=$("#cmbnumerosalas").val();
				}
					
				var dataObject = { id_articulo: $("#lblid").text(),
				idioma: $("#cmbidiomas").val(),
				tipoarticulo: $("#cmbtipoarticulo").val(),
				numsala: numsala,
				titulo: $("#txttitulo").val(),
				etiqueta: $("#txtetiqueta").val(),
				fraseintro: $("#txtfraseintro").val(),
				contenido1: $("#txtcontenido1").html(),
				contenido2: $("#txtcontenido2").html(),
				frasecierre: $("#txtfrasecierre").val(),
				imagen: $("#lblimagenarchivo").text(),
				//textopdf: $("#txttextopdf").val(),
				textopdf: '',
				pdf: '',
				estatus: $("#cmbestatus").val()};
				var idOk = "0";
				$.ajax({
					data:  dataObject,
					url:   'ajax/ajax_guarda_articulo.php',
	        type:  'post',
					async: false,
		      beforeSend: function () {
						$("#").hide();
	         		 $("#espera2").show();
	        },
	        success:  function (response) {
						//alert(response);
						var id = $("#lblid").text();
						cerrarmodalc1();
						$("#espera2").hide();
						$("#").show();
						var pos = response.indexOf("IDOK");
						idOk = response.substr(pos + 4);
						response = response.replace("IDOK" + idOk,"");
						if (response.substring(0,3) == "<tr" || response.substring(0,3) == "<td"){
							if (id == 0) {
								if ($('#tabla-principal > tbody > tr').length == 0){
									$('#tabla-principal > tbody ').html(response);
								}else{
									$('#tabla-principal > tbody > tr').eq(0).before(response);
								}
							}else{
								$("#"+id).html(response);
							}
							subeFoto(idOk);
							//debugger;
							$("#lblimagen"+idOk).html('articulos-' + idOk + '.jpg');
							
							$("#imgPreview").attr('src','../modulos/img/articulos/articulos-' + id + ".jpg?' + Math.random()");/**/
							
						}else{
							abrirmodalavisos('Artículos',response, '800px', '450px');
						}
	
						$(".mostrar-detalle").unbind("click").bind('click', function () {
							mostrarDetalle($(this).data("indice"));
					  });
					}
	
	       		});
			}
		}else{
			abrirmodalavisos("Artículos", "Debe capturar el idioma, el tipo del artículo, el título, la etiqueta, la frase intro, el bloque de contenido 1 y la imagen del artículo que desea ", '750px', '450px');
		}
	});

function subeFoto(idC){
	//debugger;
	var file_data = $("#archivoImagen").prop("files")[0];
	if(file_data!=undefined)
	{
		
	var form_data = new FormData();
//alert(idC);
	var archivo = $("#archivoImagen").val();
   	var tipoext = (archivo.substring(archivo.lastIndexOf(".")+1)).toLowerCase(); 
	form_data.append("file", file_data);
	form_data.append("tipoext", tipoext);
	form_data.append("actualizacampo", 1);
	$.ajax({
		data: form_data,
    url:   'ajax/ajax_sube_foto.php?num=' + idC + "&tipo=articulos",
    type:  'post',
		cache: false,
		async: false,
		contentType: false,
		processData: false,
		success:  function (response) {
			/*$("#imgPreview").attr('src','../modulos/img/imagenes/imagenes-' + idC + ".jpg");*/
			$("#imgPreview").attr('src','../modulos/img/articulos/articulos-' + idC + ".jpg?' + Math.random()");
			$("#lblimagenarchivo").text('articulos-' + idC + '.jpg');
		}
	});
	}
	else
	{
		$('#archivoImagen').attr({ value: '' });
	}
	
	
}
function subePdf(idC){
	//alert(idC);
	/*debugger;*/
	
	var file_data = $("#archivo-anexo").prop("files")[0];
		//alert(file_data);
	if(file_data!=undefined)
	{
	var form_data = new FormData();
	
	form_data.append("file", file_data);
	//alert(form_data);
	$.ajax({
	data: form_data,
    url:   'ajax/ajax_sube_pdf.php?num=' + idC + "&tipo=articulos",
    type:  'post',
		cache: false,
		async: false,
		contentType: false,
		processData: false,
		success:  function (response) {
			//alert(response);
			//$("#pdfPreview").attr('src','../modulos/img/cabeceras/cabeceras' + idC + ".jpg");
			$("#pdfPreviewArchivo").attr('href','../modulos/img/articulos/articulos-' + idC + ".pdf");
			//$("#lblreglas").html($('programasreglasoperacion-' + idC + '.pdf').html());
			$("#lbl-archivo-anexo").text('articulos-' + idC + '.pdf');
			//$("#pdfPreviewArchivo").attr('src','../modulos/img/cabeceras/cabeceras' + idC + ".jpg");
			
		}
	});
	}else
	{
		$('#archivoPdf').attr({ value: '' });
	}
	
}	
function borrarImagen(){
		id=$("#lblid").text();
		//alert(id);
		var dataObject = { id_detalle: id}
		$.ajax({
				data:  dataObject,
        url:   'ajax/ajax_borra_imagen_articulo.php',
        type:  'post',
      	success:  function (response) {
					//cargaFotos();
					//alert(response);
					$("#imgPreview").attr('src','');
					//$("#borra-imagen").hide();
					$("#lblimagenarchivo").html('');
					cerrarmodalc1();
					$("#lblimagen"+id).html("");

					
				/*$archivoImagen = $('#archivoImagen');
				$archivoImagen.replaceWith($archivoImagen.clone(true));*/
				}
		});/**/
	}
function borrarPdf(){
		id=$("#lblid").text();
		//alert(id);
		var dataObject = { id_detalle: id}
		$.ajax({
				data:  dataObject,
        url:   'ajax/ajax_borra_pdf_articulo.php',
        type:  'post',
      	success:  function (response) {
					//cargaFotos();
					//alert(response);
					$("#pdfPreview").attr('src','');
					$("#borra-pdf").hide();
					$("#pdfPreviewArchivo").hide();
					$("#lblpdfarchivo").html('');
					cerrarmodalc1();
					$("#lbltextopdf"+id).html("");
					$("#lblpdf"+id).html("");
					/*$archivoPdf = $('#archivoPdf');
					$archivoPdf.replaceWith($archivoPdf.clone(true));*/
				
				}
		});
		
	}
	function mostrarDetalle(i) {
		$("#cmbnumsalas").hide();
		//$("#borra-imagen").hide();
		
		$("#lbl-archivo-anexo").html("<i class='fa fa-file-text' style='padding-top:0px'></i>&nbsp;Seleccione el archivo");
		$('#imgPreview').attr('src', ' ');
		$('#archivoPdf').val("");
		$('#archivoImagen').val("");
		$("#espera2").hide();
		$( "#tabs" ).tabs( "option", "active", 0 );
		if (i != "-1"){
			$("#lblid").html($('#lblid' + i).html());
			$("#cmbidiomas").val($('#lblididioma' + i).html());
			$("#cmbtipoarticulo").val($('#lblidtipoarticulo' + i).html());
			if($('#lblidnumsala' + i).html()!=""){
				
				$("#cmbnumsalas").show();
				$("#cmbnumerosalas").val($('#lblidnumsala' + i).html());
			}
			else{
				$("#cmbnumerosalas").val(1);
			}
			$("#txttitulo").val($('#lbltitulo' + i).html());
			$("#txtetiqueta").val($('#lbletiqueta' + i).html());
			$("#txtfraseintro").val($('#lblfraseintro' + i).html());
			$("#txtcontenido1").html($('#lblcontenido1' + i).html());
			$("#txtcontenido2").html($('#lblcontenido2' + i).html());
			$("#txtfrasecierre").val($('#lblfrasecierre' + i).html());
			$("#cmbestatus").val($("#lblestatus" + i).html());
			$("#lblimagenarchivo").html($('#lblimagen' + i).html());
			$("#txttextopdf").val($('#lbltextopdf' + i).html());
			$("#lblpdf").html($('#lblpdf' + i).html());
			/*$archivoPdf = $('#archivoPdf');
				$archivoPdf.replaceWith($archivoPdf.clone(true));
				$archivoImagen = $('#archivoImagen');
				$archivoImagen.replaceWith($archivoImagen.clone(true));*/
				$("#imgPreview").attr('src','../modulos/img/articulos/articulos-' + $("#lblid" + i ).html()+ ".jpg?" + Math.random());
			$('#archivoPdf').val("");
			$('#archivoImagen').val("");
			if ($("#lblpdf" + i).html() != ""){
				//alert("si");
				$("#anexoanterior").val($("#lblpdf"+ i).html().split('\\').pop());
				$("#pdfPreview").attr('href','../modulos/img/articulos/' + $("#lblpdf" + i ).html());
				$("#pdfPreviewArchivo").show();
				$("#pdfPreviewArchivo").attr('href','../modulos/img/articulos/' + $("#lblpdf" + i ).html());
				$("#lblpdfarchivo").text($("#lblpdf"+ i).html());
				$("#borra-pdf").show();
				
				
			}
			else{
				
				/*$archivoPdf = $('#archivoPdf');
				$archivoPdf.replaceWith($archivoPdf.clone(true));
				$archivoImagen = $('#archivoImagen');
				$archivoImagen.replaceWith($archivoImagen.clone(true));*/
				$("#lblpdfarchivo").html('');
				$("#pdfPreview").attr('href',' ');
				$("#pdfPreviewArchivo").attr('href',' ');
				$("#pdfPreviewArchivo").hide();
				$("#borra-pdf").hide();
				
			
				
			}
			if ($("#lblimagen" + i).html() != ""){
				
				$("#imgPreview").attr('src','../modulos/img/articulos/articulos-' + $("#lblid" + i ).html()+ ".jpg?" + Math.random());
				//$("#borra-imagen").show();
			}
			else{
				$("#imgPreview").attr('src','');
				//$("#borra-imagen").hide();
				$("#lblimagenarchivo").html('');
				
				
				
				
			}
		}else{
			$("#tab-permisos").hide();
			$("#lblid").html("0");
			$("#cmbidiomas").val(1);
			$("#txttitulo").val("");
			$("#txtetiqueta").val("");
			$("#txtfraseintro").val("");
			$("#txtcontenido1").html("");
			$("#txtcontenido2").html("");
			$("#txtfrasecierre").val("");
			$("#cmbestatus").val(1);
			$("#txttextopdf").val("");
			$("#lblpdfarchivo").html('');
			$("#lblimagenarchivo").html('');
			$("#cmbtipoarticulo").val(1);
			$("#cmbnumerosalas").val(1);
			
			//$("#borra-imagen").hide();
			
			
			
			$("#lblpdfarchivo").html('');
			$("#pdfPreviewArchivo").attr("href", "#");
			
			$("#anexoanterior").val("");
			$("#pdfPreviewArchivo").hide();
			
			
			
		}
		abrirmodalc1('850px', '450px');

		return false;
	}

	function readURL(input) {
		var imgPath = $("#archivoImagen").val();
		var extn = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
		if (extn == "gif" || extn == "png" || extn == "jpg" || extn == "jpeg") {
    	if (input.files && input.files[0]) {
     		var reader = new FileReader();
	      reader.onload = function (e) {
			  
	      	$('#imgPreview').attr('src', e.target.result);
					
	      }
	      reader.readAsDataURL(input.files[0]);
	   	}
		}else{
			abrirmodalavisos("Archivo seleccionado", "Debe seleccionar un archivo de imagen válido.<br>(Extensión JPG, GIF, PNG)", '750px', '450px');
		}
	}
	function readURLpdf(input) {
		/*alert("readURL");
		debugger;*/
		var pdfPath = $("#archivoPdf").val();
		var extn = pdfPath.substring(pdfPath.lastIndexOf('.') + 1).toLowerCase();
		if (extn == "pdf" || extn == "PDF") {
    	if (input.files && input.files[0]) {
     		var reader = new FileReader();
	      reader.onload = function (e) {
	      	//$('#pdfPreview').attr('src', e.target.result);
			/*$("#pdfPreviewArchivo").attr('href','../modulos/img/programasreglasoperacion/programasreglasoperacion' + idC + ".pdf");*/
			//$('#pdfPreviewArchivo').attr('href', pdfPath);
			/*$("#pdfPreviewArchivo").attr('href','../modulos/img/programasreglasoperacion/programasreglasoperacion-' + idC + ".pdf");
			$("#lblreglas").html($('../modulos/img/programasreglasoperacion/programasreglasoperacion-' + idC + '.pdf' + i).html());*/
	      }
	      reader.readAsDataURL(input.files[0]);
	   	}
		}else{
			abrirmodalavisos("Archivo seleccionado", "Debe seleccionar un archivo pdf válido.<br>(Extensión PDF)", '750px', '450px');
		}
	}

	/*$("#imgInp").change(function(){
	    readURL(this);
	});*/
	$("#imgPreview").click(function() {
    $("input[id='archivoImagen']").click();
	});
	$("#pdfPreview").click(function() {
    $("input[id='archivoPdf']").click();
	});
	$("#archivo-anexo").change(function() {
		if ($("#archivo-anexo").val().split('\\').pop() != ""){
			$("#lbl-archivo-anexo").html("<i class='fa fa-file-text' style='padding-top:0px'></i>&nbsp;" + $("#archivo-anexo").val().split('\\').pop());
		}else{
			$("#lbl-archivo-anexo").html("<i class='fa fa-file-text' style='padding-top:0px'></i>&nbsp;Seleccione el archivo");
		}
  	});  
</script>
<?
include '../inputs.php';
?>
