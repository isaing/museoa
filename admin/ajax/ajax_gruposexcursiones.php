<?
require 'vs.php';
require_once '../includes/funcs.php';
?><head>
<link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Raleway" />
</head>


<div class="contenedor">

<div class="row">
<div class="col11"></div>
<div class="col1">
			<a title="Agregar como llegar" href="#" class="mostrar-detalle" data-indice="-1">
			<div class="btn-flotante">
				<i class="fa fa-plus btn-flotante-texto"></i>
			</div>
			</a>
</div>
</div>

<section>
<table id="tabla-principal" class="tabla-datos">
<thead>
<tr>
<th class="coltabla0 noexcel"></th>
<th class="col-iconos texto-centrado noexcel"></th>
<th class="col-iconos texto-centrado noexcel"></th>
<th class="coltabla4 texto-derecha">Título</th>
<th class="coltabla1 texto-derecha">Orden</th>
<th class="coltabla1 texto-derecha">Idioma</th>

</tr>
</thead>
<tbody>
<?
	$filtro = $_POST['filtro'];
	cargaDatos($filtro);
?>
</tbody>
</table>


</section>

<section>
<div id="tabladetalle" style="background-color:#fff;z-index:99001" class="modalcapa modalcapa1">
	<div id="tabs" class="titulomodal">
		<ul>
    	<li id="tab-datos"><a href="#tabs-1">Datos de Grupos y Excursiones</a></li>
  		<li id="tab-foto"><a href="#tabs-2">Imagen</a></li>
        <li id="tab-pdf"><a href="#tabs-3">PDF descarga</a></li>
  	</ul>
		<div id="tabs-1">
			<table id="detalle" class="tabla-datos">
				<tbody class="contenidomodal">
					<tr id="fila-idioma">
                   	   <td class="col3 texto-derecha">Idioma: </td>
                   	   <td class="col9 texto-izquierda"><? agregacombo('idiomas','2');?></td>
    				 </tr>
					  <tr>
					   <td class="col3 texto-derecha">Título
				         <input id="lblid" type="hidden" /></td>
					   <td class="col9 texto-izquierda"><input class="control" id="txttitulo" required></td>
					  
			      </tr>
                  <tr>
						<td class="col3 texto-derecha">Descripción:</td>
						<td class="col9 texto-izquierda">
						  <textarea name="txtdescripcion" class="control-area" id="txtdescripcion"></textarea>
						</td>
					</tr>
                    <tr>
						<td class="col3 texto-derecha">Orden:</td>
						<td class="col2 texto-izquierda"><input type="number"required class="control" id="txtorden" value="0"></td>
					</tr>
					<tr id='fila-estado'>
						<td class="col3 texto-derecha">Estado:</td>
						<td class="col2 texto-izquierda"><? agregacomboestatus(); ?></td>
					</tr>
				</tbody>
			</table>
		</div>
		<div id="tabs-2">JPG
			<form id="form1" runat="server">
	        <img id="imgPreview" width='50%' src="#" alt="Da clic para seleccionar la imagen " />
					<br>
					<input id="archivoImagen" type='file' accept=".JPG, .jpg, image/jpeg" onchange="readURL(this);" style="display:none" />
	    </form>
		</div>
        <div id="tabs-3">
			<table id="detalle" class="tabla-datos">
				<tbody class="contenidomodal">
               
					<tr>
						<td class="col3 texto-derecha">*Título pdf:
						  <input id="lblid" type="hidden"></td>
						<td class="col9 texto-izquierda"><input class="control" id="txttitulopdf" required></td>
					</tr>
					
					<tr>
						<td class="col3 texto-derecha">*Seleccione el archivo (pdf)</td>
						<td class="col9 texto-izquierda">
							
							<div id='div-agregar-anexo'>				
								<input id="archivo-anexo" type="file" class="inputfile" accept=".PDF, .pdf, application/pdf">
								<label for="archivo-anexo" id="lbl-archivo-anexo">
									<i class="fa fa-file-pdf-o" style="padding-top:0px"></i>&nbsp;Seleccione el archivo
								</label><br /><br />
                               <a href="#" id="pdfPreviewArchivo" target="_blank" ><i class="fa fa-file-pdf-o" style="padding-top:0px"></i>&nbsp;Ver Archivo</a> 
                                <input type="hidden"  id='anexoanterior'/>
							</div>
							<div id='div-ver-anexo'></div>
						
						</td>
					</tr>
          </tbody>
          </table>
          </div>
		<div style="margin-top:15px; padding-bottom:10px; width:100%; padding-left:550px;" id="espera"></div>
		<div class="row" id="areabotones" style="padding:10px;">
		<div class="col6"></div>
		<div class="col2 texto-centrado">
			<input type="button" id="guardar" name="enviar" title="Guardar" class="btn-formulario" value="Guardar">
			<img id="espera2" src='img/loading.gif' class="icono-espera">
		</div>
        <div class="col2"  id="colborrarelemento">
      <input type="button" id="borrarelemento" name="borrar" title="Borrar" class="btn-formulario borra-imagen" value="Borrar">
    </div>
		<div class="col2">
			<input type="button" id="cerrar" name="cerrar" title="Cerrar" class="btn-formulario modalcerrar" value="Cerrar">
		</div>
	</div>
	</div>
	<div id="dialog-confirm" title="Borrar elemento">
      <p><span class="ui-icon ui-icon-alert" style="float:left; margin:12px 12px 20px 0;"></span><label id='texto-confirmacion'></label>
      </p>
    </div>
</div>
</section>

<?
function cargaDatos($filtro){
	$rutaimagen="../modulos/img/gruposexcursiones/gruposexcursiones-";
	$strSQL = "CALL paCatalogoGruposExcursiones('%".$filtro."%')";
	$resultado = consulta($strSQL);
	while ($row = $resultado->fetch_array(MYSQLI_ASSOC)){
		$indice = $row["id_gruposexcursiones"];
		echo "<tr id=".$indice.">";
		echo "<td style='display:none' class='noexcel'><label id='lblestatus".$indice."'>".$row["estatus"]."</label><label id='lblid".$indice."'>".$indice."</label><label id='lblididioma".$indice."'>".$row["ididioma"]."</label><label id='lblorden".$indice."'>".$row["orden"]."</label><label id='lbldescripcion".$indice."'>".$row["descripcion"]."</label><label id='lbltitulopdf".$indice."'>".$row["titulopdf"]."</label></td>";
		echo "<td class='texto-centrado noexcel'><a title='Editar' href='#' id='editar' class='mostrar-detalle' data-indice='".$indice."'><i class='fa fa-edit icono-tablas' aria-hidden='true'></i></a></td>";
		echo "<td class='texto-centrado noexcel'><label>".semaforoestatus($row["estatus"])."</label></td>";
		echo "<td class='texto-centrado'><label id='lbltitulo".$indice."'>".$row["titulo"]."</label></td>";
		
		
		echo "<td class='texto-izquierda'><label id='lblorden".$indice."'>".$row["orden"]."</label></td>";
		
		echo "<td class='texto-izquierda'><label id='lblidioma".$indice."'>".$row["idioma"]."</label></td>";
		
		echo "</tr>";
	}
	$resultado->close();
}
?>

<script>
	$(document).ready(function() {
		$("#dialog-confirm").hide();
		$("#tabs").tabs();
		$(".modalcapa1").draggable({ cursor: "move", handle: ".titulomodal", opacity: 0.5 });
		$("#espera2").hide();
		$("#tabladetalle").hide();
		Crearlisteners();
	});

	                
	$(".mostrar-detalle").unbind("click").bind('click', function () {
		mostrarDetalle($(this).data("indice"));
  });


	function abrirmodalc1(width, height) {
		Ajustatamanocapa(".modalcapa1", width, height);
		$(".modalgeneralcapa1").fadeIn();
		$(".modalcapa1").fadeIn();
		return;
	}

	function Ajustatamanocapa(Control, width, height) {
		$(Control).css('width', width);
		$(Control).css('left', 'calc(50% - ' + width + '/2)');
		$(Control).css('top', 'calc(50% - ' + height + '/2)');

		$(Control + ' .contenidomodal').css('height', 'calc(' + height + ' - 140px)');

		return;
	}
	function cerrarmodalc1() {
		$(".modalgeneralcapa1").fadeOut();
		$('.modalcapa1').fadeOut();
	}

	function Crearlisteners(){
		$('.modalcerrar').click(function(){
			cerrarmodalc1();
			return false;
		});
	}

	$("#guardar").click(function() {
		imgsrc=0;
		 if($("#imgPreview").attr('src')!=" ")
		 {
			 imgsrc=1;
			
		}
		pdfsrc=0;
		 if($("#pdfPreviewArchivo").attr('src')!=" ")
		 {
			 pdfsrc=1;
			
		}
		if ($('#txttitulo').val() != ''  && $('#txtdescripcion').val() != ''  && ($("#archivoImagen").val() != "" || imgsrc==1) && $("#cmbidiomas").val()!=null && $('#txttitulopdf').val() != '' && ($("#archivo-anexo").val() != "" || pdfsrc==1)){
			
			var dataObject = { id_gruposexcursiones: $("#lblid").text(),
			titulo: $("#txttitulo").val(),
			descripcion: $("#txtdescripcion").val(),
			titulopdf: $("#txttitulopdf").val(),
			idioma: $("#cmbidiomas").val(),
			orden: $("#txtorden").val(),
			estatus: $("#cmbestatus").val()};
			var idOk = "0";
			$.ajax({
				data:  dataObject,
				url:   'ajax/ajax_guarda_gruposexcursiones.php',
        type:  'post',
				async: false,
	      beforeSend: function () {
					$("#guardar").hide();
          $("#espera2").show();
        },
        success:  function (response) {
			//debugger;
					var id = $("#lblid").text();
					cerrarmodalc1();
					$("#espera2").hide();
					$("#guardar").show();
					var pos = response.indexOf("IDOK");
					idOk = response.substr(pos + 4);
					response = response.replace("IDOK" + idOk,"");
					if (response.substring(0,3) == "<tr" || response.substring(0,3) == "<td"){
						if (id == 0) {
							if ($('#tabla-principal > tbody > tr').length == 0){
								$('#tabla-principal > tbody ').html(response);
							}else{
								$('#tabla-principal > tbody > tr').eq(0).before(response);
							}
						}else{
							$("#"+id).html(response);
						}
						subeFoto(idOk);
						
						$("#imgPreview").attr('src','../modulos/img/gruposexcursiones/gruposexcursiones-' + id + ".jpg?' + Math.random()");
						if($("#archivo-anexo").prop("files")[0]!="undefined")
						{
							//debugger;
							subePdf(idOk);
						}
					}else{
						abrirmodalavisos('Cómo Llegar',response, '800px', '450px');
					}

					$(".mostrar-detalle").unbind("click").bind('click', function () {
						mostrarDetalle($(this).data("indice"));
				  });
				}

       		});
		}else{
			abrirmodalavisos("Cómo Llegar", "Debe capturar el cómo llegar, descripción , la imagen, el título del pdf, el pdf y el idioma que desea guardar", '750px', '450px');
		}
	});

function subeFoto(idC){
	
	var file_data = $("#archivoImagen").prop("files")[0];
	var form_data = new FormData();

	form_data.append("file", file_data);
	$.ajax({
		data: form_data,
    url:   'ajax/ajax_sube_foto.php?num=' + idC + "&tipo=gruposexcursiones",
    type:  'post',
		cache: false,
		async: false,
		contentType: false,
		processData: false,
		success:  function (response) {
			//alert(response);
			$("#imgPreview").attr('src','../modulos/img/gruposexcursiones/gruposexcursiones-' + idC + ".jpg?' + Math.random()");
		}
	});
}
function subePdf(idC){
	//alert(idC);
	/*debugger;*/
	
	var file_data = $("#archivo-anexo").prop("files")[0];
		//alert(file_data);
	if(file_data!=undefined)
	{
	var form_data = new FormData();
	
	form_data.append("file", file_data);
	//alert(form_data);
	$.ajax({
	data: form_data,
    url:   'ajax/ajax_sube_pdf.php?num=' + idC + "&tipo=gruposexcursiones",
    type:  'post',
		cache: false,
		async: false,
		contentType: false,
		processData: false,
		success:  function (response) {
			//alert(response);
			//$("#pdfPreview").attr('src','../modulos/img/cabeceras/cabeceras' + idC + ".jpg");
			$("#pdfPreviewArchivo").attr('href','../modulos/img/gruposexcursiones/gruposexcursiones-' + idC + ".pdf");
			//$("#lblreglas").html($('programasreglasoperacion-' + idC + '.pdf').html());
			$("#lblpdfarchivo").text('gruposexcursiones-' + idC + '.pdf');
			//$("#pdfPreviewArchivo").attr('src','../modulos/img/cabeceras/cabeceras' + idC + ".jpg");
			
		}
	});
	}else
	{
		$('#archivo-anexo').attr({ value: '' });
	}
	
}	


	function mostrarDetalle(i) {
		$("#lbl-archivo-anexo").html("<i class='fa fa-file-text' style='padding-top:0px'></i>&nbsp;Seleccione el archivo");
		$('#imgPreview').attr('src', ' ');
		$("#archivoImagen").val("");
		$("#espera2").hide();
		$( "#tabs" ).tabs( "option", "active", 0 );
		if (i != "-1"){
			$("#lblid").html($('#lblid' + i).html());
			$("#colborrarelemento").show();
			$("#txttitulo").val($('#lbltitulo' + i).html());
			$("#txtdescripcion").val($('#lbldescripcion' + i).html());
			$("#txttitulopdf").val($('#lbltitulopdf' + i).html());
			$("#txtorden").val($('#lblorden' + i).html());
			$("#cmbidiomas").val($('#lblididioma' + i).html());
			$("#cmbestatus").val($("#lblestatus" + i).html())
			//alert(id);
			$('#archivoPdf').val("");
			$('#archivoImagen').val("");
			/*if ($("#lblimagen" + i).html() != ""){*/
				$("#imgPreview").attr('src','../modulos/img/gruposexcursiones/gruposexcursiones-' + $("#lblid" + i ).html()+ ".jpg?" + Math.random());
		/*	}*/
			$("#pdfPreviewArchivo").show();
				$("#pdfPreviewArchivo").attr('href','../modulos/img/gruposexcursiones/gruposexcursiones-' + i + ".pdf?" + Math.random());
		}else{
			$("#lblid").html("0");
			$("#colborrarelemento").hide();
			$("#txttitulo").val("");
			$("#txtdescripcion").val("");
			$("#txttitulopdf").val("");
			$("#txtorden").val(0);
			$("#cmbidiomas").val(1);
			$("#cmbestatus").val(1);
			$("#lblpdfarchivo").html('');
			$("#pdfPreviewArchivo").attr("href", "#");
			
			$("#anexoanterior").val("");
			$("#pdfPreviewArchivo").hide();
		}
		abrirmodalc1('750px', '450px');

		return false;
	}

	function readURL(input) {
		var imgPath = $("#archivoImagen").val();
		var extn = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
		if (extn == "gif" || extn == "png" || extn == "jpg" || extn == "jpeg") {
    	if (input.files && input.files[0]) {
     		var reader = new FileReader();
	      reader.onload = function (e) {
			  
	      	$('#imgPreview').attr('src', e.target.result);
					
	      }
	      reader.readAsDataURL(input.files[0]);
	   	}
		}else{
			abrirmodalavisos("Archivo seleccionado", "Debe seleccionar un archivo de imagen válido.<br>(Extensión JPG, GIF, PNG)", '750px', '450px');
		}
	}

	$("#imgPreview").click(function() {
    $("input[id='archivoImagen']").click();
	});
	function readURLpdf(input) {
		/*alert("readURL");
		debugger;*/
		var pdfPath = $("#archivoPdf").val();
		var extn = pdfPath.substring(pdfPath.lastIndexOf('.') + 1).toLowerCase();
		if (extn == "pdf" || extn == "PDF") {
    	if (input.files && input.files[0]) {
     		var reader = new FileReader();
	      reader.onload = function (e) {
	      	//$('#pdfPreview').attr('src', e.target.result);
			/*$("#pdfPreviewArchivo").attr('href','../modulos/img/programasreglasoperacion/programasreglasoperacion' + idC + ".pdf");*/
			//$('#pdfPreviewArchivo').attr('href', pdfPath);
			/*$("#pdfPreviewArchivo").attr('href','../modulos/img/programasreglasoperacion/programasreglasoperacion-' + idC + ".pdf");
			$("#lblreglas").html($('../modulos/img/programasreglasoperacion/programasreglasoperacion-' + idC + '.pdf' + i).html());*/
	      }
	      reader.readAsDataURL(input.files[0]);
	   	}
		}else{
			abrirmodalavisos("Archivo seleccionado", "Debe seleccionar un archivo pdf válido.<br>(Extensión PDF)", '750px', '450px');
		}
	}

	$("#archivo-anexo").change(function() {
		if ($("#archivo-anexo").val().split('\\').pop() != ""){
			$("#lbl-archivo-anexo").html("<i class='fa fa-file-text' style='padding-top:0px'></i>&nbsp;" + $("#archivo-anexo").val().split('\\').pop());
		}else{
			$("#lbl-archivo-anexo").html("<i class='fa fa-file-text' style='padding-top:0px'></i>&nbsp;Seleccione el archivo");
		}
  	}); 
	$("#borrarelemento").unbind("click").bind('click', function () {
		//alert("aqui");
		//debugger;
		$("#texto-confirmacion").html("¿Está seguro que desea borrar el grupo/excursion " + $("#txttitulo").val() +  '?');
		$( "#dialog-confirm" ).dialog({
			resizable: false,
		    height: "auto",
		    width: 400,
		    modal: true,
		    buttons: {
		        "Aceptar": function() {
		          	$( this ).dialog( "close" );
		          	var dataObject = {id_elemento: $("#lblid").text(),
					//nombreimagen: $("#nombreimagen").val(),
					catalogo: 'gruposexcursiones'
					}
					$.ajax({
						data:  dataObject,
				      //  url:   'ajax/ajax_borra_tema_sala.php',
				  		url:   'ajax/ajax_borrado_logico.php',
						type:  'post',
				      	success:  function (response) {
							//alert(response);
							if (jQuery.trim(response) == "OK"){
								abrirmodalavisos("Eliminar Grupo/Excursión","Grupo/Excursión Eliminado con éxito.", 500, 200);
								location.reload();
							}				
						}
					});
		    	},
		        "Cancelar": function() {
		          $( this ).dialog( "close" );
		          return false;
	           }
		    }
	    });		
	 });	
</script>
<?
include '../inputs.php';
?>
