<?
require 'vs.php';
require_once '../includes/funcs.php';
?><head>
<link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Raleway" />
</head>


<div class="contenedor">

<div class="row">
<div class="col11"></div>
<div class="col1">
			<!--<a title="Agregar imagen" href="#" class="mostrar-detalle" data-indice="-1">
			<div class="btn-flotante">
				<i class="fa fa-plus btn-flotante-texto"></i>
			</div>
			</a>-->
</div>
</div>

<section>
<table id="tabla-principal" class="tabla-datos">
<thead>
<tr>
<th class="coltabla0 noexcel"></th>
<th class="col-iconos texto-centrado noexcel"></th>
<th class="col-iconos texto-centrado noexcel"></th>
<th class="coltabla4 texto-derecha">Tipo Menú</th>
<th class="coltabla3 texto-derecha">Elemento</th>



</tr>
</thead>
<tbody>
<?
	$filtro = $_POST['filtro'];
	cargaDatos($filtro);
?>
</tbody>
</table>


</section>

<section>
<div id="tabladetalle" style="background-color:#fff;z-index:99001" class="modalcapa modalcapa1">
	<div id="tabs" class="titulomodal">
		<ul>
    	<li id="tab-datos"><a href="#tabs-1">Datos del Elemento</a></li>
         <li id="tab-traduccion"><a href="#tabs-2">Agregar Traducción</a></li>
        <li id="tab-traducciones"><a href="#tabs-3">Traducciones</a></li>
  	</ul>
		<div id="tabs-1">
			<table id="detalle" class="tabla-datos">
				<tbody class="contenidomodal">
                <tr>
						<td class="col3 texto-derecha">Tipo Menú:
				      </td>
						<td class="col5 texto-izquierda"><label id="lbltipomenu"></label></td>
					</tr>
                	<tr>
						<td class="col3 texto-derecha">Padre:
				      </td>
						<td class="col5 texto-izquierda"><label id="lblelementopadre"></label></td>
					</tr>
					
               	  <tr>
						<td class="col3 texto-derecha">Elemento:
			        <input id="lblid" type="hidden" /><input  id="txtID" type="hidden" /></td>
						<td class="col5 texto-izquierda"><label id="lblelemento"></label></td>
					</tr>
                  
					<tr id='fila-estado'>
						<td class="col3 texto-derecha">Estado:</td>
						<td class="col2 texto-izquierda"><? agregacomboestatus(); ?></td>
					</tr>
				</tbody>
			</table>
		</div>
		
        
      <div id="tabs-2">
			<table id="detalle" class="tabla-datos">
				<tbody class="contenidomodal">
                <tr id="fila-idioma">
                   	   <td class="col3 texto-derecha">*Idioma: </td>
                   	   <td class="col4 texto-izquierda"><div id="comboidiomas"><? agregacombo('idiomas','2');?></div></td>
    				 </tr>
					<tr>
						<td class="col3 texto-derecha">*País Traducción Menú:						  </td>
						<td class="col9 texto-izquierda"><input class="control" id="txttraduccion" required></td>
					</tr>
                    <tr>
						<td class="col3 texto-derecha">*Url Menú:						  </td>
						<td class="col9 texto-izquierda"><input class="control" id="txturl" required></td>
					</tr>
                  <!-- <tr id='fila-estado'>
						<td class="col3 texto-derecha">Estado:</td>
						<td class="col2 texto-izquierda"><? agregacomboestatus(); ?></td>
					</tr>-->
					<tr>
						<td class="col3 texto-derecha"></td>
						<td class="col9 texto-izquierda">
							<div class="row">
								
								<div class="col4"><br>
									<input type="button" id="agregar-traduccion" name="enviar" title="Agregar traducción" class="btn-formulario" value="Agregar traducción">
								</div>
							</div>
						</td>
					</tr>
				
					
				 
				</tbody>
			</table>

		</div>
        
        
        <div id="tabs-3" class='alto-fijo-modal'>
			<div class="row">
				<div class="col2 texto-derecha">Idioma:</div>
				<div class="col9"><div id="div-combo-traducciones"> </div></div>
			</div>
			<div class="row" id="fila-datos-traducciones">
				<div class="col12" >
					<div id="datos-traducciones-menus" class='alto-fijo-modal'>

					</div>
				</div>
				
			</div>
		</div>
        
        <!-- terminan divs-->
		<div style="margin-top:15px; padding-bottom:10px; width:100%; padding-left:550px;" id="espera"></div>
		<div class="row" id="areabotones" style="padding:10px;">
		<div class="col8"></div>
		<div class="col2 texto-centrado">
			<input type="button" id="guardar" name="enviar" title="Guardar" class="btn-formulario" value="Guardar">
			<img id="espera2" src='img/loading.gif' class="icono-espera">
		</div>
		<div class="col2">
			<input type="button" id="cerrar" name="cerrar" title="Cerrar" class="btn-formulario modalcerrar" value="Cerrar">
		</div>
	</div>
	</div>

</div>
</section>

<?
function cargaDatos($filtro){
	$strSQL = "CALL paCatalogoMenuPrincipal('%".$filtro."%')";
	$resultado = consulta($strSQL);
	while ($row = $resultado->fetch_array(MYSQLI_ASSOC)){
		$indice = $row["id_menuprincipal"];
		echo "<tr id=".$indice.">";
		echo "<td style='display:none' class='noexcel'><label id='lblestatus".$indice."'>".$row["estatus"]."</label><label id='lblid".$indice."'>".$indice."</label></td>";
		echo "<td class='texto-centrado noexcel'><a title='Editar' href='#' id='editar' class='mostrar-detalle' data-indice='".$indice."'><i class='fa fa-edit icono-tablas' aria-hidden='true'></i></a></td>";
		echo "<td class='texto-centrado noexcel'><label>".semaforoestatus($row["estatus"])."</label></td>";
		echo "<td class='texto-centrado'><label id='lbltipomenu".$indice."'>".$row["tipomenu"]."</label></td>";
		echo "<td class='texto-izquierda'><label id='lblelemento".$indice."'>".$row["elemento"]."</label></td>";
		echo "</tr>";
	}
	$resultado->close();
}
?>

<script>
	$(document).ready(function() {
		$("#tabs").tabs();
		$(".modalcapa1").draggable({ cursor: "move", handle: ".titulomodal", opacity: 0.5 });
		$("#espera2").hide();
		$("#tabladetalle").hide();
		Crearlisteners();
	});

	                
	$(".mostrar-detalle").unbind("click").bind('click', function () {
		mostrarDetalle($(this).data("indice"));
  });


	function abrirmodalc1(width, height) {
		Ajustatamanocapa(".modalcapa1", width, height);
		$(".modalgeneralcapa1").fadeIn();
		$(".modalcapa1").fadeIn();
		return;
	}

	function Ajustatamanocapa(Control, width, height) {
		$(Control).css('width', width);
		$(Control).css('left', 'calc(50% - ' + width + '/2)');
		$(Control).css('top', 'calc(50% - ' + height + '/2)');

		$(Control + ' .contenidomodal').css('height', 'calc(' + height + ' - 140px)');

		return;
	}
	function cerrarmodalc1() {
		$(".modalgeneralcapa1").fadeOut();
		$('.modalcapa1').fadeOut();
	}

	function Crearlisteners(){
		$('.modalcerrar').click(function(){
			cerrarmodalc1();
			return false;
		});
	}

	$("#guardar").click(function() {
		
		/*if ($('#txtpais').val() != '' ){*/
			var dataObject = { id_menu: $("#lblid").text(),
			estatus: $("#cmbestatus").val()};
			var idOk = "0";
			$.ajax({
				data:  dataObject,
				url:   'ajax/ajax_guarda_menu.php',
        type:  'post',
				async: false,
	      beforeSend: function () {
					$("#guardar").hide();
          $("#espera2").show();
        },
        success:  function (response) {
					var id = $("#lblid").text();
					cerrarmodalc1();
					$("#espera2").hide();
					$("#guardar").show();
					var pos = response.indexOf("IDOK");
					idOk = response.substr(pos + 4);
					response = response.replace("IDOK" + idOk,"");
					if (response.substring(0,3) == "<tr" || response.substring(0,3) == "<td"){
						if (id == 0) {
							if ($('#tabla-principal > tbody > tr').length == 0){
								$('#tabla-principal > tbody ').html(response);
							}else{
								$('#tabla-principal > tbody > tr').eq(0).before(response);
							}
						}else{
							$("#"+id).html(response);
						}
						
					}else{
						abrirmodalavisos('Menú Principal',response, '800px', '450px');
					}

					$(".mostrar-detalle").unbind("click").bind('click', function () {
						mostrarDetalle($(this).data("indice"));
				  });
				}

       		});
	/*	}else{
			abrirmodalavisos("Paises", "Debe capturar el país y la foto de la imagen que desea guardar", '750px', '450px');
		}*/
	});



	function mostrarDetalle(i) {
		$("#espera2").hide();
		$( "#tabs" ).tabs( "option", "active", 0 );
		if (i != "-1"){
			$("#lblid").html($('#lblid' + i).html());
			$("#txtID").val(i);
			$("#lbltipomenu").html($('#lbltipomenu' + i).html());
			$("#lblelementopadre").html($('#lblelementopadre' + i).html());
			$("#lblelemento").html($('#lblelemento' + i).html());
			$("#txtpais").val($('#lblpais' + i).html());
			$("#cmbestatus").val($("#lblestatus" + i).html())
			//alert(id);
			$("#tab-traduccion").show();
			$("#tab-traducciones").show();
			cargaTraducciones($('#lblid' + i).html());
			
		}else{
			$("#tab-permisos").hide();
			$("#lblid").html("0");
			$("#txtpais").val("");
			$("#cmbestatus").val(1);
			$("#tab-traduccion").hide();
			$("#tab-traducciones").hide();
		}
		abrirmodalc1('750px', '450px');

		return false;
	}

	
	
	
	
	/* traducciones*/
	$("#agregar-traduccion").click(function() {
		if ($('#txttraduccion').val() != '' && $('#txturl').val() != '' && $("#cmbidiomas").val()!='' ){
			
			var idDetalle = 0;
			var datos = {id_detalle:0,
					id_menu:$("#lblid").text(),
					idioma: $("#cmbidiomas").val(),
					traduccion:$("#txttraduccion").val(),
					url:$("#txturl").val()};
			$.ajax({
				data: datos,
		    url:   'ajax/ajax_guarda_traduccion_menu.php',
		    type:  'post',
				async: false,
				success:  function (response) {
					idDetalle = response;
					
					//alert(response);
					if(response=='NO')
					{
						abrirmodalavisos("Agregar traduccion","La traducción capturada del menú por idioma ya existe en la base de datos.");
						
					}
					else{
						//alert(idDetalle);
			
					cargaTraducciones($("#lblid").text());
					
						abrirmodalavisos("Agregar traducción","Traducción subida con éxito.");
					}
					
					},error : function(jqXHR, textStatus, errorThrown){
					alert(errorThrown);
				}
				
			});
			$("#txttraduccion").val("");
			$("#txturl").val("");
			$("#cmbidiomas").val(1);
		}else{
			abrirmodalavisos("Agregar traducción","Debe capturar el idioma, la Traducción y la url.");
		}
	})
	
	function cargaTraducciones(id){
		//debugger;
		var dataObject = { catalogo: "traduccionesmenus",
		id_extra: $("#txtID").val()}
		$.ajax({
				data:  dataObject,
        url:   'ajax/ajax_carga_combo.php',
        type:  'post',
      	success:  function (response) {
					$("#div-combo-traducciones").html(response);
					$( "#cmbtraduccionesmenus" ).change(function() {
						CargaDatosTraduccion($("#cmbtraduccionesmenus").val());
					});
					if ($("#cmbtraduccionesmenus").val() != null){
						$("#fila-datos-traducciones").show();
						CargaDatosTraduccion($("#cmbtraduccionesmenus").val());
					}else{
						$("#cmbtraduccionesmenus").hide();
						$("#div-combo-traducciones").html("<strong>No existen traducciones para el menú</strong>");
						$("#fila-datos-traducciones").hide();
					}
				}
			});
	}
	function CargaDatosTraduccion(idD){
		var dataObject = { id_detalle: idD}
		$.ajax({
				data:  dataObject,
        url:   'ajax/ajax_carga_datos_traduccion_menu.php',
        type:  'post',
      	success:  function (response) {
					//alert(response);
					$("#datos-traducciones-menus").html(response);
					$("#borra-traduccion").unbind("click").bind('click', function () {
						borrarTraduccion();
					});
					$("#guardarediciontraduccion").click(function(){
						cambiarDatosTraduccion();
					})
				}
		});
	}
	function cambiarDatosTraduccion(){
		if ( $("#txttraduccionedicion").val() != '' && $("#txturledicion").val() != '' ){
		
			
			var idDetalle = $("#cmbtraduccionesmenus").val();
			var datos = {id_detalle:idDetalle,
					id_menu:0,
					idioma:0,
					traduccion:$("#txttraduccionedicion").val(),
					url:$("#txturledicion").val()};
			$.ajax({
				data: datos,
		    url:   'ajax/ajax_guarda_traduccion_menu.php',
		    type:  'post',
				async: false,
				success:  function (response) {
					idDetalle = response;
					
					cargaTraducciones(idDetalle);
				},error : function(jqXHR, textStatus, errorThrown){
					alert(errorThrown);
				}
			});

			abrirmodalavisos("Traducción","Cambios guardados con éxito.", 500, 200);
		}else{
			abrirmodalavisos("Traducción","Debe capturar la traducción del país.", 500, 200);
		}
	}
	function borrarTraduccion(){
		

		var dataObject = { id_detalle: $("#cmbtraduccionesmenus").val()}
		$.ajax({
				data:  dataObject,
        url:   'ajax/ajax_borra_traduccion_menu.php',
        type:  'post',
      	success:  function (response) {
				//alert(response);
				 $("#comboidiomas").html(response);
					cargaTraducciones();
				}
		});
	}
</script>
<?
include '../inputs.php';
?>
