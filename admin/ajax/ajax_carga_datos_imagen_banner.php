<?
require 'vs.php';
require_once '../includes/funcs.php';
  
  $idDetalle = $_POST["id_detalle"];
	$strSQL = "CALL paCargaDatosImagenBanner('".$idDetalle."')";
	$resultado = consulta($strSQL);
 	 $n = 0;
	while ($row = $resultado->fetch_array(MYSQLI_ASSOC)){
    $n = $n + 1;
    echo "<table id='detalle' class='tabla-datos'>";
    echo "<tbody class='contenidomodal'>";
    echo "<tr><td class='col3 texto-derecha'>Descripción:<input type='hidden' id='anexoanterior' value='".$row["anexo"]."'><input type='hidden' id='idimagenbanner' value='".$idDetalle."'></td>";
    echo "<td class='col5 texto-izquierda'><input class='control' id='txttituloedicion' value='" . $row['descripcion'] . "' required></td></tr>";
  
	echo "<tr id='fila-url-edicion'>
						<td class='col3 texto-derecha'>URL Destino:</td>
						<td class='col9 texto-izquierda'><input class='control' id='txturledicion' value='".$row['url']."'></td>
					</tr>";
                    echo "<tr>
            <td class='col3 texto-derecha'></td>
            <td class='col9 texto-izquierda'><input type='checkbox' id='chkanexoedicion'";
            if ($row["anexo"] == 1){
                echo "checked";
            }
    echo ">Es anexo</td>
          </tr>";
	echo "<tr id='div-fila-anexo-edicion'>
						<td class='col3 texto-derecha'>Seleccione el archivo</td>
						<td class='col9 texto-izquierda'>
							<div id='div-agregar-anexo-edicion col6'>				
								<input id='archivo-anexo-edicion' type='file' class='inputfile' accept='.PDF, .pdf, application/pdf'>
								<label for='archivo-anexo-edicion' id='lbl-archivo-anexo-edicion'>
									<i class='fa fa-file-pdf-o' style='padding-top:0px'></i>&nbsp;Seleccione el archivo	
								</label>
								<label id='div-ver-anexo-edicion'></label>
							</div>
							
						</td>
					</tr>";
	
    echo "<tr><td class='col3 texto-derecha'>Ventana destino:</td>";
    echo "<td class='col4 texto-izquierda'>";
    echo "<select id='cmbdestinoedicion' class='control-combo'>";
    if ($row['target'] == '_blank'){
      echo "<option value='_blank' selected>Nueva ventana</option>";
      echo "<option value='_self'>Misma ventana</option>";
    }else{
      echo "<option value='_blank'>Nueva ventana</option>";
      echo "<option value='_self' selected>Misma ventana</option>";
    }
    echo "</select>";
    echo "</td>";
    echo "</tr>";
	 echo "<tr><td class='col3 texto-derecha'>Orden:</td>";
    echo "<td class='col1 texto-izquierda'><input type='number' class='control' id='txtordenedicion' value='" . $row["orden"] ."'></td></tr>";
    echo "</tbody></table>";
	
	
	}
	$resultado->close();
  if ($n > 0){
?>
  <div class="row" id="areabotonesedicion">
    <div class="col8"></div>
    <div class="col2 texto-centrado">
      <input type="button" id="guardaredicion" name="enviar" title="Guardar" class="btn-formulario" value="Guardar">
    </div>
    <div class="col2">
      <input type="button" id="borra-imagen" name="borrar" title="Borrar" class="btn-formulario borra-imagen" value="Borrar imagen">
    </div>
  </div>
<? } ?>

  <script>
 	 $(document).ready(function() {
		$("#dialog-confirm").hide();
		anexoactual=$("#anexoanterior").val();
		if(anexoactual=="1")
		{
			idimagen=$("#idimagenbanner").val();
			$("#fila-url-edicion").hide();
			$("#div-fila-anexo-edicion").show();
			$("#div-ver-anexo-edicion").append('&nbsp;&nbsp;<a href="../modulos/img/banners/anexos/anexo-' + idimagen + '.pdf" class="azul" target="_blank">Ver Archivo</html>');
			
		}
		else{
			$("#fila-url-edicion").show();
			$("#archivo-anexo-edicion").val("");
			$("#div-fila-anexo-edicion").hide();
			$("#div-ver-anexo-edicion").append("");
		}
		
	});
	$("#archivo-anexo-edicion").change(function() {
		if ($("#archivo-anexo-edicion").val().split('\\').pop() != ""){
			$("#lbl-archivo-anexo-edicion").html("<i class='fa fa-file-text' style='padding-top:0px'></i>&nbsp;" + $("#archivo-anexo-edicion").val().split('\\').pop());
		}else{
			$("#lbl-archivo-anexo-edicion").html("<i class='fa fa-file-text' style='padding-top:0px'></i>&nbsp;Seleccione el archivo");
		}
  	});  
   
	$("#chkanexoedicion").change(function(){
		
		if ($("#chkanexoedicion:checked").val() == "on"){
			$("#fila-url-edicion").hide();
			$("#div-fila-anexo-edicion").show();
		}else{
			$("#fila-url-edicion").show();
			$("#archivo-anexo-edicion").val("");
			$("#div-fila-anexo-edicion").hide();
		}
	});
	</script>