<?
require 'vs.php';
require_once '../includes/funcs.php';
?><head>
<link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Raleway" />
</head>


<div class="contenedor">

<div class="row">
<div class="col11"></div>
<div class="col1">
			<a title="Agregar Sección" href="#" class="mostrar-detalle" data-indice="-1">
			<div class="btn-flotante">
				<i class="fa fa-plus btn-flotante-texto"></i>
			</div>
			</a>
</div>
</div>

<section>
<table id="tabla-principal" class="tabla-datos">
<thead>
<tr>
<th class="coltabla0 noexcel"></th>
<th class="col-iconos texto-centrado noexcel"></th>
<th class="col-iconos texto-centrado noexcel"></th>
<th class="coltabla4 texto-derecha">Sección</th>
<th class="coltabla3 texto-derecha">Traducción</th>
<th class="coltabla1 texto-derecha">Idioma</th>
</tr>
</thead>
<tbody>
<?
	$filtro = $_POST['filtro'];
	cargaDatos($filtro);
?>
</tbody>
</table>


</section>

<section>
<div id="tabladetalle" style="background-color:#fff;z-index:99001" class="modalcapa modalcapa1">
	<div id="tabs" class="titulomodal">
		<ul>
    	<li id="tab-datos"><a href="#tabs-1">Datos de la Sección</a></li>
  		<li id="tab-foto"><a href="#tabs-2">Imagen Fondo</a></li>
        <li id="tab-icono"><a href="#tabs-3">Ícono</a></li>
  	</ul>
		<div id="tabs-1">
			<table id="detalle" class="tabla-datos">
				<tbody class="contenidomodal">
					<tr id="fila-idioma">
                   	   <td class="col3 texto-derecha">Idioma: </td>
                   	   <td class="col9 texto-izquierda"><? agregacombo('idiomas','2');?></td>
    				 </tr>
                     <tr >
                   	   <td class="col3 texto-derecha">Seccion: </td>
                   	   <td class="col9 texto-izquierda"><? agregacombo('numeroseccion','2');?></td>
    				 </tr>
					  <tr>
					   <td class="col3 texto-derecha">*Título
				         <input id="lblid" type="hidden" /></td>
					   <td class="col9 texto-izquierda"><input class="control" id="txtsecciontraduccion" required></td>
					  
			      </tr>
                  <tr>
						<td class="col3 texto-derecha">*Descripción:</td>
						<td class="col9 texto-izquierda">
						  <textarea name="txtdescripcion" class="control-area" id="txtdescripcion"></textarea>
						</td>
					</tr>
                  <tr>
						<td class="col3 texto-derecha">*Subtítulo:</td>
						<td class="col9 texto-izquierda">
						  <textarea name="txtsubtitulo" class="control-area" id="txtsubtitulo"></textarea>
					</td>
					</tr>
					<tr id='fila-estado'>
						<td class="col3 texto-derecha">Estado:</td>
						<td class="col2 texto-izquierda"><? agregacomboestatus(); ?></td>
					</tr>
				</tbody>
			</table>
		</div>
		<div id="tabs-2">JPG (1920 x 646px)
			<form id="form1" runat="server">
	        <img id="imgPreview" width='100%'  src="#" alt="Da clic para seleccionar la imagen " />
					<br>
					<input id="archivoImagen" type='file' accept=".JPG, .jpg, image/jpeg" onchange="readURL(this);" style="display:none" />
	    </form>
		</div>
        <div id="tabs-3">SVG / PNG 
			<form id="form2" runat="server">
	        <img id="icoPreview" width='50%'  src="#" alt="Da clic para seleccionar la imagen " />
					<br>
					<input id="archivoIcono" type='file' accept=" image/svg+xml, image/png " onchange="readURLico(this);" style="display:none" />
	    </form>
		</div>
		<div style="margin-top:15px; padding-bottom:10px; width:100%; padding-left:550px;" id="espera"></div>
		<div class="row" id="areabotones" style="padding:10px;">
		<div class="col8"></div>
		<div class="col2 texto-centrado">
			<input type="button" id="guardar" name="enviar" title="Guardar" class="btn-formulario" value="Guardar">
			<img id="espera2" src='img/loading.gif' class="icono-espera">
		</div>
		<div class="col2">
			<input type="button" id="cerrar" name="cerrar" title="Cerrar" class="btn-formulario modalcerrar" value="Cerrar">
		</div>
	</div>
	</div>

</div>
</section>

<?
function cargaDatos($filtro){
	/*$rutaimagen="../modulos/img/seccionesfondos/seccionesfondos-";
	$rutaicono="../modulos/img/seccionesiconos/seccionesiconos-";*/
	$strSQL = "CALL paCatalogoSecciones('%".$filtro."%')";
	$resultado = consulta($strSQL);
	while ($row = $resultado->fetch_array(MYSQLI_ASSOC)){
		$indice = $row["id_seccion"];
		echo "<tr id=".$indice.">";
		echo "<td style='display:none' class='noexcel'><label id='lblestatus".$indice."'>".$row["estatus"]."</label><label id='lblid".$indice."'>".$indice."</label><label id='lblididioma".$indice."'>".$row["ididioma"]."</label><label id='lblidnumero".$indice."'>".$row["idnumero"]."</label><label id='lbldescripcion".$indice."'>".$row["descripcion"]."</label><label id='lblsubtitulo".$indice."'>".$row["subtitulo"]."</label><label id='lblicono".$indice."'>".$row["icono"]."</label></td>";
		echo "<td class='texto-centrado noexcel'><a title='Editar' href='#' id='editar' class='mostrar-detalle' data-indice='".$indice."'><i class='fa fa-edit icono-tablas' aria-hidden='true'></i></a></td>";
		echo "<td class='texto-centrado noexcel'><label>".semaforoestatus($row["estatus"])."</label></td>";
		echo "<td class='texto-centrado'><label id='lblseccion".$indice."'>".$row["seccion"]."</label></td>";
		echo "<td class='texto-izquierda'><label id='lblsecciontraduccion".$indice."'>".$row["secciontraduccion"]."</label></td>";
		
		
		echo "<td class='texto-izquierda'><label id='lblidioma".$indice."'>".$row["idioma"]."</label></td>";
		
		echo "</tr>";
	}
	$resultado->close();
}
?>

<script>
	$(document).ready(function() {
		$("#tabs").tabs();
		$(".modalcapa1").draggable({ cursor: "move", handle: ".titulomodal", opacity: 0.5 });
		$("#espera2").hide();
		$("#tabladetalle").hide();
		Crearlisteners();
	});

	                
	$(".mostrar-detalle").unbind("click").bind('click', function () {
		mostrarDetalle($(this).data("indice"));
  });


	function abrirmodalc1(width, height) {
		Ajustatamanocapa(".modalcapa1", width, height);
		$(".modalgeneralcapa1").fadeIn();
		$(".modalcapa1").fadeIn();
		return;
	}

	function Ajustatamanocapa(Control, width, height) {
		$(Control).css('width', width);
		$(Control).css('left', 'calc(50% - ' + width + '/2)');
		$(Control).css('top', 'calc(40% - ' + height + '/2)');

		$(Control + ' .contenidomodal').css('height', 'calc(' + height + ' - 140px)');

		return;
	}
	function cerrarmodalc1() {
		$(".modalgeneralcapa1").fadeOut();
		$('.modalcapa1').fadeOut();
	}

	function Crearlisteners(){
		$('.modalcerrar').click(function(){
			cerrarmodalc1();
			return false;
		});
	}

	$("#guardar").click(function() {
		imgsrc=0;
		icosrc=0;
		 if($("#imgPreview").attr('src')!=" ")
		 {
			 imgsrc=1;
		}
		 if($("#icoPreview").attr('src')!=" ")
		 {
			 icosrc=1;
		}
		//debugger;
		//alert();
		if ($('#txtsecciontraduccion').val() != ''  && $('#txtdescripcion').val() != '' && $('#txtsubtitulo').val() != '' && ($("#archivoImagen").val() != "" || imgsrc==1) && ($("#archivoIcono").val() != "" || icosrc==1) && $("#cmbidiomas").val()!=null){
			var dataObject = { id_seccion: $("#lblid").text(),
			secciontraduccion: $("#txtsecciontraduccion").val(),
			descripcion: $("#txtdescripcion").val(),
			subtitulo: $("#txtsubtitulo").val(),
			idioma: $("#cmbidiomas").val(),
			numero: $("#cmbnumeroseccion").val(),
			estatus: $("#cmbestatus").val()};
			var idOk = "0";
			$.ajax({
				data:  dataObject,
				url:   'ajax/ajax_guarda_seccion.php',
        type:  'post',
				async: false,
	      beforeSend: function () {
					$("#guardar").hide();
          $("#espera2").show();
        },
        success:  function (response) {
					var id = $("#lblid").text();
					cerrarmodalc1();
					$("#espera2").hide();
					$("#guardar").show();
					var pos = response.indexOf("IDOK");
					idOk = response.substr(pos + 4);
					response = response.replace("IDOK" + idOk,"");
					if (response.substring(0,3) == "<tr" || response.substring(0,3) == "<td"){
						if (id == 0) {
							if ($('#tabla-principal > tbody > tr').length == 0){
								$('#tabla-principal > tbody ').html(response);
							}else{
								$('#tabla-principal > tbody > tr').eq(0).before(response);
							}
						}else{
							$("#"+id).html(response);
						}
						if($("#archivoImagen").prop("files")[0]=="[object File]")
						{
							subeFoto(idOk);
						}
						if($("#archivoIcono").prop("files")[0]=="[object File]")
						{
							subeIcono(idOk);
						}
						$("#imgPreview").attr('src','../modulos/img/seccionesfondos/seccionesfondos-' + id + ".jpg?' + Math.random()");
						//$("#icoPreview").attr('src','../modulos/img/seccionesiconos/seccionesiconos-' + id + ".svg?' + Math.random()");
					}else{
						abrirmodalavisos('Secciones',response, '800px', '450px');
					}

					$(".mostrar-detalle").unbind("click").bind('click', function () {
						mostrarDetalle($(this).data("indice"));
				  });
				}

       		});
		}else{
			abrirmodalavisos("Secciones", "Debe capturar el título, descripción ,subtítulo, la imagen, el ícono y el idioma que desea guardar", '750px', '450px');
		}
	});

function subeFoto(idC){
	
	var file_data = $("#archivoImagen").prop("files")[0];
	var form_data = new FormData();

	form_data.append("file", file_data);
	$.ajax({
		data: form_data,
    url:   'ajax/ajax_sube_foto.php?num=' + idC + "&tipo=seccionesfondos",
    type:  'post',
		cache: false,
		async: false,
		contentType: false,
		processData: false,
		success:  function (response) {
			$("#imgPreview").attr('src','../modulos/img/seccionesfondos/seccionesfondos-' + idC + ".jpg?' + Math.random()");
		}
	});
}
function subeIcono(idC){
	
	var file_data = $("#archivoIcono").prop("files")[0];
	var form_data = new FormData();
	//debugger;
	var archivo = $("#archivoIcono").val();
   	var tipoext = (archivo.substring(archivo.lastIndexOf(".")+1)).toLowerCase(); 

	form_data.append("file", file_data);
	form_data.append("tipoext", tipoext);
	form_data.append("actualizacampo", 1);
	
	$.ajax({
		data: form_data,
		url:   'ajax/ajax_sube_foto.php?num=' + idC + "&tipo=seccionesiconos",
    	type:  'post',
		cache: false,
		async: false,
		contentType: false,
		processData: false,
		success:  function (response) {
			if (jQuery.trim(response) != "NO"){
				
				$("#lblicono" + idC ).html('seccionesiconos-'+ idC +'.'+ tipoext);
				//$("#lblicono" + idC ).html("<img src='../modulos/img/seccionesiconos/seccionesiconos-" + idC +  "."+ tipoext +"?" + Math.random() + "' width='15%' >");
			}
		}
	});
}

	function mostrarDetalle(i) {
		//debugger;
		$('#imgPreview').attr('src', ' ');
		$("#archivoImagen").val("");
		$('#icoPreview').attr('src', ' ');
		$("#archivoIcono").val("");
		$("#espera2").hide();
		$( "#tabs" ).tabs( "option", "active", 0 );
		if (i != "-1"){
			$("#lblid").html($('#lblid' + i).html());
			$("#cmbnumeroseccion").val($('#lblidnumero' + i).html());
			$("#txtsecciontraduccion").val($('#lblsecciontraduccion' + i).html());
			$("#txtdescripcion").val($('#lbldescripcion' + i).html());
			$("#txtsubtitulo").val($('#lblsubtitulo' + i).html());
			$("#cmbidiomas").val($('#lblididioma' + i).html());
			$("#cmbestatus").val($("#lblestatus" + i).html())
			//alert(id);
			/*if ($("#lblimagen" + i).html() != ""){*/
				$("#imgPreview").attr('src','../modulos/img/seccionesfondos/seccionesfondos-' + $("#lblid" + i ).html()+ ".jpg?" + Math.random());
				if ($("#lblicono" + i).html() != ""){
				//$("#imgPreview").attr('src','../modulos/img/marcas/marcas-' + $("#lblid" + i ).html()+ ".jpg?" + Math.random());
					$("#icoPreview").attr('src','../modulos/img/seccionesiconos/' + $("#lblicono" + i ).html()+ "?" + Math.random());
				}
				
				//$("#icoPreview").attr('src','../modulos/img/seccionesiconos/seccionesiconos-' + $("#lblid" + i ).html()+ ".svg?" + Math.random());
			/*}*/
		}else{
			$("#tab-permisos").hide();
			$("#lblid").html("0");
			$("#txtsecciontraduccion").val("");
			$("#txtdescripcion").val("");
			$("#txtsubtitulo").val("");
			$("#cmbidiomas").val(1);
			$("#cmbestatus").val(1);
			$("#cmbnumeroseccion").val(1);
		}
		abrirmodalc1('750px', '450px');

		return false;
	}

	function readURL(input) {
		var imgPath = $("#archivoImagen").val();
		var extn = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
		if (extn == "gif" || extn == "png" || extn == "jpg" || extn == "jpeg") {
    	if (input.files && input.files[0]) {
     		var reader = new FileReader();
	      reader.onload = function (e) {
			  
	      	$('#imgPreview').attr('src', e.target.result);
					
	      }
	      reader.readAsDataURL(input.files[0]);
	   	}
		}else{
			abrirmodalavisos("Archivo seleccionado", "Debe seleccionar un archivo de imagen válido.<br>(Extensión JPG, GIF, PNG)", '750px', '450px');
		}
	}
	function readURLico(input) {
		var imgPath = $("#archivoIcono").val();
		var extn = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
		if (extn == "svg" || extn == "png"  ) {
    	if (input.files && input.files[0]) {
     		var reader = new FileReader();
	      reader.onload = function (e) {
			  
	      	$('#icoPreview').attr('src', e.target.result);
					
	      }
	      reader.readAsDataURL(input.files[0]);
	   	}
		}else{
			abrirmodalavisos("Archivo seleccionado", "Debe seleccionar un archivo de imagen válido.<br>(Extensión SVG/PNG)", '750px', '450px');
		}
	}

	$("#imgPreview").click(function() {
    $("input[id='archivoImagen']").click();
	});
	$("#icoPreview").click(function() {
    $("input[id='archivoIcono']").click();
	});
</script>
<?
include '../inputs.php';
?>
