<?
require 'vs.php';
require_once '../includes/funcs.php';
?><head>
<link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Raleway" />
</head>


<div class="contenedor">

<div class="row">
<div class="col11"></div>
<div class="col1">
		
             <? 
			 /*Verificar si hay menor cantidad de banners que idiomas para poder agregar*/
			/*$strSQL = "CALL paCuantosPorIdioma('datosgenerales')";
			$resultado = consulta($strSQL);
			while ($row = $resultado->fetch_array(MYSQLI_ASSOC)){
				$disponibles = $row["disponibles"];
			}
			if ($disponibles==1){?>
                <a title="Agregar datos generales" href="#" class="mostrar-detalle" data-indice="-1">
               
                <div class="btn-flotante">
                    <i class="fa fa-plus btn-flotante-texto"></i>
                </div>
                </a>
             <? }*/?>
              <a title="Agregar datos generales" href="#" class="mostrar-detalle" data-indice="-1">
               
                <div class="btn-flotante">
                    <i class="fa fa-plus btn-flotante-texto"></i>
                </div>
                </a>
</div>
</div>

<section>
<table id="tabla-principal" class="tabla-datos">
<thead>
<tr>
<th class="coltabla0 noexcel"></th>
<th class="col-iconos texto-centrado noexcel"></th>
<th class="col-iconos texto-centrado noexcel"></th>
<th class="coltabla3 texto-derecha">Dirección</th>
<th class="coltabla2 texto-derecha">Teléfono</th>
<th class="coltabla3 texto-derecha">Horarios</th>
<th class="coltabla1 texto-derecha">Idioma</th>

</tr>
</thead>
<tbody>
<?
	$filtro = $_POST['filtro'];
	cargaDatos($filtro);
?>
</tbody>
</table>


</section>

<section>
<div id="tabladetalle" style="background-color:#fff;z-index:99001" class="modalcapa modalcapa1">
	<div id="tabs" class="titulomodal">
		<ul>
    	<li id="tab-datos"><a href="#tabs-1">Datos Generales</a></li>
        <li id="tab-foto"><a href="#tabs-2">Imagen Museo</a></li>
        <li id="tab-pdf"><a href="#tabs-3">PDF imagen Museo</a></li>
  		
  	</ul>
		<div id="tabs-1">
			<table id="detalle" class="tabla-datos">
				<tbody class="contenidomodal">
					<tr id="fila-idioma">
                   	   <td class="col3 texto-derecha">*Idioma: </td>
                   	   <td class="col9 texto-izquierda"><div id="combo-idiomas"><? agregacombo('idiomas','3');?></div></td>
    				 </tr>
                     <tr id="fila-idioma-texto">
                      	<td class="col3 texto-derecha">Idioma: </td>
                      	<td class="col9 texto-izquierda"><label id="lblidiomatexto"></label></td>
    				 </tr>
					  <tr>
					   <td class="col3 texto-derecha">*Dirección 
				         <input id="lblid" type="hidden" /></td>
					   <td class="col9 texto-izquierda"><textarea name="txtdireccion" class="control-area" id="txtdireccion"></textarea></td>
					  
			      </tr>
                  <tr>
						<td class="col3 texto-derecha">*Teléfono:</td>
						<td class="col7 texto-izquierda"><input class="control" id="txttelefono" required></td>
					</tr>
                       <tr>
						<td class="col3 texto-derecha">Correo Electrónico:</td>
						<td class="col7 texto-izquierda"><input class="control" id="txtcorreo" type="email" ></td>
					</tr>
                    <tr>
						<td class="col3 texto-derecha">*Longitud:</td>
						<td class="col3 texto-izquierda"><input type="number" class="control" id="txtlongitud" required></td>
                        <td class="col3 texto-derecha">*Latitud:</td>
						<td class="col3 texto-izquierda"><input type="number" class="control" id="txtlatitud" required></td>
					</tr>
                   
                  <tr>
						<td class="col3 texto-derecha">*Horarios:</td>
						<td class="col7 texto-izquierda"><input required class="control" id="txthorarios"></td>
					</tr>
					<tr id='fila-estado'>
						<td class="col3 texto-derecha">Estado:</td>
						<td class="col2 texto-izquierda"><? agregacomboestatus(); ?></td>
					</tr>
				</tbody>
			</table>
		</div>
        <div id="tabs-2">
			<form id="form1" runat="server">
            JPG / PNG (1679 x 713px)<br />
	        <img id="imgPreview" width="100%"  src="#" alt="Da clic para seleccionar la imagen " />
					<br>
					<input id="archivoImagen" type='file' accept=" image/png, image/jpeg" onchange="readURL(this);" style="display:none" />
	    </form>
		</div>
        <div id="tabs-3">
			<table id="detalle" class="tabla-datos">
				<tbody class="contenidomodal">
               
					<tr>
						<td class="col3 texto-derecha">*Título pdf:
						  <input id="lblid" type="hidden"></td>
						<td class="col5 texto-izquierda"><input class="control" id="txttitulopdf" required></td>
					</tr>
					
					<tr>
						<td class="col3 texto-derecha">*Seleccione el archivo (pdf)</td>
						<td class="col9 texto-izquierda">
							
							<div id='div-agregar-anexo'>				
								<input id="archivo-anexo" type="file" class="inputfile" accept=".PDF, .pdf, application/pdf">
								<label for="archivo-anexo" id="lbl-archivo-anexo">
									<i class="fa fa-file-pdf-o" style="padding-top:0px"></i>&nbsp;Seleccione el archivo
								</label><br /><br />
                               <a href="#" id="pdfPreviewArchivo" target="_blank" ><i class="fa fa-file-pdf-o" style="padding-top:0px"></i>&nbsp;Ver Archivo</a> 
                                <input type="hidden"  id='anexoanterior'/>
							</div>
							<div id='div-ver-anexo'></div>
						
						</td>
					</tr>
          </tbody>
          </table>
          </div>
		
		<div style="margin-top:15px; padding-bottom:10px; width:100%; padding-left:550px;" id="espera"></div>
		<div class="row" id="areabotones" style="padding:10px;">
		<div class="col8"></div>
		<div class="col2 texto-centrado">
			<input type="button" id="guardar" name="enviar" title="Guardar" class="btn-formulario" value="Guardar">
			<img id="espera2" src='img/loading.gif' class="icono-espera">
		</div>
		<div class="col2">
			<input type="button" id="cerrar" name="cerrar" title="Cerrar" class="btn-formulario modalcerrar" value="Cerrar">
		</div>
	</div>
	</div>

</div>
</section>

<?
function cargaDatos($filtro){
	$strSQL = "CALL paCatalogoDatosGenerales('%".$filtro."%')";
	$resultado = consulta($strSQL);
	while ($row = $resultado->fetch_array(MYSQLI_ASSOC)){
		$indice = $row["id_datosgenerales"];
		echo "<tr id=".$indice.">";
		echo "<td style='display:none' class='noexcel'><label id='lblestatus".$indice."'>".$row["estatus"]."</label><label id='lblid".$indice."'>".$indice."</label><label id='lblididioma".$indice."'>".$row["ididioma"]."</label><label id='lbllongitud".$indice."'>".$row["longitud"]."</label><label id='lbllatitud".$indice."'>".$row["latitud"]."</label><label id='lblcorreo".$indice."'>".$row["correo"]."</label><label id='lblididioma".$indice."'>".$row["ididioma"]."</label><label id='lbltitulopdf".$indice."'>".$row["titulopdf"]."</label><label id='lblimagen".$indice."'>".$row["imagen"]."</label></td>";
		echo "<td class='texto-centrado noexcel'><a title='Editar' href='#' id='editar' class='mostrar-detalle' data-indice='".$indice."'><i class='fa fa-edit icono-tablas' aria-hidden='true'></i></a></td>";
		echo "<td class='texto-centrado noexcel'><label>".semaforoestatus($row["estatus"])."</label></td>";
		echo "<td class='texto-centrado'><label id='lbldireccion".$indice."'>".$row["direccion"]."</label></td>";
		echo "<td class='texto-izquierda'><label id='lbltelefono".$indice."'>".$row["telefono"]."</label></td>";
		echo "<td class='texto-izquierda'><label id='lblhorarios".$indice."'>".$row["horarios"]."</label></td>";
		echo "<td class='texto-izquierda'><label id='lblidioma".$indice."'>".$row["idioma"]."</label></td>";
		
		echo "</tr>";
	}
	$resultado->close();
}
?>

<script>
	$(document).ready(function() {
		$("#tabs").tabs();
		$(".modalcapa1").draggable({ cursor: "move", handle: ".titulomodal", opacity: 0.5 });
		$("#espera2").hide();
		$("#tabladetalle").hide();
		Crearlisteners();
	});

	                
	$(".mostrar-detalle").unbind("click").bind('click', function () {
		mostrarDetalle($(this).data("indice"));
  });


	function abrirmodalc1(width, height) {
		Ajustatamanocapa(".modalcapa1", width, height);
		$(".modalgeneralcapa1").fadeIn();
		$(".modalcapa1").fadeIn();
		return;
	}

	function Ajustatamanocapa(Control, width, height) {
		$(Control).css('width', width);
		$(Control).css('left', 'calc(50% - ' + width + '/2)');
		$(Control).css('top', 'calc(50% - ' + height + '/2)');

		$(Control + ' .contenidomodal').css('height', 'calc(' + height + ' - 140px)');

		return;
	}
	function cerrarmodalc1() {
		$(".modalgeneralcapa1").fadeOut();
		$('.modalcapa1').fadeOut();
	}

	function Crearlisteners(){
		$('.modalcerrar').click(function(){
			cerrarmodalc1();
			return false;
		});
	}

	$("#guardar").click(function() {
		imgsrc=0;
		 if($("#imgPreview").attr('src')!=" ")
		 {
			 imgsrc=1;
			
		}
		pdfsrc=0;
		 if($("#pdfPreviewArchivo").attr('src')!=" ")
		 {
			 pdfsrc=1;
			
		}
		if ($('#txtdireccion').val() != ''  && $('#txttelefono').val() != '' && $('#txthorarios').val() != '' && $('#txtlongitud').val() != '' && $('#txtlatitud').val() != '' && ( $("#cmbidiomas").val()!=null || $('#lblidiomatexto').html()!="") && ($("#archivoImagen").val() != "" || imgsrc==1) && ($("#archivo-anexo").val() != "" || pdfsrc==1) && $('#txttitulopdf').val() != ''){
			var dataObject = { id_datosgenerales: $("#lblid").text(),
			direccion: $("#txtdireccion").val(),
			telefono: $("#txttelefono").val(),
			correo: $("#txtcorreo").val(),
			idioma: $("#cmbidiomas").val(),
			longitud: $("#txtlongitud").val(),
			latitud: $("#txtlatitud").val(),
			horarios: $("#txthorarios").val(),
			titulopdf: $("#txttitulopdf").val(),
			estatus: $("#cmbestatus").val()};
			var idOk = "0";
			$.ajax({
				data:  dataObject,
				url:   'ajax/ajax_guarda_datosgenerales.php',
        type:  'post',
				async: false,
	      beforeSend: function () {
					$("#guardar").hide();
          $("#espera2").show();
        },
        success:  function (response) {
	
					var id = $("#lblid").text();
					cerrarmodalc1();
					$("#espera2").hide();
					$("#guardar").show();
					
					/* verificar si hay idiomas disponibles*/
					var dis = response.indexOf("DISP");
					disponibles = response.substr(dis + 4);
					
					var pos = response.indexOf("IDOK");
					hasta=dis-pos-4;
					
					//alert (hasta);
					idOk = response.substr(pos + 4,hasta);
					//alert(idOk);
					if(disponibles==0){
						$(".btn-flotante").hide();
					}
					/* obtener el indice */
					
					response = response.replace("IDOK" + idOk,"");
					if (response.substring(0,3) == "<tr" || response.substring(0,3) == "<td"){
						if (id == 0) {
							if ($('#tabla-principal > tbody > tr').length == 0){
								$('#tabla-principal > tbody ').html(response);
							}else{
								$('#tabla-principal > tbody > tr').eq(0).before(response);
							}
						}else{
							$("#"+id).html(response);
						}
						if($("#archivoImagen").prop("files")[0]=="[object File]")
						{
						subeFoto(idOk);
						}
						if($("#archivo-anexo").prop("files")[0]=="[object File]")
						{
							subePdf(idOk);
						}
						
					}else{
						abrirmodalavisos('Datos Generales',response, '800px', '450px');
					}

					$(".mostrar-detalle").unbind("click").bind('click', function () {
						mostrarDetalle($(this).data("indice"));
				  });
				}

       		});
		}else{
			abrirmodalavisos("Datos Generales", "Debe capturar el idioma, la dirección, teléfono, longitud, latitud , horarios, imagen, pdf y título de pdf de los datos generales que desea guardar", '750px', '450px');
		}
	});
function subeFoto(idC){
	
	var file_data = $("#archivoImagen").prop("files")[0];
	var form_data = new FormData();
	//debugger;
	var archivo = $("#archivoImagen").val();
   	var tipoext = (archivo.substring(archivo.lastIndexOf(".")+1)).toLowerCase();

	form_data.append("file", file_data);
	//var tipoext="png";
	form_data.append("tipoext", tipoext);
	form_data.append("actualizacampo", 1);
	$.ajax({
		data: form_data,
    url:   'ajax/ajax_sube_foto.php?num=' + idC + "&tipo=datosgenerales",
    type:  'post',
		cache: false,
		async: false,
		contentType: false,
		processData: false,
		success:  function (response) {
			if (jQuery.trim(response) != "NO"){
				$("#lblimagen" + idC ).html('datosgenerales-'+ idC +'.'+ tipoext);
			}
		}
	});
}
function subePdf(idC){
	//alert(idC);
	/*debugger;*/
	
	var file_data = $("#archivo-anexo").prop("files")[0];
		//alert(file_data);
	if(file_data!=undefined)
	{
	var form_data = new FormData();
	
	form_data.append("file", file_data);
	//alert(form_data);
	$.ajax({
	data: form_data,
    url:   'ajax/ajax_sube_pdf.php?num=' + idC + "&tipo=datosgenerales",
    type:  'post',
		cache: false,
		async: false,
		contentType: false,
		processData: false,
		success:  function (response) {
			//alert(response);
			//$("#pdfPreview").attr('src','../modulos/img/cabeceras/cabeceras' + idC + ".jpg");
			$("#pdfPreviewArchivo").attr('href','../modulos/img/datosgenerales/datosgenerales-' + idC + ".pdf");
			//$("#lblreglas").html($('programasreglasoperacion-' + idC + '.pdf').html());
			$("#lblpdfarchivo").text('datosgenerales-' + idC + '.pdf');
			//$("#pdfPreviewArchivo").attr('src','../modulos/img/cabeceras/cabeceras' + idC + ".jpg");
			
		}
	});
	}else
	{
		$('#archivo-anexo').attr({ value: '' });
	}
	
}	


	function mostrarDetalle(i) {
		$('#imgPreview').attr('src', ' ');
		$("#archivoImagen").val("");
		$("#lbl-archivo-anexo").html("<i class='fa fa-file-text' style='padding-top:0px'></i>&nbsp;Seleccione el archivo");
		$("#fila-idioma").hide();
		$("#fila-url").show();
		$('#imgPreview').attr('src', ' ');
		$("#espera2").hide();
		$( "#tabs" ).tabs( "option", "active", 0 );
		if (i != "-1"){
			$("#lblid").html($('#lblid' + i).html());
			$("#fila-idioma-texto").show();
			$("#lblidiomatexto").html($("#lblidioma" + i).html());
			$("#txtdireccion").val($('#lbldireccion' + i).html());
			$("#txttelefono").val($('#lbltelefono' + i).html());
			$("#txtcorreo").val($('#lblcorreo' + i).html());
			$("#txtlongitud").val($('#lbllongitud' + i).html());
			$("#txtlatitud").val($('#lbllatitud' + i).html());
			$("#txthorarios").val($('#lblhorarios' + i).html());
			$("#txttitulopdf").val($('#lbltitulopdf' + i).html());
			$("#cmbidiomas").val($('#lblididioma' + i).html());
			$("#cmbestatus").val($("#lblestatus" + i).html());
			$('#archivoPdf').val("");
			$('#archivoImagen').val("");
			
			//if ($("#lblpdf" + i).html() != ""){
				//alert("si");
				//$("#lbl-archivo-anexo").html("<i class='fa fa-file-text' style='padding-top:0px'></i>&nbsp;" + $("#lblpdf"+ i).html().split('\\').pop());
				//$("#anexoanterior").val($("#lblpdf"+ i).html().split('\\').pop());
				//$("#imgPreview").attr('src','../modulos/img/datosgenerales/datosgenerales-' + $("#lblid" + i ).html()+ ".png?" + Math.random());
				$("#imgPreview").attr('src','../modulos/img/datosgenerales/' + $("#lblimagen" + i ).html()+ "?" + Math.random());
				$("#pdfPreviewArchivo").show();
				$("#pdfPreviewArchivo").attr('href','../modulos/img/datosgenerales/datosgenerales-' + i + ".pdf?" + Math.random());
				
			//}
			
			
		}else{
			$("#tab-permisos").hide();
			$("#lblid").html("0");
			$("#fila-idioma").show();
			$("#lblidiomatexto").html("");
			$("#fila-idioma-texto").hide();
			$("#txtdireccion").val("");
			$("#txttelefono").val("");
			$("#txtcorreo").val("");
			$("#txtlongitud").val("");
			$("#txtlatitud").val("");
			$("#txthorarios").val("");
			$("#txttitulopdf").val("");
			$("#cmbestatus").val(1);
			
			$("#lblpdfarchivo").html('');
			$("#pdfPreviewArchivo").attr("href", "#");
			
			$("#anexoanterior").val("");
			$("#pdfPreviewArchivo").hide();
		}
		abrirmodalc1('750px', '450px');

		return false;
	}
	
	function readURL(input) {
		var imgPath = $("#archivoImagen").val();
		var extn = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
		//if (extn == "gif" || extn == "png" || extn == "jpg" || extn == "jpeg") {
		if ( extn == "png" || extn == "jpg" ) {
    	if (input.files && input.files[0]) {
     		var reader = new FileReader();
	      reader.onload = function (e) {
			  
	      	$('#imgPreview').attr('src', e.target.result);
					
	      }
	      reader.readAsDataURL(input.files[0]);
	   	}
		}else{
			abrirmodalavisos("Archivo seleccionado", "Debe seleccionar un archivo de imagen válido.<br>(Extensión JPG,  PNG)", '750px', '450px');
		}
	}

		$("#imgPreview").click(function() {
    $("input[id='archivoImagen']").click();
	});
	function readURLpdf(input) {
		/*alert("readURL");
		debugger;*/
		var pdfPath = $("#archivoPdf").val();
		var extn = pdfPath.substring(pdfPath.lastIndexOf('.') + 1).toLowerCase();
		if (extn == "pdf" || extn == "PDF") {
    	if (input.files && input.files[0]) {
     		var reader = new FileReader();
	      reader.onload = function (e) {
	      	//$('#pdfPreview').attr('src', e.target.result);
			/*$("#pdfPreviewArchivo").attr('href','../modulos/img/programasreglasoperacion/programasreglasoperacion' + idC + ".pdf");*/
			//$('#pdfPreviewArchivo').attr('href', pdfPath);
			/*$("#pdfPreviewArchivo").attr('href','../modulos/img/programasreglasoperacion/programasreglasoperacion-' + idC + ".pdf");
			$("#lblreglas").html($('../modulos/img/programasreglasoperacion/programasreglasoperacion-' + idC + '.pdf' + i).html());*/
	      }
	      reader.readAsDataURL(input.files[0]);
	   	}
		}else{
			abrirmodalavisos("Archivo seleccionado", "Debe seleccionar un archivo pdf válido.<br>(Extensión PDF)", '750px', '450px');
		}
	}

	$("#archivo-anexo").change(function() {
		if ($("#archivo-anexo").val().split('\\').pop() != ""){
			$("#lbl-archivo-anexo").html("<i class='fa fa-file-text' style='padding-top:0px'></i>&nbsp;" + $("#archivo-anexo").val().split('\\').pop());
		}else{
			$("#lbl-archivo-anexo").html("<i class='fa fa-file-text' style='padding-top:0px'></i>&nbsp;Seleccione el archivo");
		}
  	}); 

	
</script>
<?
include '../inputs.php';
?>
