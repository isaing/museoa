<?
require 'vs.php';
require_once '../includes/funcs.php';
  
  $idDetalle = $_POST["id_detalle"];
	$strSQL = "CALL paCargaDatosTraduccionEvento('".$idDetalle."')";

	$resultado = consulta($strSQL);
 	 $n = 0;
	while ($row = $resultado->fetch_array(MYSQLI_ASSOC)){
    $n = $n + 1;
	
    echo "<table id='detalle' class='tabla-datos'>";
    echo "<tbody class='contenidomodal'>";
	 echo "<tr><td class='col3 texto-derecha'>*Idioma:<input type='hidden' id='iddetalleedicion' value='".$idDetalle."'></td>";
    echo "<td class='col3 texto-izquierda'>".$row['idioma']."</td></tr>";
	
   
				echo "<tr>
						<td class='col3 texto-derecha'>* Título:<input type='hidden' id='anexoanterior' value='".$row["anexo"]."'>
						  </td>
						<td class='col9 texto-izquierda'><input class='control' id='txttituloeventotraduccionedicion'  value='" . $row['titulo'] . "'></td>
                        
					</tr>
                    
					<tr>
						<td class='col3 texto-derecha'>*Lugar:</td>
						<td class='col9 texto-izquierda'><input class='control' id='txtlugartraduccionedicion' value='" . $row['lugar'] . "'></td>
					</tr>
					<tr>
						<td class='col3 texto-derecha'>Costo:</td>
						<td class='col4 texto-izquierda'><input class='control' id='txtcostotraduccionedicion' value='" . $row['costo'] . "'></td>
					</tr>
                    <tr>
					   <td class='col3 texto-derecha'>*Descripción</td>
					   <td class='col9 texto-izquierda'><textarea id='txtdescripciontraduccionedicion' class='control-area'>" . $row['descripcion'] . "</textarea></td>
					  
			      </tr>";
					
                
                   
                    echo "<tr>
            <td class='col3 texto-derecha'></td>
            <td class='col9 texto-izquierda'><input type='checkbox' id='chkanexoedicion'";
            if ($row["anexo"] == 1){
                echo "checked";
            }
    echo ">Es anexo</td>
          </tr>";
	echo "<tr id='div-fila-anexo-edicion'>
						<td class='col3 texto-derecha'>Seleccione el archivo</td>
						<td class='col9 texto-izquierda'>
							<div id='div-agregar-anexo-edicion col6'>				
								<input id='archivo-anexo-edicion' type='file' class='inputfile' accept='.PDF, .pdf, application/pdf'>
								<label for='archivo-anexo-edicion' id='lbl-archivo-anexo-edicion'>
									<i class='fa fa-file-pdf-o' style='padding-top:0px'></i>&nbsp;Seleccione el archivo	
								</label>
								<label id='div-ver-anexo-edicion'></label>
								
							</div>
							
						</td>
					</tr>";
   

	
    echo "</tbody></table>";
	
	
	}
	$resultado->close();
  if ($n > 0){
?>
  <div class="row" id="areabotonesedicion">
    <div class="col4"></div>
    <div class="col4 texto-centrado">
      <input type="button" id="guardarediciontraduccion" name="enviar" title="Guardar" class="btn-formulario" value="Guardar Traducción">
    </div>
    <div class="col4">
      <input type="button" id="borra-traduccion" name="borrar" title="Borrar" class="btn-formulario" value="Borrar traduccion">
    </div>
  </div>
<? } ?>

 <script>
 	 $(document).ready(function() {
		anexoactual=$("#anexoanterior").val();
		if(anexoactual=="1")
		{
			iddetalle=$("#iddetalleedicion").val();
			$("#fila-url-edicion").hide();
			$("#div-fila-anexo-edicion").show();
			$("#div-ver-anexo-edicion").append('&nbsp;&nbsp;<a href="../modulos/img/eventos/anexos/anexos-' + iddetalle + '.pdf" class="azul" target="_blank">Ver Archivo</a>');
			//$("#div-ver-anexo-edicion").append("sddsdsdsdsdsdsds");
			
		}
		else{
			$("#fila-url-edicion").show();
			$("#archivo-anexo-edicion").val("");
			$("#div-fila-anexo-edicion").hide();
			$("#div-ver-anexo-edicion").append("");
		}
		
	});
	$("#archivo-anexo-edicion").change(function() {
		if ($("#archivo-anexo-edicion").val().split('\\').pop() != ""){
			$("#lbl-archivo-anexo-edicion").html("<i class='fa fa-file-text' style='padding-top:0px'></i>&nbsp;" + $("#archivo-anexo-edicion").val().split('\\').pop());
		}else{
			$("#lbl-archivo-anexo-edicion").html("<i class='fa fa-file-text' style='padding-top:0px'></i>&nbsp;Seleccione el archivo");
		}
  	});  
   
	$("#chkanexoedicion").change(function(){
		
		if ($("#chkanexoedicion:checked").val() == "on"){
			$("#fila-url-edicion").hide();
			$("#div-fila-anexo-edicion").show();
		}else{
			$("#fila-url-edicion").show();
			$("#archivo-anexo-edicion").val("");
			$("#div-fila-anexo-edicion").hide();
		}
	});
	</script>