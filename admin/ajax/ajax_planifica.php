<?
require 'vs.php';
require_once '../includes/funcs.php';
?>
<div class="contenedor">

<div class="row">
<div class="col11"></div>
<div class="col1">
			<a title="Agregar Planifica" href="#" class="mostrar-detalle" data-indice="-1">
			<div class="btn-flotante">
				<i class="fa fa-plus btn-flotante-texto"></i>
			</div>
			</a>
</div>
</div>

<section>
<table id="tabla-principal" class="tabla-datos">
<thead>
<tr>
<th class="coltabla0 noexcel"></th>
<th class="col-iconos texto-centrado noexcel"></th>
<th class="col-iconos texto-centrado noexcel"></th>
<th class="coltabla4 texto-derecha">Pregunta</th>
<th class="coltabla5 texto-derecha">Descripción</th>
<th class="coltabla1 texto-derecha">Orden</th>
<th class="coltabla2 texto-derecha">Idioma</th>

</tr>
</thead>
<tbody>
<?
	$filtro = $_POST['filtro'];
	cargaDatos($filtro);
?>
</tbody>
</table>


</section>

<section>
<div id="tabladetalle" style="background-color:#fff;z-index:99001" class="modalcapa modalcapa1">
	<div id="tabs">
		<ul>
    	<li id="tab-datos"><a href="#tabs-1">Datos Planifica tu visita</a></li>
    	<li id="tab-foto"><a href="#tabs-2">Imagen</a></li>
        <li id="tab-icono"><a href="#tabs-3">Ícono</a></li>
			
  	</ul>
		<div id="tabs-1">
			<table id="detalle" class="tabla-datos">
				<tbody class="contenidomodal">
					<tr id="fila-idioma">
                   	   <td class="col3 texto-derecha">*Idioma: </td>
                   	   <td class="col9 texto-izquierda"><? agregacombo('idiomas','2');?></td>
    				 </tr>
					<tr>
						<td class="col3 texto-derecha">*Pregunta:
						  <input id="lblid" type="hidden"></td>
						<td class="col9 texto-izquierda"><input class="control" id="txtpregunta"></td>
						
					</tr>
                      <tr>
					   <td class="col3 texto-derecha">*Descripción
				       </td>
					   <td class="col9 texto-izquierda"><textarea id="txtdescripcion" class="control-area"></textarea></td>
					  
			      </tr>
                    <tr>
						<td class="col3 texto-derecha">Orden:</td>
						<td class="col2 texto-izquierda"><input class="control" id="txtorden" maxlength="3"></td>
						
					</tr>
					<tr id='fila-estado'>
						<td class="col3 texto-derecha">Estado:</td>
						<td class="col2 texto-izquierda"><? agregacomboestatus(); ?></td>
					</tr>
				</tbody>
			</table>
		</div>
        <div id="tabs-2">SVG/PNG
			<form id="form1" runat="server">
	        <img id="imgPreview" width='30%'  src="#" alt="Da clic para seleccionar la imagen " />
					<br>
					<input id="archivoImagen" type='file' accept="image/svg+xml, image/png" onchange="readURL(this);" style="display:none" />
	    </form>
		</div>
        <div id="tabs-3">SVG/PNG
			<form id="form2" runat="server">
	        <img id="icoPreview" width='30%'  src="#" alt="Da clic para seleccionar la imagen " />
					<br>
					<input id="archivoIcono" type='file' accept=" image/svg+xml, image/png" onchange="readURLico(this);" style="display:none" />
	    </form>
		</div>
    <div style="margin-top:15px; padding-bottom:10px; width:100%; padding-left:550px;" id="espera"></div>
		<div class="row" id="areabotones" style="padding:10px;">
		<div class="col6"></div>
		<div class="col2 texto-centrado">
			<input type="button" id="guardar" name="enviar" title="Guardar" class="btn-formulario" value="Guardar">
			<img id="espera2" src='img/loading.gif' class="icono-espera">
		</div>
        <div class="col2"  id="colborrarelemento">
      <input type="button" id="borrarelemento" name="borrar" title="Borrar" class="btn-formulario borra-imagen" value="Borrar">
    </div>
		<div class="col2">
			<input type="button" id="cerrar" name="cerrar" title="Cerrar" class="btn-formulario modalcerrar" value="Cerrar">
		</div>
	</div>

	</div>
    <div id="dialog-confirm" title="Borrar elemento">
      <p><span class="ui-icon ui-icon-alert" style="float:left; margin:12px 12px 20px 0;"></span><label id='texto-confirmacion'></label>
      </p>
    </div>
</div>
</section>

<?
function cargaDatos($filtro){
	$strSQL = "CALL paCatalogoPlanifica('%".$filtro."%')";
	//echo $strSQL;
	$resultado = consulta($strSQL);
	while ($row = $resultado->fetch_array(MYSQLI_ASSOC)){
		$indice = $row["id_planifica"];
		echo "<tr id=".$indice.">";
		echo "<td style='display:none' class='noexcel'>
		<label id='lblididioma".$indice."'>".$row["ididioma"]."</label>
		<label id='lblestatus".$indice."'>".$row["estatus"]."</label><label id='lblid".$indice."'>".$indice."</label><label id='lblimagen".$indice."'>".$row["imagen"]."</label><label id='lblicono".$indice."'>".$row["icono"]."</label></td>";
		echo "<td class='texto-centrado noexcel'><a title='Editar' href='#' id='editar' class='mostrar-detalle' data-indice='".$indice."'><i class='fa fa-edit icono-tablas' aria-hidden='true'></i></a></td>";
		echo "<td class='texto-centrado noexcel'><label>".semaforoestatus($row["estatus"])."</label></td>";
		echo "<td class='texto-izquierda'><label id='lblpregunta".$indice."'>".$row["pregunta"]."</label></td>";
		echo "<td class='texto-izquierda'><label id='lbldescripcion".$indice."'>".$row["descripcion"]."</label></td>";
		echo "<td class='texto-izquierda'><label id='lblorden".$indice."'>".$row["orden"]."</label></td>";
		echo "<td class='texto-izquierda'><label id='lblidioma".$indice."'>".$row["idioma"]."</label></td>";
		
		
		
		echo "</tr>";
	}
	$resultado->close();
}
?>

<script>
	$(document).ready(function() {
		$("#dialog-confirm").hide();
		$("#tabs").tabs();
		$(".modalcapa1").draggable({ cursor: "move", handle: ".titulomodal", opacity: 0.5 });
		$("#espera2").hide();
		$("#tabladetalle").hide();
		Crearlisteners();
		
	});

	                
	$(".mostrar-detalle").unbind("click").bind('click', function () {
		mostrarDetalle($(this).data("indice"));
		
  });


	function abrirmodalc1(width, height) {
		Ajustatamanocapa(".modalcapa1", width, height);
		$(".modalgeneralcapa1").fadeIn();
		$(".modalcapa1").fadeIn();
		return;
	}

	function Ajustatamanocapa(Control, width, height) {
		$(Control).css('width', width);
		$(Control).css('left', 'calc(50% - ' + width + '/2)');
		$(Control).css('top', 'calc(50% - ' + height + '/2)');

		$(Control + ' .contenidomodal').css('height', 'calc(' + height + ' - 140px)');

		return;
	}
	function cerrarmodalc1() {
		$(".modalgeneralcapa1").fadeOut();
		$('.modalcapa1').fadeOut();
	}

	function Crearlisteners(){
		$('.modalcerrar').click(function(){
			
			cerrarmodalc1();
			return false;
		});
	}

	$("#guardar").click(function() {
		imgsrc=0;
		icosrc=0;
		 if($("#imgPreview").attr('src')!=" ")
		 {
			 imgsrc=1;
		}
		 if($("#icoPreview").attr('src')!=" ")
		 {
			 icosrc=1;
		}
			
		if ($('#txtpregunta').val() != '' && $("#cmbidiomas").val()!=null && $('#txtdescripcion').val() != ''
		&& ($("#archivoImagen").val() != "" || imgsrc==1) && ($("#archivoIcono").val() != "" || icosrc==1)){
			var ban = 0;
			var dataObject = { id_planifica: $("#lblid").text(),
			pregunta: $("#txtpregunta").val(),
			descripcion: $("#txtdescripcion").val(),
			idioma: $("#cmbidiomas").val(),
			orden: $("#txtorden").val(),
			estatus: $("#cmbestatus").val()};
			var idOk = "0";
			$.ajax({
				data:  dataObject,
				url:   'ajax/ajax_guarda_planifica.php',
        		type:  'post',
				async: false,
	      beforeSend: function () {
					$("#guardar").hide();
          $("#espera2").show();
        },
        success:  function (response) {
					var id = $("#lblid").text();
					
					cerrarmodalc1();
					$("#espera2").hide();
					$("#guardar").show();
					var pos = response.indexOf("IDOK");
					idOk = response.substr(pos + 4);
					response = response.replace("IDOK" + idOk,"");
					if (response.substring(0,3) == "<tr" || response.substring(0,3) == "<td"){
						if (id == 0) {
							if ($('#tabla-principal > tbody > tr').length == 0){
								$('#tabla-principal > tbody ').html(response);
							}else{
								$('#tabla-principal > tbody > tr').eq(0).before(response);
							}
						}else{
							$("#"+id).html(response);
						}
						if($("#archivoImagen").prop("files")[0]=="[object File]")
						{
							subeFoto(idOk);
						}
						if($("#archivoIcono").prop("files")[0]=="[object File]")
						{
							subeIcono(idOk);
						}
						/*$("#imgPreview").attr('src','../modulos/img/planificaimagenes/planificaimagenes-' + id + ".svg?' + Math.random()");
						$("#icoPreview").attr('src','../modulos/img/planificaiconos/planificaiconos-' + id + ".svg?' + Math.random()");*/
						
					}else{
						abrirmodalavisos('Cargos',response, '800px', '450px');
					}

					$(".mostrar-detalle").unbind("click").bind('click', function () {
						mostrarDetalle($(this).data("indice"));
				  });
				}

       		});
		}else{
			abrirmodalavisos("Planifica tu visita", "Debe capturar el idioma, la pregunta, la descripción, la imagen y el ícono  que desea guardar", '750px', '450px');
		}
	});
function subeFoto(idC){
	
	var file_data = $("#archivoImagen").prop("files")[0];
	var form_data = new FormData();
	//var tipoext="svg";
	var archivo = $("#archivoImagen").val();
   	var tipoext = (archivo.substring(archivo.lastIndexOf(".")+1)).toLowerCase(); 
	
	form_data.append("tipoext", tipoext);
	form_data.append("file", file_data);
	form_data.append("actualizacampo", 1);
	$.ajax({
		data: form_data,
    url:   'ajax/ajax_sube_foto.php?num=' + idC + "&tipo=planificaimagenes",
    type:  'post',
		cache: false,
		async: false,
		contentType: false,
		processData: false,
		success:  function (response) {
			//$("#imgPreview").attr('src','../modulos/img/planificaimagenes/planificaimagenes-' + idC + ".svg?' + Math.random()");
			if (jQuery.trim(response) != "NO"){
				
				$("#lblimagen" + idC ).html('planificaimagenes-'+ idC +'.'+ tipoext);
				
				
			}
		}
	});
}
function subeIcono(idC){
	
	var file_data = $("#archivoIcono").prop("files")[0];
	var form_data = new FormData();
	
	//var tipoext="svg";
	var archivo = $("#archivoIcono").val();
   	var tipoext = (archivo.substring(archivo.lastIndexOf(".")+1)).toLowerCase(); 
	
	form_data.append("file", file_data);
	form_data.append("tipoext", tipoext);
	form_data.append("actualizacampo", 1);
	$.ajax({
		data: form_data,
		url:   'ajax/ajax_sube_foto.php?num=' + idC + "&tipo=planificaiconos",
    	type:  'post',
		cache: false,
		async: false,
		contentType: false,
		processData: false,
		success:  function (response) {
			//alert(response);
			//$("#icoPreview").attr('src','../modulos/img/planificaiconos/planificaiconos-' + idC + ".svg?' + Math.random()");
			if (jQuery.trim(response) != "NO"){
				
				$("#lblicono" + idC ).html('planificaiconos-'+ idC +'.'+ tipoext);
				
				
			}
		}
	});
}


	function mostrarDetalle(i) {
		$('#imgPreview').attr('src', ' ');
		$("#archivoImagen").val("");
		$('#icoPreview').attr('src', ' ');
		$("#archivoIcono").val("");
		$("#espera2").hide();
		$( "#tabs" ).tabs( "option", "active", 0 );
		if (i != "-1"){
			$("#lblid").html($('#lblid' + i).html());
			$("#colborrarelemento").show();
			$("#txtpregunta").val($('#lblpregunta' + i).html());
			$("#txtdescripcion").val($('#lbldescripcion' + i).html());
			$("#cmbidiomas").val($('#lblididioma' + i).html());
			$("#txtorden").val($('#lblorden' + i).html());
			$("#cmbestatus").val($("#lblestatus" + i).html());
			/*$("#imgPreview").attr('src','../modulos/img/planificaimagenes/planificaimagenes-' + $("#lblid" + i ).html()+ ".svg?" + Math.random());
			$("#icoPreview").attr('src','../modulos/img/planificaiconos/planificaiconos-' + $("#lblid" + i ).html()+ ".svg?" + Math.random());*/
			if ($("#lblicono" + i).html() != ""){
				$("#icoPreview").attr('src','../modulos/img/planificaiconos/' + $("#lblicono" + i ).html()+ "?" + Math.random());
			}
			if ($("#lblimagen" + i).html() != ""){
				$("#imgPreview").attr('src','../modulos/img/planificaimagenes/' + $("#lblimagen" + i ).html()+ "?" + Math.random());
			}
			
		}else{
			$("#lblid").html("0");
			$("#colborrarelemento").hide();
			$("#txtpregunta").val("");
			$("#txtdescripcion").val("");
			$("#txtorden").val(0);
			$("#cmbidiomas").val(1);
			$("#cmbestatus").val(1);
		}
		abrirmodalc1('750px', '450px');

		return false;
	}
	function readURL(input) {
		var imgPath = $("#archivoImagen").val();
		var extn = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
		//if (extn == "gif" || extn == "png" || extn == "jpg" || extn == "jpeg") {
		if (extn == "svg" || extn=="png") {
    	if (input.files && input.files[0]) {
     		var reader = new FileReader();
	      reader.onload = function (e) {
			  
	      	$('#imgPreview').attr('src', e.target.result);
					
	      }
	      reader.readAsDataURL(input.files[0]);
	   	}
		}else{
			abrirmodalavisos("Archivo seleccionado", "Debe seleccionar un archivo de imagen válido.<br>(Extensión SVG/PNG)", '750px', '450px');
		}
	}
	function readURLico(input) {
		var imgPath = $("#archivoIcono").val();
		var extn = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
		if (extn == "svg" || extn=="png") {
    	if (input.files && input.files[0]) {
     		var reader = new FileReader();
	      reader.onload = function (e) {
			  
	      	$('#icoPreview').attr('src', e.target.result);
					
	      }
	      reader.readAsDataURL(input.files[0]);
	   	}
		}else{
			abrirmodalavisos("Archivo seleccionado", "Debe seleccionar un archivo de imagen válido.<br>(Extensión SVG/PNG)", '750px', '450px');
		}
	}

	$("#imgPreview").click(function() {
    $("input[id='archivoImagen']").click();
	});
	$("#icoPreview").click(function() {
    $("input[id='archivoIcono']").click();
	});
	$("#borrarelemento").unbind("click").bind('click', function () {
		//alert("aqui");
		//debugger;
		$("#texto-confirmacion").html("¿Está seguro que desea borrar la recomendacion " + $("#txtpregunta").val() +  '?');
		$( "#dialog-confirm" ).dialog({
			resizable: false,
		    height: "auto",
		    width: 400,
		    modal: true,
		    buttons: {
		        "Aceptar": function() {
		          	$( this ).dialog( "close" );
		          	var dataObject = {id_elemento: $("#lblid").text(),
					//nombreimagen: $("#nombreimagen").val(),
					catalogo: 'recomendaciones'
					}
					$.ajax({
						data:  dataObject,
				      //  url:   'ajax/ajax_borra_tema_sala.php',
				  		url:   'ajax/ajax_borrado_logico.php',
						type:  'post',
				      	success:  function (response) {
							//alert(response);
							if (jQuery.trim(response) == "OK"){
								abrirmodalavisos("Eliminar Recomendación","Recomendación Eliminada con éxito.", 500, 200);
								location.reload();
							}				
						}
					});
		    	},
		        "Cancelar": function() {
		          $( this ).dialog( "close" );
		          return false;
	           }
		    }
	    });		
	 });	

	
</script>
<?
include '../inputs.php';
?>
