<?php
include '../includes/vars.php';
$idUsu=$_POST['idUsu'];
$mysqli = new mysqli($vservidor,$vusuario, $vpwd, $vbd);
$mysqli->set_charset("utf8");
if ($mysqli->connect_errno) {
	printf("Error: Fallo al conectarse a MySQL %s\n", $mysqli->connect_error);
    exit;
}
$sql = "CALL paGetPermisos('".$idUsu."')";

if (!$resultado = $mysqli->query($sql)) {
    printf("Error: La ejecución de la consulta falló debido a: \n");
    printf("Query: " . $sql . "\n");
    printf("Errno: " . $mysqli->errno . "\n");
    printf("Error: " . $mysqli->error . "\n");
    exit;
}
if ($resultado->num_rows === 0) {
    printf("Lo sentimos. No se pudo encontrar una coincidencia para el ID Inténtelo de nuevo.");
    exit;
}
$myArray = array();
while($row = $resultado->fetch_array(MYSQLI_ASSOC)) {
            $myArray[] = $row;
}
echo json_encode($myArray);
$resultado->free();
$mysqli->close();

?>
