<?
require 'vs.php';
require_once '../includes/funcs.php';
session_start();
$idPerfil = $_SESSION['IDP'];


?>
<div class="contenedor">

<div class="row">
<div class="col11"></div>
<div class="col1">
			<a title="Agregar evento" href="#" class="mostrar-detalle" data-indice="-1">
			<div class="btn-flotante">
				<i class="fa fa-plus btn-flotante-texto"></i>
			</div>
			</a>
</div>
</div>

<section>
<table id="tabla-principal" class="tabla-datos">
<thead>
<tr>
<th class="coltabla0 noexcel"></th>
<th class="col-iconos texto-centrado noexcel"></th>
<th class="col-iconos texto-centrado noexcel"></th>
<th class="coltabla3 texto-derecha">Título</th>
<!--<th class="coltabla3 texto-derecha">Etiqueta</th>-->
<th class="coltabla1 texto-derecha">Fecha(s)</th>
<th class="coltabla1 texto-derecha">Horario(s)</th>
<th class="coltabla1 texto-derecha">Fecha inicio</th>
<th class="coltabla1 texto-derecha">Fecha Fin</th>
<th class="coltabla1 texto-derecha">Inicio Pub</th>
<th class="coltabla1 texto-derecha">Fin Pub</th>

<th class="col-iconos texto-derecha"></th>
</tr>
</thead>
<tbody>
<?
	$filtro = $_POST['filtro'];
	cargaDatos($filtro);
?>
</tbody>
</table>


</section>

<section>
<div id="tabladetalle" style="background-color:#fff;z-index:99001" class="modalcapa modalcapa1">
	<div id="tabs" class="titulomodal">
		<ul>
    	<li id="tab-datos"><a href="#tabs-1">Datos del Evento</a></li>
  		<li id="tab-foto"><a href="#tabs-2">Imagen del evento</a></li>
		<!--<li id="tab-fechas"><a href="#tabs-3">Fechas del Evento</a></li>-->
        <li id="tab-traduccion"><a href="#tabs-3">Agregar Traducción</a></li>
        <li id="tab-traducciones"><a href="#tabs-4">Traducciones</a></li>
  	</ul>
   
		<div id="tabs-1">
			<table id="detalle" class="tabla-datos">
				<tbody class="contenidomodal">
					<tr>
						<td class="col3 texto-derecha">* Evento:
						  <input id="lblid" type="hidden"></td>
						<td class="col9 texto-izquierda"><input class="control" id="txtevento" required></td>
                        
					</tr>
                     <!--<tr id="fila-idioma">
                      	
                          <td class="col2 texto-derecha">*Etiqueta</td>
					   <td class="col4 texto-izquierda"><input class="control" id="txtetiqueta" required></td>
    				 </tr>-->
				    
				    <tr>
						<td class="col3 texto-derecha">*Fechas:</td>
						<td class="col5 texto-izquierda"><input class="control" id="txtfechas"></td>
						<td class="col4 texto-izquierda"><label><input type="checkbox" id="chkbanner">Página Principal</label></td>
					</tr>
					<tr>
                    <?
				$date2 = date("d/m/Y");
						
					?>
						<td class="col3 texto-derecha">*Fecha Inicio:</td>
						<td class="col3 texto-izquierda">
						  <input type="text" id="txtfechaevento1" class="control texto-centrado" value='<? echo $date2; ?>' required="required" />
						</td>
						<td class="col3 texto-derecha">*Fecha Fin:</td>
						<td class="col3 texto-izquierda">
						  <input type="text" id="txtfechaevento2" class="control texto-centrado" value='<? echo $date2; ?>' required="required" />
						</td>
				  </tr>
					<tr>
						<td class="col3 texto-derecha">*Horario:</td>
						<td class="col4 texto-izquierda"><input class="control" id="txthorario"></td>
					
					</tr>
					
					<tr>
                    <?
				$date2 = date("d/m/Y");
						
					?>
						<td class="col3 texto-derecha">*Inicio publicación:</td>
						<td class="col3 texto-izquierda"><input type="text" id="txtiniciopub" class="control texto-centrado" value='<? echo $date2; ?>'></td>
						<td class="col3 texto-derecha">*Fin publicación:</td>
						<td class="col3 texto-izquierda"><input type="text" id="txtfinpub" class="control texto-centrado" value='<? echo $date2; ?>'></td>
					</tr>
                    
					<tr id='fila-estado'>
						<td class="col3 texto-derecha">Estado:</td>
						<td class="col2 texto-izquierda"><? agregacomboestatus(); ?></td>
					</tr>
				</tbody>
			</table>
		</div>
        
	  <div id="tabs-2">JPG (532 X 436 px)
			<form id="form1" runat="server">
	        <img id="imgPreview" width="40%" src="#" alt="Da clic para seleccionar imagen del evento" />
					<br>
					<input id="archivoImagen" type='file' accept=".JPG, .jpg, image/jpeg" onchange="readURL(this);" style="display:none" />
                 
	    </form>
		</div>
		<div id="tabs-3">
			<table id="detalle" class="tabla-datos">
				<tbody class="contenidomodal">
                <tr id="fila-idioma">
                   	   <td class="col3 texto-derecha">*Idioma: </td>
                   	   <td class="col4 texto-izquierda"><div id="comboidiomas"><? agregacombo('idiomas','2');?></div></td>
    				 </tr>
                     <tr>
						<td class="col3 texto-derecha">* Título:
						  <input id="lblid" type="hidden"><input  id="txtID" type="hidden" /></td>
						<td class="col9 texto-izquierda"><input class="control" id="txttituloeventotraduccion" required></td>
                        
					</tr>
                    
					<tr>
						<td class="col3 texto-derecha">*Lugar:</td>
						<td class="col9 texto-izquierda"><input class="control" id="txtlugartraduccion"></td>
					</tr>
					<tr>
						<td class="col3 texto-derecha">Costo:</td>
						<td class="col4 texto-izquierda"><input class="control" id="txtcostotraduccion"></td>
					</tr>
                    <tr>
					   <td class="col3 texto-derecha">*Descripción</td>
					   <td class="col9 texto-izquierda"><textarea id="txtdescripciontraduccion" class="control-area"></textarea></td>
					  
			      </tr>
					
                    <tr>
						<td class="col3 texto-derecha"></td>
						<td class="col9 texto-izquierda"><input type='checkbox' id='chkanexo' >Es anexo  </td>
					</tr>
					<tr id='div-fila-anexo'>
						<td class="col3 texto-derecha">Seleccione el archivo</td>
						<td class="col9 texto-izquierda">
							<div id='div-agregar-anexo'>				
								<input id="archivo-anexo" type="file" class="inputfile" accept=".PDF, .pdf, application/pdf">
								<label for="archivo-anexo" id="lbl-archivo-anexo">
									<i class="fa fa-file-pdf-o" style="padding-top:0px"></i>&nbsp;Seleccione el archivo
								</label>
                                <!--<label id='div-ver-anexo-edicion'></label>-->
							</div>
							
						</td>
					</tr>
                  <!-- <tr id='fila-estado'>
						<td class="col3 texto-derecha">Estado:</td>
						<td class="col2 texto-izquierda"><? agregacomboestatus(); ?></td>
					</tr>-->
					<tr>
						<td class="col3 texto-derecha"></td>
						<td class="col9 texto-izquierda">
							<div class="row">
								
								<div class="col4"><br>
									<input type="button" id="agregar-traduccion" name="enviar" title="Agregar traducción" class="btn-formulario" value="Agregar traducción">
								</div>
							</div>
						</td>
					</tr>
				
					
				 
				</tbody>
			</table>

		</div>
         <div id="tabs-4" class='alto-fijo-modal2'>
			<div class="row">
				<div class="col2 texto-derecha">Idioma:</div>
				<div class="col9"><div id="div-combo-traducciones"> </div></div>
			</div>
			<div class="row" id="fila-datos-traducciones">
				<div class="col12" >
					<div id="datos-traducciones-eventos" class='alto-fijo-modal2'>

					</div>
				</div>
				
			</div>
		</div>
		<div style="margin-top:15px; padding-bottom:10px; width:100%; padding-left:550px;" id="espera"></div>
		<div class="row" id="areabotones" style="padding:10px;">
		<div class="col6"></div>
        
		<div class="col2 texto-centrado">
        
			<input type="button" id="guardar" name="enviar" title="Guardar" class="btn-formulario" value="Guardar">
			<img id="espera2" src='img/loading.gif' class="icono-espera">
		</div>
         <div class="col2 texto-centrado"  id="colborrarelemento">
        <input type="button" id="borrar" name="enviar" title="Borrar" class="btn-formulario" value="Borrar">
			
		</div>
		<div class="col2">
			<input type="button" id="cerrar" name="cerrar" title="Cerrar" class="btn-formulario modalcerrar" value="Cerrar">
		</div>
       
	</div>
	</div>
<div id="dialog-confirm" title="Borrar elemento">
  <p><span class="ui-icon ui-icon-alert" style="float:left; margin:12px 12px 20px 0;"></span><label id='texto-confirmacion'></label>
  </p>
</div>
</div>
</section>

<?
function cargaDatos($filtro){
	$strSQL = "CALL paCatalogoEventos('%".$filtro."%')";
	//echo $strSQL;
	$resultado = consulta($strSQL);
	
	while ($row = $resultado->fetch_array(MYSQLI_ASSOC)){
		$indice = $row["id_evento"];
		echo "<tr id=".$indice.">";
		echo "<td style='display:none' class='noexcel'><label id='lblbanner".$indice."'>".$row["banner"]."</label><label id='lblimagen".$indice."'>".$row["imagen"]."</label>
		<label id='lblestatus".$indice."'>".$row["estatus"]."</label><label id='lblid".$indice."'>".$indice."</label>
		</td>";
		echo "<td class='texto-centrado noexcel'><a title='Editar' href='#' id='editar' class='mostrar-detalle' data-indice='".$indice."'><i class='fa fa-edit icono-tablas' aria-hidden='true'></i></a></td>";
		echo "<td class='texto-centrado noexcel'><label>".semaforoestatus($row["estatus"])."</label></td>";
		echo "<td class='texto-izquierda'><label id='lblevento".$indice."'>".$row["evento"]."</label></td>";
		//echo "<td class='texto-izquierda'><label id='lbletiqueta".$indice."'>".$row["etiqueta"]."</label></td>";
		echo "<td class='texto-izquierda'><label id='lblfechas".$indice."'>".$row["fechas"]."</label></td>";
		echo "<td class='texto-izquierda'><label id='lblhorario".$indice."'>".$row["horario"]."</label></td>";
		echo "<td class='texto-derecha'><label id='lblfecha1".$indice."'>".$row["fecha1"]."</label></td>";
		echo "<td class='texto-derecha'><label id='lblfecha2".$indice."'>".$row["fecha2"]."</label></td>";
		echo "<td class='texto-derecha'><label id='lbliniciopub".$indice."'>".$row["inicio_pub"]."</label></td>";
		echo "<td class='texto-derecha'><label id='lblfinpub".$indice."'>".$row["fin_pub"]."</label></td>";
		if ($row["banner"] == 1){
			echo "<td><i class='fa fa-star icono-tablas' aria-hidden='true'></i></td>";
		}else{
			echo "<td></td>";
		}
		echo "</tr>";
	}
	$resultado->close();
}
?>
 <script>
	$(document).ready(function() {
		
		$("#dialog-confirm").hide();
		$("#div-fila-anexo").hide();
		$("#tabs").tabs();
		$(".modalcapa1").draggable({ cursor: "move", handle: ".titulomodal", opacity: 0.5 });
		$("#espera2").hide();
		$("#tabladetalle").hide();
		$("#txtiniciopub").datepicker();
		$("#txtfinpub").datepicker();
		$("#txtfechaevento1").datepicker(
		{
		  dateFormat: 'dd/mm/yy',
		  firstDay: 1
		}).datepicker("setDate", new Date());
		$("#txtfechaevento2").datepicker(
		{
		  dateFormat: 'dd/mm/yy',
		  firstDay: 1
		}).datepicker("setDate", new Date());
		
		Crearlisteners();
		
	});
$("#archivo-anexo").change(function() {
		if ($("#archivo-anexo").val().split('\\').pop() != ""){
			$("#lbl-archivo-anexo").html("<i class='fa fa-file-text' style='padding-top:0px'></i>&nbsp;" + $("#archivo-anexo").val().split('\\').pop());
		}else{
			$("#lbl-archivo-anexo").html("<i class='fa fa-file-text' style='padding-top:0px'></i>&nbsp;Seleccione el archivo");
		}
  	});  
   
	$("#chkanexo").change(function(){
		
		if ($("#chkanexo:checked").val() == "on"){
			//$("#fila-url").hide();
			$("#div-fila-anexo").show();
		}else{
			//$("#fila-url").show();
			$("#archivo-anexo").val("");
			$("#div-fila-anexo").hide();
		}
	});
	                
	$(".mostrar-detalle").unbind("click").bind('click', function () {
		mostrarDetalle($(this).data("indice"));
  });


	function abrirmodalc1(width, height) {
		Ajustatamanocapa(".modalcapa1", width, height);
		$(".modalgeneralcapa1").fadeIn();
		$(".modalcapa1").fadeIn();
		return;
	}

	function Ajustatamanocapa(Control, width, height) {
		$(Control).css('width', width);
		$(Control).css('left', 'calc(50% - ' + width + '/2)');
		$(Control).css('top', 'calc(35% - ' + height + '/2)');

		$(Control + ' .contenidomodal').css('height', 'calc(' + height + ' - 140px)');

		return;
	}
	function cerrarmodalc1() {
		$(".modalgeneralcapa1").fadeOut();
		$('.modalcapa1').fadeOut();
	}

	function Crearlisteners(){
		$('.modalcerrar').click(function(){
			cerrarmodalc1();
			return false;
		});
	}

	$("#guardar").click(function() {
		imgsrc=0;
		 if($("#imgPreview").attr('src'))
		 {
			 imgsrc=1;
			
		}
		if ($('#txtevento').val() != '' && $('#txtfechas').val() != '' && $('#txthorario').val() != ''  && $('#txtfechaevento1').val() != '' && $('#txtfechaevento2').val() != '' && $('#txtiniciopub1').val() != '' && $('#txtfinpub').val() != ''  && ($("#archivoImagen").val() != "" || imgsrc==1)){
			var ban = 0;
			if ($("#chkbanner").prop( "checked" )){
				ban = 1;
			}
			
				var dataObject = { id_evento: $("#lblid").text(),
				evento: $("#txtevento").val(),
				/*etiqueta: $("#txtetiqueta").val(),*/
				fechas: $("#txtfechas").val(),
				horario: $("#txthorario").val(),
				banner: ban,
				lugar: $("#txtlugar").val(),
				url: '',
				fecha1: $("#txtfechaevento1").val(),
				fecha2: $("#txtfechaevento2").val(),
				iniciopub: $("#txtiniciopub").val(),
				finpub: $("#txtfinpub").val(),
				estatus: $("#cmbestatus").val()};
				var idOk = "0";
				$.ajax({
					data:  dataObject,
					url:   'ajax/ajax_guarda_evento.php',
	        type:  'post',
					async: false,
		      beforeSend: function () {
						$("#guardar").hide();
	          $("#espera2").show();
	        },
	        success:  function (response) {
						var id = $("#lblid").text();
						cerrarmodalc1();
						$("#espera2").hide();
						$("#guardar").show();
						var pos = response.indexOf("IDOK");
						idOk = response.substr(pos + 4);
						response = response.replace("IDOK" + idOk,"");
						if (response.substring(0,3) == "<tr" || response.substring(0,3) == "<td"){
							
							if (id == 0) {
								if ($('#tabla-principal > tbody > tr').length == 0){
									$('#tabla-principal > tbody ').html(response);
								}else{
									$('#tabla-principal > tbody > tr').eq(0).before(response);
								}
							}else{
								$("#"+id).html(response);
							}
							subeFoto(idOk);
							
						}else{
							abrirmodalavisos('Eventos',response, '800px', '450px');
						}
	
						$(".mostrar-detalle").unbind("click").bind('click', function () {
							mostrarDetalle($(this).data("indice"));
					  });
					}
	
	       		});
			


		}else{
			abrirmodalavisos("Eventos", "Debe capturar el título, fechas, horario,  fechas y la foto del evento que desea guardar", '750px', '450px');
		}
		
	});
	
	
	
/*$("#borrar").click(function() {
			var dataObject = { id_evento: $("#lblid").text()};
			var idOk = "0";
			$.ajax({
				data:  dataObject,
				url:   'ajax/ajax_borra_evento.php',
        type:  'post',
				async: false,
	      beforeSend: function () {
					$("#borrar").hide();
					$("#guardar").hide();
          			$("#espera2").show();
        },
        success:  function (response) {
					var id = $("#lblid").text();
					cerrarmodalc1();
					$("#espera2").hide();
					$("#guardar").show();
					// remueve la fila
					$("#"+id).remove();
			
					$(".mostrar-detalle").unbind("click").bind('click', function () {
						mostrarDetalle($(this).data("indice"));
				  });
				}

       		});
		
	});*/
	$("#borrar").unbind("click").bind('click', function () {
		//alert("aqui");
		$("#texto-confirmacion").html("¿Está seguro que desea borrar el elemento " + $("#txtevento").val() + '?');
		$( "#dialog-confirm" ).dialog({
			resizable: false,
		    height: "auto",
		    width: 400,
		    modal: true,
		    buttons: {
		        "Aceptar": function() {
		          	$( this ).dialog( "close" );
		          	var dataObject = { id_elemento: $("#lblid").text(),
					//nombreimagen: $("#nombreimagen").val(),
					catalogo: 'eventos'
					}
					$.ajax({
						data:  dataObject,
				      //  url:   'ajax/ajax_borra_tema_sala.php',
				  		url:   'ajax/ajax_borrado_logico.php',
						type:  'post',
				      	success:  function (response) {
							//alert(response);
							if (jQuery.trim(response) == "OK"){
								abrirmodalavisos("Eliminar evento","Evento Eliminado con éxito.", 500, 200);
								location.reload();
							}				
						}
					});
		    	},
		        "Cancelar": function() {
		          $( this ).dialog( "close" );
		          return false;
	           }
		    }
	    });
  });

function subeFoto(idC){
	var file_data = $("#archivoImagen").prop("files")[0];
	var form_data = new FormData();

	form_data.append("file", file_data);
	$.ajax({
		data: form_data,
    url:   'ajax/ajax_sube_foto.php?num=' + idC + "&tipo=eventos",
    type:  'post',
		cache: false,
		async: false,
		contentType: false,
		processData: false,
		success:  function (response) {
			$("#imgPreview").attr('src','');
			$("#imgPreview").attr('src','../modulos/img/eventos/eventos-' + idC + ".jpg?' + Math.random()");
		}
	});
}

	function mostrarDetalle(i) {
		//debugger;
		// $('#imgPreview').attr('src', ' ');
		$("#archivoImagen").val("");
		$("#espera2").hide();
		
		$("#chkbanner").prop( "checked", true);
		$("#chkanexo").prop( "checked", false);
		$( "#tabs" ).tabs( "option", "active", 0 );
		if (i != "-1"){
			$("#lblid").html($('#lblid' + i).html());
			$("#txtID").val(i);
			$("#colborrarelemento").show();
			$("#txtevento").val($('#lblevento' + i).html());
			/*$("#txtetiqueta").val($('#lbletiqueta' + i).html());*/
			$("#txtfechas").val($('#lblfechas' + i).html());
			$("#txthorario").val($('#lblhorario' + i).html());
			$("#txtfechaevento1").val($('#lblfecha1' + i).html());
			$("#txtfechaevento2").val($('#lblfecha2' + i).html());
			$("#txtiniciopub").val($('#lbliniciopub' + i).html());
			$("#txtfinpub").val($('#lblfinpub' + i).html());
			$("#cmbestatus").val($("#lblestatus" + i).html());
			if ($("#lblbanner" + i).html() == 0){
				$("#chkbanner").prop( "checked", false);
			}
			$("#borrar").show();
			$("#imgPreview").attr('src','');
			$("#imgPreview").attr('src','../modulos/img/eventos/eventos-' + $("#lblid" + i ).html() + ".jpg?" + Math.random());
			$("#tab-traduccion").show();
			$("#tab-traducciones").show();
			cargaTraducciones($('#lblid' + i).html());
			
			
			/*$("#tab-fechas").show();
			cargaFechas(i);*/
		}else{
			var today = new Date();
			var dd = today.getDate();
			var mm = today.getMonth()+1; //January is 0!
			var yyyy = today.getFullYear();
			if(dd<10){
				dd='0'+dd;
			} 
			if(mm<10){
				mm='0'+mm;
			} 
			var fechahoy = dd+'/'+mm+'/'+yyyy;
			$("#imgPreview").attr('src','');
			$("#lblid").html("0");
			$("#colborrarelemento").hide();
			$("#txtevento").val("");
			/*$("#txtetiqueta").val("");*/
			$("#txtfechas").val("");
			$("#txthorario").val("");
			$("#txttituloeventotraduccion").val("");
			$("#txtdescripciontraduccion").val("");
			$("#chkanexo").val(1);
			$("#txtlugartraduccion").val("");
			
			$("#txtfechaevento1").val(fechahoy);
			$("#txtfechaevento2").val(fechahoy);
			$("#txtiniciopub").val(fechahoy);
			$("#txtfinpub").val(fechahoy);
			$("#chkbanner").prop( "checked", false );
			$("#cmbestatus").val(1);
			$("#borrar").hide();
			$("#tab-traduccion").hide();
			$("#tab-traducciones").hide();
			
		}
		abrirmodalc1('750px', '450px');

		return false;
	}

	function readURL(input) {
		var imgPath = $("#archivoImagen").val();
		var extn = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
		if (extn == "jpg" || extn == "jpeg") {
    	if (input.files && input.files[0]) {
     		var reader = new FileReader();
	      reader.onload = function (e) {
	      	$('#imgPreview').attr('src', e.target.result);
	      }
	      reader.readAsDataURL(input.files[0]);
	   	}
		}else{
			abrirmodalavisos("Archivo seleccionado", "Debe seleccionar un archivo de imagen válido.<br>(Extensión JPG)", '750px', '450px');
		}
	}

	$("#imgPreview").click(function() {
    $("input[id='archivoImagen']").click();
	});
	
	$("#agregar-traduccion").click(function() {
		debugger;
		if ($('#txttituloeventotraduccion').val() != '' && $('#txtlugartraduccion').val() != '' && $('#txtcostotraduccion').val() != '' && $('#txtdescripciontraduccion').val() != '' && $("#cmbidiomas").val()!='' ){
			var esAnexo = 0;
			if ($('#chkanexo').is(":checked"))
			{
  				esAnexo = 1;
			}
			var archivo=$('#archivo-anexo').prop('files')[0];
			//var anexoanterior=$("#anexoanterior").val();
			var esAnexo = 0;
			if ($('#chkanexo').is(":checked") && $('#archivo-anexo').prop('files')[0] != undefined)
			{
  				esAnexo = 1;
			}
			else{
				if ($('#chkanexo').is(":checked") && $('#archivo-anexo').prop('files')[0] == undefined  
				)
				{
				abrirmodalavisos("Agregar Evento","Debe seleccionar un archivo Anexo.");
				 return false;
				}
			}
			if(esAnexo==1 && archivo==undefined)
			{
				if(anexoanterior==1){
					anexo=1;
				}else{
					anexo=0;
				}
			}
			else{
				if(esAnexo==0)
				{
					anexo=0;
				}
				else{
					anexo=1;
				}
				
			}
			var idDetalle = 0;
			var datos = {id_detalle:0,
					id_evento:$("#lblid").text(),
					idioma: $("#cmbidiomas").val(),
					titulo:$("#txttituloeventotraduccion").val(),
					lugar:$("#txtlugartraduccion").val(),
					costo:$("#txtcostotraduccion").val(),
					descripcion:$("#txtdescripciontraduccion").val(),
					anexo: anexo,
					anexoanterior: ''};
			$.ajax({
				data: datos,
		    url:   'ajax/ajax_guarda_traduccion_evento.php',
		    type:  'post',
				async: false,
				success:  function (response) {
					idDetalle = response;
					if(response=='NO')
					{
						abrirmodalavisos("Agregar traduccion","La traducción capturada del evento por idioma ya existe en la base de datos.");
						
					}
					else{
					if ($('#archivo-anexo').prop('files')[0] != undefined){
								var form_data = new FormData();
								var file_data = $('#archivo-anexo').prop('files')[0];
								form_data.append('archivo-anexo', file_data);
								form_data.append('id_elemento', idDetalle);
									
								$.ajax({
									url:   'ajax/ajax_sube_anexo_eventos.php',
									dataType: 'text',  
									cache: false,
									contentType: false,
									processData: false,
									data: form_data,
							        type:  'post',
						        	success:  function (response) {
										//alert(response);
										if (jQuery.trim(response) == "OK"){
										return true;
										}else{
											abrirmodalavisos("Anexo", "Hubo un problema al subir su anexo.");
											return false;
										}
									}
								});
							}
					cargaTraducciones($("#lblid").text());
					
					
						abrirmodalavisos("Agregar traducción","Traducción subida con éxito.");
					}
					
					},error : function(jqXHR, textStatus, errorThrown){
					alert(errorThrown);
				}
				
			});
			$("#txttituloeventotraduccion").val("");
			$("#txtlugartraduccion").val("");
			$("#txtcostotraduccion").val("");
			$("#txtdescripciontraduccion").val("");
			$("#cmbidiomas").val(1);
			$("#chkanexo").prop('checked', false);
			$("#archivo-anexo").val("");
			$("#div-fila-anexo").hide();
			$("#lbl-archivo-anexo").html("<i class='fa fa-file-pdf-o' style='padding-top:0px'></i>&nbsp;Seleccione el archivo");
		}else{
			abrirmodalavisos("Agregar traducción","Debe capturar las Traducciones del título, lugar, costo, descripción  y el idioma.");
		}
	})
	
	function cargaTraducciones(id){
		//debugger;
		var dataObject = { catalogo: "traduccioneseventos",
		id_extra: $("#txtID").val()}
		$.ajax({
				data:  dataObject,
        url:   'ajax/ajax_carga_combo.php',
        type:  'post',
      	success:  function (response) {
					$("#div-combo-traducciones").html(response);
					$( "#cmbtraduccioneseventos" ).change(function() {
						CargaDatosTraduccion($("#cmbtraduccioneseventos").val());
					});
					if ($("#cmbtraduccioneseventos").val() != null){
						$("#fila-datos-traducciones").show();
						CargaDatosTraduccion($("#cmbtraduccioneseventos").val());
					}else{
						$("#cmbtraduccioneseventos").hide();
						$("#div-combo-traducciones").html("<strong>No existen traducciones para el evento</strong>");
						$("#fila-datos-traducciones").hide();
					}
				}
			});
	}
	function CargaDatosTraduccion(idD){
		var dataObject = { id_detalle: idD}
		$.ajax({
				data:  dataObject,
        url:   'ajax/ajax_carga_datos_traduccion_evento.php',
        type:  'post',
      	success:  function (response) {
					//alert(response);
					$("#datos-traducciones-eventos").html(response);
					$("#borra-traduccion").unbind("click").bind('click', function () {
						borrarTraduccion();
					});
					$("#guardarediciontraduccion").click(function(){
						cambiarDatosTraduccion();
					})
				}
		});
	}
	function cambiarDatosTraduccion(){
		//debugger;
		if ( $("#txttituloeventotraduccionedicion").val() != '' && $("#txtlugartraduccionedicion").val() != ''
		&& $("#txtlugartraduccionedicion").val() != '' && $("#txtdescripciontraduccionedicion").val() != ''){
		
			var esAnexo = 0;
			var anexoanterior=$('#anexoanterior').val();
			if ((anexoanterior==0 && $('#chkanexoedicion').is(":checked") && $('#archivo-anexo-edicion').prop('files')[0] != undefined)|| anexoanterior==1 )
			{
				esAnexo = 1;
			}
			else{
				if ($('#chkanexoedicion').is(":checked") && $('#archivo-anexo-edicion').prop('files')[0] == undefined  && anexoanterior==0)
				{
				abrirmodalavisos("Modificar imagen","Debe seleccionar un archivo Anexo.");
				 return false;
				}
			}
			var idDetalle = $("#cmbtraduccioneseventos").val();
			var datos = {id_detalle:idDetalle,
					id_evento:0,
					idioma:0,
					titulo:$("#txttituloeventotraduccionedicion").val(),
					lugar:$("#txtlugartraduccionedicion").val(),
					costo:$("#txtcostotraduccionedicion").val(),
					descripcion:$("#txtdescripciontraduccionedicion").val(),
					anexo: esAnexo,
					anexoanterior: anexoanterior};
			$.ajax({
				data: datos,
		    url:   'ajax/ajax_guarda_traduccion_evento.php',
		    type:  'post',
				async: false,
				success:  function (response) {
					idDetalle = response;
					if ($('#archivo-anexo-edicion').prop('files')[0] != undefined && $('#chkanexoedicion').is(":checked")){
							var form_data = new FormData();
							var file_data = $('#archivo-anexo-edicion').prop('files')[0];
							form_data.append('archivo-anexo-edicion', file_data);
							form_data.append('id_elemento', idDetalle);
							form_data.append('edicion', 1);
								
							$.ajax({
								url:   'ajax/ajax_sube_anexo_eventos.php',
								dataType: 'text',  
								cache: false,
								contentType: false,
								processData: false,
								data: form_data,
						        type:  'post',
					        	success:  function (response) {
									if (jQuery.trim(response) == "OK"){
				
										return true;
									}else{
										abrirmodalavisos("Anexo", "Hubo un problema al subir su anexo.");
										return false;
									}
								}
							});
						}
					
					cargaTraducciones(idDetalle);
				},error : function(jqXHR, textStatus, errorThrown){
					alert(errorThrown);
				}
			});

			abrirmodalavisos("Traducción","Cambios guardados con éxito.", 500, 200);
		}else{
			abrirmodalavisos("Traducción","Debe capturar la traducción del país.", 500, 200);
		}
	}
	
	
	function borrarTraduccion(){
		//alert("aqui");
		//debugger;
		$("#texto-confirmacion").html("¿Está seguro que desea borrar la traducción " + $("#cmbtraduccioneseventos option:selected").text().toUpperCase() + '?');
		$( "#dialog-confirm" ).dialog({
			resizable: false,
		    height: "auto",
		    width: 400,
		    modal: true,
		    buttons: {
		        "Aceptar": function() {
		          	$( this ).dialog( "close" );
		          	var dataObject = { id_elemento: $("#cmbtraduccioneseventos").val(),
					//nombreimagen: $("#nombreimagen").val(),
					catalogo: 'traduccioneventos'
					}
					$.ajax({
						data:  dataObject,
				      //  url:   'ajax/ajax_borra_tema_sala.php',
				  		url:   'ajax/ajax_borrado_logico.php',
						type:  'post',
				      	success:  function (response) {
							//alert(response);
							if (jQuery.trim(response) == "OK"){
								abrirmodalavisos("Eliminar Traducción","Traducción Eliminada con éxito.", 500, 200);
								cargaTraducciones($("#lblid").text());
							}				
						}
					});
		    	},
		        "Cancelar": function() {
		          $( this ).dialog( "close" );
		          return false;
	           }
		    }
	    });		
	}
	
	/*function borrarTraduccion(){
		

		var dataObject = { id_detalle: $("#cmbtraduccioneseventos").val()}
		$.ajax({
				data:  dataObject,
        url:   'ajax/ajax_borra_traduccion_evento.php',
        type:  'post',
      	success:  function (response) {
				//alert(response);
				 $("#comboidiomas").html(response);
					cargaTraducciones();
				}
		});
	}*/

/*	function cargaFechas(i){
		var dataObject = { id_evento: i}
		$.ajax({
				data:  dataObject,
        url:   'ajax/ajax_fechas_evento.php',
        type:  'post',
      	success:  function (response) {
					//debugger;
					
					$("#div-tabla-fechas").html(response);
					$(".borra-fecha").unbind("click").bind('click', function () {
						quitaFecha($(this).data("indice"));
					});
				}

		});
	}
	function quitaFecha(idD){
		var dataObject = { id_evento: idD,
		fecha: '',
		modo: "-"};
		$.ajax({
			data:  dataObject,
		      url:   'ajax/ajax_cambia_fecha_evento.php',
		      type:  'post',
		      success:  function (response) {
						cargaFechas($("#lblid").text());
					}
		});
	}
	function agregaFecha(idE, fechaX, fecha2X){
		if (fechaX == '' || fecha2X == ''){
			abrirmodalavisos("Agregar fecha del evento","Debe seleccionar fechas válidas", 600, 280);
			return false;
		}
		var dataObject = { id_evento: idE,
		fecha: fechaX,
		fecha2: fecha2X,
		modo: "+"};
		$.ajax({
			data:  dataObject,
		      url:   'ajax/ajax_cambia_fecha_evento.php',
		      type:  'post',
		      success:  function (response) {
					//alert(response);
						cargaFechas($("#lblid").text());
					}
		});
	}*/

/*	$("#agregafecha").unbind("click").bind('click', function () {
		agregaFecha($("#lblid").text(), $("#txtfechaevento").val(), $("#txtfechaevento2").val());
	});*/
	/* traducciones*/
	
</script>

<?
include '../inputs.php';
?>
