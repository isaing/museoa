<?
require 'vs.php';
include_once '../includes/funcs.php';
?>
<table id="tabla-principal" class="tabla-datos">
<thead>
<tr>
<th class="coltabla0 noexcel"></th>
<th class="col-iconos texto-centrado noexcel"></th>
<th class="coltabla3 texto-derecha">Fecha</th>
</tr>
</thead>
<tbody>
<?
	$idX = $_POST['id_evento'];
	$strSQL = "CALL paCargaFechasEvento ('".$idX."')";
	$resultado = consulta($strSQL);
	while ($row = $resultado->fetch_array(MYSQLI_ASSOC)){
		$indice = $row["id_detalle"];
		echo "<tr id=".$indice.">";
		echo "<td style='display:none;' class='noexcel'><label id='lblid".$indice."'>".$indice."</label></td>";
		echo "<td class='texto-centrado noexcel'><a title='Quitar fecha' href='#' id='quitar' class='borra-fecha' data-indice='".$indice."'><i class='fa fa-remove icono-tablas' aria-hidden='true'></i></a></td>";
		echo "<td class='texto-izquierda'><label id='lblcatalogo".$indice."'>".$row["fecha"]."</label></td>";
		echo "</tr>";
	}
	$resultado->close();
?>
</tbody>
</table>
<script>

</script>
