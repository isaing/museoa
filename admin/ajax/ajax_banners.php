<?
require 'vs.php';
require_once '../includes/funcs.php';
/*session_start();
$idPerfil = $_SESSION['IDP'];
*/

?>
<div class="contenedor">

<div class="row">
<div class="col11"></div>
<div class="col1">
			 <? 
			 /*Verificar si hay menor cantidad de banners que idiomas para poder agregar*/
		/*	$strSQL = "CALL paCuantosPorIdioma('banners')";
			$resultado = consulta($strSQL);
			while ($row = $resultado->fetch_array(MYSQLI_ASSOC)){
				$disponibles = $row["disponibles"];
			}
			if ($disponibles==1){?>
                <a title="Agregar slider" href="#" class="mostrar-detalle" data-indice="-1">
               
                <div class="btn-flotante">
                    <i class="fa fa-plus btn-flotante-texto"></i>
                </div>
                </a>
             <? }*/?>
             <a title="Agregar slider" href="#" class="mostrar-detalle" data-indice="-1">
               
                <div class="btn-flotante">
                    <i class="fa fa-plus btn-flotante-texto"></i>
                </div>
                </a>
</div>
</div>


	
<section>
<table id="tabla-principal" class="tabla-datos">
<thead>
<tr>
<th class="coltabla0 noexcel"></th>
<th class="col-iconos texto-centrado noexcel"></th>
<th class="col-iconos texto-centrado noexcel"></th>
<th class="coltabla7 texto-derecha">Slider</th>
<th class="coltabla4 texto-derecha">Idioma</th>

</tr>
</thead>
<tbody>

<?
	$filtro = $_POST['filtro'];
	cargaDatos($filtro);
?>
</tbody>
</table>


</section>

<section>
<div id="tabladetalle" style="background-color:#fff;z-index:99001" class="modalcapa modalcapa1">
	<div id="tabs" class="titulomodal">
		<ul>
    	<li id="tab-datos"><a href="#tabs-1">Datos Generales</a></li>
		<li id="tab-agregar"><a href="#tabs-2">Agregar imagen</a></li>
  		<li id="tab-imagenes"><a href="#tabs-3">Imagenes del slider</a></li>

  	</ul>
	<div id="tabs-1">
			<table id="detalle" class="tabla-datos">
				<tbody class="contenidomodal">
					<!--<tr id="id-banner">
						<td class="col3 texto-derecha">ID Slider:</td>
						<td class="col3 texto-izquierda"><strong></strong></td>
					</tr>-->
                     <tr id="fila-idioma">
                      	<td class="col3 texto-derecha">*Idioma: </td>
                      	<td class="col9 texto-izquierda"><? agregacombo('idiomas','1');?></td>
    				 </tr>
                      <tr id="fila-idioma-texto">
                      	<td class="col3 texto-derecha">Idioma: </td>
                      	<td class="col9 texto-izquierda"><label id="lblidiomatexto"></label></td>
    				 </tr>
					<tr>
						<td class="col3 texto-derecha">*Slider:
						  <input id="lblid" type="hidden">
                        <input  id="txtID" type="hidden"></td>
						<td class="col9 texto-izquierda"><input class="control" id="txtbanner" required></td>
					</tr>
					<tr id='fila-estado'>
						<td class="col3 texto-derecha">Estado:</td>
						<td class="col2 texto-izquierda"><? agregacomboestatus(); ?></td>
					</tr>
				</tbody>
			</table>
		</div>

		<div id="tabs-2">
			<table id="detalle" class="tabla-datos">
				<tbody class="contenidomodal">
					<tr>
						<td class="col3 texto-derecha">Descripción:<input id="lblid" type="hidden"></td>
						<td class="col5 texto-izquierda"><input class="control" id="txttitulo" required></td>
					</tr>

					<tr id="fila-url">
						<td class="col3 texto-derecha">URL Destino:</td>
						<td class="col9 texto-izquierda"><input class="control" id="txturl"></td>
					</tr>
                    <tr>
						<td class="col3 texto-derecha"></td>
						<td class="col9 texto-izquierda"><input type='checkbox' id='chkanexo' >Es anexo </td>
					</tr>
					<tr id='div-fila-anexo'>
						<td class="col3 texto-derecha">Seleccione el archivo</td>
						<td class="col9 texto-izquierda">
							<div id='div-agregar-anexo'>				
								<input id="archivo-anexo" type="file" class="inputfile" accept=".PDF, .pdf, application/pdf">
								<label for="archivo-anexo" id="lbl-archivo-anexo">
									<i class="fa fa-file-pdf-o" style="padding-top:0px"></i>&nbsp;Seleccione el archivo
								</label>
							</div>
							<div id='div-ver-anexo'></div>
						</td>
					</tr>
					<tr>
						<td class="col3 texto-derecha">Ventana destino:</td>
						<td class="col4 texto-izquierda"><? agregacombodestinosurl(); ?></td>
					</tr>
                   
					<tr>
						<td class="col3 texto-derecha">Imagen <br />(1920 x 748px)</td>
						<td class="col9 texto-izquierda">
							<div class="row">
								<div class="col8">
									<form id="form1" runat="server">
										<a href="#" id ="buscaArchivo">Seleccionar imagen</a>
										<input id="archivoImagen" type='file' accept=".JPG, .jpg, image/jpeg" onchange="readURL(this);" style="display:none" />
									</form>
									<img src='' id="imgPreview" width="100%" height="100%">
								</div>
                                 <div class="col3">
									<br><br><br><br><br><br>
									<input type="button" id="agregar-imagen" name="enviar" title="Agregar imagen" class="btn-formulario" value="Agregar imagen">
								</div>
								
							</div>
						</td>
					</tr>
				
					<tr id='fila-estado'>
						<td class="col3 texto-derecha">Estado:</td>
						<td class="col2 texto-izquierda"><? agregacomboestatus(); ?></td>
                       
					</tr>
				 
				</tbody>
			</table>

		</div>

		<div id="tabs-3" class='alto-fijo-modal'>
			<div class="row">
				<div class="col2 texto-derecha">Imagen:</div>
				<div class="col9"><div id="div-combo-imagenes"> </div></div>
			</div>
			<div class="row" id="fila-datos-imagen">
				<div class="col9" >
					<div id="datos-imagen" class='alto-fijo-modal'>

					</div>
				</div>
				<div class="col3 texto-centrado">
					<br>
					<img id="imgLista" width="100%" height="auto">
				</div>
			</div>
		</div>

		<div style="margin-top:15px; padding-bottom:10px; width:100%; padding-left:550px;" id="espera"></div>
		<div class="row" id="areabotones" style="padding:10px;">
			<div class="col8"></div>
			<div class="col2 texto-centrado">
				<input type="button" id="guardar" name="enviar" title="Guardar" class="btn-formulario" value="Guardar">
				<img id="espera2" src='img/loading.gif' class="icono-espera">
			</div>
			<div class="col2">
				<input type="button" id="cerrar" name="cerrar" title="Cerrar" class="btn-formulario modalcerrar" value="Cerrar">
			</div>
		</div>
	</div>
	<div id="dialog-confirm" title="Borrar elemento">
      <p><span class="ui-icon ui-icon-alert" style="float:left; margin:12px 12px 20px 0;"></span><label id='texto-confirmacion'></label>
      </p>
    </div>
</div>
</section> 


<?
function cargaDatos($filtro){
	$strSQL = "CALL paCatalogoBanners('%".$filtro."%')";
	//echo $strSQL;
	$resultado = consulta($strSQL);
	while ($row = $resultado->fetch_array(MYSQLI_ASSOC)){
		$indice = $row["id_banner"];
		echo "<tr id=".$indice.">";
		echo "<td style='display:none' class='noexcel'><label id='lblididioma".$indice."'>".$row["ididioma"]."</label><label id='lblbanner".$indice."'>".$row["banner"]."</label><label id='lblestatus".$indice."'>".$row["estatus"]."</label><label id='lblid".$indice."'>".$indice."</label></td>";
		echo "<td class='texto-centrado noexcel'><a title='Editar' href='#' id='editar' class='mostrar-detalle' data-indice='".$indice."'><i class='fa fa-edit icono-tablas' aria-hidden='true'></i></a></td>";
		echo "<td class='texto-centrado noexcel'><label>".semaforoestatus($row["estatus"])."</label></td>";
		echo "<td class='texto-izquierda'><label id='lblbanner".$indice."'>".$row["banner"]."</label></td>";
		echo "<td class='texto-izquierda'><label id='lblidioma".$indice."'>".$row["idioma"]."</label></td>";
		
		echo "</tr>";
	}
	$resultado->close();
}
?>
       <script>


	$(document).ready(function() {
		$("#dialog-confirm").hide();
		$("#div-fila-anexo").hide();
		$("#tabs").tabs({
			select: function(event,ui) {
				 alert('selected: '+ ui.index);
			}
		});
		$(".modalcapa1").draggable({ cursor: "move", handle: ".titulomodal", opacity: 0.5 });
		$("#espera2").hide();
		$("#tabladetalle").hide();
		$("#txtiniciopub").datepicker();
		$("#txtfinpub").datepicker();
		Crearlisteners();
		$(".mostrar-imagen").unbind("click").bind('click', function () {
			$("#imgLista").prop("src","../../modulos/img/banners/banners-" + $(this).data("indice") + ".jpg");
		});
		$("#cmbdependencias").prepend("<option value='0' selected='selected'>NINGUNA</option>");

	});

$("#archivo-anexo").change(function() {
		if ($("#archivo-anexo").val().split('\\').pop() != ""){
			$("#lbl-archivo-anexo").html("<i class='fa fa-file-text' style='padding-top:0px'></i>&nbsp;" + $("#archivo-anexo").val().split('\\').pop());
		}else{
			$("#lbl-archivo-anexo").html("<i class='fa fa-file-text' style='padding-top:0px'></i>&nbsp;Seleccione el archivo");
		}
  	});  
   
	$("#chkanexo").change(function(){
		
		if ($("#chkanexo:checked").val() == "on"){
			$("#fila-url").hide();
			$("#div-fila-anexo").show();
		}else{
			$("#fila-url").show();
			$("#archivo-anexo").val("");
			$("#div-fila-anexo").hide();
		}
	});
                
	
  
 
                
	$(".mostrar-detalle").unbind("click").bind('click', function () {
		
		mostrarDetalle($(this).data("indice"));
  });

	function abrirmodalc1(width, height) {
		Ajustatamanocapa(".modalcapa1", width, height);
		$(".modalgeneralcapa1").fadeIn();
		$(".modalcapa1").fadeIn();
		return;
	}

	function Ajustatamanocapa(Control, width, height) {
		$(Control).css('width', width);
		$(Control).css('left', 'calc(50% - ' + width + '/2)');
		$(Control).css('top', 'calc(50% - ' + height + '/2)');

		$(Control + ' .contenidomodal').css('height', 'calc(' + height + ' - 140px)');

		return;
	}
	function cerrarmodalc1() {
		$(".modalgeneralcapa1").fadeOut();
		$('.modalcapa1').fadeOut();
	}

	function Crearlisteners(){
		$('.modalcerrar').click(function(){
			cerrarmodalc1();
			return false;
		});
	}

	$("#agregar-imagen").click(function() {
		if ($('#txtbanner').val() != '' && $("#txttitulo").val() != '' && $("#archivoImagen").val() != ""){
			var esAnexo = 0;
			if ($('#chkanexo').is(":checked") && $('#archivo-anexo').prop('files')[0] != undefined)
			{
  				esAnexo = 1;
			}
			else{
				if ($('#chkanexo').is(":checked") && $('#archivo-anexo').prop('files')[0] == undefined)
				{
				abrirmodalavisos("Agregar imagen","Debe seleccionar un archivo Anexo.");
				 return false;
				}
			}
			var idDetalle = 0;
			var datos = {id_detalle:0,
					id_banner:$("#lblid").text(),
					descripcion:$("#txttitulo").val(),
					url:$("#txturl").val(),
					anexo: esAnexo,
					target:$("#cmbdestino").val()};
			$.ajax({
				data: datos,
		    url:   'ajax/ajax_guarda_imagen_banner.php',
		    type:  'post',
				async: false,
				success:  function (response) {
					idDetalle = response;
					/* cargar anexo*/
					if ($('#archivo-anexo').prop('files')[0] != undefined){
							var form_data = new FormData();
							var file_data = $('#archivo-anexo').prop('files')[0];
							form_data.append('archivo-anexo', file_data);
							form_data.append('id_elemento', idDetalle);
								
							$.ajax({
								url:   'ajax/ajax_sube_anexo_banners.php',
								dataType: 'text',  
								cache: false,
								contentType: false,
								processData: false,
								data: form_data,
						        type:  'post',
					        	success:  function (response) {
									//alert(response);
									if (jQuery.trim(response) == "OK"){
				
										return true;
									}else{
										abrirmodalavisos("Anexo", "Hubo un problema al subir su anexo.");
										return false;
									}
								}
							});
						}	
					},error : function(jqXHR, textStatus, errorThrown){
					alert(errorThrown);
				}
			});
			//alert(idDetalle);
			SubeFoto(idDetalle);
			cargaFotos($("#lblid").text());
			
			$("#txttitulo").val("");
			$("#txturl").val("");
			$('#imgPreview').attr('src','');
			
			$("#chkanexo").prop('checked', false);
			$("#archivo-anexo").val("");
			$("#lbl-archivo-anexo").html("<i class='fa fa-file-pdf-o' style='padding-top:0px'></i>&nbsp;Seleccione el archivo");
			
			abrirmodalavisos("Agregar imagen","Imagen subida con éxito.");
		}else{
			abrirmodalavisos("Agregar imagen","Debe capturar al menos una descripción y seleccionar una imagen válida.");
		}
	})

	$("#guardar").click(function() {
		if ($('#txtbanner').val() != '' && $("#cmbidiomas").val()!=null){
			var ban = 0;
			
			var dataObject = { id_banner: $("#lblid").text(),
			banner: $("#txtbanner").val(),
			idioma: $("#cmbidiomas").val(),
			estatus: $("#cmbestatus").val()};
			var idOk = "0";
			$.ajax({
				data:  dataObject,
				url:   'ajax/ajax_guarda_banner.php',
        type:  'post',
				async: false,
	      beforeSend: function () {
					$("#guardar").hide();
          $("#espera2").show();
        },
        success:  function (response) {
					var id = $("#lblid").text();
					cerrarmodalc1();
					$("#espera2").hide();
					$("#guardar").show();
					/* verificar si hay idiomas disponibles*/
					/*var dis = response.indexOf("DISP");
					disponibles = response.substr(dis + 4);
					if(disponibles==0){
						$(".btn-flotante").hide();
					}*/
					/* obtener el indice */
					var pos = response.indexOf("IDOK");
					idOk = response.substr(pos + 4);
					response = response.replace("IDOK" + idOk,"");
					if (response.substring(0,3) == "<tr" || response.substring(0,3) == "<td"){
						if (id == 0) {
							if ($('#tabla-principal > tbody > tr').length == 0){
								$('#tabla-principal > tbody ').html(response);
							}else{
								$('#tabla-principal > tbody > tr').eq(0).before(response);
							}
						}else{
							$("#"+id).html(response);
						}
					}else{
						abrirmodalavisos('Banners',response, '800px', '450px');
					}

					$(".mostrar-detalle").unbind("click").bind('click', function () {
						mostrarDetalle($(this).data("indice"));
				  });
				}

       		});
		}else{
			abrirmodalavisos("Sliders", "Debe capturar el título y la foto del evento que desea guardar", '750px', '450px');
		}
	});

function SubeFoto(idC){
	var file_data = $("#archivoImagen").prop("files")[0];
	var form_data = new FormData();

	form_data.append("file", file_data);
	$.ajax({
		data: form_data,
    url:   'ajax/ajax_sube_foto.php?num=' + idC + "&tipo=banners",
    type:  'post',
		cache: false,
		async: false,
		contentType: false,
		processData: false,
		success:  function (response) {

		}
	});
}


	function mostrarDetalle(i) {
		$("#fila-idioma").hide();
		$("#fila-url").show();
		$("#div-fila-anexo").hide();
		$( "#chkanexo" ).prop( "checked", false );
		
		$('#imgPreview').attr('src', ' ');
		$("#espera2").hide();
		$("#chkprincipal").prop( "checked", false);
		$("#archivo-anexo").val("");
		$( "#tabs" ).tabs( "option", "active", 0 );
		if (i != "-1"){
			$("#id-banner").show();
			$("#txtID").val(i);
			$("#lblid").html($('#lblid' + i).html());
			$("#fila-idioma-texto").show();
			$("#lblidiomatexto").html($("#lblidioma" + i).html());
			$("#txtbanner").val($('#lblbanner' + i).html());
			$("#cmbdependencias").val($("#lbliddependencia" + i).html());
			$("#cmbestatus").val($("#lblestatus" + i).html());
			if ($("#lblprincipal" + i).html() == 1){
				$("#chkprincipal").prop( "checked", true);
			}
			$("#tab-agregar").show();
			$("#tab-imagenes").show();
			cargaFotos($('#lblid' + i).html());

		}else{
			$("#id-banner").hide();
			$("#tab-agregar").hide();
			$("#tab-imagenes").hide();
			$("#imgPreview").attr('src','');
			$("#lblid").html("0");
			$("#fila-idioma").show();
			$("#lblidiomatexto").html("");
			$("#fila-idioma-texto").hide();
			$("#txtbanner").val("");
			$("#cmbdependencias").val(0);
			$("#chkbanner").prop( "checked", false );
			$("#cmbestatus").val(1);
		}
		abrirmodalc1('calc(90%)', '450px');

		return false;
	}



	function cargaFotos(){
		var dataObject = { catalogo: "imagenesbanner",
		id_extra: $("#txtID").val()}
		$.ajax({
				data:  dataObject,
        url:   'ajax/ajax_carga_combo.php',
        type:  'post',
      	success:  function (response) {
					$("#div-combo-imagenes").html(response);
					$( "#cmbimagenesbanner" ).change(function() {
						
						CargaDatosFoto($("#cmbimagenesbanner").val());
					});
					if ($("#cmbimagenesbanner").val() != null){
						$("#fila-datos-imagen").show();
						CargaDatosFoto($("#cmbimagenesbanner").val());
					}else{
						$("#fila-datos-imagen").hide();
						$("#div-combo-imagenes").html("<strong>No existen imagenes para el banner.</strong>");
					}
				}
			});
	}

	function CargaDatosFoto(idD){
		var dataObject = { id_detalle: idD}
		$.ajax({
				data:  dataObject,
        url:   'ajax/ajax_carga_datos_imagen_banner.php',
        type:  'post',
      	success:  function (response) {
					
					$("#datos-imagen").html(response);
					$("#imgLista").prop("src","../modulos/img/banners/banners-" + idD + ".jpg");
					$("#borra-imagen").unbind("click").bind('click', function () {
						borrarImagen();
					});
					$("#guardaredicion").click(function(){
						cambiarDatosImagen();
					})
				}
		});
	}


	function cambiarDatosImagen(){
		if ( $("#txttituloedicion").val() != '' ){
			var esAnexo = 0;
			var anexoanterior=$('#anexoanterior').val();
			/*alert(anexoanterior);
			alert($('#chkanexoedicion').is(":checked"));
			alert($('#archivo-anexo-edicion').prop('files')[0]);*/
			if ((anexoanterior==0 && $('#chkanexoedicion').is(":checked") && $('#archivo-anexo-edicion').prop('files')[0] != undefined) || anexoanterior==1 )
			{
				esAnexo = 1;
			}
			else{
				if ($('#chkanexoedicion').is(":checked") && $('#archivo-anexo-edicion').prop('files')[0] == undefined  && anexoanterior==0)
				{
				abrirmodalavisos("Modificar imagen","Debe seleccionar un archivo Anexo.");
				 return false;
				}
			}
			//alert(esAnexo);
			//debugger;
			var idDetalle = $("#cmbimagenesbanner").val();
			
			var datos = {id_detalle:idDetalle,
					id_banner:0,
					descripcion:$("#txttituloedicion").val(),
					url:$("#txturledicion").val(),
					target:$("#cmbdestinoedicion").val(),
					orden:$("#txtordenedicion").val(),
					anexo:esAnexo,
					anexoanterior:anexoanterior};
			$.ajax({
				data: datos,
		    url:   'ajax/ajax_guarda_imagen_banner.php',
		    type:  'post',
				async: false,
				success:  function (response) {
					//alert(response);
					idDetalle = response;
					/* cargar anexo*/
					if ($('#archivo-anexo-edicion').prop('files')[0] != undefined && $('#chkanexoedicion').is(":checked")){
							debugger;
							var form_data = new FormData();
							var file_data = $('#archivo-anexo-edicion').prop('files')[0];
							form_data.append('archivo-anexo-edicion', file_data);
							form_data.append('id_elemento', idDetalle);
							form_data.append('edicion', 1);
								
							$.ajax({
								url:   'ajax/ajax_sube_anexo_banners.php',
								dataType: 'text',  
								cache: false,
								contentType: false,
								processData: false,
								data: form_data,
						        type:  'post',
					        	success:  function (response) {
									//alert(response);
									if (jQuery.trim(response) == "OK"){
				
										return true;
									}else{
										abrirmodalavisos("Anexo", "Hubo un problema al subir su anexo.");
										return false;
									}
								}
							});
						}
					cargaFotos();
				},error : function(jqXHR, textStatus, errorThrown){
					alert(errorThrown);
				}
			});

			abrirmodalavisos("Imagen","Cambios guardados con éxito.", 500, 200);
		}else{
			abrirmodalavisos("Imagen","Debe capturar al menos una descripción y seleccionar una imagen válida.", 500, 200);
		}
	}


	
	function borrarImagen(){
		debugger;
	$("#texto-confirmacion").html("¿Está seguro que desea borrar el banner de " + $("#cmbimagenesbanner option:selected").text().toUpperCase() + '?');
		
		$( "#dialog-confirm" ).dialog({
			resizable: false,
		    height: "auto",
		    width: 400,
		    modal: true,
		    buttons: {
		        "Aceptar": function() {
		          	$( this ).dialog( "close" );
				
		          	var dataObject = { id_elemento: $("#cmbimagenesbanner").val(),
					//nombreimagen: $("#nombreimagen").val(),
					catalogo: 'imagenesbanner'
					}
					$.ajax({
						data:  dataObject,
				      //  url:   'ajax/ajax_borra_tema_sala.php',
				  		url:   'ajax/ajax_borrado_logico.php',
						type:  'post',
				      	success:  function (response) {
							//alert(response);
							
							if (jQuery.trim(response) == "OK"){
								abrirmodalavisos("Eliminar banner","Banner Eliminado con éxito.", 500, 200);
								cargaFotos();
							}				
						}
					});
		    	},
		        "Cancelar": function() {
		          $( this ).dialog( "close" );
		          return false;
	           }
		    }
	    });	
	}
	/*function borrarImagen(){
		

		var dataObject = { id_detalle: $("#cmbimagenesbanner").val()}
		$.ajax({
				data:  dataObject,
        url:   'ajax/ajax_borra_imagen_banner.php',
        type:  'post',
      	success:  function (response) {
					cargaFotos();
				}
		});
	}*/
	function readURL(input) {
		var imgPath = $("#archivoImagen").val();
		var extn = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
		if (extn == "jpg" || extn == "jpeg") {
    	if (input.files && input.files[0]) {
     		var reader = new FileReader();
	      reader.onload = function (e) {
	      	$('#imgPreview').attr('src', e.target.result);
	      }
	      reader.readAsDataURL(input.files[0]);
	   	}
		}else{
			abrirmodalavisos("Archivo seleccionado", "Debe seleccionar un archivo de imagen válido.<br>(Extensión JPG)", '750px', '450px');
		}
	}
	$("#buscaArchivo").click(function() {
    $("input[id='archivoImagen']").click();
	});
</script>


<?
include '../inputs.php';
?>