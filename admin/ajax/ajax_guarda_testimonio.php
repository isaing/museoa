<?
require 'vs.php';
require_once '../includes/funcs.php';

$usuario = $_SESSION['NOM'];
$idTestimonio = $_POST["id_testimonio"];
$idioma = $_POST["idioma"];
$testimonio = reemplazacomilla($_POST["testimonio"]);
$persona = reemplazacomilla($_POST["persona"]);
$orden = $_POST["orden"];
$principal = $_POST["principal"];
$estatus = $_POST["estatus"];

$sentencia = "CALL paGuardaTestimonio('".$idTestimonio."','".$idioma."','".$testimonio."','".$persona."','".$orden."','".$principal."','".$estatus."','".$usuario."')";

$resultado = consulta($sentencia);
while ($row = $resultado->fetch_array(MYSQLI_ASSOC)){
	if ($row["res"] == "OK"){
		$indice = $row["id_testimonio"];
		
		if ($idTestimonio == 0) {
			echo "<tr id=".$indice.">";
		}
  	echo "<td style='display:none' class='noexcel'><label id='lblestatus".$indice."'>".$row["estatus"]."</label><label id='lblid".$indice."'>".$indice."</label><label id='lblididioma".$indice."'>".$row["ididioma"]."</label><label id='lblprincipal".$indice."'>".$row["principal"]."</label><label id='lblarchivo".$indice."'>".$archivo."</label></td>";
		echo "<td class='texto-centrado noexcel'><a title='Editar' href='#' id='editar' class='mostrar-detalle' data-indice='".$indice."'><i class='fa fa-edit icono-tablas' aria-hidden='true'></i></a></td>";
		echo "<td class='texto-centrado noexcel'><label>".semaforoestatus($row["estatus"])."</label></td>";
		echo "<td class='texto-izquierda'><label id='lbltestimonio".$indice."'>".$row["testimonio"]."</label></td>";
		echo "<td class='texto-izquierda'><label id='lblpersona".$indice."'>".$row["persona"]."</label></td>";
		
		echo "<td class='texto-izquierda'><label id='lblorden".$indice."'>".$row["orden"]."</label></td>";
		echo "<td class='texto-izquierda'><label id='lblidioma".$indice."'>".$row["idioma"]."</label></td>";
		if ($row["principal"] == 1){
			echo "<td><i class='fa fa-star icono-tablas' aria-hidden='true'></i></td>";
		}else{
			echo "<td></td>";
		}
		if ($idTestimonio == 0) {
			echo "</tr>";
		}
    echo "IDOK".$indice;
	}else{
		echo $row["msj"];
	}
}
$resultado->close();

/* bitácora*/
if($idTestimonio==0)
{	$accion="INSERTA";	
}
else{
	$accion="ACTUALIZA";	
}
$sentencia = "CALL paGuardaBitacora('".$accion."','ctestimonios','".$indice."','".$usuario."')";
$resultado = consulta($sentencia);
$resultado->close();
?>
