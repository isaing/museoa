<?
require 'vs.php';
require_once '../includes/funcs.php';
?>
<div class="contenedor">

<div class="row">
<div class="col11"></div>
<div class="col1">
			<!--<a title="Agregar Planifica" href="#" class="mostrar-detalle" data-indice="-1">
			<div class="btn-flotante">
				<i class="fa fa-plus btn-flotante-texto"></i>
			</div>
			</a>-->
</div>
</div>

<section>
<table id="tabla-principal" class="tabla-datos">
<thead>
<tr>
<th class="coltabla0 noexcel"></th>
<th class="col-iconos texto-centrado noexcel"></th>
<th class="col-iconos texto-centrado noexcel"></th>
<th class="coltabla4 texto-derecha">Título</th>
<th class="coltabla5 texto-derecha">Url</th>

</tr>
</thead>
<tbody>
<?
	$filtro = $_POST['filtro'];
	cargaDatos($filtro);
?>
</tbody>
</table>


</section>

<section>
<div id="tabladetalle" style="background-color:#fff;z-index:99001" class="modalcapa modalcapa1">
	<div id="tabs">
		<ul>
    	<li id="tab-datos"><a href="#tabs-1">Datos Maqueta</a></li>
    	<li id="tab-foto"><a href="#tabs-2">Imagen 1</a></li>
        <li id="tab-icono"><a href="#tabs-3">Imagen 2</a></li>
         <li id="tab-traduccion"><a href="#tabs-4">Agregar Traducción</a></li>
        <li id="tab-traducciones"><a href="#tabs-5">Traducciones</a></li>
			
  	</ul>
		<div id="tabs-1">
			<table id="detalle" class="tabla-datos">
				<tbody class="contenidomodal">
					
					<tr>
						<td class="col3 texto-derecha">*Título:
						  <input id="lblid" type="hidden">
						  <input  id="txtID" type="hidden" /></td>
						<td class="col9 texto-izquierda"><label id="lbltitulo"></label></td>
						
					</tr>
                      <tr>
					   <td class="col3 texto-derecha">*Url
				       </td>
					   <td class="col9 texto-izquierda">
					     <input class="control" id="txturl" required="required" />
					</td>
					  
			      </tr>
                  <tr>
						<td class="col3 texto-derecha">Ventana destino:</td>
						<td class="col4 texto-izquierda"><? agregacombodestinosurl(); ?></td>
					</tr>
					<tr id='fila-estado'>
						<td class="col3 texto-derecha">Estado:</td>
						<td class="col2 texto-izquierda"><? agregacomboestatus(); ?></td>
					</tr>
				</tbody>
			</table>
		</div>
        <div id="tabs-2">JPG
			<form id="form1" runat="server">
	        <img id="imgPreview" width='50%'  src="#" alt="Da clic para seleccionar la imagen " />
					<br>
					<input id="archivoImagen" type='file' accept="image/image/jpeg" onchange="readURL(this);" style="display:none" />
	    </form>
		</div>
        <div id="tabs-3">JPG
			<form id="form2" runat="server">
	        <img id="icoPreview" width='50%'  src="#" alt="Da clic para seleccionar la imagen " />
					<br>
					<input id="archivoIcono" type='file' accept="image/jpeg" onchange="readURLico(this);" style="display:none" />
	    </form>
		</div>
         <div id="tabs-4">
			<table id="detalle" class="tabla-datos">
				<tbody class="contenidomodal">
                <tr id="fila-idioma">
                   	   <td class="col3 texto-derecha">*Idioma: </td>
                   	   <td class="col4 texto-izquierda"><div id="comboidiomas"><? agregacombo('idiomas','2');?></div></td>
    				 </tr>
					<tr>
						<td class="col3 texto-derecha">*Título Traducción:						  </td>
						<td class="col9 texto-izquierda"><input class="control" id="txttitulotraduccion" required></td>
					</tr>
                    <tr>
						<td class="col3 texto-derecha">*Subtítulo Traducción:						  </td>
						<td class="col9 texto-izquierda"><input class="control" id="txtsubtitulotraduccion" required></td>
					</tr>
                      <tr>
						<td class="col3 texto-derecha">*Descripcion Traducción:</td>
						<td class="col9 texto-izquierda"><textarea id="txtdescripciontraduccion" class="control-area"></textarea></td>
					</tr>
                  <!-- <tr id='fila-estado'>
						<td class="col3 texto-derecha">Estado:</td>
						<td class="col2 texto-izquierda"><? agregacomboestatus(); ?></td>
					</tr>-->
					<tr>
						<td class="col3 texto-derecha"></td>
						<td class="col9 texto-izquierda">
							<div class="row">
								
								<div class="col4"><br>
									<input type="button" id="agregar-traduccion" name="enviar" title="Agregar traducción" class="btn-formulario" value="Agregar traducción">
								</div>
							</div>
						</td>
					</tr>
				
					
				 
				</tbody>
			</table>

		</div>
        
        
        <div id="tabs-5" class='alto-fijo-modal2'>
			<div class="row">
				<div class="col2 texto-derecha">Idioma:</div>
				<div class="col9"><div id="div-combo-traducciones"> </div></div>
			</div>
			<div class="row" id="fila-datos-traducciones">
				<div class="col12" >
					<div id="datos-traducciones-pais" class='alto-fijo-modal2'>

					</div>
				</div>
				
			</div>
		</div>
    <div style="margin-top:15px; padding-bottom:10px; width:100%; padding-left:550px;" id="espera"></div>
		<div class="row" id="areabotones" style="padding:10px;">
		<div class="col8"></div>
		<div class="col2 texto-centrado">
			<input type="button" id="guardar" name="enviar" title="Guardar" class="btn-formulario" value="Guardar">
			<img id="espera2" src='img/loading.gif' class="icono-espera">
		</div>
		<div class="col2">
			<input type="button" id="cerrar" name="cerrar" title="Cerrar" class="btn-formulario modalcerrar" value="Cerrar">
		</div>
	</div>
    
    

</div>
</div>
</section>

<?
function cargaDatos($filtro){
	$strSQL = "CALL paCatalogoMaqueta('%".$filtro."%')";
	//echo $strSQL;
	$resultado = consulta($strSQL);
	while ($row = $resultado->fetch_array(MYSQLI_ASSOC)){
		$indice = $row["id_maqueta"];
		echo "<tr id=".$indice.">";
		echo "<td style='display:none' class='noexcel'>
		<label id='lblestatus".$indice."'>".$row["estatus"]."</label><label id='lblid".$indice."'>".$indice."</label><label id='lbltarget".$indice."'>".$row["target"]."</label></td>";
		echo "<td class='texto-centrado noexcel'><a title='Editar' href='#' id='editar' class='mostrar-detalle' data-indice='".$indice."'><i class='fa fa-edit icono-tablas' aria-hidden='true'></i></a></td>";
		echo "<td class='texto-centrado noexcel'><label>".semaforoestatus($row["estatus"])."</label></td>";
		echo "<td class='texto-izquierda'><label id='lbltitulo".$indice."'>".$row["titulo"]."</label></td>";
		echo "<td class='texto-izquierda'><label id='lblurl".$indice."'>".$row["url"]."</label></td>";
		
		
		
		
		echo "</tr>";
	}
	$resultado->close();
}
?>

<script>
	$(document).ready(function() {
		$("#tabs").tabs();
		$(".modalcapa1").draggable({ cursor: "move", handle: ".titulomodal", opacity: 0.5 });
		$("#espera2").hide();
		$("#tabladetalle").hide();
		Crearlisteners();
		
	});

	                
	$(".mostrar-detalle").unbind("click").bind('click', function () {
		mostrarDetalle($(this).data("indice"));
		
  });


	function abrirmodalc1(width, height) {
		Ajustatamanocapa(".modalcapa1", width, height);
		$(".modalgeneralcapa1").fadeIn();
		$(".modalcapa1").fadeIn();
		return;
	}

	function Ajustatamanocapa(Control, width, height) {
		$(Control).css('width', width);
		$(Control).css('left', 'calc(50% - ' + width + '/2)');
		$(Control).css('top', 'calc(50% - ' + height + '/2)');

		$(Control + ' .contenidomodal').css('height', 'calc(' + height + ' - 140px)');

		return;
	}
	function cerrarmodalc1() {
		$(".modalgeneralcapa1").fadeOut();
		$('.modalcapa1').fadeOut();
	}

	function Crearlisteners(){
		$('.modalcerrar').click(function(){
			
			cerrarmodalc1();
			return false;
		});
	}

	$("#guardar").click(function() {
		imgsrc=0;
		icosrc=0;
		 if($("#imgPreview").attr('src')!=" ")
		 {
			 imgsrc=1;
		}
		 if($("#icoPreview").attr('src')!=" ")
		 {
			 icosrc=1;
		}
		
			/*debugger;
			alert($('#txturl').val());*/
		if ($('#txturl').val() != ''  && ($("#archivoImagen").val() != "" || imgsrc==1) && ($("#archivoIcono").val() != "" || icosrc==1)){
			var ban = 0;
			var dataObject = { id_maqueta: $("#lblid").text(),
			url: $("#txturl").val(),
			target: $("#cmbdestino").val(),
			estatus: $("#cmbestatus").val()};
			var idOk = "0";
			$.ajax({
				data:  dataObject,
				url:   'ajax/ajax_guarda_maqueta.php',
        		type:  'post',
				async: false,
	      beforeSend: function () {
					$("#guardar").hide();
          $("#espera2").show();
        },
        success:  function (response) {
					var id = $("#lblid").text();
					
					cerrarmodalc1();
					$("#espera2").hide();
					$("#guardar").show();
					var pos = response.indexOf("IDOK");
					idOk = response.substr(pos + 4);
					response = response.replace("IDOK" + idOk,"");
					if (response.substring(0,3) == "<tr" || response.substring(0,3) == "<td"){
						if (id == 0) {
							if ($('#tabla-principal > tbody > tr').length == 0){
								$('#tabla-principal > tbody ').html(response);
							}else{
								$('#tabla-principal > tbody > tr').eq(0).before(response);
							}
						}else{
							$("#"+id).html(response);
						}
						subeFoto(idOk);
						subeIcono(idOk);
						$("#imgPreview").attr('src','../modulos/img/maqueta/maqueta_a-' + id + ".jpg?' + Math.random()");
						$("#icoPreview").attr('src','../modulos/img/maqueta/maqueta_b-' + id + ".jpg?' + Math.random()");
						
					}else{
						abrirmodalavisos('Cargos',response, '800px', '450px');
					}

					$(".mostrar-detalle").unbind("click").bind('click', function () {
						mostrarDetalle($(this).data("indice"));
				  });
				}

       		});
		}else{
			abrirmodalavisos("Planifica tu visita", "Debe capturar la url,  la imagen 1 e imagen 2  que desea guardar", '750px', '450px');
		}
	});
function subeFoto(idC){
	
	var file_data = $("#archivoImagen").prop("files")[0];
	var form_data = new FormData();
	var tipoext="jpg";
	form_data.append("tipoext", tipoext);
	form_data.append("file", file_data);
	form_data.append("maqueta", '_a');
	$.ajax({
		data: form_data,
    url:   'ajax/ajax_sube_foto.php?num=' + idC + "&tipo=maqueta",
    type:  'post',
		cache: false,
		async: false,
		contentType: false,
		processData: false,
		success:  function (response) {
			$("#imgPreview").attr('src','../modulos/img/maqueta/maqueta_a' + idC + ".jpg?' + Math.random()");
		}
	});
}
function subeIcono(idC){
	
	var file_data = $("#archivoIcono").prop("files")[0];
	var form_data = new FormData();
	form_data.append("file", file_data);
	var tipoext="jpg";
	form_data.append("tipoext", tipoext);
	form_data.append("maqueta", '_b');
	$.ajax({
		data: form_data,
		url:   'ajax/ajax_sube_foto.php?num=' + idC + "&tipo=maqueta",
    	type:  'post',
		cache: false,
		async: false,
		contentType: false,
		processData: false,
		success:  function (response) {
			//alert(response);
			$("#icoPreview").attr('src','../modulos/img/maqueta/maqueta_b-' + idC + ".jpg?' + Math.random()");
		}
	});
}


	function mostrarDetalle(i) {
		$('#imgPreview').attr('src', ' ');
		$("#archivoImagen").val("");
		$('#icoPreview').attr('src', ' ');
		$("#archivoIcono").val("");
		$("#espera2").hide();
		$( "#tabs" ).tabs( "option", "active", 0 );
		if (i != "-1"){
			$("#lblid").html($('#lblid' + i).html());
			$("#txtID").val(i);
			$("#lbltitulo").html($('#lbltitulo' + i).html());
			$("#txturl").val($('#lblurl' + i).html());
			$("#cmbdestino").val($('#lbltarget' + i).html());
			$("#cmbestatus").val($("#lblestatus" + i).html());
			$("#imgPreview").attr('src','../modulos/img/maqueta/maqueta_a-' + $("#lblid" + i ).html()+ ".jpg?" + Math.random());
			$("#icoPreview").attr('src','../modulos/img/maqueta/maqueta_b-' + $("#lblid" + i ).html()+ ".jpg?" + Math.random());
			$("#tab-traduccion").show();
			$("#tab-traducciones").show();
			cargaTraducciones($('#lblid' + i).html());
			
		}else{
			$("#lblid").html("0");
			$("#txtpregunta").val("");
			$("#txtdescripcion").val("");
			$("#txtorden").val(0);
			$("#cmbidiomas").val(1);
			$("#cmbestatus").val(1);
			$("#tab-traduccion").hide();
			$("#tab-traducciones").hide();
		}
		abrirmodalc1('750px', '450px');

		return false;
	}
	function readURL(input) {
		var imgPath = $("#archivoImagen").val();
		var extn = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
		//if (extn == "gif" || extn == "png" || extn == "jpg" || extn == "jpeg") {
		if (extn == "jpg" ) {
    	if (input.files && input.files[0]) {
     		var reader = new FileReader();
	      reader.onload = function (e) {
			  
	      	$('#imgPreview').attr('src', e.target.result);
					
	      }
	      reader.readAsDataURL(input.files[0]);
	   	}
		}else{
			abrirmodalavisos("Archivo seleccionado", "Debe seleccionar un archivo de imagen válido.<br>(Extensión JPG, GIF, PNG)", '750px', '450px');
		}
	}
	function readURLico(input) {
		var imgPath = $("#archivoIcono").val();
		var extn = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
		if (extn == "jpg" ) {
    	if (input.files && input.files[0]) {
     		var reader = new FileReader();
	      reader.onload = function (e) {
			  
	      	$('#icoPreview').attr('src', e.target.result);
					
	      }
	      reader.readAsDataURL(input.files[0]);
	   	}
		}else{
			abrirmodalavisos("Archivo seleccionado", "Debe seleccionar un archivo de imagen válido.<br>(Extensión SVG)", '750px', '450px');
		}
	}

	$("#imgPreview").click(function() {
    $("input[id='archivoImagen']").click();
	});
	$("#icoPreview").click(function() {
    $("input[id='archivoIcono']").click();
	});
	
	
/* traducciones*/
	$("#agregar-traduccion").click(function() {
		if ($('#txttitulotraduccion').val() != '' && $('#txtsubtitulotraduccion').val() != ''
		&& $('#txtdescripciontraduccion').val() != '' && $("#cmbidiomas").val()!='' ){
			
			var idDetalle = 0;
			var datos = {id_detalle:0,
					id_maqueta:$("#lblid").text(),
					idioma: $("#cmbidiomas").val(),
					titulo:$("#txttitulotraduccion").val(),
					subtitulo:$("#txtsubtitulotraduccion").val(),
					descripcion:$("#txtdescripciontraduccion").val()
					};
			$.ajax({
				data: datos,
		    url:   'ajax/ajax_guarda_traduccion_maqueta.php',
		    type:  'post',
				async: false,
				success:  function (response) {
					idDetalle = response;
					
					//alert(response);
					if(response=='NO')
					{
						abrirmodalavisos("Agregar traduccion","La traducción capturada de la maqueta por idioma ya existe en la base de datos.");
						
					}
					else{
						//alert(idDetalle);
			
					cargaTraducciones($("#lblid").text());
					
						abrirmodalavisos("Agregar traducción","Traducción subida con éxito.");
					}
					
					},error : function(jqXHR, textStatus, errorThrown){
					alert(errorThrown);
				}
				
			});
			$("#txttitulotraduccion").val("");
			$("#txtsubtitulotraduccion").val("");
			$("#txtdescripciontraduccion").val("");
			$("#cmbidiomas").val(1);
		}else{
			abrirmodalavisos("Agregar traducción","Debe capturar el título, subtítulo y la descripción de la Traducción y el idioma.");
		}
	})
	
	function cargaTraducciones(id){
		//debugger;
		var dataObject = { catalogo: "traduccionesmaqueta",
		id_extra: $("#txtID").val()}
		$.ajax({
				data:  dataObject,
        url:   'ajax/ajax_carga_combo.php',
        type:  'post',
      	success:  function (response) {
					//alert(response);
					$("#div-combo-traducciones").html(response);
					$( "#cmbtraduccionesmaqueta" ).change(function() {
						CargaDatosTraduccion($("#cmbtraduccionesmaqueta").val());
					});
					if ($("#cmbtraduccionesmaqueta").val() != null){
						$("#fila-datos-traducciones").show();
						CargaDatosTraduccion($("#cmbtraduccionesmaqueta").val());
					}else{
						$("#cmbtraduccionesmaqueta").hide();
						$("#div-combo-traducciones").html("<strong>No existen traducciones para la maqueta</strong>");
						$("#fila-datos-traducciones").hide();
					}
				}
			});
	}
	function CargaDatosTraduccion(idD){
		var dataObject = { id_detalle: idD}
		$.ajax({
				data:  dataObject,
        url:   'ajax/ajax_carga_datos_traduccion_maqueta.php',
        type:  'post',
      	success:  function (response) {
					//alert(response);
					$("#datos-traducciones-pais").html(response);
					$("#borra-traduccion").unbind("click").bind('click', function () {
						borrarTraduccion();
					});
					$("#guardarediciontraduccion").click(function(){
						cambiarDatosTraduccion();
					})
				}
		});
	}
	function cambiarDatosTraduccion(){
		if ($('#txttitulotraduccionedicion').val() != '' && $('#txtsubtitulotraduccionedicion').val() != ''
		&& $('#txtdescripciontraduccionedicion').val() != ''  ){
		
			//debugger;
			var idDetalle = $("#cmbtraduccionesmaqueta").val();
			var datos = {id_detalle:idDetalle,
					id_maqueta:0,
					idioma:0,
					titulo:$("#txttitulotraduccionedicion").val(),
					subtitulo:$("#txtsubtitulotraduccionedicion").val(),
					descripcion:$("#txtdescripciontraduccionedicion").val()
					
					
					};
			$.ajax({
				data: datos,
		    url:   'ajax/ajax_guarda_traduccion_maqueta.php',
		    type:  'post',
				async: false,
				success:  function (response) {
					idDetalle = response;
					
					cargaTraducciones(idDetalle);
				},error : function(jqXHR, textStatus, errorThrown){
					alert(errorThrown);
				}
			});

			abrirmodalavisos("Traducción","Cambios guardados con éxito.", 500, 200);
		}else{
			abrirmodalavisos("Traducción","Debe capturar el título, subtítulo y la descripción de la Traducción y el idioma.");
		}
	}
	function borrarTraduccion(){
		

		var dataObject = { id_detalle: $("#cmbidiomastraducciones").val()}
		$.ajax({
				data:  dataObject,
        url:   'ajax/ajax_borra_traduccion_maqueta.php',
        type:  'post',
      	success:  function (response) {
				//alert(response);
				 $("#comboidiomas").html(response);
					cargaTraducciones();
				}
		});
	}
	
</script>
<?
include '../inputs.php';
?>
