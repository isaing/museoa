<?
require 'vs.php';
require_once '../includes/funcs.php';
  
  $idTema = $_POST["id_tema"];
	$strSQL = "CALL paCargaDatosTemaSala('".$idTema."')";
	
	$resultado = consulta($strSQL);
  $n = 0;
	while ($row = $resultado->fetch_array(MYSQLI_ASSOC)){
    $n = $n + 1;
	echo "<div class='row'>";
	echo "<div class='col9'>";
    echo "<table id='detalle' class='tabla-datos'>";
    echo "<tbody class='contenidomodal'>";
    echo "<tr>
            <td class='col3 texto-derecha'>*Título del tema:</td>
            <td class='col9 texto-izquierda'><input class='control' id='txttituloedicion' required value='".$row['titulo']."'></td>
          </tr>";
	echo "<tr>
            <td class='col3 texto-derecha'>*Subtítulo del tema:</td>
            <td class='col9 texto-izquierda'><input class='control' id='txtsubtituloedicion' required value='".$row['subtitulo']."'></td>
          </tr>";
	echo "	  <tr>
			<td class='col3 texto-derecha'>*Descripción del tema:<input id='lblid' type='hidden'></td>
						<td class='col9 texto-izquierda'><textarea id='txtdescripciontemaedicion' class='control-area'>".$row['descripcion']."</textarea></td>
					</tr>";
			 echo "<tr>
            <td class='col3 texto-derecha'>*URL Destino:</td>
            <td class='col9 texto-izquierda'><input class='control' id='txturledicion' value='".$row['url']."'></td>
          </tr>";		
		echo "<tr>			 <td class='col3 texto-derecha'>Ventana destino:</td>
            <td class='col4 texto-izquierda'>
              <select id='cmbdestinoedicion' class='control-combo'>";
              if ($row['target'] == '_blank'){
                echo "<option value='_blank' selected>Nueva ventana</option>";
                echo "<option value='_self'>Misma ventana</option>";
              }else{
                echo "<option value='_blank'>Nueva ventana</option>";
                echo "<option value='_self' selected>Misma ventana</option>";
              }
          echo "</select></td></tr>";
	/*echo "<tr id='fila-color'>
                   	   <td class='col3 texto-derecha'>Color de Fondo: </td>
                   	   <td class='col3 texto-izquierda'>";
					   agregacombo('coloresedicion','1');
		echo "<input type='hidden' id='txtcoloredicion' value='".$row['color']."'></td>
                       <td class='col3 texto-izquierda'> <div id='div-color-edicion' style='padding:3px'>Texto</div>
    				 </tr>";*/
			echo "<tr>
                       <td class='col3 texto-derecha'>Color de Fondo</td>
                       <td class='col3 texto-izquierda'><input type='text' id='colorfondoedicion' name='colorfondoedicion' value='".$row['colorfondo']."' />
                         <div id='colorpickeredicion1'></div></td>
                         <td class='col2 texto-derecha'>Color de Texto</td>
                       <td class='col3 texto-izquierda'><input type='text' id='colortextoedicion' name='colortextoedicion' value='".$row['colortexto']."' />
                         <div id='colorpickeredicion2'></div></td>
                       <td class='col1 texto-izquierda'>                     
                  </tr>	";		
					
 
		 
		 
     echo "<tr>
            
		  <td class='col2 texto-derecha'>Estado:</td>
						<td class='col3 texto-izquierda'>
						<select id='cmbestatusedicion' class='control-combo'>";
              if ($row['estatus'] == '1'){
				  echo "<option value='1' selected>ALTA</option>
				  <option value='0'>BAJA</option>";
              }else{
                echo "<option value='1' >ALTA</option>
				  <option value='0' selected>BAJA</option>";
              }
          echo "</select>
						
	</td><td class='col3 texto-derecha'>Orden:</td>
            <td class='col2 texto-derecha'><input type='number' class='control' id='txtordenelementoedicion' required value='".$row['orden']."'></td>
          </tr>";
   
   
	echo "
   
   </tbody></table>";
   echo "</div>";
   echo "<div class='col3'>JPG/PNG/GIF (916 x 386px)";
 /* echo "<div id='div-combo-iconos-edicion'></div>
							
							<img width='80' height='80' id='img-icono-edicion' src='".$row['icono']."'>";
		
      echo "<input type='hidden' value='".$row['icono']."' id='imgiconoedicion'></div>";*/
	  echo " <img id='imgPreviewTemaEdicion' width='100%'  src='../modulos/img/salastemas/".$row['imagen']."?".rand()."' alt='Da clic para seleccionar la imagen ' />
					<br>
					<input id='archivoImagenTemaEdicion' type='file' accept='image/jpeg, image/png, image/gif' onchange='readURLTemaEdicion(this);' style='display:none'/><input id='nombreimagen' type='hidden' value='".$row['imagen']."'></div>";
	}
	$resultado->close();
if ($n > 0){
?>
  <div class="row" id="areabotonesedicion">
    <div class="col6"></div>
    <div class="col3 texto-centrado">
      <input type="button" id="guardaredicion" name="enviar" title="Guardar" class="btn-formulario" value="Guardar elemento">
    </div>
    <div class="col3">
      <input type="button" id="borrarelemento" name="borrar" title="Borrar" class="btn-formulario borra-imagen" value="Borrar elemento">
    </div>
  </div>
<? } ?>
<script>
	$(document).ready(function() {
		$('#colorpickeredicion1').farbtastic('#colorfondoedicion');
		$('#colorpickeredicion2').farbtastic('#colortextoedicion');
		coloredicion=$("#txtcoloredicion").val();
		
		
		$("#imgPreviewTemaEdicion").click(function() {
		$("input[id='archivoImagenTemaEdicion']").click();
		});
	});
</script>
