<?
require 'vs.php';
require_once '../includes/funcs.php';
/*error_reporting(E_ALL);
ini_set('display_errors', '1');*/
session_start();
$idPerfil = $_SESSION['IDP'];


?><head>
<link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Raleway" />
</head>
<!-- Import jQuery -->

<!--<script src="//ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="js/vendor/jquery-3.2.1.min.js"><\/script>')</script>-->




<div class="contenedor">

<div class="row">
<div class="col11"></div>
<div class="col1">
			<a title="Agregar imagen" href="#" class="mostrar-detalle" data-indice="-1">
			<div class="btn-flotante">
				<i class="fa fa-plus btn-flotante-texto"></i>
			</div>
			</a>
</div>
</div>

<section>
<table id="tabla-principal" class="tabla-datos">
<thead>
<tr>
<th class="coltabla0 noexcel"></th>
<th class="col-iconos texto-centrado noexcel"></th>
<th class="col-iconos texto-centrado noexcel"></th>
<th class="coltabla1 texto-derecha">Id Adjunto</th>
<th class="coltabla3 texto-derecha">Título</th>
<th class="coltabla2 texto-derecha">Enlace</th>


</tr>
</thead>
<tbody>
<?
	$filtro = $_POST['filtro'];
	cargaDatos($filtro);
?>
</tbody>
</table>


</section>

<section>
<div id="tabladetalle" style="background-color:#fff;z-index:99001" class="modalcapa modalcapa1">
	
	<div id="tabs" class="titulomodal">
		<ul>
    		<li id="tab-datos"><a href="#tabs-1">Datos del adjunto</a></li>
  			<!--<li id="tab-foto"><a href="#tabs-2">Imagen</a></li>
        	<li id="tab-archivo"><a href="#tabs-3">Archivo Pdf</a></li>-->
  		</ul>
		<div id="tabs-1">
			<table id="detalle" class="tabla-datos">
				<tbody class="contenidomodal">
               
					<tr>
						<td class="col3 texto-derecha">*Título:
						  <input id="lblid" type="hidden"></td>
						<td class="col5 texto-izquierda"><input class="control" id="txttitulo" required></td>
					</tr>
					
					<tr>
						<td class="col3 texto-derecha">*Seleccione el archivo (pdf)</td>
						<td class="col9 texto-izquierda">
							
							<div id='div-agregar-anexo'>				
								<input id="archivo-anexo" type="file" class="inputfile" accept=".PDF, .pdf, application/pdf">
								<label for="archivo-anexo" id="lbl-archivo-anexo">
									<i class="fa fa-file-pdf-o" style="padding-top:0px"></i>&nbsp;Seleccione el archivo
								</label><br /><br />
                               <a href="#" id="pdfPreviewArchivo" target="_blank" ><i class="fa fa-file-pdf-o" style="padding-top:0px"></i>&nbsp;Ver Archivo</a> 
                                <input type="hidden"  id='anexoanterior'/>
							</div>
							<div id='div-ver-anexo'></div>
						
						</td>
					</tr>
					<tr id='fila-estado'>
						<td class="col3 texto-derecha">Estado:</td>
						<td class="col2 texto-izquierda"><? agregacomboestatus(); ?></td>
					</tr>
				</tbody>
			</table>
		</div>
	<div style="margin-top:15px; padding-bottom:10px; width:100%; padding-left:550px;" id="espera"></div>
		<div class="row" id="areabotones" style="padding:10px;">
		<div class="col8"></div>
		<div class="col2 texto-centrado">
			<input type="button" id="guardar" name="enviar" title="Guardar" class="btn-formulario" value="Guardar">
			<img id="espera2" src='img/loading.gif' class="icono-espera">
		</div>
		<div class="col2">
			<input type="button" id="cerrar" name="cerrar" title="Cerrar" class="btn-formulario modalcerrar" value="Cerrar">
		</div>
	</div>
	</div>
    
	</div>
</section>
</div>


<?
function cargaDatos($filtro){
	$strSQL = "CALL paCatalogoAdjuntos('%".$filtro."%')";
	//echo $strSQL;
	$resultado = consulta($strSQL);
	while ($row = $resultado->fetch_array(MYSQLI_ASSOC)){
		$indice = $row["id_adjunto"];
		echo "<tr id=".$indice.">";
		echo "<td style='display:none' class='noexcel'>
		<label id='lblestatus".$indice."'>".$row["estatus"]."</label><label id='lblid".$indice."'>".$indice."</label><label id='lbltitulo".$indice."'>".$row["titulo"]."</label><label id='lblpdf".$indice."'>".$row["adjunto"]."</label></td>";
		echo "<td class='texto-centrado noexcel'><a title='Editar' href='#' id='editar' class='mostrar-detalle' data-indice='".$indice."'><i class='fa fa-edit icono-tablas' aria-hidden='true'></i></a></td>";
		echo "<td class='texto-centrado noexcel'><label>".semaforoestatus($row["estatus"])."</label></td>";
		echo "<td class='texto-izquierda'><label id='lblid".$indice."'>".$indice."</label></td>";
		echo "<td class='texto-izquierda'><label id='lbltitulo".$indice."'>".$row["titulo"]."</label></td>";
		echo "<td class='texto-izquierda'><label id='lblvinculo".$indice."'>adjunto/".$indice."</label></td>";
		echo "</tr>";
	}
	$resultado->close();
}
?>

<script>
	$(document).ready(function() {
		$("#tabs").tabs();
		$(".modalcapa1").draggable({ cursor: "move", handle: ".titulomodal", opacity: 0.5 });
		$("#espera2").hide();
		$("#tabladetalle").hide();
		Crearlisteners();
		
	});

	                
	$(".mostrar-detalle").unbind("click").bind('click', function () {
		mostrarDetalle($(this).data("indice"));
  });
	/*	$("#borra-imagen").unbind("click").bind('click', function () {
						borrarImagen();
	});
	$("#borra-pdf").unbind("click").bind('click', function () {
						borrarPdf();
	});*/

	function abrirmodalc1(width, height) {
		Ajustatamanocapa(".modalcapa1", width, height);
		$(".modalgeneralcapa1").fadeIn();
		$(".modalcapa1").fadeIn();
		return;
	}

	function Ajustatamanocapa(Control, width, height) {
		$(Control).css('width', width);
		$(Control).css('left', 'calc(50% - ' + width + '/2)');
		$(Control).css('top', 'calc(30% - ' + height + '/2)');

		$(Control + ' .contenidomodal').css('height', 'calc(' + height + ' - 140px)');

		return;
	}
	function cerrarmodalc1() {
		$(".modalgeneralcapa1").fadeOut();
		$('.modalcapa1').fadeOut();
	}

	function Crearlisteners(){
		$('.modalcerrar').click(function(){
			cerrarmodalc1();
			return false;
		});
	}

	$("#guardar").click(function() {
		imgsrc=0;
		 if($("#imgPreview").attr('src')!=" ")
		 {
			 imgsrc=1;
			
		}
		imgsrc=0;
		 if($("#imgPreview").attr('src')!=" ")
		 {
			 imgsrc=1;
			
		}
		if ($('#txttitulo').val() != '' && ($("#archivo-anexo").val()!="" || $("#pdfPreviewArchivo").attr("href")!="")){
			id=$("#lblid").text();
			var dataObject = { id_adjunto: $("#lblid").text(),
			titulo: $("#txttitulo").val(),
			estatus: $("#cmbestatus").val()};
			var idOk = "0";
			$.ajax({
				data:  dataObject,
				url:   'ajax/ajax_guarda_adjunto.php',
        type:  'post',
				async: false,
	      beforeSend: function () {
					$("#guardar").hide();
          $("#espera2").show();
        },
        success:  function (response) {
					//alert(response);
					var id = $("#lblid").text();
					cerrarmodalc1();
					$("#espera2").hide();
					$("#guardar").show();
					var pos = response.indexOf("IDOK");
					idOk = response.substr(pos + 4);
					response = response.replace("IDOK" + idOk,"");
					if (response.substring(0,3) == "<tr" || response.substring(0,3) == "<td"){
						if (id == 0) {
							if ($('#tabla-principal > tbody > tr').length == 0){
								$('#tabla-principal > tbody ').html(response);
							}else{
								$('#tabla-principal > tbody > tr').eq(0).before(response);
							}
						}else{
							$("#"+id).html(response);
						}
						if($("#archivo-anexo").prop("files")[0]!="undefined")
						{
							//debugger;
							subePdf(idOk);
						}
						//subeFoto(idOk);
						/*$("#imgPreview").attr('src','../modulos/img/imagenes/imagenes-' + id + ".jpg");*/
						
						/*$("#imgPreview").attr('src','../modulos/img/articulos/articulos-' + id + ".jpg?' + Math.random()");*/
						
					}else{
						abrirmodalavisos('Adjuntos',response, '800px', '450px');
					}

					$(".mostrar-detalle").unbind("click").bind('click', function () {
						mostrarDetalle($(this).data("indice"));
				  });
				}

       		});
		}else{
			abrirmodalavisos("Adjuntos", "Debe capturar el título y el adjunto que desea guardar", '750px', '450px');
		}
	});


function subePdf(idC){
	//alert(idC);
	/*debugger;*/
	
	var file_data = $("#archivo-anexo").prop("files")[0];
		//alert(file_data);
	if(file_data!=undefined)
	{
	var form_data = new FormData();
	
	form_data.append("file", file_data);
	//alert(form_data);
	$.ajax({
	data: form_data,
    url:   'ajax/ajax_sube_pdf.php?num=' + idC + "&tipo=adjuntos",
    type:  'post',
		cache: false,
		async: false,
		contentType: false,
		processData: false,
		success:  function (response) {
			//alert(response);
			//$("#pdfPreview").attr('src','../modulos/img/cabeceras/cabeceras' + idC + ".jpg");
			$("#pdfPreviewArchivo").attr('href','../modulos/img/adjuntos/adjuntos-' + idC + ".pdf");
			//$("#lblreglas").html($('programasreglasoperacion-' + idC + '.pdf').html());
			$("#lblpdfarchivo").text('adjuntos-' + idC + '.pdf');
			//$("#pdfPreviewArchivo").attr('src','../modulos/img/cabeceras/cabeceras' + idC + ".jpg");
			
		}
	});
	}else
	{
		$('#archivo-anexo').attr({ value: '' });
	}
	
}	

function borrarPdf(){
		id=$("#lblid").text();
		//alert(id);
		var dataObject = { id_detalle: id}
		$.ajax({
				data:  dataObject,
        url:   'ajax/ajax_borra_pdf_articulo.php',
        type:  'post',
      	success:  function (response) {
					//cargaFotos();
					//alert(response);
					$("#pdfPreview").attr('src','');
					$("#borra-pdf").hide();
					$("#pdfPreviewArchivo").hide();
					$("#lblpdfarchivo").html('');
					cerrarmodalc1();
					$("#lbltextopdf"+id).html("");
					$("#lblpdf"+id).html("");
					/*$archivoPdf = $('#archivoPdf');
					$archivoPdf.replaceWith($archivoPdf.clone(true));*/
				
				}
		});
		
	}
	function mostrarDetalle(i) {
		$("#lbl-archivo-anexo").html("<i class='fa fa-file-text' style='padding-top:0px'></i>&nbsp;Seleccione el archivo");
		$("#espera2").hide();
		$( "#tabs" ).tabs( "option", "active", 0 );
		if (i != "-1"){
			$("#lblid").html($('#lblid' + i).html());
			$("#cmbdependencias").val($("#lbliddependencia" + i).html());
			$("#txttitulo").val($('#lbltitulo' + i).html());
			$("#cmbestatus").val($("#lblestatus" + i).html());
			$("#lblimagen").html($('#lblimagen' + i).html());
			$("#txttextopdf").val($('#lbltextopdf' + i).html());
			$("#lblpdf").html($('#lblpdf' + i).html());
			/*$archivoPdf = $('#archivoPdf');
				$archivoPdf.replaceWith($archivoPdf.clone(true));
				$archivoImagen = $('#archivoImagen');
				$archivoImagen.replaceWith($archivoImagen.clone(true));*/
			$('#archivoPdf').val("");
			$('#archivoImagen').val("");
			
			if ($("#lblpdf" + i).html() != ""){
				//alert("si");
				//$("#lbl-archivo-anexo").html("<i class='fa fa-file-text' style='padding-top:0px'></i>&nbsp;" + $("#lblpdf"+ i).html().split('\\').pop());
				$("#anexoanterior").val($("#lblpdf"+ i).html().split('\\').pop());
				$("#pdfPreviewArchivo").show();
				$("#pdfPreviewArchivo").attr('href','../modulos/img/adjuntos/adjuntos-' + i + ".pdf?" + Math.random());
				
			}
			
			/*if ($("#lblimagen" + i).html() != ""){
				
				$("#imgPreview").attr('src','../modulos/img/articulos/articulos-' + $("#lblid" + i ).html()+ ".jpg?" + Math.random());
				$("#borra-imagen").show();
			}
			else{
				$("#imgPreview").attr('src','');
				$("#borra-imagen").hide();
				$("#lblimagenarchivo").html('');
			}*/
		}else{
			$("#tab-permisos").hide();
			$("#lblid").html("0");
			$("#cmbdependencias").val(0);
			$("#txttitulo").val("");
			$("#cmbestatus").val(1);
			$("#lblpdfarchivo").html('');
			$("#pdfPreviewArchivo").attr("href", "#");
			
			$("#anexoanterior").val("");
			$("#pdfPreviewArchivo").hide();
		}
		abrirmodalc1('750px', '450px');

		return false;
	}

	function readURL(input) {
		var imgPath = $("#archivoImagen").val();
		var extn = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
		if (extn == "gif" || extn == "png" || extn == "jpg" || extn == "jpeg") {
    	if (input.files && input.files[0]) {
     		var reader = new FileReader();
	      reader.onload = function (e) {
			  
	      	$('#imgPreview').attr('src', e.target.result);
					
	      }
	      reader.readAsDataURL(input.files[0]);
	   	}
		}else{
			abrirmodalavisos("Archivo seleccionado", "Debe seleccionar un archivo de imagen válido.<br>(Extensión JPG, GIF, PNG)", '750px', '450px');
		}
	}
	function readURLpdf(input) {
		/*alert("readURL");
		debugger;*/
		var pdfPath = $("#archivoPdf").val();
		var extn = pdfPath.substring(pdfPath.lastIndexOf('.') + 1).toLowerCase();
		if (extn == "pdf" || extn == "PDF") {
    	if (input.files && input.files[0]) {
     		var reader = new FileReader();
	      reader.onload = function (e) {
	      	//$('#pdfPreview').attr('src', e.target.result);
			/*$("#pdfPreviewArchivo").attr('href','../modulos/img/programasreglasoperacion/programasreglasoperacion' + idC + ".pdf");*/
			//$('#pdfPreviewArchivo').attr('href', pdfPath);
			/*$("#pdfPreviewArchivo").attr('href','../modulos/img/programasreglasoperacion/programasreglasoperacion-' + idC + ".pdf");
			$("#lblreglas").html($('../modulos/img/programasreglasoperacion/programasreglasoperacion-' + idC + '.pdf' + i).html());*/
	      }
	      reader.readAsDataURL(input.files[0]);
	   	}
		}else{
			abrirmodalavisos("Archivo seleccionado", "Debe seleccionar un archivo pdf válido.<br>(Extensión PDF)", '750px', '450px');
		}
	}

	$("#archivo-anexo").change(function() {
		if ($("#archivo-anexo").val().split('\\').pop() != ""){
			$("#lbl-archivo-anexo").html("<i class='fa fa-file-text' style='padding-top:0px'></i>&nbsp;" + $("#archivo-anexo").val().split('\\').pop());
		}else{
			$("#lbl-archivo-anexo").html("<i class='fa fa-file-text' style='padding-top:0px'></i>&nbsp;Seleccione el archivo");
		}
  	});  
</script>
<?
include '../inputs.php';
?>
