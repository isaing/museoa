<?
require 'vs.php';
require_once '../includes/funcs.php';
session_start();
$idPerfil = $_SESSION['IDP'];

?><head>
<link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Raleway" />


</head>


<div class="contenedor">

<div class="row">
<div class="col11"></div>
<div class="col1">
			<a title="Agregar imagen" href="#" class="mostrar-detalle" data-indice="-1">
			<div class="btn-flotante">
				<i class="fa fa-plus btn-flotante-texto"></i>
			</div>
			</a>
</div>
</div>

<section>
<table id="tabla-principal" class="tabla-datos">
<thead>
<tr>
<th class="coltabla0 noexcel"></th>
<th class="col-iconos texto-centrado noexcel"></th>
<th class="col-iconos texto-centrado noexcel"></th>
<th class="coltabla1 texto-derecha">Id </th>
<th class="coltabla2 texto-derecha">Número de sala</th>
<th class="coltabla3 texto-derecha">Sala</th>
<th class="coltabla2 texto-derecha">Idioma</th>
<th class="col-iconos texto-centrado noexcel"></th>


</tr>
</thead>
<tbody>
<?
	$filtro = $_POST['filtro'];
	cargaDatos($filtro);
?>
</tbody>
</table>


</section>

<section>
<div id="tabladetalle" style="background-color:#fff;z-index:99001" class="modalcapa modalcapa1">
	<div id="tabs" class="titulomodal">
		<ul>
    	<li id="tab-datos"><a href="#tabs-1">Datos de la Sala</a></li>
        <li id="tab-datos2"><a href="#tabs-1b">Estilo de la Sala</a></li>
  		<li id="tab-foto"><a href="#tabs-2">Imagen</a></li>
        <li id="tab-agregar"><a href="#tabs-3">Agregar Tema</a></li>
        <li id="tab-elementos"><a href="#tabs-4">Temas de la Sala</a></li>
  	</ul>
		<div id="tabs-1">
			<table id="detalle" class="tabla-datos">
				<tbody class="contenidomodal">
                <tr id="id-banner">
						<td class="col3 texto-derecha">ID Sala:</td>
						<td class="col3 texto-izquierda"><strong><input class="control" id="txtID" required></strong></td>
					</tr>
                    <tr id="fila-idioma">
                   	   <td class="col3 texto-derecha">*Idioma: </td>
                   	   <td class="col3 texto-izquierda"><? agregacombo('idiomas','2');?></td>
                       <td class="col3 texto-derecha">Sala Especial: </td>
                   	   <td class="col3 texto-izquierda"><input name="chkespecial" type="checkbox"  id="chkespecial"/></td>
    				 </tr>
                    <tr>
						<td class="col3 texto-derecha">*Número de Sala:						  </td>
						<td class="col3 texto-izquierda"><? agregacombo('numerosalas','2');?></td>
                        <td class="col3 texto-derecha">Nomenclatura:</td>
						<td class="col2 texto-izquierda"><input class="control" id="txtnomenclatura" required></td>
					</tr>
					<tr>
						<td class="col3 texto-derecha">*Nombre:
					    <input id="lblid" type="hidden" /></td>
						<td class="col5 texto-izquierda"><input class="control" id="txtnombre" required></td>
					</tr>
                   
                   <!-- <tr>
						<td class="col3 texto-derecha">Temática:</td>
						<td class="col5 texto-izquierda"><input class="control" id="txttematica" required></td>
					</tr>-->
					
					<tr>
						<td class="col3 texto-derecha">Objetivo:</td>
						<td class="col9 texto-izquierda"><textarea id="txtobjetivo" class="control-area"></textarea></td>
					</tr>
                   <tr>
						<td class="col3 texto-derecha">*Descripcion:</td>
						<td class="col9 texto-izquierda"><textarea id="txtdescripcion" class="control-area"></textarea></td>
					</tr>
                    <tr>
						<td class="col3 texto-derecha">Frase de Ámbito:</td>
						<td class="col9 texto-izquierda"><textarea id="txtfrase" class="control-area"></textarea></td>
					</tr>
                    <tr>
						<td class="col3 texto-derecha">Autor de Frase:</td>
						<td class="col5 texto-izquierda"><input class="control" id="txtautorfrase" required></td>
					</tr>
                    <tr>
						<td class="col3 texto-derecha">Video:</td>
						<td class="col5 texto-izquierda"><input class="control" id="txtvideo" required></td>
                        <td class="col2 texto-derecha">Estado:</td>
						<td class="col2 texto-izquierda"><? agregacomboestatus(); ?></td>
					</tr>
                    
                   
					
						
					
				</tbody>
			</table>
		</div>
         <div id="tabs-1b">
         <div class="row" id="datos-colores">
         
        	 </div>
			<!--<table id="detalle" class="tabla-datos">
				<tbody class="contenidomodal">
					
                     <tr>
                       <td class="col3 texto-derecha">Color de Título de Sala</td>
                       <td class="col3 texto-izquierda"><input type="text" id="colortitulosala" name="colortitulosala" value="#0097AC"  pattern="^#+([a-fA-F0-9]{6}|[a-fA-F0-9]{3})$" />
                         <div id="colorpicker3"></div></td>
                         <td class="col3 texto-derecha">Color de Autor de Frase de sala</td>
                       <td class="col3 texto-izquierda"><input type="text" id="colorautorfrase" name="colorautorfrase" value="#0097AC" pattern="^#+([a-fA-F0-9]{6}|[a-fA-F0-9]{3})$"/>
                         <div id="colorpicker4"></div></td>
                       <td class="col1 texto-izquierda">                     
                  </tr>
                    <tr>
                       <td class="col3 texto-derecha">Color de Texto</td>
                       <td class="col3 texto-izquierda"><input type="text" id="colortextosala" name="colortextosala" value="#000000"  pattern="^#+([a-fA-F0-9]{6}|[a-fA-F0-9]{3})$" />
                         <div id="colorpicker5"></div></td>
                                          
                  </tr>
                   
					
                   
					
				</tbody>
			</table>-->
		</div>
		<div id="tabs-2">Jpg (1920 x 1363px)
			<form id="form1" runat="server">
	        <img id="imgPreview" width="30%" height="30%" src="#" alt="Da clic para seleccionar la imagen " />
					<br>
					<input id="archivoImagen" type='file' accept=".JPG, .jpg, image/jpeg" onchange="readURL(this);" style="display:none" />
	    </form>
		</div>
        
        
        <div id="tabs-3">
			<table id="detalle" class="tabla-datos">
				<tbody class="contenidomodal">
					<tr>
						<td class="col2 texto-derecha">*Título del tema:
						  </td>
						<td class="col4 texto-izquierda"><input class="control" id="txttitulo" required></td>
                        <td class="col2 texto-derecha">*Subtítulo del tema:						  </td>
						<td class="col4 texto-izquierda"><input class="control" id="txtsubtitulo" required></td>
					</tr>
                  
					<tr>
						<td class="col3 texto-derecha">*Breve Descripción del tema:</td>
						<td class="col9 texto-izquierda"><textarea id="txtdescripciontema" class="control-area"></textarea></td>
					</tr>
                      <tr >
						<td class="col2 texto-derecha">*URL Destino:</td>
						<td class="col5 texto-izquierda"><input class="control" id="txturl"></td>
					<td class="col2 texto-derecha">Ventana destino:</td>
						<td class="col3 texto-izquierda"><? agregacombodestinosurl(); ?></td>
						
					</tr>
                     <tr>
                       <td class="col3 texto-derecha">Color de Fondo</td>
                       <td class="col3 texto-izquierda"><input type="text" id="colorfondo" name="colorfondo" value="#0097AC" pattern="^#+([a-fA-F0-9]{6}|[a-fA-F0-9]{3})$" />
                         <div id="colorpicker1"></div></td>
                         <td class="col2 texto-derecha">Color de Texto</td>
                       <td class="col3 texto-izquierda"><input type="text" id="colortexto" name="colortexto" value="#FFFFFF"  pattern="^#+([a-fA-F0-9]{6}|[a-fA-F0-9]{3})$"/>
                         <div id="colorpicker2"></div></td>
                       <td class="col1 texto-izquierda">                     
                  </tr>
                   
					
                    <tr >
                       <td class="col3 texto-derecha">*Imagen <br /></td>
                       <td class="col9 texto-izquierda">JPG/PNG/GIF (916 x 386px)<form id="form2" runat="server">
	        <img id="imgPreviewTema" width="40%" src="#" alt="Da clic para seleccionar la imagen " /><br>
					<input id="archivoImagenTema" type='file' accept="image/jpeg, image/png, image/gif" onchange="readURLTema(this);" style="display:none" />
	    </form></td>
                  </tr>
                  
					<tr>
						<td class="col3 texto-derecha">Orden:</td>
						<td class="col1 texto-derecha"><input  type="number" class="control" id="txtorden" required value="0"></td>
                        <td class="col2 texto-derecha">Estado:</td>
						<td class="col2 texto-izquierda"><? agregacomboestatus(); ?></td>
                        <td class="col1"></td>
                        <td class="col3">
							<input type="button" id="btn-agregar-tema" name="enviar" title="Agregar tema" class="btn-formulario" value="Agregar Tema">
						</td>
					</tr>
					
				</tbody>
			</table>
		</div>
        
        <div id="tabs-4" class='alto-fijo-modal2'>
			<div class="row">
				<div class="col2 texto-derecha">Tema:</div>
				<div class="col9"><div id="div-combo-elementos"> </div></div>
			</div>
			<div class="row" id="fila-datos-elemento">
				<div class="col12" >
					<div id="datos-elemento">

					</div>
				</div>
			</div>
		</div>
        
        
	  <div style="margin-top:15px; padding-bottom:10px; width:100%; padding-left:550px;" id="espera"></div>
		<div class="row" id="areabotones" >
		<div class="col8"></div>
		<div class="col2 texto-centrado">
			<input type="button" id="guardar" name="enviar" title="Guardar" class="btn-formulario" value="Guardar">
			<img id="espera2" src='img/loading.gif' class="icono-espera">
		</div>
		<div class="col2">
			<input type="button" id="cerrar" name="cerrar" title="Cerrar" class="btn-formulario modalcerrar" value="Cerrar">
		</div>
        
	</div>
	</div>

<div id="dialog-confirm" title="Borrar elemento">
  <p><span class="ui-icon ui-icon-alert" style="float:left; margin:12px 12px 20px 0;"></span><label id='texto-confirmacion'></label>
  </p>
</div>
</div>
</section>

<?
function cargaDatos($filtro){
	$strSQL = "CALL paCatalogoSalas('%".$filtro."%')";
	//echo $strSQL;
	$resultado = consulta($strSQL);
	
	while ($row = $resultado->fetch_array(MYSQLI_ASSOC)){
		$indice = $row["id_sala"];
		echo "<tr id=".$indice.">";
		echo "<td style='display:none' class='noexcel'><label id='lblestatus".$indice."'>".$row["estatus"]."</label><label id='lblid".$indice."'>".$indice."</label><label id='lblididioma".$indice."'>".$row["ididioma"]."</label><label id='lblidnumero".$indice."'>".$row["idnumero"]."</label><label id='lblnomenclatura".$indice."'>".$row["nomenclatura"]."</label><label id='lblobjetivo".$indice."'>".$row["objetivo"]."</label><label id='lbldescripcion".$indice."'>".$row["descripcion"]."</label><label id='lblfrase".$indice."'>".$row["frase"]."</label><label id='lblautorfrase".$indice."'>".$row["autorfrase"]."</label><label id='lblespecial".$indice."'>".$row["especial"]."</label>
		<label id='lblcolortitulosala".$indice."'>".$row["colortitulosala"]."</label><label id='lblcolorautorfrase".$indice."'>".$row["colorautorfrase"]."</label><label id='lblcolortextosala".$indice."'>".$row["colortextosala"]."</label></td>";
		echo "<td class='texto-centrado noexcel'><a title='Editar' href='#' id='editar' class='mostrar-detalle' data-indice='".$indice."'><i class='fa fa-edit icono-tablas' aria-hidden='true'></i></a></td>";
		echo "<td class='texto-centrado noexcel'><label>".semaforoestatus($row["estatus"])."</label></td>";
		echo "<td class='texto-centrado'><label id='lblidmicrositio".$indice."'>".$row["id_sala"]."</label></td>";
		echo "<td class='texto-centrado'><label id='lblnumero".$indice."'>".$row["numero"]."</label></td>";
		echo "<td class='texto-izquierda'><label id='lblsala".$indice."'>".$row["sala"]."</label></td>";
		echo "<td class='texto-izquierda'><label id='lblidioma".$indice."'>".$row["idioma"]."</label></td>";
		if ($row["especial"] == 1){
			echo "<td><i class='fa fa-star icono-tablas' aria-hidden='true'></i></td>";
		}else{
			echo "<td></td>";
		}
		if ($idEvento == 0) {
			echo "</tr>";
		}
		
		echo "</tr>";
		//echo "<td class='texto-izquierda'><label id='lblvinculo".$indice."'>sala.php?s=".$indice."</label></td>";
	}
	$resultado->close();
}
?>

<script>

	$(document).ready(function() {
		/* menus tematicos*/
		$('#colorpicker1').farbtastic('#colorfondo');
		$('#colorpicker2').farbtastic('#colortexto');
		
		
		
	
		
		
		$("#dialog-confirm").hide();
		$("#tabs").tabs();
		$(".modalcapa1").draggable({ cursor: "move", handle: ".titulomodal", opacity: 0.5 });
		$("#espera2").hide();
		$("#tabladetalle").hide();
		Crearlisteners();
		
	});

	                
	$(".mostrar-detalle").unbind("click").bind('click', function () {
		mostrarDetalle($(this).data("indice"));
  });


	function abrirmodalc1(width, height) {
		Ajustatamanocapa(".modalcapa1", width, height);
		$(".modalgeneralcapa1").fadeIn();
		$(".modalcapa1").fadeIn();
		return;
	}

	function Ajustatamanocapa(Control, width, height) {
		$(Control).css('width', width);
		$(Control).css('left', 'calc(50% - ' + width + '/2)');
		$(Control).css('top', 'calc(30% - ' + height + '/2)');

		$(Control + ' .contenidomodal').css('height', 'calc(' + height + ' - 140px)');

		return;
	}
	function cerrarmodalc1() {
		$(".modalgeneralcapa1").fadeOut();
		$('.modalcapa1').fadeOut();
	}

	function Crearlisteners(){
		$('.modalcerrar').click(function(){
			cerrarmodalc1();
			return false;
		});
	}

	$("#guardar").click(function() {
		imgsrc=0;
		especial=0;
		 if($("#imgPreview").attr('src')!="")
		 {
			 imgsrc=1;
			
		}
		if ($("#chkespecial").prop( "checked" )){
				especial = 1;
			}
		//debugger;
		
		var colortitulosala = $("#colortitulosala").val();
		var colorautorfrase= $("#colorautorfrase").val();
		var colortextosala= $("#colortextosala").val();
		
			var isOk1  = /^#[0-9A-F]{6}$/i.test(colortitulosala);
			var isOk2  = /^#[0-9A-F]{6}$/i.test(colorautorfrase);
			var isOk3  = /^#[0-9A-F]{6}$/i.test(colortextosala);
			if(isOk1 == false  || isOk2 ==false  || isOk2 ==false)
			{
				abrirmodalavisos("Sala", "Coloca los colores de la sala correctamente", '800px', '450px');
				return false;
			}
		
		if ($('#cmbnumerosalas').val() != '' && $('#txtnombre').val() != ''   && $('#txtdescripcion').val() != ''   && $("#cmbidiomas").val()!=null && ($("#archivoImagen").val() != "" || imgsrc==1) && $("#colortitulosala").val() != '' && $("#colorautorfrase").val() != '' && $("#colortextosala").val() != ''){
			
			
			
			var dataObject = { id_sala: $("#lblid").text(),
			idioma: $("#cmbidiomas").val(),
			numero: $("#cmbnumerosalas").val(),
			nombre: $("#txtnombre").val(),
			nomenclatura: $("#txtnomenclatura").val(),
			/*tematica: $("#txttematica").val(),*/
			objetivo: $("#txtobjetivo").val(),
			descripcion: $("#txtdescripcion").val(),
			frase: $("#txtfrase").val(),
			autorfrase: $("#txtautorfrase").val(),
			especial: especial,
			colortitulosala: $("#colortitulosala").val(),
			colorautorfrase: $("#colorautorfrase").val(),
			colortextosala: $("#colortextosala").val(),
			estatus: $("#cmbestatus").val()};
			var idOk = "0";
			$.ajax({
				data:  dataObject,
				url:   'ajax/ajax_guarda_sala.php',
        type:  'post',
				async: false,
	      beforeSend: function () {
					$("#guardar").hide();
          $("#espera2").show();
        },
        success:  function (response) {
					var id = $("#lblid").text();
					cerrarmodalc1();
					$("#espera2").hide();
					$("#guardar").show();
					var pos = response.indexOf("IDOK");
					idOk = response.substr(pos + 4);
					
					//alert(idOk);
					response = response.replace("IDOK" + idOk,"");
					if (response.substring(0,3) == "<tr" || response.substring(0,3) == "<td"){
						if (id == 0) {
							if ($('#tabla-principal > tbody > tr').length == 0){
								$('#tabla-principal > tbody ').html(response);
							}else{
								$('#tabla-principal > tbody > tr').eq(0).before(response);
							}
						}else{
							//$("#"+id).html(response);
								$('#tabla-principal > tbody').html(response);
						}
						//alert(response);
					
						
						subeFoto(idOk);
						$("#imgPreview").attr('src','../modulos/img/salas/salas-' + id + ".jpg?' + Math.random()");
					}else{
						abrirmodalavisos('Salas',response, '800px', '450px');
					}

					$(".mostrar-detalle").unbind("click").bind('click', function () {
						mostrarDetalle($(this).data("indice"));
				  });
				}

       		});
		}else{
			abrirmodalavisos("Salas", "Debe capturar el idioma, número de sala, la sala, descripción, la url destino, los colores de la sala y la imagen de la sala que desea guardar", '750px', '450px');
		}
	});

function subeFoto(idC){
	var file_data = $("#archivoImagen").prop("files")[0];
	var form_data = new FormData();
	//debugger;
	form_data.append("file", file_data);
	$.ajax({
		data: form_data,
    url:   'ajax/ajax_sube_foto.php?num=' + idC + "&tipo=salas",
    type:  'post',
		cache: false,
		async: false,
		contentType: false,
		processData: false,
		success:  function (response) {
			$("#imgPreview").attr('src','../modulos/img/salas/salas-' + idC + ".jpg?' + Math.random()");
		}
	});
}
function subeFotoTema(idC){
	var file_data = $("#archivoImagenTema").prop("files")[0];
	var form_data = new FormData();
	//debugger;
	var archivo = $("#archivoImagenTema").val();
   	var tipoext = (archivo.substring(archivo.lastIndexOf(".")+1)).toLowerCase(); 
	//alert(extension);
	form_data.append("file", file_data);
	form_data.append("tipoext", tipoext);
	form_data.append("actualizacampo", 1);
	$.ajax({
		data: form_data,
    url:   'ajax/ajax_sube_foto.php?num=' + idC + "&tipo=salastemas",
    type:  'post',
		cache: false,
		async: false,
		contentType: false,
		processData: false,
		success:  function (response) {
			//alert(response);
			//$("#imgPreviewTema").attr('src','../modulos/img/salastemas/salastemas-' + idC + ".jpg?' + Math.random()");
			$("#imgPreviewTema").attr('src','../modulos/img/salastemas/salastemas-' + idC + "."+ tipoext +"?' + Math.random()");
		}
	});
}

function subeFotoTemaEdicion(idC){
	var file_data = $("#archivoImagenTemaEdicion").prop("files")[0];
	var form_data = new FormData();
	//debugger;
	var archivo = $("#archivoImagenTemaEdicion").val();
   	var tipoext = (archivo.substring(archivo.lastIndexOf(".")+1)).toLowerCase(); 
	form_data.append("file", file_data);
	form_data.append("tipoext", tipoext);
	form_data.append("actualizacampo", 1);
	$.ajax({
		data: form_data,
    url:   'ajax/ajax_sube_foto.php?num=' + idC + "&tipo=salastemas",
    type:  'post',
		cache: false,
		async: false,
		contentType: false,
		processData: false,
		success:  function (response) {
			$("#imgPreviewTemaEdicion").attr('src','../modulos/img/salastemas/salastemas-' + idC + "."+ tipoext +"?' + Math.random()");
		}
	});
}

$("#btn-agregar-tema").click(function() {
	
 			//alert($("#lblid").text());
		 if($("#imgPreviewTema").attr('src')!=" ")
		 {
			 imgsrc=1;
			
		}
		var colorfondo = $("#colorfondo").val();
		var colortexto= $("#colortexto").val();
		
			var isOk1  = /^#[0-9A-F]{6}$/i.test(colorfondo);
			var isOk2  = /^#[0-9A-F]{6}$/i.test(colortexto);
			if(isOk1 == false  || isOk2 ==false)
			{
				abrirmodalavisos("Agregar tema", "Coloca los colores correctamente", '800px', '450px');
				return false;
			}
		if ($('#txttitulo').val() != '' && $('#txtsubtitulo').val() != '' &&$("#txtdescripciontema").val() != '' && $("#txturl").val() != "" && 
		  $("#colorfondo").val() != '' && $("#colortexto").val() != '' && $("#txturl").val() != '' && ($("#archivoImagenTema").val() != "" || imgsrc==1)){
			
			
			
			
			var idDetalle = 0;
				

			var datos = {id_tema:0,
					id_sala:$("#lblid").text(),
					titulo:$("#txttitulo").val(),
					subtitulo:$("#txtsubtitulo").val(),
					descripcion:$("#txtdescripciontema").val(),
					colorfondo: $("#colorfondo").val(),
					colortexto: $("#colortexto").val(),
					url:$("#txturl").val(),
					orden: $("#txtorden").val(),
					target:$("#cmbdestino").val(),
					estatus: $("#cmbestatus").val()};
			var idTema = 0;
			$.ajax({
				data: datos,
			    url:   'ajax/ajax_guarda_tema_sala.php',
			    type:  'post',
				async: false,
				success:  function (response) {
					/*alert (response);
					debugger;*/
					if (jQuery.trim(response) != "NO"){
						idTema = jQuery.trim(response);
						//alert (idTema);
						subeFotoTema(idTema);
						cargaTemas($("#lblid").text());
						
						
					}else{
						abrirmodalavisos("Agregar tema", "Ya existe otro tema con el mismo título", '800px', '450px');
						return false;
					}
				},error : function(jqXHR, textStatus, errorThrown){
					alert(errorThrown);
				}
			});
			$("#txttitulo").val("");
			$("#txtsubtitulo").val("");
			$("#txtdescripciontema").val("");
			$("#colorfondo").val("#0097AC");
			$("#colortexto").val("#FFFFFF");
			$("#txtorden").val("0");
			$("#txturl").val("");
			$("#txttarget").val("");
			$("#imgPreviewTema").attr('src','');
			$("#archivoImagenTema").val("");
			
			
			abrirmodalavisos("Agregar tema","Tema subido con éxito.");
		}else{
			abrirmodalavisos("Agregar tema","Debe capturar al menos un título, subtítulo,  descripción, color de fondo y texto, foto y URL destino");
		}
	})



	

	function mostrarDetalle(i) {
		//alert($('#lblespecial' + i).html());
		//debugger;
		$("#archivoImagen").val("");
		$("#archivoImagenTema").val("");
		$('#imgPreview').attr('src', ' ');
		$('#imgPreviewTema').attr('src', ' ');
		$("#txtID").prop("readonly", true);
		$("#txtID").val("");
		
		$("#chkespecial").prop( "checked", false);
		$("#espera2").hide();
		$("#txttitulo").val("");
		$("#txtsubtitulo").val("");
		$("#txtdescripciontema").val("");
		$("#colorfondo").val("#0097AC");
		$("#colortexto").val("#FFFFFF");
		
		
		/* colores sala*/
		/*$("#colortitulosala").val("#0097AC");
		$("#colorautorfrase").val("#0097AC");
		$("#colortextosala").val("#000000");*/
		/*$('#colorpicker3').farbtastic('#0097AC');
		$('#colorpicker4').farbtastic('#0097AC');
		$('#colorpicker5').farbtastic('#000000');*/
		
		$('#imgPreviewTema').attr('src', ' ');
		$("#archivoImagenTema").val("");
		$( "#tabs" ).tabs( "option", "active", 0 );
		if (i != "-1"){
			$("#lblid").html($('#lblid' + i).html());
			$("#txtID").val(i);
			$("#cmbidiomas").val($('#lblididioma' + i).html());
			$("#cmbnumerosalas").val($('#lblidnumero' + i).html());
			$("#txtnombre").val($('#lblsala' + i).html());
			$("#txtnomenclatura").val($('#lblnomenclatura' + i).html());
			/*$("#txttematica").val($('#lbltematica' + i).html());*/
			$("#txtobjetivo").val($('#lblobjetivo' + i).html());
			$("#txtdescripcion").val($('#lbldescripcion' + i).html());
			$("#txtfrase").val($('#lblfrase' + i).html());
			$("#txtautorfrase").val($('#lblautorfrase' + i).html());
			$("#txtvideo").val($('#lblvideo' + i).html());
			/* colores default*/
			/*if($('#lblcolortitulosala' + i).html()!='')
			{
				$("#colortitulosala").val($('#lblcolortitulosala' + i).html());
				alert($("#colortitulosala").val());
				$('#colorpicker3').farbtastic($("#colortitulosala").val());
			}
			if($('#lblcolorautorfrase' + i).html()!='')
			{
				$("#colorautorfrase").val($('#lblcolorautorfrase' + i).html());
				alert($("#colorautorfrase").val());
				//$('#colorpicker4').farbtastic($("#colorautorfrase").val());
				$('#colorpicker4').farbtastic($("#colorautorfrase").val());
			}
			if($('#lblcolortextosala' + i).html()!='')
			{
				$("#colortextosala").val($('#lblcolortextosala' + i).html());
				alert($("#colortextosala").val());
				$('#colorpicker5').farbtastic($("#colortextosala").val());
			}*/
			
		/*	color=''+$('#lblcolortitulosala' + i).html();
			alert(color);
			$("#colortitulosala").val(color);
			var s= document.getElementById('colortitulosala');
  			s.value = color;*/
			/*$("#colorautorfrase").val($('#lblcolorautorfrase' + i).html());
			$("#colortextosala").val($('#lblcolortextosala' + i).html());*/
			/* Sala*/
			/*$('#colorpicker3').farbtastic('#colortitulosala');
			$('#colorpicker4').farbtastic('#colorautorfrase');
			$('#colorpicker5').farbtastic('#colortextosala');*/
			//$('#colorpicker3').setColor($("#colortitulosala").val());
			//$.farbtastic('#colorpicker3').setColor($("#colortitulosala").val());
			

			/*$('#colorpicker3').farbtastic('#colortitulosala');
			$('#colorpicker4').farbtastic('#colorautorfrase');
			$('#colorpicker5').farbtastic('#colortextosala');*/
			
			
		
			
			$("#cmbestatus").val($("#lblestatus" + i).html())
			if ($("#lblimagen" + i).html() != ""){
				$("#imgPreview").attr('src','../modulos/img/salas/salas-' + $("#lblid" + i ).html()+ ".jpg?" + Math.random());
			}
			if($('#lblespecial' + i).html()==1)
			{
				$("#chkespecial").prop( "checked", true);
			}
			$("#chkbanner").prop( "checked", false);
			$("#tab-agregar").show();
			$("#tab-elementos").show();
			cargaTemas($('#lblid' + i).html());
			cargaColores($('#lblcolortitulosala' + i).html(),$('#lblcolorautorfrase' + i).html(),$('#lblcolortextosala' + i).html());
		}else{
			
			
	  		$("#imgPreview").attr('src','');
			$("#archivoImagen").val("");
			$("#tab-agregar").hide();
			$("#tab-elementos").hide();
			$("#lblid").html("0");
			$("#cmbnumerosalas").val(1);
			$("#cmbidiomas").val(1);
			$("#txtnumero").val("");
			$("#txtnombre").val("");
			$("#txtnomenclatura").val("");
			/*$("#txttematica").val("");*/
			$("#txtobjetivo").val("");
			$("#txtdescripcion").val("");
			$("#txtfrase").val("");
			$("#txtautorfrase").val("");
			$("#txtvideo").val("");
			/* colores sala*/
		/*$("#colortitulosala").val("#0097AC");
		$("#colorautorfrase").val("#0097AC");
		$("#colortextosala").val("#000000");*/
		/*$('#colorpicker3').farbtastic('#colortitulosala');
		$('#colorpicker4').farbtastic('#colorautorfrase');
		$('#colorpicker5').farbtastic('#colortextosala');*/
			cargaColores("#0097AC","#0097AC","#000000");
			$("#cmbestatus").val(1);
			
		}
		abrirmodalc1('90%', '450px');

		return false;
	}

	function readURL(input) {
		var imgPath = $("#archivoImagen").val();
		var extn = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
		if (extn == "gif" || extn == "png" || extn == "jpg" || extn == "jpeg") {
    	if (input.files && input.files[0]) {
     		var reader = new FileReader();
	      reader.onload = function (e) {
			  
	      	$('#imgPreview').attr('src', e.target.result);
					
	      }
	      reader.readAsDataURL(input.files[0]);
	   	}
		}else{
			abrirmodalavisos("Archivo seleccionado", "Debe seleccionar un archivo de imagen válido.<br>(Extensión JPG, GIF, PNG)", '750px', '450px');
		}
	}
	
	function readURLTema(input) {
		var imgPath = $("#archivoImagenTema").val();
		var extn = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
		if (extn == "gif" || extn == "png" || extn == "jpg" || extn == "jpeg") {
    	if (input.files && input.files[0]) {
     		var reader = new FileReader();
	      reader.onload = function (e) {
			  
	      	$('#imgPreviewTema').attr('src', e.target.result);
					
	      }
	      reader.readAsDataURL(input.files[0]);
	   	}
		}else{
			abrirmodalavisos("Archivo seleccionado", "Debe seleccionar un archivo de imagen válido.<br>(Extensión JPG, GIF, PNG)", '750px', '450px');
		}
	}
	function readURLTemaEdicion(input) {
		var imgPath = $("#archivoImagenTemaEdicion").val();
		var extn = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
		if (extn == "gif" || extn == "png" || extn == "jpg" || extn == "jpeg") {
    	if (input.files && input.files[0]) {
     		var reader = new FileReader();
	      reader.onload = function (e) {
			  
	      	$('#imgPreviewTemaEdicion').attr('src', e.target.result);
					
	      }
	      reader.readAsDataURL(input.files[0]);
	   	}
		}else{
			abrirmodalavisos("Archivo seleccionado", "Debe seleccionar un archivo de imagen válido.<br>(Extensión JPG, GIF, PNG)", '750px', '450px');
		}
	}
	
/*	function cargaIconosEdicion(){
		var idDependenciasesion = $("#iddependenciasesion").val();
		var idDependencia = $("#cmbdependencias").val();
		var datos = {id_dependencia: idDependencia,id_dependenciasesion: idDependenciasesion};
		//alert(idDependenciasesion);
		$.ajax({
			data: datos,
		    url:   'ajax/ajax_carga_iconos_micrositio_edicion.php',
		    type:  'post',
			async: false,
			success:  function (response) {
				$("#div-combo-iconos-edicion").html(response);
				if (jQuery.trim(response) != ''){
					$("#img-icono-edicion").prop("src",$("#cmb-iconos-edicion").val());
				}
				else{
					$("#img-icono-edicion").prop("src","");
				}
				$( " #cmb-iconos-edicion").change(function() {
					//alert("aqui");
					$("#img-icono-edicion").prop("src",$("#cmb-iconos-edicion").val());
				});

			},error : function(jqXHR, textStatus, errorThrown){
				alert(errorThrown);
			}
		});
	}*/
	
	function cargaTemas(){
		/*alert($("#txtID").val());*/
		//debugger;
		var dataObject = { catalogo: "temassalas",
		id_extra: $("#txtID").val()}
		$.ajax({
			data:  dataObject,
	        url:   'ajax/ajax_carga_combo.php',
	        type:  'post',
      		success:  function (response) {
				//alert(response);
				$("#div-combo-elementos").html(response);
				$( "#cmbtemassalas" ).change(function() {
					CargaDatosTema($("#cmbtemassalas").val());
				});
				if ($("#cmbtemassalas").val() != null){
					$("#fila-datos-elemento").show();
					CargaDatosTema($("#cmbtemassalas").val());
				}else{
					$("#fila-datos-elemento").hide();
					$("#div-combo-elementos").html("<strong>No existen elementos en esta sección.</strong>");
				}
			}
		});
	}
	function CargaDatosTema(idT){
		//debugger;
		
		var dataObject = { id_tema: idT}
		$.ajax({
			data:  dataObject,
	        url:   'ajax/ajax_carga_datos_tema_salas.php',
	        type:  'post',
	      	success:  function (response) {
				
				$("#datos-elemento").html(response);
				//cargaIconosEdicion();
				//alert($("#imgiconoedicion").val());
				//$("#cmb-iconos-edicion").val($("#imgiconoedicion").val());
				//$("#img-icono-edicion").prop("src",$("#imgiconoedicion").val());
				$("#borrarelemento").unbind("click").bind('click', function () {
					borrarElemento();
				});
				$("#guardaredicion").click(function(){
					cambiarDatosTema();
				})
				
			}
		});
	}
	
function cambiarDatosTema(){
		imgsrc=0;
		if($("#imgPreviewTemaEdicion").attr('src')!=" ")
		 {
			 imgsrc=1;
			
		}
		//debugger;
		if ($('#txttituloedicion').val() != '' && $('#txtsubtituloedicion').val() != '' &&$("#txtdescripciontemaedicion").val() != ''  && $("#txturledicion").val() != ''
		 && ($("#archivoImagenTemaEdicion").val() != "" || imgsrc==1)){
		//if ($('#txtelementoedicion').val() != '' && $("#txtdescripcionelementoedicion").val() != '' && $("#txturledicion").val() != ''){
			var colorfondo = $("#colorfondoedicion").val();
			var colortexto= $("#colortextoedicion").val();
			var isOk1  = /^#[0-9A-F]{6}$/i.test(colorfondo);
			var isOk2  = /^#[0-9A-F]{6}$/i.test(colortexto);
			if(isOk1 == false  || isOk2 ==false)
			{
				abrirmodalavisos("Agregar tema", "Coloca los colores correctamente", '800px', '450px');
				return false;
			}
			var idTema = $("#cmbtemassalas").val();
			//alert($("#img-icono-edicion").attr('src'));
			//alert($("#cmb-iconos-edicion").val());
			var datos = {id_tema:idTema,
					id_sala:$("#lblid").text(),
					id_tema:idTema,
					titulo:$("#txttituloedicion").val(),
					subtitulo:$("#txtsubtituloedicion").val(),
					descripcion:$("#txtdescripciontemaedicion").val(),
					colorfondo: $("#colorfondoedicion").val(),
					colortexto: $("#colortextoedicion").val(),
					url:$("#txturledicion").val(),
					orden: $("#txtordenelementoedicion").val(),
					target:$("#cmbdestinoedicion").val(),
					estatus: $("#cmbestatusedicion").val() };
			$.ajax({
				data: datos,
			    url:   'ajax/ajax_guarda_tema_sala.php',
		        type:  'post',
				async: false,
				success:  function (response) {
					/*debugger;*/
				//alert(response);
					if($("#archivoImagenTemaEdicion").prop("files")[0]=="[object File]")
						{
					subeFotoTemaEdicion(idTema);
						}
					if (jQuery.trim(response) =="OK"){
						//debugger;
						$("#txtID").val(idTema);
						cargaTemas();
					}
					cargaTemas();
				},error : function(jqXHR, textStatus, errorThrown){
					alert(errorThrown);
				}
			});
			
			abrirmodalavisos("Modificar tema","Cambios guardados con éxito.", 500, 200);
		}else{
			abrirmodalavisos("Modificar tema","Debe capturar al menos un título, subtítulo,  descripción, url,  imagen y URL destino");
		}
	}


	function borrarElemento(){
		//alert("aqui");
		//debugger;
		$("#texto-confirmacion").html("¿Está seguro que desea borrar el elemento " + $("#cmbtemassalas option:selected").text().toUpperCase() + '?');
		$( "#dialog-confirm" ).dialog({
			resizable: false,
		    height: "auto",
		    width: 400,
		    modal: true,
		    buttons: {
		        "Aceptar": function() {
		          	$( this ).dialog( "close" );
		          	var dataObject = { id_elemento: $("#cmbtemassalas").val(),
					//nombreimagen: $("#nombreimagen").val(),
					catalogo: 'menutematico'
					}
					$.ajax({
						data:  dataObject,
				      //  url:   'ajax/ajax_borra_tema_sala.php',
				  		url:   'ajax/ajax_borrado_logico.php',
						type:  'post',
				      	success:  function (response) {
							//alert(response);
							if (jQuery.trim(response) == "OK"){
								abrirmodalavisos("Eliminar Tema","Tema Eliminado con éxito.", 500, 200);
								cargaTemas();	
							}				
						}
					});
		    	},
		        "Cancelar": function() {
		          $( this ).dialog( "close" );
		          return false;
	           }
		    }
	    });		
	}
	function cargaColores(colorTitulo,colorAutor,colorTexto){
		//debugger;
		
		var dataObject = { colorTitulo: colorTitulo, colorAutor:colorAutor, colorTexto:colorTexto}
		$.ajax({
			data:  dataObject,
	        url:   'ajax/ajax_carga_colores_salas.php',
	        type:  'post',
	      	success:  function (response) {
				//alert(response);
				$("#datos-colores").html(response);
				
				
			}
		});
	}


	$("#imgPreview").click(function() {
    $("input[id='archivoImagen']").click();
	});
	$("#imgPreviewTema").click(function() {
    $("input[id='archivoImagenTema']").click();
	});
	
</script>
<?
include '../inputs.php';
?>
