<?
require 'vs.php';
require_once '../includes/funcs.php';

?><head>
<link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Raleway" />
</head>


<div class="contenedor">

<div class="row">
<div class="col11"></div>
<div class="col1">
			<a title="Agregar idioma" href="#" class="mostrar-detalle" data-indice="-1">
			<div class="btn-flotante">
				<i class="fa fa-plus btn-flotante-texto"></i>
			</div>
			</a>
</div>
</div>

<section>
<table id="tabla-principal" class="tabla-datos">
<thead>
<tr>
<th class="coltabla0 noexcel"></th>
<th class="col-iconos texto-centrado noexcel"></th>
<th class="col-iconos texto-centrado noexcel"></th>
<th class="coltabla4 texto-derecha">Id</th>
<th class="coltabla4 texto-derecha">Idioma</th>
<th class="coltabla4 texto-derecha">Abreviatura</th>
<!--<th class="coltabla3 texto-derecha">ícono</th>-->


</tr>
</thead>
<tbody>
<?
	$filtro = $_POST['filtro'];
	cargaDatos($filtro);
?>
</tbody>
</table>


</section>

<section>
<div id="tabladetalle" style="background-color:#fff;z-index:99001" class="modalcapa modalcapa1">
	<div id="tabs" class="titulomodal">
		<ul>
    	<li id="tab-datos"><a href="#tabs-1">Datos del Idioma</a></li>
  		<!--<li id="tab-foto"><a href="#tabs-2">Icono</a></li>-->
  	</ul>
		<div id="tabs-1">
			<table id="detalle" class="tabla-datos">
				<tbody class="contenidomodal">
					<tr>
						<td class="col3 texto-derecha">*Idioma:
						  <input id="lblid" type="hidden"></td>
						<td class="col5 texto-izquierda"><input class="control" id="txtidioma" required></td>
					</tr>
                   <!-- <tr>
						<td class="col3 texto-derecha">*Abreviatura:
						  <input id="lblid" type="hidden"></td>
						<td class="col5 texto-izquierda"><input class="control" id="txtabreviatura" maxlength="2"></td>
					</tr>-->
					
					
					
					<tr id='fila-estado'>
						<td class="col3 texto-derecha">Estado:</td>
						<td class="col2 texto-izquierda"><? agregacomboestatus(); ?></td>
					</tr>
				</tbody>
			</table>
		</div>
	<!--	<div id="tabs-2">
			<form id="form1" runat="server">
	        <img id="imgPreview" width="100" height="50" src="#" alt="Da clic para seleccionar la imagen " />
					<br>
					<input id="archivoImagen" type='file' accept=".JPG, .jpg, image/jpeg" onchange="readURL(this);" style="display:none" />
	    </form>
		</div>-->
		<div style="margin-top:15px; padding-bottom:10px; width:100%; padding-left:550px;" id="espera"></div>
		<div class="row" id="areabotones" style="padding:10px;">
		<div class="col8"></div>
		<div class="col2 texto-centrado">
			<input type="button" id="guardar" name="enviar" title="Guardar" class="btn-formulario" value="Guardar">
			<img id="espera2" src='img/loading.gif' class="icono-espera">
		</div>
		<div class="col2">
			<input type="button" id="cerrar" name="cerrar" title="Cerrar" class="btn-formulario modalcerrar" value="Cerrar">
		</div>
	</div>
	</div>

</div>
</section>

<?
function cargaDatos($filtro){
	$rutaimagen="../modulos/img/idiomas/idiomas-";
	$strSQL = "CALL paCatalogoIdiomas('%".$filtro."%')";
	
	$resultado = consulta($strSQL);
	while ($row = $resultado->fetch_array(MYSQLI_ASSOC)){
		$indice = $row["id_idioma"];
		echo "<tr id=".$indice.">";
		echo "<td style='display:none' class='noexcel'><label id='lblestatus".$indice."'>".$row["estatus"]."</label><label id='lblid".$indice."'>".$indice."</label><label id='lblidioma".$row["idioma"]."'>".$indice."</label><label id='lblicono".$row["icono"]."'>".$indice."</label></td>";
		echo "<td class='texto-centrado noexcel'><a title='Editar' href='#' id='editar' class='mostrar-detalle' data-indice='".$indice."'><i class='fa fa-edit icono-tablas' aria-hidden='true'></i></a></td>";
		echo "<td class='texto-centrado noexcel'><label>".semaforoestatus($row["estatus"])."</label></td>";
		echo "<td class='texto-centrado'><label id='lblididioma".$indice."'>".$row["id_idioma"]."</label></td>";
		echo "<td class='texto-izquierda'><label id='lblidioma".$indice."'>".$row["idioma"]."</label></td>";
		echo "<td class='texto-izquierda'><label id='lblabreviatura".$indice."'>".$row["abreviatura"]."</label></td>";
		/*echo "<td class='texto-izquierda'><label id='lblicono".$indice."'><img src='".$rutaimagen.$indice.".jpg' width='100' height='50'></label></td>";*/
		echo "</tr>";
	}
	$resultado->close();
}
?>

<script>
	$(document).ready(function() {
		$("#tabs").tabs();
		$(".modalcapa1").draggable({ cursor: "move", handle: ".titulomodal", opacity: 0.5 });
		$("#espera2").hide();
		$("#tabladetalle").hide();
		Crearlisteners();
	});

	                
	$(".mostrar-detalle").unbind("click").bind('click', function () {
		mostrarDetalle($(this).data("indice"));
  });


	function abrirmodalc1(width, height) {
		Ajustatamanocapa(".modalcapa1", width, height);
		$(".modalgeneralcapa1").fadeIn();
		$(".modalcapa1").fadeIn();
		return;
	}

	function Ajustatamanocapa(Control, width, height) {
		$(Control).css('width', width);
		$(Control).css('left', 'calc(50% - ' + width + '/2)');
		$(Control).css('top', 'calc(50% - ' + height + '/2)');

		$(Control + ' .contenidomodal').css('height', 'calc(' + height + ' - 140px)');

		return;
	}
	function cerrarmodalc1() {
		$(".modalgeneralcapa1").fadeOut();
		$('.modalcapa1').fadeOut();
	}

	function Crearlisteners(){
		$('.modalcerrar').click(function(){
			cerrarmodalc1();
			return false;
		});
	}

	$("#guardar").click(function() {
		//if ($('#txtidioma').val() != '' && $('#imgPreview').attr('src')!=' '){
			if ($('#txtidioma').val() != '' ){
			var dataObject = { id_idioma: $("#lblid").text(),
			idioma: $("#txtidioma").val(),
			//abreviatura: $("#txtabreviatura").val(),
			estatus: $("#cmbestatus").val()};
			var idOk = "0";
			$.ajax({
				data:  dataObject,
				url:   'ajax/ajax_guarda_idioma.php',
        type:  'post',
				async: false,
	      beforeSend: function () {
					$("#guardar").hide();
          $("#espera2").show();
        },
        success:  function (response) {
					var id = $("#lblid").text();
					cerrarmodalc1();
					$("#espera2").hide();
					$("#guardar").show();
					var pos = response.indexOf("IDOK");
					idOk = response.substr(pos + 4);
					response = response.replace("IDOK" + idOk,"");
					if (response.substring(0,3) == "<tr" || response.substring(0,3) == "<td"){
						if (id == 0) {
							if ($('#tabla-principal > tbody > tr').length == 0){
								$('#tabla-principal > tbody ').html(response);
							}else{
								$('#tabla-principal > tbody > tr').eq(0).before(response);
							}
						}else{
							$("#"+id).html(response);
						}
						//subeFoto(idOk);
						
						
					/*	$("#imgPreview").attr('src','../modulos/img/idiomas/idiomas-' + id + ".jpg?' + Math.random()");*/
					}else{
						abrirmodalavisos('Idiomas',response, '800px', '450px');
					}

					$(".mostrar-detalle").unbind("click").bind('click', function () {
						mostrarDetalle($(this).data("indice"));
				  });
				}

       		});
		}else{
			abrirmodalavisos("Idiomas", "Debe capturar el idioma que desea guardar", '750px', '350px');
		}
	});

/*function subeFoto(idC){
	var file_data = $("#archivoImagen").prop("files")[0];
	var form_data = new FormData();

	form_data.append("file", file_data);
	$.ajax({
		data: form_data,
    url:   'ajax/ajax_sube_foto.php?num=' + idC + "&tipo=idiomas",
    type:  'post',
		cache: false,
		async: false,
		contentType: false,
		processData: false,
		success:  function (response) {
			
			$("#imgPreview").attr('src','../modulos/img/idiomas/idiomas-' + idC + ".jpg?' + Math.random()");
		}
	});
}*/

	function mostrarDetalle(i) {
		$('#imgPreview').attr('src', ' ');
		$("#espera2").hide();
		$( "#tabs" ).tabs( "option", "active", 0 );
		if (i != "-1"){
			$("#lblid").html($('#lblid' + i).html());
			$("#txtidioma").val($('#lblidioma' + i).html());
			//$("#txtabreviatura").val($('#lblabreviatura' + i).html());
			$("#cmbestatus").val($("#lblestatus" + i).html())
			/*if ($("#lblicono" + i).html() != ""){
				$("#imgPreview").attr('src','../modulos/img/idiomas/idiomas-' + $("#lblid" + i ).html()+ ".jpg?" + Math.random());
			}*/
		}else{
			$("#tab-permisos").hide();
			$("#lblid").html("0");
			$("#txtidioma").val("");
			//$("#txtabreviatura").val("");
			$("#cmbestatus").val(1);
		}
		abrirmodalc1('750px', '450px');

		return false;
	}

	/*function readURL(input) {
		var imgPath = $("#archivoImagen").val();
		var extn = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
		if (extn == "gif" || extn == "png" || extn == "jpg" || extn == "jpeg") {
    	if (input.files && input.files[0]) {
     		var reader = new FileReader();
	      reader.onload = function (e) {
			  
	      	$('#imgPreview').attr('src', e.target.result);
					
	      }
	      reader.readAsDataURL(input.files[0]);
	   	}
		}else{
			abrirmodalavisos("Archivo seleccionado", "Debe seleccionar un archivo de imagen válido.<br>(Extensión JPG)", '100px', '50px');
		}
	}*/

	/*$("#imgPreview").click(function() {
    $("input[id='archivoImagen']").click();
	});*/
</script>
<?
include '../inputs.php';
?>
