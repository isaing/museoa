<?
require 'vs.php';
require_once '../includes/funcs.php';

$usuario = $_SESSION['NOM'];
$idDatosGenerales = $_POST["id_datosgenerales"];
$idioma = intval($_POST["idioma"]);
$direccion = reemplazacomilla($_POST["direccion"]);
$telefono = reemplazacomilla($_POST["telefono"]);
$correo = reemplazacomilla($_POST["correo"]);
$latitud = $_POST["latitud"];
$longitud = $_POST["longitud"];
$horarios = reemplazacomilla($_POST["horarios"]);
$titulopdf = $_POST["titulopdf"];
$estatus = $_POST["estatus"];

$sentencia = "CALL paGuardaDatosGenerales('".$idDatosGenerales."','".$idioma."','".$direccion."','".$telefono."','".$correo."','".$latitud."','".$longitud."','".$horarios."','".$titulopdf."','".$estatus."','".$usuario."')";
$resultado = consulta($sentencia);
while ($row = $resultado->fetch_array(MYSQLI_ASSOC)){
	if ($row["res"] == "OK"){
		$indice = $row["id_datosgenerales"];
		if ($idDatosGenerales == 0) {
			echo "<tr id=".$indice.">";
		}
 		echo "<td style='display:none' class='noexcel'><label id='lblestatus".$indice."'>".$row["estatus"]."</label><label id='lblid".$indice."'>".$indice."</label><label id='lblididioma".$indice."'>".$row["ididioma"]."</label><label id='lbllongitud".$indice."'>".$row["longitud"]."</label><label id='lbllatitud".$indice."'>".$row["latitud"]."</label><label id='lblcorreo".$indice."'>".$row["correo"]."</label><label id='lblididioma".$indice."'>".$row["ididioma"]."</label><label id='lbltitulopdf".$indice."'>".$row["titulopdf"]."</label><label id='lblimagen".$indice."'>".$row["imagen"]."</label></td>";
		echo "<td class='texto-centrado noexcel'><a title='Editar' href='#' id='editar' class='mostrar-detalle' data-indice='".$indice."'><i class='fa fa-edit icono-tablas' aria-hidden='true'></i></a></td>";
		echo "<td class='texto-centrado noexcel'><label>".semaforoestatus($row["estatus"])."</label></td>";
		echo "<td class='texto-centrado'><label id='lbldireccion".$indice."'>".$row["direccion"]."</label></td>";
		echo "<td class='texto-izquierda'><label id='lbltelefono".$indice."'>".$row["telefono"]."</label></td>";
		echo "<td class='texto-izquierda'><label id='lblhorarios".$indice."'>".$row["horarios"]."</label></td>";
		echo "<td class='texto-izquierda'><label id='lblidioma".$indice."'>".$row["idioma"]."</label></td>";
		if ($idDatosGenerales == 0) {
			echo "</tr>";
		}
   		 echo "IDOK".$indice;
		 /*consulta para ver si hay idiomas disponibles*/
		$strSQL = "CALL paCuantosPorIdioma('datosgenerales')";
				$resultado = consulta($strSQL);
				while ($row = $resultado->fetch_array(MYSQLI_ASSOC)){
					$disponibles = $row["disponibles"];
				}
		echo "DISP".$disponibles;
	
	}else{
		echo $row["msj"];
	}
}
$resultado->close();

/* bitácora*/
if($idDatosGenerales==0)
{	$accion="INSERTA";	
}
else{
	$accion="ACTUALIZA";	
}
$sentencia = "CALL paGuardaBitacora('".$accion."','cdatosgenerales','".$indice."','".$usuario."')";
$resultado = consulta($sentencia);
$resultado->close();
?>
