<?
require 'vs.php';
require_once '../includes/funcs.php';
?><head>
<link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Raleway" />
</head>


<div class="contenedor">

<div class="row">
<div class="col11"></div>
<div class="col1">
			<a title="Agregar imagen" href="#" class="mostrar-detalle" data-indice="-1">
			<div class="btn-flotante">
				<i class="fa fa-plus btn-flotante-texto"></i>
			</div>
			</a>
</div>
</div>

<section>
<table id="tabla-principal" class="tabla-datos">
<thead>
<tr>
<th class="coltabla0 noexcel"></th>
<th class="col-iconos texto-centrado noexcel"></th>
<th class="col-iconos texto-centrado noexcel"></th>
<th class="coltabla4 texto-derecha">Tipo</th>
<th class="coltabla3 texto-derecha">Costo</th>
<th class="coltabla1 texto-derecha">Orden</th>
<th class="coltabla1 texto-derecha">Imagen</th>
<th class="coltabla1 texto-derecha">Idioma</th>

</tr>
</thead>
<tbody>
<?
	$filtro = $_POST['filtro'];
	cargaDatos($filtro);
?>
</tbody>
</table>


</section>

<section>
<div id="tabladetalle" style="background-color:#fff;z-index:99001" class="modalcapa modalcapa1">
	<div id="tabs" class="titulomodal">
		<ul>
    	<li id="tab-datos"><a href="#tabs-1">Datos del Costo</a></li>
  		<li id="tab-foto"><a href="#tabs-2">Imagen</a></li>
  	</ul>
		<div id="tabs-1">
			<table id="detalle" class="tabla-datos">
				<tbody class="contenidomodal">
					<tr id="fila-idioma">
                   	   <td class="col3 texto-derecha">Idioma: </td>
                   	   <td class="col9 texto-izquierda"><? agregacombo('idiomas','2');?></td>
    				 </tr>
					  <tr>
					   <td class="col3 texto-derecha">Tipo
				        <input id="lblid" type="hidden" /></td>
					   <td class="col9 texto-izquierda"><input class="control" id="txttipo" required></td>
					  
			      </tr>
                  <tr>
						<td class="col3 texto-derecha">Costo:</td>
						<td class="col5 texto-izquierda"><input class="control" id="txtcosto" required></td>
					</tr>
                    <tr>
						<td class="col3 texto-derecha">Orden:</td>
						<td class="col2 texto-izquierda"><input type="number"required class="control" id="txtorden" value="0"></td>
					</tr>
					<tr id='fila-estado'>
						<td class="col3 texto-derecha">Estado:</td>
						<td class="col2 texto-izquierda"><? agregacomboestatus(); ?></td>
					</tr>
				</tbody>
			</table>
		</div>
		<div id="tabs-2">SVG / PNG
			<form id="form1" runat="server">
	        <img id="imgPreview" width='30%' src="#" alt="Da clic para seleccionar la imagen " />
					<br>
					<input id="archivoImagen" type='file'  accept=" image/svg+xml, image/png" onchange="readURL(this);" style="display:none" />
	    </form>
		</div>
		<div style="margin-top:15px; padding-bottom:10px; width:100%; padding-left:550px;" id="espera"></div>
		<div class="row" id="areabotones" style="padding:10px;">
		<div class="col6"></div>
		<div class="col2 texto-centrado">
			<input type="button" id="guardar" name="enviar" title="Guardar" class="btn-formulario" value="Guardar">
			<img id="espera2" src='img/loading.gif' class="icono-espera">
		</div>
        <div class="col2"  id="colborrarelemento">
      <input type="button" id="borrarelemento" name="borrar" title="Borrar" class="btn-formulario borra-imagen" value="Borrar">
    </div>
		<div class="col2">
			<input type="button" id="cerrar" name="cerrar" title="Cerrar" class="btn-formulario modalcerrar" value="Cerrar">
		</div>
	</div>
	</div>
	<div id="dialog-confirm" title="Borrar elemento">
      <p><span class="ui-icon ui-icon-alert" style="float:left; margin:12px 12px 20px 0;"></span><label id='texto-confirmacion'></label>
      </p>
    </div>
</div>
</section>

<?
function cargaDatos($filtro){
	$rutaimagen="../modulos/img/costos/";
	$strSQL = "CALL paCatalogoCostos('%".$filtro."%')";
	$resultado = consulta($strSQL);
	while ($row = $resultado->fetch_array(MYSQLI_ASSOC)){
		$indice = $row["id_costo"];
		echo "<tr id=".$indice.">";
		echo "<td style='display:none' class='noexcel'><label id='lblestatus".$indice."'>".$row["estatus"]."</label><label id='lblid".$indice."'>".$indice."</label><label id='lblididioma".$indice."'>".$row["ididioma"]."</label><label id='lblorden".$indice."'>".$row["orden"]."</label><label id='lblicono".$indice."'>".$row["icono"]."</label></td>";
		echo "<td class='texto-centrado noexcel'><a title='Editar' href='#' id='editar' class='mostrar-detalle' data-indice='".$indice."'><i class='fa fa-edit icono-tablas' aria-hidden='true'></i></a></td>";
		echo "<td class='texto-centrado noexcel'><label>".semaforoestatus($row["estatus"])."</label></td>";
		echo "<td class='texto-centrado'><label id='lbltipo".$indice."'>".$row["tipo"]."</label></td>";
		echo "<td class='texto-izquierda'><label id='lblcosto".$indice."'>".$row["costo"]."</label></td>";
		
		echo "<td class='texto-izquierda'><label id='lblorden".$indice."'>".$row["orden"]."</label></td>";
		echo "<td class='texto-izquierda'><label id='lbliconoi".$indice."'><img src='".$rutaimagen.$row["icono"]."?".rand()."' width='50%' ></label></td>";
		echo "<td class='texto-izquierda'><label id='lblidioma".$indice."'>".$row["idioma"]."</label></td>";
		
		echo "</tr>";
	}
	$resultado->close();
}
?>

<script>
	$(document).ready(function() {
		$("#dialog-confirm").hide();
		$("#tabs").tabs();
		$(".modalcapa1").draggable({ cursor: "move", handle: ".titulomodal", opacity: 0.5 });
		$("#espera2").hide();
		$("#tabladetalle").hide();
		Crearlisteners();
	});

	                
	$(".mostrar-detalle").unbind("click").bind('click', function () {
		mostrarDetalle($(this).data("indice"));
  });


	function abrirmodalc1(width, height) {
		Ajustatamanocapa(".modalcapa1", width, height);
		$(".modalgeneralcapa1").fadeIn();
		$(".modalcapa1").fadeIn();
		return;
	}

	function Ajustatamanocapa(Control, width, height) {
		$(Control).css('width', width);
		$(Control).css('left', 'calc(50% - ' + width + '/2)');
		$(Control).css('top', 'calc(50% - ' + height + '/2)');

		$(Control + ' .contenidomodal').css('height', 'calc(' + height + ' - 140px)');

		return;
	}
	function cerrarmodalc1() {
		$(".modalgeneralcapa1").fadeOut();
		$('.modalcapa1').fadeOut();
	}

	function Crearlisteners(){
		$('.modalcerrar').click(function(){
			cerrarmodalc1();
			return false;
		});
	}

	$("#guardar").click(function() {
		imgsrc=0;
		 if($("#imgPreview").attr('src')!=" ")
		 {
			 imgsrc=1;
			
		}
		
		//alert();
		if ($('#txttipo').val() != ''  && $('#txtcosto').val() != ''  && ($("#archivoImagen").val() != "" || imgsrc==1) && $("#cmbidiomas").val()!=null){
			var dataObject = { id_costo: $("#lblid").text(),
			tipo: $("#txttipo").val(),
			costo: $("#txtcosto").val(),
			idioma: $("#cmbidiomas").val(),
			orden: $("#txtorden").val(),
			estatus: $("#cmbestatus").val()};
			var idOk = "0";
			$.ajax({
				data:  dataObject,
				url:   'ajax/ajax_guarda_costo.php',
        type:  'post',
				async: false,
	      beforeSend: function () {
					$("#guardar").hide();
          $("#espera2").show();
        },
        success:  function (response) {
					var id = $("#lblid").text();
					cerrarmodalc1();
					$("#espera2").hide();
					$("#guardar").show();
					var pos = response.indexOf("IDOK");
					idOk = response.substr(pos + 4);
					response = response.replace("IDOK" + idOk,"");
					if (response.substring(0,3) == "<tr" || response.substring(0,3) == "<td"){
						if (id == 0) {
							if ($('#tabla-principal > tbody > tr').length == 0){
								$('#tabla-principal > tbody ').html(response);
							}else{
								$('#tabla-principal > tbody > tr').eq(0).before(response);
							}
						}else{
							$("#"+id).html(response);
						}
						if($("#archivoImagen").prop("files")[0]=="[object File]")
						{
							subeFoto(idOk);
							//$("#imgPreview").attr('src','../modulos/img/costos/costos-' + id + ".svg?' + Math.random()");
						}
					}else{
						abrirmodalavisos('Costos',response, '800px', '450px');
					}

					$(".mostrar-detalle").unbind("click").bind('click', function () {
						mostrarDetalle($(this).data("indice"));
				  });
				}

       		});
		}else{
			abrirmodalavisos("Costos", "Debe capturar el tipo, costo , la imagen y el idioma que desea guardar", '750px', '450px');
		}
	});

function subeFoto(idC){
	
	var file_data = $("#archivoImagen").prop("files")[0];
	var form_data = new FormData();
	//var tipoext="svg";
	var archivo = $("#archivoImagen").val();
   	var tipoext = (archivo.substring(archivo.lastIndexOf(".")+1)).toLowerCase(); 
	
	form_data.append("file", file_data);
	form_data.append("tipoext", tipoext);
	form_data.append("actualizacampo", 1);
	
	$.ajax({
		data: form_data,
    url:   'ajax/ajax_sube_foto.php?num=' + idC + "&tipo=costos",
    type:  'post',
		cache: false,
		async: false,
		contentType: false,
		processData: false,
		success:  function (response) {
			//$("#imgPreview").attr('src','../modulos/img/costos/costos-' + idC + ".svg?' + Math.random()");
			if (jQuery.trim(response) != "NO"){
				
				$("#lblicono" + idC ).html('costos-'+ idC +'.'+ tipoext);
				$("#lbliconoi" + idC ).html("<img src='../modulos/img/costos/costos-" + idC +  "."+ tipoext +"?" + Math.random() + "' width='50%' >");
				
			}
		}
	});
}

	function mostrarDetalle(i) {
		$('#imgPreview').attr('src', ' ');
		$("#archivoImagen").val("");
		$("#espera2").hide();
		$( "#tabs" ).tabs( "option", "active", 0 );
		if (i != "-1"){
			$("#lblid").html($('#lblid' + i).html());
			$("#colborrarelemento").show();
			$("#txttipo").val($('#lbltipo' + i).html());
			$("#txtcosto").val($('#lblcosto' + i).html());
			$("#txtorden").val($('#lblorden' + i).html());
			$("#cmbidiomas").val($('#lblididioma' + i).html());
			$("#cmbestatus").val($("#lblestatus" + i).html())
			//alert(id);
			if ($("#lblicono" + i).html() != ""){
				$("#imgPreview").attr('src','../modulos/img/costos/' + $("#lblicono" + i ).html()+ "?" + Math.random());
			}
			/*if ($("#lblimagen" + i).html() != ""){
				$("#imgPreview").attr('src','../modulos/img/costos/costos-' + $("#lblid" + i ).html()+ ".svg?" + Math.random());
			}*/
		}else{
			$("#tab-permisos").hide();
			$("#lblid").html("0");
			$("#colborrarelemento").hide();
			$("#txttipo").val("");
			$("#txtcosto").val("");
			$("#txtorden").val(0);
			$("#cmbidiomas").val(1);
			$("#cmbestatus").val(1);
		}
		abrirmodalc1('750px', '450px');

		return false;
	}

	function readURL(input) {
		var imgPath = $("#archivoImagen").val();
		var extn = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
		/*if (extn == "gif" || extn == "png" || extn == "jpg" || extn == "jpeg") {*/
		if (extn == "svg" || extn == "png") {
    	if (input.files && input.files[0]) {
     		var reader = new FileReader();
	      reader.onload = function (e) {
			  
	      	$('#imgPreview').attr('src', e.target.result);
					
	      }
	      reader.readAsDataURL(input.files[0]);
	   	}
		}else{
			abrirmodalavisos("Archivo seleccionado", "Debe seleccionar un archivo de imagen válido.<br>(Extensión SVG/PNG)", '750px', '450px');
		}
	}

	$("#imgPreview").click(function() {
    $("input[id='archivoImagen']").click();
	});
	$("#borrarelemento").unbind("click").bind('click', function () {
		//alert("aqui");
		//debugger;
		$("#texto-confirmacion").html("¿Está seguro que desea borrar el Costo " + $("#txttipo").val() +  '?');
		$( "#dialog-confirm" ).dialog({
			resizable: false,
		    height: "auto",
		    width: 400,
		    modal: true,
		    buttons: {
		        "Aceptar": function() {
		          	$( this ).dialog( "close" );
		          	var dataObject = {id_elemento: $("#lblid").text(),
					//nombreimagen: $("#nombreimagen").val(),
					catalogo: 'costos'
					}
					$.ajax({
						data:  dataObject,
				      //  url:   'ajax/ajax_borra_tema_sala.php',
				  		url:   'ajax/ajax_borrado_logico.php',
						type:  'post',
				      	success:  function (response) {
							//alert(response);
							if (jQuery.trim(response) == "OK"){
								abrirmodalavisos("Eliminar Costos","Costo Eliminado con éxito.", 500, 200);
								location.reload();
							}				
						}
					});
		    	},
		        "Cancelar": function() {
		          $( this ).dialog( "close" );
		          return false;
	           }
		    }
	    });		
	 });	
</script>
<?
include '../inputs.php';
?>
