<?
require 'vs.php';
require_once '../includes/funcs.php';

?><head>
<link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Raleway" />
</head>


<div class="contenedor">

<div class="row">
<div class="col11"></div>
<div class="col1">
			<a title="Agregar idioma" href="#" class="mostrar-detalle" data-indice="-1">
			<div class="btn-flotante">
				<i class="fa fa-plus btn-flotante-texto"></i>
			</div>
			</a>
</div>
</div>

<section>
<table id="tabla-principal" class="tabla-datos">
<thead>
<tr>
<th class="coltabla0 noexcel"></th>
<th class="col-iconos texto-centrado noexcel"></th>
<th class="col-iconos texto-centrado noexcel"></th>
<th class="coltabla4 texto-derecha">Id</th>
<th class="coltabla4 texto-derecha">Usuario</th>
<!--<th class="coltabla3 texto-derecha">ícono</th>-->


</tr>
</thead>
<tbody>
<?
	$filtro = $_POST['filtro'];
	cargaDatos($filtro);
?>
</tbody>
</table>


</section>

<section>
<div id="tabladetalle" style="background-color:#fff;z-index:99001" class="modalcapa modalcapa1">
	<div id="tabs" class="titulomodal">
		<ul>
    	<li id="tab-datos"><a href="#tabs-1">Datos del Usuario</a></li>
  		<!--<li id="tab-foto"><a href="#tabs-2">Icono</a></li>-->
  	</ul>
		<div id="tabs-1">
			<table id="detalle" class="tabla-datos">
				<tbody class="contenidomodal">
					<tr>
						<td class="col3 texto-derecha">*Nombre:
						  <input id="lblid" type="hidden"></td>
						<td class="col5 texto-izquierda"><input class="control" id="txtnombre" required></td>
					</tr>
					<tr>
						<td class="col3 texto-derecha">*Usuario:</td>
						<td class="col5 texto-izquierda"><input class="control" id="txtusuario" required></td>
					</tr>
					<tr>
						<td class="col3 texto-derecha">*Contraseña:</td>
						<td class="col5 texto-izquierda"><input type="password" class="control" id="txtpass" required></td>
					</tr>
					<tr>
						<td class="col3 texto-derecha">*Correo:</td>
						<td class="col5 texto-izquierda"><input type="email" class="control" id="txtcorreo" required></td>
					</tr>
					<tr>
						<td class="col3 texto-derecha">*Sexo:</td>
						<td class="col5 texto-izquierda"><select id="sexo" class="control-combo"><option value="1">Mujer</option><option value="0">Hombre</option></select></td>
					</tr>
                    
					<?php cargaChecks();?>
                    <tr id='fila-estado'>
						<td class="col3 texto-derecha">Estado:</td>
						<td class="col2 texto-izquierda"><? agregacomboestatus(); ?></td>
					</tr>
				</tbody>
			</table>
		</div>
	<!--	<div id="tabs-2">
			<form id="form1" runat="server">
	        <img id="imgPreview" width="100" height="50" src="#" alt="Da clic para seleccionar la imagen " />
					<br>
					<input id="archivoImagen" type='file' accept=".JPG, .jpg, image/jpeg" onchange="readURL(this);" style="display:none" />
	    </form>
		</div>-->
		<div style="margin-top:15px; padding-bottom:10px; width:100%; padding-left:550px;" id="espera"></div>
		<div class="row" id="areabotones" style="padding:10px;">
		<div class="col6"></div>
		<div class="col2 texto-centrado">
			<input type="button" id="guardar" name="enviar" title="Guardar" class="btn-formulario" value="Guardar">
			<img id="espera2" src='img/loading.gif' class="icono-espera">
		</div>
        <div class="col2" id="colborrarelemento">
      <input type="button" id="borrarelemento" name="borrar" title="Borrar" class="btn-formulario borra-imagen" value="Borrar">
    </div>
		<div class="col2">
			<input type="button" id="cerrar" name="cerrar" title="Cerrar" class="btn-formulario modalcerrar" value="Cerrar">
		</div>
	</div>
	</div>
	<div id="dialog-confirm" title="Borrar elemento">
  <p><span class="ui-icon ui-icon-alert" style="float:left; margin:12px 12px 20px 0;"></span><label id='texto-confirmacion'></label>
  </p>
</div>
</div>
</section>

<?
function cargaChecks(){
	$padre = "";
	$html="";
	$strSQL = "CALL paCargaChecks()";
	$resultado = consulta($strSQL);
	while ($row = $resultado->fetch_array(MYSQLI_ASSOC)){
		if ($padre <> $row["nombre_padre"]){
			if ($padre <> "") {
				$html .="</td></tr>";
			}
			$html .= "<tr>
						<td class='col3 texto-derecha'>Permisos ".$row["nombre_padre"].":</td>
						<td class='col9 texto-izquierda'><label><input type='checkbox' class='chk' id='chk".$row["id"]."' value='".$row["id"]."'> ".$row["nombre"]."</label>";
			$padre = $row["nombre_padre"];
		}else{
			$html .= "<label><input type='checkbox' class='chk' id='chk".$row["id"]."' value='".$row["id"]."'> ".$row["nombre"]."</label>";
		}
	}
	if ($padre <> ""){
		$html .= "</td></tr>";
	}
	print_r($html);
} 
function cargaDatos($filtro){
	$rutaimagen="../modulos/img/idiomas/idiomas-";
	$strSQL = "CALL paCatalogoUsuarios('%".$filtro."%')";
	
	$resultado = consulta($strSQL);
	while ($row = $resultado->fetch_array(MYSQLI_ASSOC)){
		$indice = $row["id_usuario"];
		echo "<tr id=".$indice.">";
		echo "<td style='display:none' class='noexcel'><label id='lblestatus".$indice."'>".$row["estatus"]."</label><label id='lblid".$indice."'>".$indice."</label><label id='lblnombre".$row["nombre"]."'>".$indice."</label><label id='lblicono".$indice."'>".$indice."</label><label id='lblusuario".$indice."'>".$row["usuario"]."</label><label id='lblcorreo".$indice."'>".$row["correo"]."</label><label id='lblsexo".$indice."'>".$row["sexo"]."</label></td>"; 
		echo "<td class='texto-centrado noexcel'><a title='Editar' href='#' id='editar' class='mostrar-detalle' data-indice='".$indice."'><i class='fa fa-edit icono-tablas' aria-hidden='true'></i></a></td>";
		echo "<td class='texto-centrado noexcel'><label>".semaforoestatus($row["estatus"])."</label></td>";
		echo "<td class='texto-centrado'><label id='lblididioma".$indice."'>".$row["id_usuario"]."</label></td>";
		echo "<td class='texto-izquierda'><label id='lblidioma".$indice."'>".$row["nombre"]."</label></td>";
		/*echo "<td class='texto-izquierda'><label id='lblicono".$indice."'><img src='".$rutaimagen.$indice.".jpg' width='100' height='50'></label></td>";*/
		echo "</tr>";
	}
	$resultado->close();
}
?>

<script>
	$(document).ready(function() {
		$("#dialog-confirm").hide();
		$("#tabs").tabs();
		$(".modalcapa1").draggable({ cursor: "move", handle: ".titulomodal", opacity: 0.5 });
		$("#espera2").hide();
		$("#tabladetalle").hide();
		Crearlisteners();
	});

	                
	$(".mostrar-detalle").unbind("click").bind('click', function () {
		mostrarDetalle($(this).data("indice"));
  });


	function abrirmodalc1(width, height) {
		Ajustatamanocapa(".modalcapa1", width, height);
		$(".modalgeneralcapa1").fadeIn();
		$(".modalcapa1").fadeIn();
		return;
	}

	function Ajustatamanocapa(Control, width, height) {
		$(Control).css('width', width);
		$(Control).css('left', 'calc(50% - ' + width + '/2)');
		$(Control).css('top', 'calc(50% - ' + height + '/2)');

		$(Control + ' .contenidomodal').css('height', 'calc(' + height + ' - 140px)');

		return;
	}
	function cerrarmodalc1() {
		$(".modalgeneralcapa1").fadeOut();
		$('.modalcapa1').fadeOut();
	}

	function Crearlisteners(){
		$('.modalcerrar').click(function(){
			cerrarmodalc1();
			return false;
		});
	}

	$("#guardar").click(function() {
		//if ($('#txtidioma').val() != '' && $('#imgPreview').attr('src')!=' '){
			var chkArray = [];
	
	$(".chk:checked").each(function() {
		chkArray.push($(this).val());
	});  
	cuantosmodulos= chkArray.length;
	//alert(cuantosmodulos);
	if ($('#txtnombre').val() != ''  &&  $('#txtusuario').val() != ''  && $('#txtcorreo').val() != ''  && cuantosmodulos >0){
		if($("#lblid").text() =='0' &&  $('#txtpass').val() == '' )
		{
			abrirmodalavisos("Usuarios", "Debe capturar la contraseña para el usuario que desea guardar", '750px', '350px');
			return false;
		}
		
	

			var dataObject = { id_usuario: $("#lblid").text(),
			nombre: $("#txtnombre").val(),
			usuario: $("#txtusuario").val(),
			password: $("#txtpass").val(),
			sexo: $("#sexo").val(),
			correo: $("#txtcorreo").val(),
			estatus: $("#cmbestatus").val(),
		    chkarray: chkArray};
			var idOk = "0";
			$.ajax({
				data:  dataObject,
				url:   'ajax/ajax_guarda_usuario.php',
        type:  'post',
				async: false,
	      beforeSend: function () {
					$("#guardar").hide();
          $("#espera2").show();
        },
        success:  function (response) {
					var id = $("#lblid").text();
					console.log("nuevo is es: "+id);
					cerrarmodalc1();
					$("#espera2").hide();
					$("#guardar").show();
					var pos = response.indexOf("IDOK");
					idOk = response.substr(pos + 4);
					response = response.replace("IDOK" + idOk,"");
					if (response.substring(0,3) == "<tr" || response.substring(0,3) == "<td"){
						if (id == 0) {
							if ($('#tabla-principal > tbody > tr').length == 0){
								$('#tabla-principal > tbody ').html(response);
							}else{
								$('#tabla-principal > tbody > tr').eq(0).before(response);
							}
						}else{
							$("#"+id).html(response);
						}
						//subeFoto(idOk);
						
						
					/*	$("#imgPreview").attr('src','../modulos/img/idiomas/idiomas-' + id + ".jpg?' + Math.random()");*/
					}else{
						abrirmodalavisos('Usuarios',response, '800px', '450px');
					}

					$(".mostrar-detalle").unbind("click").bind('click', function () {
						mostrarDetalle($(this).data("indice"));
				  });
				}

       		});
		}else{
			abrirmodalavisos("Usuarios", "Debe capturar el nombre, usuario, correo y módulos permitidos para el usuario que desea guardar", '750px', '350px');
		}
	});


	function mostrarDetalle(i) {
		$('#imgPreview').attr('src', ' ');
		$("#espera2").hide();
		$( "#tabs" ).tabs( "option", "active", 0 );
		if (i != "-1"){
			$("#lblid").html($('#lblid' + i).html());
			if($('#lblid' + i).html()!='1')
			{
				$("#colborrarelemento").show();
			}
			else{
				$("#colborrarelemento").hide();
			}
			$("#txtnombre").val($('#lblidioma' + i).html());
			$("#txtusuario").val($('#lblusuario' + i).html());
			$("#txtcorreo").val($('#lblcorreo' + i).html());
			$("#sexo").val($('#lblsexo' + i).html());
			$("#txtpass").val("");
			$("#cmbestatus").val($("#lblestatus" + i).html());
			$("#sexo").val($("#lblsexo" + i).html());
        $('.chk').prop('checked', false);
	    $.ajax({
        async: false,
        type: 'post', // the method (could be GET btw)
        url: 'ajax/ajax_get_permisos.php', // The file where my php code is
        data: {
            'idUsu': i // all variables i want to pass.
        },
        success: function(data) { // in case of success get the output, i named data
            json = JSON.parse(data);
            for (var u=0; u< json.length; u++){
              var idMod = json[u]['cModId'];
              $('#chk'+idMod).prop('checked', true);
            }
        }
    });
			/*if ($("#lblicono" + i).html() != ""){
				$("#imgPreview").attr('src','../modulos/img/idiomas/idiomas-' + $("#lblid" + i ).html()+ ".jpg?" + Math.random());
			}*/
		}else{
			$("#colborrarelemento").hide();
			$('.chk').prop('checked', false);
			$("#txtnombre").val("");
			$("#txtusuario").val("");
			$("#txtcorreo").val("");
			$("#txtpass").val("");

			$("#tab-permisos").hide();
			$("#lblid").html("0");
			$("#txtidioma").val("");
			$("#cmbestatus").val(1);
		}
		abrirmodalc1('750px', '450px');

		return false;
	}

	$("#borrarelemento").unbind("click").bind('click', function () {
		//alert("aqui");
		$("#texto-confirmacion").html("¿Está seguro que desea borrar el elemento " + $("#txtnombre").val() + '?');
		$( "#dialog-confirm" ).dialog({
			resizable: false,
		    height: "auto",
		    width: 400,
		    modal: true,
		    buttons: {
		        "Aceptar": function() {
		          	$( this ).dialog( "close" );
		          	var dataObject = { id_elemento: $("#lblid").text(),
					//nombreimagen: $("#nombreimagen").val(),
					catalogo: 'usuarios'
					}
					$.ajax({
						data:  dataObject,
				      //  url:   'ajax/ajax_borra_tema_sala.php',
				  		url:   'ajax/ajax_borrado_logico.php',
						type:  'post',
				      	success:  function (response) {
							//alert(response);
							if (jQuery.trim(response) == "OK"){
								abrirmodalavisos("Eliminar usuario","Usuario Eliminado con éxito.", 500, 200);
								location.reload();
							}				
						}
					});
		    	},
		        "Cancelar": function() {
		          $( this ).dialog( "close" );
		          return false;
	           }
		    }
	    });
  });
</script>
<?
include '../inputs.php';
?>
