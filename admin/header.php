 <? include_once("idioma.php");?>
 <?php
/* ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL); */

  require_once('includes/funcs.php');
  /* datos generales*/
  $sentencia = "CALL paPaginaDatosGenerales('$idIdioma')";
  $resultado = consulta($sentencia);
  $cuantos=$resultado->num_rows;
  if($cuantos>0){
	   while ($row = $resultado->fetch_array(MYSQLI_ASSOC)){
		   $iddatosgenerales=$row["iddatosgenerales"];
		   $direccion=$row["direccion"];
		   $telefono=$row["telefono"];
		   $longitud=$row["longitud"];
		   $latitud=$row["latitud"];
		   $horarios=$row["horarios"];
		   $titulopdfgeneral=$row["titulopdf"];
	 }
  }
  else{
	  $direccion=""; $telefono=""; $longitud=""; $latitud=""; $horarios="";
	 }
  ?>
 
 <section class="navigation">
        <div class="menu-usuario">
            <div class="idioma">
            </div>
            <!--idioma-->
        
           <? include_once("modulos/redes/redes.php");?>
            <!--redes-sociales-->
        </div>
        
        <div class="logo">
        	<div class="logo-prin">
	        	<img class="logo-km-h" src="img/museo-km-h-gto.svg" alt="Museo Km/h">
            </div>
             <!--Museo km/h-->
             
             <div class="busqueda">
             </div>
             <!--búsqueda-->
        </div>
        <!--Museo km/h-búsqueda-->
        
        <? include_once("modulos/menuprincipal/menuprincipal.php");?>
        <!--menú-principal-->
		<script src="https://code.jquery.com/jquery-1.12.4.min.js" 
		></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/fastclick/1.0.6/fastclick.min.js"></script>
        <script src="js/menu.min.js"></script>
        <script>
            $('.ve-menu').menu({
                fontSize: 17,
                fontColor: '#222',
                bgColor: '#fff',
                hoverFontColor: '#0694a7',
                hoverBgColor: '#fff',
                itemSpace: 5,
                subFontSize: 17,
                itemWidth: 70,
                animate: 'slide',
                speed: 500,
            });
        </script>
    </section>