<?
require 'vs.php';
include_once 'includes/funcs.php';
session_start();

	$nombreUsuario = $_SESSION["NOM"];
	$usuario = $_SESSION["USU"];
	$sexo = $_SESSION['SXO'];
	$dep = $_SESSION['DEP'];
	$idUsuario = $_SESSION['IDU'];


?>

<!DOCTYPE html>
<html lang="en">
<head>

  <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">
  <link rel="icon" href="img/favicon.ico" type="image/x-icon">
  <title>Administrador</title>
  <meta charset="utf-8">
  <link href="css/select2.min.css" rel="stylesheet" />
  <link rel="stylesheet" href="css/font-awesome.css" />
  <link rel="stylesheet" href="css/estilos.css">

  <link href="css/jquery-ui.css" rel="stylesheet" />

  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  <!-- <script type="text/javascript" src="js/jquery-1.11.1.js"></script> -->

  <script type="text/javascript" src="js/jquery-1.12.4.js"></script>

  <script src="js/jquery-ui.js"></script>

  <script src="js/select2.min.js"></script>
  <script type="text/javascript" src="js/sha1.js"></script>
  <!-- <script type="text/javascript" src="js/jquery.touchSwipe.min.js"></script> -->

  <!--<script type="text/javascript" src="js/jquery-ui-timepicker-addon.js"></script> -->
  <script type="text/javascript" src="js/funciones.js"></script>


</head>

<body>

<div class="contenedor-principal">

	<div class="page-top">
		<div class="btn-display-menu">
			<a title="Desplazar menús" href="#" data-toggle=".container" id="sidebar-toggle">
				<i class='fa fa-bars mnu-barras' aria-hidden='true'></i>
			</a>
		</div>
        <div class="contenedorlogo">
		<div class="logo">
			<a href="principal_inicio.php"><img src="../img/museo-km-h-gto.svg" width="70%"></a>
		</div>
        </div>

		<div class="mnu-superior">
			<div class="contenedor-mnuusuario">
				<div class="perfilhover">
					<div class="nombre-usuario">
						<?php echo $nombreUsuario; ?>
						<br>
						<span class="tipo-usuario">
						<?php echo $dep; ?>
						</span>
					</div>
					<div class="foto-usuario">
						<?php if ($sexo == "0") { ?>
							<img src="./img/hombre.png">
						<?php }else{ ?>
							<img src="./img/mujer.png">
						<?php } ?>
					</div><!-- Menu usuario -->
				
				</div>
				<div id="mnuusuario"><div class="indicador-mnu-gris"></div>
					<ul>
						<a href="#" class="cambiarcontrasena"><i class='fa fa-lock' aria-hidden='true'></i><li>Cambiar contraseña</li></a>
					
						<a href="#" class="logout"><i class='fa fa-times' aria-hidden='true'></i><li>Salir</li></a>
					</ul>
				</div>

			</div>
		</div>
	</div>

<div class="container">
    <div id="sidebar">
		<div class="movible">
			<ul id="accordion">
				<!-- <li><a title="Módulos" href="#"><i class='fa fa-database menus' style='padding-right:10px;' aria-hidden='true'></i>Módulos</a></li>
				<ul> -->
			  	<?php	cargaMenus($idUsuario);	?>
				<!--
					</ul>
				</ul>
				-->


			</ul>
		</div>
	</div>

    <div class="contenido-central">
        <div class="contenido">
		    <?php include($pagina_mostrar); ?>
		</div>
	</div>
</div>

</div>

<div class="capa-sombra modalgeneralcapa1"></div>
<div class="capa-sombra modalgeneralcapa2"></div>

<div id="enviacomentarios" class="enviacomentarios">
	<div class="col12 texto-centrado titulo-reportes">
		<h3>Reporte del sistema</h3>
	</div>
	<div class="row">
		<div class="col1"></div>
		<div class="col11">
			Nombre
		</div>
		<div class="col1"></div>
		<div class="col8">
			<input type="text" class="control" id="nombre" placeholder="Capture su nombre">
		</div>
	</div>
	<div class="row">
		<div class="col1"></div>
		<div class="col11">
			Correo electrónico
		</div>
		<div class="col1"></div>
		<div class="col8">
			<input type="text" class="control" id="email" placeholder="Capture su email">
		</div>
	</div>
	<div class="row">
		<div class="col1"></div>
		<div class="col11">
			Tipo de reporte
		</div>
		<div class="col1"></div>
		<div class="col3">
			<select id="tiporeporte" class="">
				<option>Sugerencia</option>
				<option>Error / bug</option>
				<option>Comentarios</option>
			</select>
		</div>
	</div>
	<div class="row">
		<div class="col1"></div>
		<div class="col11">
			<textarea class="control" id="comentarios" placeholder="Capture sus comentarios"></textarea>
		</div>
	</div>
	<div class="row" id="areabotonesreporte" style="padding:10px;">
		<div class="col8"></div>
		<div class="col2 texto-centrado">
			<input type="button" id="enviarcomentarios" name="aceptar" title="Enviar" class="btn-formulario" value="Enviar">
			<img id="esperacomentarios" src='img/loading.gif' class="icono-espera">
		</div>
		<div class="col2">
			<input type="button" id="cancelarcomentarios" name="cancelar" title="Cancelar" class="btn-formulario modalcerrarcomentarios" value="Cancelar">
		</div>
	</div>
</div>


<div id="avisos" class="avisos">
	<div class="col12 texto-centrado titulo-reportes">
		<h3 id="tituloavisos" ></h3>
	</div>
	<div class="row">
		<div id="contenidoavisos" class="col12 texto-centrado"></div>
	</div>
	<div class="row" id="areabotonesaviso" style="padding:10px;">
		<div class="col9"></div>
		<div class="col2 texto-centrado">
			<input type="button" id="aceptaravisos" name="aceptar" title="Aceptar" class="btn-formulario modalcerraraviso" value="Aceptar">
		</div>
	</div>
</div>


<div id="alertalogout" class="alertalogout">
	<div class="col12 texto-centrado titulo-reportes">
		<h3 id="tituloalerta"></h3>
	</div>
	<div class="row">
		<div id="contenidoalerta" class="col12 texto-centrado"></div>
	</div>
	<div class="row" id="areabotonesalerta" style="padding:10px;">
		<div class="col8"></div>
		<div class="col2 texto-centrado">
			<input type="button" id="aceptarlogout" name="aceptar" title="Aceptar" class="btn-formulario" value="Aceptar">
		</div>
		<div class="col2">
			<input type="button" id="cancelar" name="cancelar" title="Cancelar" class="btn-formulario modalcerraralerta" value="Cancelar">
		</div>
	</div>
</div>

<div id="cambiapwd" class="pwd">
	<div class="col12 texto-centrado titulo-reportes">
		<h3 id="titulomodalpwd">Cambio de contraseña</h3>
	</div>

	<ul>
		<div id="contenidopwd">
		<div class="row">
			<div class="col6 texto-derecha">
				Contraseña actual:
			</div>
			<div class="col4 texto-izquierda">
				<input placeholder="Contraseña actual" class="controlpwd" type='password' id="txtpwdactual" name='pwdactual' >
			</div>
		</div>

		<div class="row">
			<div class="col6 texto-derecha">
				Nueva contraseña:
			</div>
			<div class="col4 texto-izquierda">
				<input placeholder="Nueva contraseña" class="controlpwd" type='password' id="txtpwdnuevo1" name='pwdnuevo1' >
			</div>
		</div>

		<div class="row">
			<div class="col6 texto-derecha">
				Escriba de nuevo su nueva contraseña:
			</div>
			<div class="col4 texto-izquierda">
				<input placeholder="Nueva contraseña" class="controlpwd" type='password' id="txtpwdnuevo2" name='pwdnuevo2' >
			</div>
		</div>
		</div>
		<div class="row" id="areabotones" style="padding:10px;">
			<div class="col8"></div>
			<div class="col2 texto-centrado">
				<input type="button" id="guardarpwd" name="guardarpwd" title="Guardar" class="btn-formulario" value="Guardar">
				<img id="espera2" src='img/loading.gif' class="icono-espera">
			</div>
			<div class="col2">
				<input type="button" id="cerrar" name="cerrar" title="Cerrar" class="btn-formulario modalcerrarpwd" value="Cerrar">
			</div>
		</div>
	</ul>

</div>

</body>
</html>

<script>
	$(document).ready(function () {
		$("#espera2").hide();
		$("#esperacomentarios").hide();
		$(".pwd").draggable({ cursor: "move", handle: ".titulomodal", opacity: 0.5 });

		$(".avisos").draggable({ cursor: "move", handle: ".titulo-reportes", opacity: 0.5 });
		$(".alertalogout").draggable({ cursor: "move", handle: ".titulomodal", opacity: 0.5 });
		$(".alertalogout").hide();
		$(".enviacomentarios").hide();
		$(".pwd").hide();
		$(".avisos").hide();
		Crearlisteners();
	});
	             
	$(".cambiarcontrasena").unbind("click").bind('click', function () {
		debugger;
		mostrarCambioPwd();
    });

	$(".btn-comentarios").unbind("click").bind('click', function () {
		abrirmodalcomentarios();
    });

	$("#aceptarlogout").click(function() {
		window.location.href = "logout.php";
    });

	$(".logout").unbind("click").bind('click', function () {
		debugger;
		Ajustatamanocapa('.alertalogout', '750px', '450px');
		$(".modalgeneralcapa1").fadeIn();
		$("#contenidoalerta").html("Se dispone a cerrar su sesión, ¿Desea continuar?");
		$("#tituloalerta").html("Cerrar sesión");
		$(".alertalogout").fadeIn();
    });

	function abrirmodalpwd(width, height) {
		Ajustatamanocapa('.pwd', width, height);

		$(".modalgeneralcapa1").fadeIn();
		$('.pwd').fadeIn();
		return ;
	}

	function abrirmodalavisos(titulo, contenido, width = '700px', height = '200px') {
		$('.avisos').css('width', width);
		$('.avisos').css('height', height);
		$("#tituloavisos").html(titulo);
		$("#contenidoavisos").html(contenido);
		$('.avisos').css('top', 'calc(30%)');
		$('.avisos').css('left', 'calc(30%)');
		$(".modalgeneralcapa2").fadeIn();
		$('.avisos').fadeIn();
	}

	function cerrarmodalavisos() {
		$(".modalgeneralcapa2").fadeOut();
		$(".avisos").fadeOut();
	}

	function abrirmodalcomentarios() {
		Ajustatamanocapa('.enviacomentarios', '750px', '450px');
		$("#esperacomentarios").hide();
		$("#enviarcomentarios").show();
		$(".modalgeneralcapa1").fadeIn();
		$(".enviacomentarios").fadeIn();
		return false;
	}

	function Ajustatamanocapa(Control, width, height) {
		$(Control).css('width', width);
		$(Control).css('left', 'calc(50% - ' + width + '/2)');
		$(Control).css('top', 'calc(50% - ' + height + '/2)');
		$(Control + ' .pwd').css('height', 'calc(' + height + ' - 140px)');
		$(Control + ' .avisos').css('left', 'calc(50% - ' + width + '/2)');
		return;
	}


	function cerrarmodalpwd() {
		$("#txtpwdactual").val("");
		$("#txtpwdnuevo1").val("");
		$("#txtpwdnuevo2").val("");
		$("#titulomodalpwd").html("Cambio de contraseña");
		$(".modalgeneralcapa1").fadeOut();
		$(".pwd").fadeOut();
		$("#guardarpwd").show();
		$("#contenidopwd").show();
	}

	function cerrarmodalalerta() {
		$(".modalgeneralcapa1").fadeOut();
		$(".alertalogout").fadeOut();
	}



	function cerrarmodalcomentarios() {
		$(".modalgeneralcapa1").fadeOut();
		$(".enviacomentarios").fadeOut();
	}

	function Crearlisteners(){
		$(".modalcerrarpwd").click(function(){
			cerrarmodalpwd();
			return false;
		});

		$(".modalcerraralerta").click(function(){
			cerrarmodalalerta();
			return false;
		});

		$(".modalcerraraviso").click(function(){
			cerrarmodalavisos();
			return false;
		});

		$(".modalcerrarcomentarios").click(function(){
			cerrarmodalcomentarios();
			return false;
		});
	}

	$("#guardarpwd").click(function() {
		if ($("#txtpwdnuevo1").val() == $("#txtpwdnuevo2").val()){
			if ($("#txtpwdnuevo1").val().length < 5) {
				abrirmodalavisos("Cambio de contraseña", "La contraseña debe ser de mínimo 5 caracteres.", '750px', '450px');
				return;
			}

			var pwdactual = $("#txtpwdactual").val();
			var pwdnuevo = $("#txtpwdnuevo1").val();
			var dataObject = { pwd0: pwdactual,
		    pwd1: pwdnuevo
			};
			$.ajax({

				data:  dataObject,
                url:   'ajax/ajax_cambia_pwd.php',
                type:  'post',
                beforeSend: function () {
						$("#guardar").hide();
						$("#espera2").show();
                },
                success:  function (response) {
					$("#contenidopwd").hide();
					$("#espera2").hide();
					$("#guardarpwd").hide();

					$("#titulomodalpwd").html(response);

				}
        	});
		}else{
			abrirmodalavisos("Cambio de contraseña", "Las contraseñas nuevas no coinciden", '750px', '450px');
			return;
		}
	});

	

	function mostrarCambioPwd() {
		abrirmodalpwd('750px', '450px');
		return false;
	}
</script>

<?


function cargaMenus($idUsuario){
	$archivo_actual=basename($_SERVER['REQUEST_URI'], ".php").PHP_EOL;
	$cadena_formateada = trim($archivo_actual);
	$permiso="no";
	$padre = "";
	$strSQL = "CALL paPermisoUsuario('".$idUsuario."')";
	$resultado = consulta($strSQL);
	while ($row = $resultado->fetch_array(MYSQLI_ASSOC)){
		if ($padre <> $row["nombre_padre"]){
			if ($padre <> ""){
				echo "</ul>";
			}
			echo "<li><a title='".$row["nombre_padre"]."' href='#'><i class='fa ".$row["icono_padre"]." menus' style='padding-right:10px;' aria-hidden='true'></i>".$row["nombre_padre"]."</a></li>";
			echo "<ul>";
			$padre = $row["nombre_padre"];
		}
		echo "<li><a title='".$row["nombre"]."' href='".$row["archivo_master"].".php'><i class='fa ".$row["icono"]." menus' style='padding-right:6px;' aria-hidden='true'></i>".$row["nombre"]."</a></li>";

		if ($cadena_formateada==$row["archivo_master"]) {
			$permiso="si";
		}
	}



	if ($padre <> ""){
		echo "</ul>";
	}
	//var_dump($permiso);
	//var_dump($cadena_formateada);
	if ($cadena_formateada != "principal_inicio" AND $permiso=="no") {
		header("Location: principal_inicio.php");

	}
	
} 

/*
function cargaMenus($idU){
	$strSQL = "CALL paCargaPermisosUsuario ('".$idU."')";
	$resultado = consulta($strSQL);
	$pag0 = "";
	while ($row = $resultado->fetch_array(MYSQLI_ASSOC)){
		if ($pag0 == ""){
			$pag0 = trim($row["archivo_master"]).".php";
		}
		echo "<li><a title='".$row["nombre"]."' href='".trim($row["archivo_master"]).".php'><i class='fa ".$row["icono"]." menus' style='padding-right:6px' aria-hidden='true'></i>".$row["nombre"]."</a></li>";
	}
	return $pag0;
}
*/
