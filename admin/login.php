﻿<?
ini_set('display_errors', 1);
session_start();
if (isset($_SESSION['PL8703455b36cb44c39cf27733f7c979e5'])){
	header("Location: principal_inicio.php");
  die();
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Frameset//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-frameset.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
   <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="css/font-awesome.css" />
  <link rel="stylesheet" href="css/estilos.css">

  <script type="text/javascript" src="js/jquery-1.12.4.js"></script>
  <script src="js/jquery-ui.js"></script>
  <script type="text/javascript" src="js/funciones.js"></script>
  <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">
  <link rel="icon" href="img/favicon.ico" type="image/x-icon">

  <title>Portal de Administración</title>
</head>


<body>
	<div class="contenedor-principal-login">
		<div class="contenedor-login">
			<form name="forma" method="post" action="acceso.php" >
				<div class="row">
					<div class="row">
						<div class="col12 texto-centrado">
							<img src="../img/museo-km-h-gto.svg" width="240px">
						</div>
					</div>
						<div class="col12 texto-centrado">
							<h3>Administrador del Sitio<br></h3>
						</div>

					<div class="row">
						<div class="col12">
							Usuario:
						</div>
					</div>
					<div class="row">
						<div class="col12">
							<div style="float:left; padding-top:0px">
								<i class="fa fa-user texto-derecho"></i>
							</div>
							<div style="float:left; padding-left:8px; width:90%">
								<input type="text" class="control-login" id="nombre" name="nombre" placeholder="Capture su nombre de usuario" required>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col12">
							Contraseña:
						</div>
					</div>
					<div class="row">
						<div class="col12">
							<div style="float:left; padding-top:0px">
								<i class="fa fa-lock texto-derecho"></i>
							</div>
							<div style="float:left; padding-left:8px; width:90%">
								<input type="password" name="pwd" class="control-login" placeholder="Escriba su contraseña" required id="pwd">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col12 texto-derecha">
							<input type="submit" name="go" title="Ingresar" class="btn-formulario" value="Ingresar">
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
<div style="text-align:center"><font style="color:white;font-family:'Tahoma'; font-size:14px;"></font></div>
</body>
</html>
