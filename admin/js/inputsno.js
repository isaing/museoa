$('input').bind('keypress', function (event) {
	// /^[a-z\d\-_\s]+$/i
var regex = new RegExp("[a-zA-Z0-9Ññ.: ]");
var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
if (!regex.test(key)) {
   event.preventDefault();
   return false;
}
});