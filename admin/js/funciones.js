// funciones para menu desplegable
$(document).ready(function() {

	/*$("#btn-comentarios").click( function(){
			$(".reportes-usuario").fadeIn(800);
    });*/

  ControlFechaEspanol();

  $( function() {
    $( "#datepicker" ).datepicker();
  });



  $(".contenedor-login").fadeIn(1000);

  $("#datepicker").datepicker();
  $("#mostrar-calendario").click(function() {
   $("#datepicker").datepicker( "show" );
  });

  $("[data-toggle]").click(function() {
    var toggle_el = $(this).data("toggle");
	$(toggle_el).toggleClass("open-sidebar");
	$(".container .contenido-central").toggleClass("anchocontenido");
  });

// Funcion para menus acordeon
$("#accordion > li").click(function(){
	//debugger;
	if(false == $(this).next().is(':visible')) {
		$('#accordion > ul').slideUp(300);
	}
	$(this).next().slideToggle(300);
});

$(".notifhover").hover(function(){
		setTimeout(function() {$("#listanotif").fadeIn(800); $("#mnuusuario").fadeOut(0);},10);
	}, function(){
	//	$("#listanotif").css("display","none");
});

$("#listanotif").hover(function(){

},function(){
	 setTimeout(function() {
        $("#listanotif").fadeOut(800);
    },10);
});


$(".perfilhover").hover(function(){

},function(){
	 setTimeout(function() {
        $("#listanotif").fadeOut(800);
    },10);
});


$(".main-content").hover(function(){
	 setTimeout(function() {
        $("#listanotif").fadeOut(800);
		$("#mnuusuario").fadeOut(800);
    },10);
});

$(".perfilhover").hover(function(){
	setTimeout(function() {
        $("#mnuusuario").fadeIn(800);
		//alert($(".perfil").css("right"));
		$("#listanotif").fadeOut(0);
	},10);
});

$("#mnuusuario").hover(function(){

},function(){
	 setTimeout(function() {
        $("#mnuusuario").fadeOut(800);
    },10);
});

$(".fondoModal").click(function(){
	 setTimeout(function() {
        $(".fondoModal").fadeOut(800);
		$(".reportes-usuario").fadeOut(800);

    },10);
});

$(".reportes-usuario").draggable({ cursor: "move", handle: ".titulo-reportes", opacity: 0.5 });

// https://select2.github.io/examples.html


/* $('select').select2(); */



});

function ControlFechaEspanol() {
    $.datepicker.regional['es'] = {
        closeText: 'Cerrar',
        prevText: '<Ant',
        nextText: 'Sig>',
        currentText: 'Hoy',
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
        dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
        dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mié', 'Juv', 'Vie', 'Sáb'],
        dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sá'],
        weekHeader: 'Sm',
        dateFormat: 'dd/mm/yy',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''
    };
    $.datepicker.setDefaults($.datepicker.regional['es']);
}

/*
$(".swipe-area").swipe({
	swipeStatus:function(event, phase, direction, distance, duration, fingers)
        {
			if (phase=="move" && direction =="right") {
				$(".container").addClass("open-sidebar");
				return false;
            }
            if (phase=="move" && direction =="left") {
				$(".container").removeClass("open-sidebar");
				return false;
            }

        }
});
*/

function muestraModal (cual){
	$('.fondoModal').fadeIn(800);
	$(cual).fadeIn(800);
}
