﻿<?php
include '../includes/vars.php';
$cadena=$_POST['str'];
$idIdioma=$_POST['idIdio'];
$mysqli = new mysqli($vservidor,$vusuario, $vpwd, $vbd);
$mysqli->set_charset("utf8");
if ($mysqli->connect_errno) {
	printf("Error: Fallo al conectarse a MySQL %s\n"."usuario: ".$vusuario." ", $mysqli->connect_error);
    exit;
}
$trozos = explode(" ",$cadena);   //CUENTA EL NUMERO DE PALABRAS
$numero = count($trozos);
if ($numero==1) { //SI SOLO HAY UNA PALABRA DE BUSQUEDA SE ESTABLECE UNA INSTRUCION CON LIKE
    $sql="CALL paBuscador('".$numero."','%".$cadena."%','".$idIdioma."')";
} elseif ($numero>1) { //SI HAY UNA FRASE SE UTILIZA EL ALGORTIMO DE BUSQUEDA AVANZADO DE MATCH AGAINST
    $sql = "CALL paBuscador('".$numero."','".$cadena."','".$idIdioma."')";
}


if (!$resultado = $mysqli->query($sql)) {
    printf("Error: La ejecución de la consulta falló debido a: \n");
    printf("Query: " . $sql . "\n");
    printf("Errno: " . $mysqli->errno . "\n");
    printf("Error: " . $mysqli->error . "\n");
    exit;
}
if ($resultado->num_rows === 0) {
    printf("no");
    exit;
}
$myArray = array();
while($row = $resultado->fetch_array(MYSQLI_ASSOC)) {
            $myArray[] = $row;
}
echo json_encode($myArray);
$resultado->free();
$mysqli->close();

/*
CREATE PROCEDURE buscar_against (IN cadena varchar(150)) SELECT titulo,desarrollo , MATCH ( titulo, desarrollo ) AGAINST ( cadena ) AS Score FROM tbl_articulos WHERE MATCH ( titulo, desarrollo) AGAINST ( cadena ) ORDER BY Score DESC LIMIT 50

CREATE PROCEDURE buscar_like (IN cadena varchar(150))
SELECT  titulo,desarrollo FROM tbl_articulos WHERE titulo LIKE  cadena OR desarrollo LIKE  cadena LIMIT 50
*/

?>
