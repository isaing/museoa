<script>
$('input').bind('keypress', function (event) {
	var regex = new RegExp("[\-a-zA-Z0-9Ññ._@ÁÉÍÓÚáéíóú:/$# ]");
	var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
	if (!regex.test(key)) {
		event.preventDefault();
		return false;
	}
});
$('#txtetiqueta').bind('keypress', function (event) {
	var regex = new RegExp("[\-a-zA-Z]");
	var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
	if (!regex.test(key)) {
		event.preventDefault();
		return false;
	}
});
$('#txtabreviatura').bind('keypress', function (event) {
	var regex = new RegExp("[\-a-z]");
	var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
	if (!regex.test(key)) {
		event.preventDefault();
		return false;
	}
});

$(".botonExcel").click(function(event) {
		var tablat = $("#tabla-principal").eq(0).clone();
		tablat.find('.noexcel').remove();
		$("#datos_a_enviar").val( $("<div>").append( tablat ).html());
		$("#FormularioExportacion").submit();
});

$("#filtro").keyup(function(e){
	var code = e.which;
	if (code==13) {
		CargaDatos();
	}
});

</script>
