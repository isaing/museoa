<?php
function c($valor)
{
 return str_replace("'","''",$valor);
}
function consulta($sentencia,$modulo = 'portal')
{
	switch ($modulo) {
    case 'portal':
        include 'vars.php';
        break;
   
	}

	$mysqli = new mysqli($vservidor,$vusuario, $vpwd, $vbd);
	$mysqli->set_charset("utf8");
	if (mysqli_connect_errno())
	{
		printf("No es posible realizar la conexión ");
		exit();
	}
	$SQL = $sentencia;
	if ( ($result = $mysqli->query($SQL))=== false )
	{
		printf("Consulta inválida"); // : %s\nWhole query: %s\n", $mysqli->error, $SQL);
		exit();
	}
	return $result;
	$mysqli->close();
}

function semaforoestatus($es){
	$res = "";
	if ($es == 'A') {
		$res = "<i class='fa fa-circle ico-centrado verde'></i>";
	}else if ($es == 'B'){
		$res = "<i class='fa fa-circle ico-centrado rojo'></i>";
	}
	return $res;
}
function agregacomboestatus(){
	echo "<select id='cmbestatus' class='control-combo'><option value='A'>ALTA</option><option value='B'>BAJA</option></select>";
}

function titulotraduccion($idIdioma, $titulo)
{
	$strSQL = "CALL paPaginaTituloTraduccion('".$idIdioma."','".$titulo."')";
	$resultado = consulta($strSQL);
	//echo $strSQL;
	while ($row = $resultado->fetch_array(MYSQLI_ASSOC)){
		return $row["traduccion"];
	}
	$resultado->close();
}


/*function agregacombo($catalogo, $default)
{
	$strSQL = "CALL combo('".$catalogo."')";
	$resultado = consulta($strSQL);
	echo "<select class='control-combo' style='width:100%' id='cmb".strtolower($catalogo)."' name='cmb".strtolower($catalogo)."'>";
	while ($row = $resultado->fetch_array(MYSQLI_ASSOC)){
		$seleccionado = "";
		if ($row["valor"] == $default) {
			$seleccionado = "selected";
		}
		echo "<option value='".$row["id"]."' ".$seleccionado.">".utf8_encode($row["valor"])."</option>";
	}
	echo "</select>";
	$resultado->close();
}*/
function dia($i)
    {
        $dias = array("Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","Sábado");
        return $dias[$i];
    }
	
	
function agregacombo($catalogo, $idExtra = 0, $default = '')
{
	
	$strSQL = "CALL paCargaDatosCombo('".$catalogo."','".$idExtra."')";
	$resultado = consulta($strSQL);
	echo "<select class='caja-busqueda'  id='cmb".strtolower($catalogo)."' name='cmb".strtolower($catalogo)."'>";
	while ($row = $resultado->fetch_array(MYSQLI_ASSOC)){
		$seleccionado = "";
		if ($row["valor"] == $default) {
			$seleccionado = "selected";
		}
		echo "<option value='".$row["id"]."' ".$seleccionado.">".$row["valor"]."</option>";
	}
	echo "</select>";
	$resultado->close();
}

function mes($i)
    {
        $meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
        return $meses[$i];
    }


?>
