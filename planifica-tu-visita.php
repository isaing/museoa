<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="es"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title>Museo Km/h Guanajuato</title>
    <base href="http://masviral.mx/ftpmasviral/web/"; />
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="estilo.css">
    <link rel="stylesheet" href="css/menu.min.css">    
    <link rel="stylesheet" href="bower_components/animate.css/animate.min.css">
    <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
</head>
<body id="top">
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v3.1';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<section class="mod-principal">
    <section class="navigation">
        <div class="menu-usuario">
            <div class="idioma">
            </div>
            <!--idioma-->
        
            <div class="redes">
                <a class="facebook" href="#"></a>
                <a class="twitter" href="#"></a>
                <a class="youtube" href="#"></a>
                <a class="instagram" href="#"></a>
            </div>
            <!--redes-sociales-->
        </div>
        
        <div class="logo">
        	<div class="logo-prin">
	        	<img class="logo-km-h" src="img/museo-km-h-gto.svg" alt="Museo Km/h">
            </div>
             <!--Museo km/h-->
             
             <div class="busqueda">
             </div>
             <!--búsqueda-->
        </div>
        <!--Museo km/h-búsqueda-->
        
        <div class="menu-principal">
        	<div class="contenido">
                <div class="ve-menu">
                  <ul class="ve-menu-pc">
                    <li><a href="index.php">Inicio</a></li>
                    <li><a href="acerca-del-museo.php">Acerca del Museo</a></li>
                    <li><a href="#">Salas de Exhibición</a>
                      <ul>
                        <li><a href="#">Evolución e Historia del Automóvil</a></li>
                        <li><a href="sala-2.php">Anatomía del Automóvil</a></li>
                        <li><a href="#">Cadena de ensamblaje</a></li>
                        <li><a href="sala-4.php">Galería de Autos</a></li>
                        <li><a href="#">El futuro</a></li>
                      </ul>
                    </li>
                    <li><a href="eventos.php">Eventos</a></li>
                    <li><a href="#">Información para el visitante</a>
                      <ul>
                        <li><a href="como-llegar.php">Como llegar</a></li>
                        <li><a href="costos.php">Costos</a></li>
                        <li><a href="mapa-del-museo.php">Mapa del Museo</a></li>
                        <li><a href="planifica-tu-visita.php">Planifica tu visita</a></li>
                        <li><a href="grupos-excursiones.php">Grupos y excursiones</a></li>
                      </ul>
                    </li>
                  </ul>
                </div>       
            </div>
        </div>
        <!--menú-principal-->
		<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/fastclick/1.0.6/fastclick.min.js"></script>
        <script src="js/menu.min.js"></script>
        <script>
            $('.ve-menu').menu({
                fontSize: 17,
                fontColor: '#222',
                bgColor: '#fff',
                hoverFontColor: '#0694a7',
                hoverBgColor: '#fff',
                itemSpace: 5,
                subFontSize: 17,
                itemWidth: 70,
                animate: 'slide',
                speed: 500,
            });
        </script>
    </section>
</section>
<!--Módulo-principal-->

<div class="encabezado-info-visitante">
	<div class="contenido">
    	<h1 class="tit-info-visit">Planifica tu visita</h1>
        <div class="txt-info-visit">
        	Te compartimos algunos consejos prácticos para aplicarlos en tu próxima visita.
        </div>
        <!--Planifica tu visita Km/h-->
    </div>
</div>
<!--Información para el Visitante-->

<div class="contenido">
	<div class="sub-tit-visit">
    	<img class="ico-planifica-visit" src="img/planifica-visita.svg" alt="Planifica tu visita">Disfruta de tu visita en el Km/h.
    </div>
    
    <div class="flow-a">
    	<div class="item-planifica">
        	<div class="tip">
	            <img class="ico-planear" src="img/calendar.svg" alt="Agenda">
                <span class="tiket">¿Qué día vienes al Km/h?</span><br><br>
                
                Prepárate un poco antes de entrar en el museo: lee alguna cosa sobre aquello que vas a ver (contexto histórico, autores, etc.) y 
                sobre la historia del museo (origen de sus colecciones, fundación del museo etc.). Esto te ayudará a planificar tu visita.         
            </div>
            
            <div class="linea-tiempo">
            	<img class="ico-line-time" src="img/line-time-1.svg" alt="Línea de tiempo">
            </div>
            <!--Línea-de-tiempo-->
        </div>
        <!--pregunta-1-->
        
    	<div class="item-planifica">
        	<div class="tip">
	            <img class="ico-planear" src="img/time.svg" alt="Tiempo">
                <span class="tiket">¿Cuánto tiempo estarás en el km/h?</span><br><br>
                
                Ten en cuenta el tiempo que tienes para realizar la visita y cuál es la extensión y número de salas del museo. No te agobies con el 
                tiempo: Si nos gusta algo en concreto, dediquémosle todo el tiempo necesario, sin pensar en lo que aún nos queda por ver. Es 
                conveniente que observemos las cosas con tranquilidad y sin asignar ritmos.         
            </div>
            
            <div class="linea-tiempo">
            	<img class="ico-line-time" src="img/line-time-2.svg" alt="Línea de tiempo">
            </div>
            <!--Línea-de-tiempo-->
        </div>
        <!--pregunta-2-->

    	<div class="item-planifica">
        	<div class="tip">
	            <img class="ico-planear" src="img/group-planifica.svg" alt="Grupos y perfiles">
                <span class="tiket">¿Qué edades tienes en tu grupo?</span><br><br>
                Si vamos en familia, con pequeños, es bueno saber si hay actividades para ellos. Esto es una gran ventaja, porque los niños jugarán 
                aprendiendo y adquirirán el gusto de ir a los museos.
            </div>
            
            <div class="linea-tiempo">
            	<img class="ico-line-time" src="img/line-time-3.svg" alt="Línea de tiempo">
            </div>
            <!--Línea-de-tiempo-->
        </div>
        <!--pregunta-3-->

    	<div class="item-planifica">
        	<div class="tip">
	            <img class="ico-planear" src="img/tiket-planifica.svg" alt="Grupos y perfiles">
                <span class="tiket">Compra tus entradas con anticipación</span><br><br>
                Fundamental para evitar colas.  Lo ideal es saber con antelación si hay algún espectáculo al que queramos asistir, y comprar las 
                entradas online sino es gratuito.
            </div>
            
            <div class="linea-tiempo">
            	<img class="ico-line-time" src="img/line-time-4.svg" alt="Línea de tiempo">
            </div>
            <!--Línea-de-tiempo-->
        </div>
        <!--pregunta-4-->

    	<div class="item-planifica">
        	<div class="tip">
	            <img class="ico-planear" src="img/mapa-planifica.svg" alt="Grupos y perfiles">
                <span class="tiket">Adquiere un mapa del museo</span><br><br>
                Ve el mapa cuando llegues al museo, así descubrirás cuáles son los hitos que más te interesan. El recorrido debe ser en dirección de 
                más interesante a menos interesante para nosotros, pensando en que el tiempo irá pasando y el cansancio aumentará.
            </div>
            
            <div class="linea-tiempo">
            	<img class="ico-line-time" src="img/line-time-5.svg" alt="Línea de tiempo">
            </div>
            <!--Línea-de-tiempo-->
        </div>
        <!--pregunta-5-->
    </div>
</div>
<!--Planifica tu visita Km/h-->

<div class="flow-b"></div>

     <footer>
     	<div class="contenido">
        	<div class="km-h">
            	<img src="img/km-h-museo.svg" alt="Museo Km/h">
            </div>
            <!--Logotipo Km/h-->
            
            <div class="col-4">
            	<h2>Salas de Exhibición</h2>
                <ul class="submenu">
                	<li><a href="#">Autocinema</a></li>
                    <li><a href="#">Anatomía del  automóvil</a></li>
                    <li><a href="#">Cadena de producción</a></li>
                    <li><a href="#">Galería de autos</a></li>
                    <li><a href="#">El futuro</a></li>
                </ul>
            </div>
            <!--submenú-1 Km/h-->

            <div class="col-4">
            	<h2>Información para el Visitante</h2>
                <ul class="submenu">
                	<li><a href="como-llegar.php">Como llegar</a></li>
                    <li><a href="costos.php">Costos</a></li>
                    <li><a href="mapa-del-museo.php">Mapa del Museo</a></li>
                    <li><a href="planifica-tu-visita.php">Planifica tu visita</a></li>
                    <li><a href="grupos-excursiones.php">Grupos y excursiones</a></li>
                </ul>
            </div>
            <!--submenú-2 Km/h-->

            <div class="col-4">
            	<h2>Ubicación y horarios</h2>
                <div class="dato">
                	<img class="ico-dato" src="img/address-f.svg"> Carretera de Cuota Silao-Gto. Km 3.8, Los Rodríguez, 36270 Silao, Gto.
                </div>
                
                <div class="dato">
                	<img class="ico-dato" src="img/phone-f.svg"> <a class="link-footer" href="#">01 472 + 0 000 000</a>
                </div>
                
                <div class="dato">
                    Abierto: 10:00 a 19:00 hrs.<br>
                    Martes a Domingo                
                </div>
            </div>
            <!--dirceción Km/h-->
        </div>
     </footer>
     <!--Datos del Museo Km/h-->
     
	<div class="derechos"> 
    	Copyright ©  <?php include ("copy-date.php")?> <img class="logo-museo" src="img/km-h-gto.svg" alt="Museo Km/h">
    </div>
    <!--copyright © 2018 Km/h-->     
    
    <section class="to-top">
            <div class="row">
                <div class="to-top-wrap">
                    <a href="#top" class="top"><span class="arriba">&#710;</span></i></a>
                </div>
            </div>
        </div>
    </section>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.min.js"><\/script>')</script>
    <script src="js/jquery.fancybox.pack.js"></script>
    <script src="js/vendor/bootstrap.min.js"></script>
    <script src="js/scripts.js"></script>
    <script src="js/jquery.flexslider-min.js"></script>
    <script src="bower_components/classie/classie.js"></script>
    <script src="bower_components/jquery-waypoints/lib/jquery.waypoints.min.js"></script>
</body>
</html>
