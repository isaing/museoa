<div class="contenido">
	<div class="tit-siguenos">
    	<? echo titulotraduccion($idIdioma, 'Síguenos en');?>:
    </div>
    <!--título-de-sección-->

	<div class="red-social-facebook">
    	<div class="social-media"><img class="ico-red" src="img/facebook-window.svg" alt="facebook">/KmGuanajuato</div>
        <div class="marco-social">
		<div class="fb-page" data-href="https://www.facebook.com/ParqueGuanajuatoBicentenario/" data-tabs="timeline" 
        data-height="350" data-small-header="true" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="false"><blockquote 
        cite="https://www.facebook.com/ParqueGuanajuatoBicentenario/" class="fb-xfbml-parse-ignore">
        <a href="https://www.facebook.com/ParqueGuanajuatoBicentenario/">Parque Guanajuato Bicentenario</a>
        </blockquote></div>    
        </div>
    </div>
    <!--facebook-Km/h-->
    
	<div class="red-social-twitter">
    	<div class="social-media"><img class="ico-red" src="img/twitter-window.svg" alt="twitter">/KmGuanajuato</div>
        <div class="marco-social">
		<a class="twitter-timeline" data-height="350" href="https://twitter.com/PGBicentenario?ref_src=twsrc%5Etfw">Tweets by PGBicentenario</a> 
		<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
        </div>
    </div>
    <!--twitter-Km/h-->
    
	<div class="red-social-instagram">
    	<div class="social-media-bis"><img class="ico-red" src="img/instagram-window.svg" alt="instagram">/KmGuanajuato</div>
        <!-- LightWidget WIDGET -->
        <div class="marco-social">
        <script src="https://cdn.lightwidget.com/widgets/lightwidget.js"></script>
        <iframe src="//lightwidget.com/widgets/e953a0a42a9e5ca3adeec3759fb5eb2b.html" scrolling="no" allowtransparency="true" 
        class="lightwidget-widget" style="width:100%;border:0;overflow:hidden;"></iframe>
        </div>
    </div>
    <!--facebook-Km/h-->
</div>