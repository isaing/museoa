<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="es"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title>Museo Km/h Guanajuato</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="estilo.css">
    <link rel="stylesheet" href="css/menu.min.css">    
    <link rel="stylesheet" href="bower_components/animate.css/animate.min.css">
    <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
    <link rel="stylesheet" type="text/css" href="css/base.css" />
	<script>document.documentElement.className="js";var supportsCssVars=function(){var e,t=document.createElement("style");
	return t.innerHTML="root: { --tmp-var: bold; }",document.head.appendChild(t),e=!!
	(window.CSS&&window.CSS.supports&&window.CSS.supports("font-weight","var(--tmp-var)")),t.parentNode.removeChild(t),e};supportsCssVars()||
	alert("Please view this demo in a modern browser that supports CSS Variables.");</script>    
</head>
<body id="top">
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v3.1';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<section class="mod-principal">
    <section class="navigation">
        <div class="menu-usuario">
            <div class="idioma">
            </div>
            <!--idioma-->
        
            <div class="redes">
                <a class="facebook" href="#"></a>
                <a class="twitter" href="#"></a>
                <a class="youtube" href="#"></a>
                <a class="instagram" href="#"></a>
            </div>
            <!--redes-sociales-->
        </div>
        
        <div class="logo">
        	<div class="logo-prin">
	        	<img class="logo-km-h" src="img/museo-km-h-gto.svg" alt="Museo Km/h">
            </div>
             <!--Museo km/h-->
             
             <div class="busqueda">
             </div>
             <!--búsqueda-->
        </div>
        <!--Museo km/h-búsqueda-->
        
        <div class="menu-principal">
        	<div class="contenido">
                <div class="ve-menu">
                  <ul class="ve-menu-pc">
                    <li><a href="index.php">Inicio</a></li>
                    <li><a href="acerca-del-museo.php">Acerca del Museo</a></li>
                    <li><a href="#">Salas de Exhibición</a>
                      <ul>
                        <li><a href="#">Evolución e Historia del Automóvil</a></li>
                        <li><a href="sala-2.php">Anatomía del Automóvil</a></li>
                        <li><a href="#">Cadena de ensamblaje</a></li>
                        <li><a href="sala-4.php">Galería de Autos</a></li>
                        <li><a href="#">El futuro</a></li>
                      </ul>
                    </li>
                    <li><a href="eventos.php">Eventos</a></li>
                    <li><a href="#">Información para el visitante</a>
                      <ul>
                        <li><a href="#">Como llegar</a></li>
                        <li><a href="#">Costos</a></li>
                        <li><a href="#">Mapa del Museo</a></li>
                        <li><a href="#">Planifica tu visita</a></li>
                        <li><a href="#">Grupos y excursiones</a></li>
                      </ul>
                    </li>
                  </ul>
                </div>       
            </div>
        </div>
        <!--menú-principal-->
		<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/fastclick/1.0.6/fastclick.min.js"></script>
        <script src="js/menu.min.js"></script>
        <script>
            $('.ve-menu').menu({
                fontSize: 17,
                fontColor: '#222',
                bgColor: '#fff',
                hoverFontColor: '#0694a7',
                hoverBgColor: '#fff',
                itemSpace: 5,
                subFontSize: 17,
                itemWidth: 70,
                animate: 'slide',
                speed: 500,
            });
        </script>
    </section>
</section>
<!--Módulo-principal-->
	
    <div class="contenido">
    	<h1 class="sala-detalle">Sala 04 <span class="acento">Galería de autos</span></h2>
        
        <div class="auto-detalle-1">
        	<div class="flow-a">
            	<img class="movil-detalle" src="img/autos/toyota-2000-gt.jpg" alt="TOYOTA 2000GT">
                <!--automóvil-km/h-->
                
                <div class="planifica-home">
                    <a href="#">
                    <img class="planea-detalle" src="img/planifica-visita.svg" alt="Planifica tu visita al Km/h"></a>
                </div>
                <!--planifica-tu-visita-km/h-->
                
                <div class="compartir-home">
                    <div class="social">
                        <div class="fb-share-button" data-href="https://developers.facebook.com/docs/plugins/" data-layout="button_count" 
                        data-size="small" data-mobile-iframe="true">
                        <a target="_blank" href=
                        "https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fdevelopers.facebook.com%2Fdocs%2Fplugins%2F&amp;src=sdkpreparse"
                        class="fb-xfbml-parse-ignore">Compartir</a></div>
                    </div>
                    <!--facebook-->
                    
                    <div class="social">
                        <a href="https://twitter.com/share?ref_src=twsrc%5Etfw" class="twitter-share-button" data-show-count="false">Tweet</a>
                        <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
                    </div>
                    <!--twitter-->
                </div>
                <!--compartir-web-km/h-->
            </div>
        </div>
        <!--fotografía-automóvil-->

        <div class="auto-detalle-2">
        	<div class="tit-detalle"><span class="marca">TOYOTA</span> 2000GT</div>
            <div class="txt-detalle">
				Automóvil gran turismo, biplaza producido entre 1967 y 1970para el fabricante Toyota. Se presentó por primera vez en el Salón del 
                Automóvil de Tokio en 1965, y revolucionó la visión del mundo del automóvil de Japón.
            </div>
            <div class="txt-origen">
            	<img class="bandera-detalle" src="img/banderas/japon.svg" alt="Japón"> País de origen:Japón
            </div>
            <div class="txt-origen">
            	Año:1966
            </div>
        </div>
        <!--datos-automóvil-->
    </div>
	<!--información-gral.-automóvil-->

    <main>
    	<div class="grid">
				<div class="grid__item theme-9">
					<div class="grid__item-content">
						<span class="grid__item-meta">Datos del Vehículo:</span>
                        <p class="grid__item-text">
                            Longitud / longitud: 2547 mm<br>
                            Ancho: 1454 mm<br>
                            Altura / Altura: 1623 mm<br>
                            Masa del vehículo / masa del vehículo: 313 kg<br>
                            Distancia entre ejes / distancia entre ejes: 1450 mm<br>
                            Tipo de motor / tipo de motor: Solo cilindro refrigerado por agua<br>
                            Refrigerado por agua, horizontal, monocilíndrico<br>
                            Desplazamiento total / Desplazamiento total: 984 cm 3<br>
                            Salida máxima: 0.9 / 0.7 / 400 (HP / kW / min - 1)                        
                        </p>
					</div>
				</div>
				<div class="grid__item grid__item--bg theme-10">
					<div class="grid__item-img" data-displacement="img/displacement/10.jpg" data-intensity="0.7" data-speedIn="1" 
                    data-speedOut="0.5" data-easing="Power2.easeOut">
                        <img src="img/autos/toyota-2000-gt-interiores-1.jpg" alt="TOYOTA 2000GT"/>
                        <img src="img/autos/toyota-2000-gt-interiores-2.jpg" alt="TOYOTA 2000GT"/>
					</div>
					<div class="grid__item-content">
					</div>
				</div>
        </div>
    </main>
    <!--Maqueta-Región-Dorada-->
    <script src="js/imagesloaded.pkgd.min.js"></script>
    <script src="js/three.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/1.20.3/TweenMax.min.js"></script>
	<script src="js/hover.js"></script>
		<script>
			Array.from(document.querySelectorAll('.grid__item-img')).forEach((el) => {
				const imgs = Array.from(el.querySelectorAll('img'));
				new hoverEffect({
					parent: el,
					intensity: el.dataset.intensity || undefined,
					speedIn: el.dataset.speedin || undefined,
					speedOut: el.dataset.speedout || undefined,
					easing: el.dataset.easing || undefined,
					hover: el.dataset.hover || undefined,
					image1: imgs[0].getAttribute('src'),
					image2: imgs[1].getAttribute('src'),
					displacementImage: el.dataset.displacement
				});
			});
		</script>
        
     <footer>
     	<div class="contenido">
        	<div class="km-h">
            	<img src="img/km-h-museo.svg" alt="Museo Km/h">
            </div>
            <!--Logotipo Km/h-->
            
            <div class="col-4">
            	<h2>Salas de Exhibición</h2>
                <ul class="submenu">
                	<li><a href="#">Autocinema</a></li>
                    <li><a href="#">Anatomía del  automóvil</a></li>
                    <li><a href="#">Cadena de producción</a></li>
                    <li><a href="#">Galería de autos</a></li>
                    <li><a href="#">El futuro</a></li>
                </ul>
            </div>
            <!--submenú-1 Km/h-->

            <div class="col-4">
            	<h2>Información para el Visitante</h2>
                <ul class="submenu">
                	<li><a href="#">Como llegar</a></li>
                    <li><a href="#">Costos</a></li>
                    <li><a href="#">Mapa del Museo</a></li>
                    <li><a href="#">Planifica tu visita</a></li>
                    <li><a href="#">Grupos y excursiones</a></li>
                </ul>
            </div>
            <!--submenú-2 Km/h-->

            <div class="col-4">
            	<h2>Ubicación y horarios</h2>
                <div class="dato">
                	<img class="ico-dato" src="img/address-f.svg"> Carretera de Cuota Silao-Gto. Km 3.8, Los Rodríguez, 36270 Silao, Gto.
                </div>
                
                <div class="dato">
                	<img class="ico-dato" src="img/phone-f.svg"> <a class="link-footer" href="#">01 472 + 0 000 000</a>
                </div>
                
                <div class="dato">
                    Abierto: 10:00 a 19:00 hrs.<br>
                    Martes a Domingo                
                </div>
            </div>
            <!--dirceción Km/h-->
        </div>
     </footer>
     <!--Datos del Museo Km/h-->
     
	<div class="derechos"> 
    	Copyright ©  <?php include ("copy-date.php")?> <img class="logo-museo" src="img/km-h-gto.svg" alt="Museo Km/h">
    </div>
    <!--copyright © 2018 Km/h-->     
    
    <section class="to-top">
            <div class="row">
                <div class="to-top-wrap">
                    <a href="#top" class="top"><span class="arriba">&#710;</span></i></a>
                </div>
            </div>
        </div>
    </section>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.min.js"><\/script>')</script>
    <script src="js/jquery.fancybox.pack.js"></script>
    <script src="js/vendor/bootstrap.min.js"></script>
    <script src="js/scripts.js"></script>
    <script src="js/jquery.flexslider-min.js"></script>
    <script src="bower_components/classie/classie.js"></script>
    <script src="bower_components/jquery-waypoints/lib/jquery.waypoints.min.js"></script>
</body>
</html>
