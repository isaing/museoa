 <? include_once("idioma.php");?>
 <?


  require_once('includes/funcs.php');
  /* datos generales*/
  $sentencia = "CALL paPaginaDatosGenerales('$idIdioma')";
  $resultado = consulta($sentencia);
  $cuantos=$resultado->num_rows;
  if($cuantos>0){
	   while ($row = $resultado->fetch_array(MYSQLI_ASSOC)){
		   $iddatosgenerales=$row["iddatosgenerales"];
		   $direccion=$row["direccion"];
		   $telefono=$row["telefono"];
		   $correo=$row["correo"];
		   $longitud=$row["longitud"];
		   $latitud=$row["latitud"];
		   $horarios=$row["horarios"];
		   $titulopdfgeneral=$row["titulopdf"];
		   $imagengeneral=$row["imagen"];
	 }
  }
  else{
	  $direccion=""; $telefono=""; $longitud=""; $latitud=""; $horarios="";
	 }
  ?> 
 <section class="navigation">
        <div class="menu-usuario">
            <div class="idioma">
                <div id="dd" class="wrapper-dropdown-3" tabindex="1">
                  <img class="ico-idioma" src="img/idioma.svg" alt="Idioma"><span id="spanidioma">Idioma</span>
                    <ul class="dropdown">
                      <li><a id="espanol" href="#">Español</a></li>
                        <li><a id="ingles" href="#">English</a></li>
                    </ul>
                </div>
            </div>

            <!--idioma-->
         <? include_once("modulos/redes/redes.php");?>
            <!--redes-sociales-->
        </div>
        
        <div class="logo">
        	<div class="logo-prin">
	        	<a href="./"><img class="logo-km-h" src="img/museo-km-h-gto.svg" alt="Museo Km/h"></a>
            </div>
             <!--Museo km/h-->
             
             <div class="busqueda">
                <div id="sb-search" class="sb-search">
                    <form id="frm-buscar">
                        <input class="sb-search-input" placeholder="Ingresa tu término de búsqueda..." type="text" value="" name="search" id="search">
                        <input class="sb-search-submit" type="submit" value="">
                        <span class="sb-icon-search"></span>
                    </form>
                </div>
 

               <!-- <input type="buscador" class="buscador" name="buscador"> <button class="bt-buscar">Buscar</button>-->
                 <p class="mb-0">
                    <a id="btnbox" data-fancybox data-animation-duration="700" data-src="#animatedModal" href="javascript:;" class="btn btn-primary">Open demo</a>
                 </p>
               <div  id="animatedModal" class="animated-modal">
                 <div id="areabusca"><input type="buscador" class="buscador2" name="buscador"> <button class="bt-buscar2">Buscar</button></div>
                 <div id="animatedModal2"></div>
               </div>
            <script src="js/modernizr.custom.js"></script>    
            <script src="js/classie.js"></script>
            <script src="js/uisearch.js"></script>
            <script>
                new UISearch( document.getElementById( 'sb-search' ) );
            </script>
             </div>
             <!--búsqueda-->
        </div>
        <!--Museo km/h-búsqueda-->
        
        <? include_once("modulos/menuprincipal/menuprincipal.php");?>
        <!--menú-principal-->
		<script src="https://code.jquery.com/jquery-1.12.4.min.js" 
		></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/fastclick/1.0.6/fastclick.min.js"></script>
        <script src="js/menu.min.js"></script>
        <script src="js/cookie.js"></script>
        <script src="js/buscador.js"></script>
        <script>
            $('.ve-menu').menu({
                fontColor: '#222',
                bgColor: '#fff',
                hoverFontColor: '#0694a7',
                hoverBgColor: '#fff',
                itemSpace: 5,
                animate: 'slide',
                speed: 500,
            });
        </script>
                        <script type="text/javascript">
                    
                    function DropDown(el) {
                        this.dd = el;
                        this.placeholder = this.dd.children('span');
                        this.opts = this.dd.find('ul.dropdown > li');
                        this.val = '';
                        this.index = -1;
                        this.initEvents();
                    }
                    DropDown.prototype = {
                        initEvents : function() {
                            var obj = this;
        
                            obj.dd.on('click', function(event){
                                $(this).toggleClass('active');
                                return false;
                            });
        
                            obj.opts.on('click',function(){
                                var opt = $(this);
                                obj.val = opt.text();
                                obj.index = opt.index();
                                obj.placeholder.text(obj.val);
                            });
                        },
                        getValue : function() {
                            return this.val;
                        },
                        getIndex : function() {
                            return this.index;
                        }
                    }
        
                    $(function() {
        
                        var dd = new DropDown( $('#dd') );
        
                        $(document).click(function() {
                            // all dropdowns
                            $('.wrapper-dropdown-3').removeClass('active');
                        });
        
                    });
        
                </script>
    </section>