  <main>
    	<div class="grid">
        	<div class="grid__item grid__item--bg theme-3">
            	<div class="grid__item-img" data-displacement="img/displacement/4.png" data-intensity="0.2" data-speedIn="1.6" data-speedOut="1.6">
                	<img src="img/maqueta-region-dorada.jpg" alt="Maqueta Región Dorada"/>
                    <img src="img/maqueta-region-dorada-gto.jpg" alt="Guanajuato Región Dorada"/>
                </div>
            </div>
            <div class="grid__item theme-4">
            	<div class="grid__item-content">
                	<h2 class="grid__item-title galeria">Maqueta</h2>
                    <h2 class="grid__item-title grid__item-title--small galeria">Región Dorada</h2>
                	<p class="grid__item-text">
                    	Lorem Ipsum is simply dummy text of the printing  and typesetting industry. Lorem Ipsum has been the industry's standard 
                        dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.
                        <br><br>
                        <a class="grid__item-link" href="#">ver más</a>
                    </p>
                    
                </div>
            </div>
        </div>
    </main>
    <!--Maqueta-Región-Dorada-->
    <script src="js/imagesloaded.pkgd.min.js"></script>
    <script src="js/three.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/1.20.3/TweenMax.min.js"></script>
	<script src="js/hover.js"></script>
		<script>
			Array.from(document.querySelectorAll('.grid__item-img')).forEach((el) => {
				const imgs = Array.from(el.querySelectorAll('img'));
				new hoverEffect({
					parent: el,
					intensity: el.dataset.intensity || undefined,
					speedIn: el.dataset.speedin || undefined,
					speedOut: el.dataset.speedout || undefined,
					easing: el.dataset.easing || undefined,
					hover: el.dataset.hover || undefined,
					image1: imgs[0].getAttribute('src'),
					image2: imgs[1].getAttribute('src'),
					displacementImage: el.dataset.displacement
				});
			});
		</script>
        
    <div class="galeria-de-autos">
    	<div class="contenido">
        	<h1>Galería de autos</h1>
        	<div class="auto">
            	<a href="ficha-auto.php">
                <img class="automovil" src="img/autos/toyota-2000-gt.jpg" alt="TOYOTA 2000GT"></a>
            	<div class="modelo"><span class="marca">TOYOTA</span> 2000GT</div> 
                <div class="origen-edit"><img class="bandera" src="img/banderas/japon.svg" alt="Japón">País de origen: Japón</div>
                <div class="origen-edit">Año: 1966</div>
            </div>
            <!--Auto 1- Km/h-->

        	<div class="auto">
            	<a href="#">
                <img class="automovil" src="img/autos/mustang.jpg" alt="Ford Mustang"></a>
            	<div class="modelo"><span class="marca">Ford</span> Mustang</div> 
                <div class="origen-edit"><img class="bandera" src="img/banderas/estados-unidos.svg" alt="USA">País de origen: USA</div>
                <div class="origen-edit">Año: 1967</div>
            </div>
            <!--Auto 2- Km/h-->
            
        	<div class="auto">
            	<a href="#">
                <img class="automovil" src="img/autos/chevrolet-corvette.jpg" alt="CHEVROLET CORVETTE"></a>
            	<div class="modelo"><span class="marca">CHEVROLET</span> CORVETTE</div> 
                <div class="origen-edit"><img class="bandera" src="img/banderas/estados-unidos.svg" alt="USA">País de origen: USA</div>
                <div class="origen-edit">Año: 1966</div>
            </div>
            <!--Auto 3- Km/h-->
        </div>
    </div>        
    <!--Galería de Autos Km/h-->