<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="es"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title>Mi foto en el Museo</title>
<base href="http://masviral.mx/ftpmasviral/web/"; />
 
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta property="og:url"           content="https://www.museo.gto/mi-foto" />
  <meta property="og:type"          content="website" />
  <meta property="og:title"         content="Mi foto en el museo" />
  <meta property="og:description"   content="Foto de mi visita al museo Km/h" />
  <meta property="og:image"         content="http://masviral.mx/ftpmasviral/web/img/museo-km-h-gto.svg" />
  <meta name="twitter:card" content="summary_large_image">
  <meta name="twitter:site" content="@museo">
  <meta name="twitter:title" content="Mi foto en el museo">
  <meta name="twitter:description" content="Foto de mi visita al museo Km/h">
  <meta name="twitter:image" content="http://masviral.mx/ftpmasviral/web/img/museo-km-h-gto.svg">
  <meta name="twitter:domain" content="museokmh.com">
  <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="estilo.css">
  <link rel="stylesheet" type="text/css" href="bower_components/dist/jquery.fancybox.min.css">
    <link rel="stylesheet" href="css/menu.min.css">    
    <link rel="stylesheet" href="bower_components/animate.css/animate.min.css">
    <link rel="stylesheet" type="text/css" href="css/gale.css">
    
    <link rel="stylesheet" type="text/css" href="bower_components/datepicker/css/bootstrap-datepicker.css">
    <style type="text/css">
    #fechadetalle{
      padding: 15px;
    font-size: 22px;
    font-weight: 700;
      }
     </style>
    <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
</head>
<body id="top">
<div id="fb-root"></div>


<section class="mod-principal">
    <? include_once("header.php");?>
</section>
<!--Módulo-principal-->


<div class="contenido" id="sandbox-container">
	<div class="tit-eventos">
    	<? echo "Mi foto en el Museo";?>
    </div>
	<? 
	require_once('includes/funcs.php');
  $idimg = $_GET['s'];
	?>
  <div id="sandbox-container2">
  <div id="fechadetalle"></div>
    <?php echo "<input type='text' id='idimg' value='$idimg' style='display:none;'><img class='fotoapp' src='https://guanajuato.gob.mx/apiCabina/tmp/output$idimg.png'>"; ?>
  
  </div>
</div>

	<? //include_once("modulos/redes/redes_socialmedia.php");?>
    
    <!--social-media-Km/h-->


     <?php include_once("footer.php");?>
     <!--Datos del Museo Km/h--> 
     
	 <?php include_once("derechos.php");?>
	 <!--copyright © 2018 Km/h-->      
    

    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.min.js"><\/script>')</script>
    
    <script src="bower_components/dist/jquery.fancybox.min.js"></script>
    <script src="js/vendor/bootstrap.min.js"></script>
    <script src="js/scripts.js"></script>
    <script src="js/detallefoto.js"></script>
    <script src="js/jquery.flexslider-min.js"></script>
    <script src="bower_components/classie/classie.js"></script>
    <script src="bower_components/jquery-waypoints/lib/jquery.waypoints.min.js"></script>
    <script src="bower_components/datepicker/js/bootstrap-datepicker.js"></script>
    
<script type="text/javascript">



</script>

<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v3.1';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>




</body>
</html>
