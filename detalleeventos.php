<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="es"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title>Museo Km/h Guanajuato</title>
  <base href="http://masviral.mx/ftpmasviral/web/"; />
    <!--<base href="http://localhost/museoautomovil/"; />-->

    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

  <link rel="stylesheet" type="text/css" href="bower_components/dist/jquery.fancybox.min.css">
    <link rel="stylesheet" href="estilo.css">
    <link rel="stylesheet" href="css/menu.min.css">    
    <link rel="stylesheet" href="bower_components/animate.css/animate.min.css">
    <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
</head>
<body id="top">
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v3.1';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<section class="mod-principal">
    <? include_once("header.php");?>
</section>
<!--Módulo-principal-->


<div class="contenido">
	<div class="tit-eventos">
    	<? echo titulotraduccion($idIdioma, 'Eventos');?>
    </div>
	<? 
	require_once('includes/funcs.php');
	$traduccionfecha=titulotraduccion($idIdioma, 'Fecha');
	$traduccionhora=titulotraduccion($idIdioma, 'Hora');
	$traduccionlugar=titulotraduccion($idIdioma, 'Lugar');
	$traduccioncosto=titulotraduccion($idIdioma, 'Costo');
  	$evento=htmlspecialchars($_GET["s"]);
	//echo $evento;
	$descargar= titulotraduccion($idIdioma, 'Descargar');
	$sentenciap = "CALL paPaginaEventos('$idIdioma','2','$evento')";
	//echo $sentenciap;
  	$resultadop = consulta($sentenciap);
   $cuantosp=$resultadop->num_rows;
	
  if($cuantosp>0){
	
	  while ($rowp= $resultadop->fetch_array(MYSQLI_ASSOC)){
		 	$id_evento=$rowp["id_evento"];
			$evento=$rowp["evento"];
			$fechas=$rowp["fechas"];
			$horario=$rowp["horario"];
			$lugar=$rowp["lugar"];
			$costo=$rowp["costo"];
			$descripcion=$rowp["descripcion"];
			$anexo=$rowp["anexo"];
			$target=$rowp["target"];
			$idtraduccion=$rowp["id_eventotraduccion"];
			
		  
	?>
      
     <div class="detalle-eventos">
	<div class="mod-evento">
    	<div class="detalle-espectaculo"><? echo $evento;?></div>
        <div class="img-detalle"><img class="foto-detalle" src="modulos/img/eventos/eventos-<? echo $id_evento;?>.jpg" alt="<? echo $evento;?>"></div>
        
        <div class="txt-detalle">
        	<span class="detalle"><? echo $traduccionfecha;?>: </span> <? echo $fechas;?>	<span class="detalle"><? echo $traduccionhora;?>: </span> <? echo $horario;?>	<span class="detalle"><? echo $traduccionlugar;?>: </span><? echo $lugar;?>   <? if ($costo<>""){?>
            <!--<span class="tiket">Costo:</span>--> <span class="detalle"><? echo $traduccioncosto;?>: </span> <? echo $costo;?><br>   
            <? } ?>   <br><br>
           
			<? echo nl2br($descripcion);?>  <br><br>
             <? if ($anexo==1){?>
             	<? $pesoarchivo=filesize("modulos/img/eventos/anexos/anexos-".$idtraduccion.".pdf");
				$pesoarchivo=round(number_format($pesoarchivo / 1024, 2)) . ' KB';;
				?>
            	<a href="modulos/img/eventos/anexos/anexos-<? echo $idtraduccion;?>.pdf" target="_blank"><? echo $descargar;?> (<? echo $pesoarchivo;?>)</a>
            <? }?>  
        </div>
        
    </div>
</div>
      
     <?
	  }
	 
	}
	$resultadop->close();
?>

	
    
    
    	<? include_once("modulos/eventos/listaeventos_interna.php");?>
  
    <!--Próximos Eventos-->
    
    <div class="flow-b"></div>
</div>

     <?php include_once("footer.php");?>
     <!--Datos del Museo Km/h-->
     
	 <?php include_once("derechos.php");?>
	 <!--copyright © 2018 Km/h-->          
    

    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.min.js"><\/script>')</script>
    <script src="bower_components/dist/jquery.fancybox.min.js"></script>
    <script src="js/vendor/bootstrap.min.js"></script>
    <script src="js/scripts.js"></script>
    <script src="js/jquery.flexslider-min.js"></script>
    <script src="bower_components/classie/classie.js"></script>
    <script src="bower_components/jquery-waypoints/lib/jquery.waypoints.min.js"></script>

</body>
</html>
