<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="es"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title>Museo Km/h Guanajuato</title>
    <base href="http://masviral.mx/ftpmasviral/web/"; />
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="bower_components/dist/jquery.fancybox.min.css">
    <link rel="stylesheet" href="estilo.css">
    <link rel="stylesheet" href="css/menu.min.css">    
    <link rel="stylesheet" href="bower_components/animate.css/animate.min.css">
    <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
</head>
<body id="top">
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v3.1';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<section class="mod-principal">
    <? include_once("header.php");?>
</section>
<!--Módulo-principal-->
<? $idtipo= intval($_GET["t"]);
	$etiqueta=htmlspecialchars($_GET["e"]);
	$sentencias = "CALL paPaginaArticulo('$idIdioma','$idtipo','$etiqueta')";
	//echo $sentencias;
	$resultados = consulta($sentencias);
	$cuantoss=$resultados->num_rows;
	if($cuantoss>0){
		while ($rows = $resultados->fetch_array(MYSQLI_ASSOC)){
		$idarticulo=$rows["id_articulo"];
		$idtipoarticulo=$rows["idtipoarticulo"];
		$nombresala=$rows["nombresala"];
		$idnumsala=$rows["idnumsala"];
		//echo $idnumsala. " ".$nombresala;
		$titulo=$rows["titulo"];
		$fraseintro=$rows["fraseintro"];
		$contenido1=$rows["contenido1"];
		$contenido2=$rows["contenido2"];
		$frasecierre=$rows["frasecierre"];
		$imagen=$rows["imagen"];
		$textopdf=$rows["textopdf"];
		$pdf=$rows["pdf"];
		$numerosala=titulotraduccion($idIdioma, 'Sala'). " " .str_pad ( $idnumsala , 2 ,"0" , STR_PAD_LEFT  );
		}
	}
		

?>
<? if($cuantoss>0){?>	
        <div class="contenido">
        	<? if ($idtipo==3){?>
				<h1 class="sala-detalle"><? echo $numerosala;?> <span class="acento"><? echo $nombresala;?></span></h2>
			<? }?>
            <div class="tit-eventos">
                <? echo $titulo;?>
            </div>
            <!--título-de-sección-->
            <? if ($imagen<>""){?>
            <div class="img-ilustra-seccion">
                <img class="imagen-km" src="modulos/img/articulos/<? echo $imagen;?>" alt="Museo km/h">
            </div>
            <? }?>
            <!--ilustra Km/h-->
            
            <div class="frase-destacada">
                 <? echo $fraseintro;?>
            </div>
            <!--frase-introductoria-->
        
            <div class="txt-mod-1">
                 <? echo $contenido1;?>     
            </div>
            <!--contenido-1-->
        </div>
        <!--Acerca-del_Museo-->
        
        <div class="estilo-museo-1">
            <div class="contenido">
                <div class="txt-mod-1">
                     <? echo $contenido2;?>  
                </div>
                <!--contenido-2-->
                
                <div class="frase-destacada">
                    <? echo $frasecierre;?>
                </div>
                <!--frase-destacada-->
            </div>
        </div>
		<!--Acerca-del_Km/h-->
      <? }?>
      
      
     <? include_once("modulos/redes/redes_socialmedia.php");?>
    
    <!--social-media-Km/h-->
     <?php include_once("footer.php");?>
     <!--Datos del Museo Km/h-->
     
	 <?php include_once("derechos.php");?>
	 <!--copyright © 2018 Km/h-->       
    

    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.min.js"><\/script>')</script>
    
     <script src="bower_components/dist/jquery.fancybox.min.js"></script>
    <script src="js/vendor/bootstrap.min.js"></script>
    <script src="js/scripts.js"></script>
    <script src="js/jquery.flexslider-min.js"></script>
    <script src="bower_components/classie/classie.js"></script>
    <script src="bower_components/jquery-waypoints/lib/jquery.waypoints.min.js"></script>
    
    
</body>
</html>
