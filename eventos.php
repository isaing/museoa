<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="es"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title>Museo Km/h Guanajuato</title>
<base href="http://masviral.mx/ftpmasviral/web/"; />
 
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="estilo.css">
  <link rel="stylesheet" type="text/css" href="bower_components/dist/jquery.fancybox.min.css">
    <link rel="stylesheet" href="css/menu.min.css">    
    <link rel="stylesheet" href="bower_components/animate.css/animate.min.css">
    <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
</head>
<body id="top">
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v3.1';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<section class="mod-principal">
    <? include_once("header.php");?>
</section>
<!--Módulo-principal-->


<div class="contenido">
	<div class="tit-eventos">
    	<? echo titulotraduccion($idIdioma, 'Eventos');?>
    </div>
	<? 
	require_once('includes/funcs.php');
  	$traduccionfecha=titulotraduccion($idIdioma, 'Fecha');
	$traduccionhora=titulotraduccion($idIdioma, 'Hora');
	$traduccionlugar=titulotraduccion($idIdioma, 'Lugar');
	$traduccioncosto=titulotraduccion($idIdioma, 'Costo');
	
	$sentenciap = "CALL paPaginaEventos('$idIdioma','0','')";
	$resultadop = consulta($sentenciap);
   $cuantosp=$resultadop->num_rows;
	
  if($cuantosp>0){
	
	  while ($rowp= $resultadop->fetch_array(MYSQLI_ASSOC)){
		 	$id_evento=$rowp["id_evento"];
			$evento=$rowp["evento"];
			$fechas=$rowp["fechas"];
			$horario=$rowp["horario"];
			$lugar=$rowp["lugar"];
			$costo=$rowp["costo"];
			$url=$rowp["url"];
			$anexo=$rowp["anexo"];
			$target=$rowp["target"];
		  
	?>
      
      <div class="eventos">
    	<a href="detalleeventos/<? echo $id_evento;?>">
    	<img class="img-evento" src="modulos/img/eventos/eventos-<? echo $id_evento;?>.jpg" alt="<? echo $evento;?>"></a>
        <div class="txt-evento">
            <img class="ico-escena" src="img/escenario.svg" alt="Eventos Km/h">
            <span class="espectaculo"><? echo $evento;?></span><br><br>
            <span class="tiket"><? echo $traduccionfecha;?>: </span><? echo $fechas;?><br>
            <span class="tiket"><? echo $traduccionhora;?>: </span><? echo $horario;?><br>
            <span class="tiket"><? echo $traduccionlugar;?>: </span><? echo $lugar;?><br>
            <? if ($costo<>""){?>
            <span class="tiket"><? echo $traduccioncosto;?>: </span><? echo $costo;?><br>   
            <? } ?>    
        </div>
    </div>
      
     <?
	  }
	 
	}
	$resultadop->close();
?>

	
    
    
    	<? include_once("modulos/eventos/listaeventos_interna.php");?>
  
    <!--Próximos Eventos-->
    
    <div class="flow-b"></div>
</div>
	<? include_once("modulos/redes/redes_socialmedia.php");?>
    
    <!--social-media-Km/h-->


     <?php include_once("footer.php");?>
     <!--Datos del Museo Km/h-->
     
	 <?php include_once("derechos.php");?>
	 <!--copyright © 2018 Km/h-->      
    

    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.min.js"><\/script>')</script>
    <script src="bower_components/dist/jquery.fancybox.min.js"></script>
    <script src="js/vendor/bootstrap.min.js"></script>
    <script src="js/scripts.js"></script>
    <script src="js/jquery.flexslider-min.js"></script>
    <script src="bower_components/classie/classie.js"></script>
    <script src="bower_components/jquery-waypoints/lib/jquery.waypoints.min.js"></script>
</body>
</html>
