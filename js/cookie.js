var url2=location.protocol + '//' + location.host + location.pathname;
var url = window.location.href;
var idiomaurl=window.location.href.substr(window.location.href.lastIndexOf('/') + 1);
$(function () {
	checkCookie();
    $('#espanol').on( 'click', function () {
        setcookie1();
    });
    $('#ingles').on( 'click', function () {
        setcookie2();
    });
    $(".top").attr("href", url+"#top");
    $(".menusrep").attr("href", url+"#");
    $('.menusrep').click(false); 
    if (window.location.href.substr(window.location.href.lastIndexOf('/') + 1)=="en") {
        $("#spanidioma").text("English");
        $('#ingles').hide();
        $('#espanol').show();
    }
    if (window.location.href.substr(window.location.href.lastIndexOf('/') + 1)=="es") {
        $("#spanidioma").text("Español");
        $('#ingles').show();
        $('#espanol').hide();
    }
    $("#slide-principal").on("swipeleft", function () {
        $(".button-next").click();
    });

    $("#slide-principal").on("swiperight", function () {
        $(".button-previous").click();
    });

    var ley=getCookie("ley");
      if (ley=="ok") {
      }else{
        $("#politica").css("display", "block");
      }
      $( "#btnok" ).click(function() {
        //set cookie
        setCookie('ley','ok',730);
        $("#politica").css("display", "none");
       });
      $( "#btncancelar" ).click(function() {
        $("#politica").css("display", "none");
       });
});

function setcookie1(){
	setCookie('idioma','espanol',730);
	 window.location = reemplaza(url,'es');
	 $("#spanidioma").text("Español");
        $('#ingles').show();
        $('#espanol').hide();
    }

function setcookie2(){
	setCookie('idioma','ingles',730);
	window.location = reemplaza(url,'en');
	$("#spanidioma").text("English");
        $('#ingles').hide();
        $('#espanol').show();
}

function checkCookie() {
    var idioma=getCookie("idioma");
    switch(idioma) {
    case "ingles":
        if (window.location.href.substr(window.location.href.lastIndexOf('/') + 1)!="en") {
        	window.location = reemplaza(url,'en');
            $("#spanidioma").text("English");
        $('#ingles').hide();
        $('#espanol').show();
        }
        
        break;
    case "espanol":
        if (window.location.href.substr(window.location.href.lastIndexOf('/') + 1)!="es") {
        	
        	window.location = reemplaza(url,'es');
            $("#spanidioma").text("Español");
        $('#ingles').show();
        $('#espanol').hide();
        }
        
        break;
    default:
    
        if (idiomaurl != "es" && idiomaurl != "en") {
            $.ajax('http://ip-api.com/json').then(function success(response) {
                
                if (response.country=="Mexico") {
                  window.location = reemplaza(url,'es');
                  $("#spanidioma").text("Español");
        $('#ingles').show();
        $('#espanol').hide();
                }
                if (response.country=="United States") {
                  window.location = reemplaza(url,'en');
                  $("#spanidioma").text("English");
                    $('#ingles').hide();
                  $('#espanol').show();
                }
            },
            function fail(data, status) {
                window.location = reemplaza(url,'es');
                $("#spanidioma").text("Español");
        $('#ingles').show();
        $('#espanol').hide();
            });
        }
        break;
    }
}

function reemplaza(url,newVal){
    var pos = url.lastIndexOf('/');
    //return url.substring(0,pos)+'/'+newVal;
            if (url.substring(pos+1)=="es" || url.substring(pos+1)=="en") {
                return url.substring(0,pos)+'/'+newVal;
            }
            if(url.substring(pos+1)==""){
                return url.substring(0,pos)+'/'+url.substring(pos+1)+newVal;
            }else{
                return url.substring(0,pos)+'/'+url.substring(pos+1)+'/'+newVal;
            }
}

function replaceQueryParam(param, newval, search) {
    var regex = new RegExp("([?;&])" + param + "[^&;]*[;&]?");
    var query = search.replace(regex, "$1").replace(/&$/, '');

    return (query.length > 2 ? query + "&" : "?") + (newval ? param + "=" + newval : '');
}

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, '\\$&');
    var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, ' '));
}

function setCookie(cname,cvalue,exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires=" + d.toGMTString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}
var delete_cookie = function(name) {
    document.cookie = name + '=;expires=Thu, 01 Jan 1970 00:00:01 GMT;';
};
