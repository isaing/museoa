<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="es"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title>Museo Km/h Guanajuato</title>
  <!-- <base href="http://localhost/museoautomovil/"; />-->
    <base href="http://masviral.mx/ftpmasviral/web/"; />
    
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<!--<link rel="stylesheet" href="estilo.css">-->
    <link rel="stylesheet" href="estilo.css">
    <link rel="stylesheet" type="text/css" href="bower_components/dist/jquery.fancybox.min.css">
    <link rel="stylesheet" href="css/menu.min.css">    
    <link rel="stylesheet" href="bower_components/animate.css/animate.min.css">
    <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
</head>
<body id="top">
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v3.1';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<section class="mod-principal">
     <? include_once("header.php");?>
</section>

<? $idsala= intval($_GET["s"]);
	$sentencias = "CALL paPaginaSalas('$idIdioma','2','$idsala')";
	//echo $sentencias;
	$resultados = consulta($sentencias);
	$cuantoss=$resultados->num_rows;
	if($cuantoss>0){
		while ($rows = $resultados->fetch_array(MYSQLI_ASSOC)){
		//$idsala=$rows["idsala"];
		$idsalaidioma=$rows["idsala"];
		//echo $idsalaidioma;
		$numsala=$rows["numerosala"];
		$numerosala=$rows["numerosala"];
		$nombresala=$rows["nombresala"];
		$descripcionsala=$rows["descripcionsala"];
		$fraseambito=$rows["fraseambito"];
		$autorfrase=$rows["autorfrase"];
		$especial=$rows["especial"];
		$videosala=$rows["videosala"];
		$colortitulosala=$rows["colortitulosala"];
		$colortextosala=$rows["colortextosala"];
		$colorautorfrasesala=$rows["colorautorfrasesala"];
		}
	}
		$numerosala= titulotraduccion($idIdioma, 'Sala'). " " .str_pad ( $numerosala , 2 ,"0" , STR_PAD_LEFT  );

?>
<!--Módulo-principal-->
<? if($cuantoss>0){
	
	if($numsala!=5){
	?>	
	<div class="encabezado-sala" style="background: url(modulos/img/salas/salas-<? echo $idsalaidioma;?>.jpg) center center;
    background-repeat: no-repeat;
    background-color: #0a5264;
    background-size: cover;
    background-position: center;
    background-position-y: initial;">
    	<div class="contenido">
        	<div class="sala-titulo">
            	<h1 class="sala" style="color:<? echo $colortitulosala;?>"><? echo $numerosala;?></h1>
                <h2 class="tit-sala" style="color:<? echo $colortextosala;?>"><? echo $nombresala;?></h2>
            </div>
            <!--Sala-de Exhibición-->
            
        	<div class="frase" style="color:<? echo $colortextosala;?>">
            	<?  if($fraseambito <>""){?>
				<? echo $fraseambito;?>
                 <? }?>
            </div>
            <!--frase-->
			
			<div class="personaje" style="color:<? echo $colorautorfrasesala;?>">
             <?  if($autorfrase <>""){ echo $autorfrase; }?>
             </div>            
            
            <div class="descripcion-sala" style="color:<? echo $colortextosala;?>">
			<? echo nl2br($descripcionsala);?>
            </div>
            <!--Descripción-de-la-Sala-de-Exhibición-->
        </div>
        
            <div class="caja-elementos">
                <div class="elemento">
                  <?  if($videosala <>""){?>	
             		<a href="<? echo $video;?>" target="_blank"><img class="ico-video" src="img/video.svg" alt="Video Km/h"></a>
                 <? }?>
                </div>
                <!--video-->
    
                <div class="elemento">
                    <a href="seccion/4"><img class="ico-planifica" src="img/planifica-visita.svg" alt="Planifica tu visita al Km/h"></a>
                </div>
                <!--planifica-tu-visita-->
    
                <div class="elemento">
                        <div class="social">
                            <div class="fb-share-button" data-href="https://developers.facebook.com/docs/plugins/" data-layout="button_count" 
                            data-size="small" data-mobile-iframe="true">
                            <a target="_blank" href=
                            "https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fdevelopers.facebook.com%2Fdocs%2Fplugins%2F&amp;src=sdkpreparse"
                            class="fb-xfbml-parse-ignore">Compartir</a></div>
                        </div>
                        <!--facebook-->
                        
                        <div class="social">
                            <a href="https://twitter.com/share?ref_src=twsrc%5Etfw" class="twitter-share-button" data-show-count="false">Tweet</a>
                            <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
                        </div>
                </div>
                <!--compartir-redes-sociales-->
            </div>
            <!--social-media-->
    </div>
    <!-- sala del futuro-->
    
    <? }else{?>
    <div class="encabezado-sala-b">
    	<div class="contenido">
        	<div class="sala-titulo-b">
            	<h1 class="sala" style="color:<? echo $colortitulosala;?>"><? echo $numerosala;?></h1>
                <h2 class="tit-sala" style="color:<? echo $colortextosala;?>"><? echo $nombresala;?></h2>
            </div>
            <!--Sala-de Exhibición-->
            
        	<div class="frase" style="color:<? echo $colortextosala;?>">
            	<?  if($fraseambito <>""){?>
				<? echo $fraseambito;?>
                 <? }?> <br><br>
                
                <div class="personaje" style="color:<? echo $colorautorfrasesala;?>"> <?  if($autorfrase <>""){ echo $autorfrase; }?></div>
            </div>
            <!--Sala-de Exhibición-->
            
            <div class="descripcion-sala"  style="color:<? echo $colortextosala;?>">
			<? echo nl2br($descripcionsala);?>
            </div>
            <!--Descripción-de-la-Sala-de-Exhibición-->
            
            <div class="elemento">
            	 <?  if($videosala <>""){?>	
             		<a href="<? echo $video;?>" target="_blank"><img class="ico-video" src="img/video.svg" alt="Video Km/h"></a>
                 <? }?>
            	
            </div>
            <!--video-->

            <div class="elemento">
            	 <a href="seccion/4"><img class="ico-planifica" src="img/planifica-visita.svg" alt="Planifica tu visita al Km/h"></a>
            </div>
            <!--planifica-tu-visita-->

            <div class="elemento">
                    <div class="social">
                        <div class="fb-share-button" data-href="https://developers.facebook.com/docs/plugins/" data-layout="button_count" 
                        data-size="small" data-mobile-iframe="true">
                        <a target="_blank" href=
                        "https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fdevelopers.facebook.com%2Fdocs%2Fplugins%2F&amp;src=sdkpreparse"
                        class="fb-xfbml-parse-ignore">Compartir</a></div>
                    </div>
                    <!--facebook-->
                    
                    <div class="social">
                        <a href="https://twitter.com/share?ref_src=twsrc%5Etfw" class="twitter-share-button" data-show-count="false">Tweet</a>
                        <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
                    </div>
            </div>
            <!--compartir-redes-sociales-->
        </div>
    </div>
    
    
    <? }?>
    
          <!-- si son salas normales-->
           <? if($especial ==0){?>
           		 <!-- menús temáticos o secciones--> 
                <? $sentenciam = "CALL paPaginaSalasTematicos('$idIdioma','1','$idsalaidioma')";
            // echo $sentenciam;
                $resultadom = consulta($sentenciam);
                $cuantosm=$resultadom->num_rows;
				$estiloitem="class='mod-sala-1'";
				$estiloimagen="class='ilustra-sala-item-a'";
				if($cuantosm==1)
				{
					$estiloitem="class='mod-sala-0'";
					$estiloimagen="class='ilustra-sala-item-0'";
				}
				if($numsala==5){
					$estiloimagen="class='ilus-sala-5'";
				}
                if($cuantosm>0){
                    while ($rowm = $resultadom->fetch_array(MYSQLI_ASSOC)){
                    $idmenutematico=$rowm["idmenutematico"];
					$titulomenutematico=$rowm["titulomenutematico"];
                    $brevedescripcion=$rowm["brevedescripcion"];
                    $urlmenutematico=$rowm["urlmenutematico"];
                    $targetmenutematico=$rowm["targetmenutematico"];
					$imagenmenutematico=$rowm["imagenmenutematico"];
                    $colorfondo=$rowm["colorfondo"];
                    $colortexto=$rowm["colortexto"];
                    if ($urlmenutematico !="") {
                        echo"<div ".$estiloitem." style='background-color:".$colorfondo."'>";
					if($numsala!=5){
                    echo "<h2  style='color:".$colortexto."'>".$titulomenutematico."</h2>";
					}
					echo "
                    <div id='img-sala'>
                        <img ".$estiloimagen."  src='modulos/img/salastemas/".$imagenmenutematico."' alt='".$titulomenutematico."'>
                    </div>
                    <div class='link-descrip-sala' style='color:".$colortexto."'>
                        <a class='link-sala-a' href='".$urlmenutematico."' target='".$targetmenutematico."' style='color:".$colortexto."'>
                        ".$brevedescripcion."          
                        </a>
                    </div>
                </div>";
                    }else{
                        echo"<div ".$estiloitem."  style='background-color:".$colorfondo."'>
                    <h2 class='item-sala-a' style='color:".$colortexto."'>".$titulomenutematico."</h2>
                    <div id='img-sala'>
                        <img ".$estiloimagen."  src='modulos/img/salastemas/".$imagenmenutematico."' alt='".$titulomenutematico."'>
                    </div>
                    <div class='link-descrip-sala' style='color:".$colortexto."'>
                        ".$brevedescripcion."          
                    </div>
                </div>";
                    }
					
					
					
					
					
					}
				}?>
                
              
                <!--  terminan menús temáticos-->
            <? }?>
    
   		 <!-- si son salas normales-->
         <!-- --> <? if($especial ==1){?>
           	
    			<? include_once("salaespecial.php");?>
    
     		<? }?>
    <? }?>

      <?php include_once("footer.php");?>
     <!--Datos del Museo Km/h-->
     
	 <?php include_once("derechos.php");?>
	 <!--copyright © 2018 Km/h-->         
    

    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.min.js"><\/script>')</script>
    <script src="bower_components/dist/jquery.fancybox.min.js"></script>
    <script src="js/vendor/bootstrap.min.js"></script>
    <script src="js/scripts.js"></script>
    <script src="js/jquery.flexslider-min.js"></script>
    <script src="bower_components/classie/classie.js"></script>
    <script src="bower_components/jquery-waypoints/lib/jquery.waypoints.min.js"></script>
</body>
</html>
