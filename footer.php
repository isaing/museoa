  <footer>
     	<div class="contenido">
        	<div class="km-h">
            	<img src="img/km-h-museo.svg" alt="Museo Km/h">
            </div>
            <!--Logotipo Km/h-->
            
            <div class="col-4">
            	<? $tipomenu=2;
				include("modulos/menuprincipal/submenufooter.php");?>
            </div>
            <!--submenú-1 Km/h-->

            <div class="col-4">
            
                <? $tipomenu=3;
				include("modulos/menuprincipal/submenufooter.php");?>
            </div>
            <!--submenú-2 Km/h-->

            <div class="col-4">
            	<h2><? echo titulotraduccion($idIdioma, 'Ubicación y horarios');?></h2>
                <div class="dato">
                	<img class="ico-dato" src="img/address-f.svg"><? echo $direccion;?>
                </div>
                
                <div class="dato">
                	<img class="ico-dato" src="img/phone-f.svg"> <a href="tel:<? echo $telefono;?>" class="link-footer"><? echo $telefono;?></a>
                </div>
                <div class="dato">
                    <? echo $horarios;?>               
                </div>
                <? if ($correo!= ""){?>
                <div class="dato">
                    <img class="ico-dato" src="img/mail.svg"> <a class="link-footer" href="mailto:<? echo $correo;?>"><? echo $correo;?></a>             
                </div>
                <? }?>
            </div>
            <!--dirceción Km/h-->
        </div>
     </footer>
     <div id="politica">Este sitio utiliza cookies para mejorar tu experiencia, al continuar navegando aceptas nuestra política de cookies<br> <div id="btnok">Ok</div><div id="btncancelar">Cancelar</div></div>