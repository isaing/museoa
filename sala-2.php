<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="es"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title>Museo Km/h Guanajuato</title>
    <base href="http://masviral.mx/ftpmasviral/web/"; />
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="estilo.css">
    <link rel="stylesheet" href="css/menu.min.css">    
    <link rel="stylesheet" href="bower_components/animate.css/animate.min.css">
    <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
</head>
<body id="top">
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v3.1';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<section class="mod-principal">
    <section class="navigation">
        <div class="menu-usuario">
            <div class="idioma">
            </div>
            <!--idioma-->
        
            <div class="redes">
                <a class="facebook" href="#"></a>
                <a class="twitter" href="#"></a>
                <a class="youtube" href="#"></a>
                <a class="instagram" href="#"></a>
            </div>
            <!--redes-sociales-->
        </div>
        
        <div class="logo">
        	<div class="logo-prin">
	        	<img class="logo-km-h" src="img/museo-km-h-gto.svg" alt="Museo Km/h">
            </div>
             <!--Museo km/h-->
             
             <div class="busqueda">
             </div>
             <!--búsqueda-->
        </div>
        <!--Museo km/h-búsqueda-->
        
        <div class="menu-principal">
        	<div class="contenido">
                <div class="ve-menu">
                  <ul class="ve-menu-pc">
                    <li><a href="index.php">Inicio</a></li>
                    <li><a href="acerca-del-museo.php">Acerca del Museo</a></li>
                    <li><a href="#">Salas de Exhibición</a>
                      <ul>
                        <li><a href="#">Evolución e Historia del Automóvil</a></li>
                        <li><a href="sala-2.php">Anatomía del Automóvil</a></li>
                        <li><a href="#">Cadena de ensamblaje</a></li>
                        <li><a href="sala-4.php">Galería de Autos</a></li>
                        <li><a href="#">El futuro</a></li>
                      </ul>
                    </li>
                    <li><a href="eventos.php">Eventos</a></li>
                    <li><a href="#">Información para el visitante</a>
                      <ul>
                        <li><a href="#">Como llegar</a></li>
                        <li><a href="#">Costos</a></li>
                        <li><a href="#">Mapa del Museo</a></li>
                        <li><a href="#">Planifica tu visita</a></li>
                        <li><a href="#">Grupos y excursiones</a></li>
                      </ul>
                    </li>
                  </ul>
                </div>       
            </div>
        </div>
        <!--menú-principal-->
		<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/fastclick/1.0.6/fastclick.min.js"></script>
        <script src="js/menu.min.js"></script>
        <script>
            $('.ve-menu').menu({
                fontSize: 17,
                fontColor: '#222',
                bgColor: '#fff',
                hoverFontColor: '#0694a7',
                hoverBgColor: '#fff',
                itemSpace: 5,
                subFontSize: 17,
                itemWidth: 70,
                animate: 'slide',
                speed: 500,
            });
        </script>
    </section>
</section>
<!--Módulo-principal-->

	<div class="encabezado-sala">
    	<div class="contenido">
        	<div class="sala-titulo">
            	<h1 class="sala">Sala 02</h1>
                <h2 class="tit-sala">Anatomía del Automóvil</h2>
            </div>
            <!--Sala-de Exhibición-->
            
        	<div class="frase">
            	“Si hubiera preguntado a la gente lo que ellos querían, hubieran dicho: caballos más rápidos” <br><br>
                
                <div class="personaje">Henry Ford</div>
            </div>
            <!--Sala-de Exhibición-->
            
            <div class="descripcion-sala">
			La fabricación de automóviles se ha sofisticado con el tiempo. Luego de las primeras versiones, en las que los pasajeros carecían de 
            protección, surgió la necesidad de cubrir el auto. Así nacieron las carrocerías. Al principio, los materiales más usados eran madera y 
            chapa estampada y los responsables de “vestir” los carruajes, con técnicas casi artesanales, eran los carroceros, antiguos fabricantes de 
            carrozas.<br><br>

			El desarrollo de la tecnología permitió el empleo de materiales y diseños aerodinámicos que ofrecieran grandes ventajas. Surgieron, 
            entonces, las carrocerías de aluminio pulido. Sin embargo, como eran costosas, se adoptó la chapa de acero estampada, que aún sigue 
            utilizándose. El problema de este material es su peso, lo que implicó mayor consumo en otros aspectos de resistencia del auto. Con los 
            avances, el acero adquirió varios tipos de grosor, además de que es más ligero y resistente.
            </div>
            <!--Descripción-de-la-Sala-de-Exhibición-->
            
            <div class="elemento">
            	<a href="#"><img class="ico-video" src="img/video.svg" alt="Video Km/h"></a>
            </div>
            <!--video-->

            <div class="elemento">
            	<a href="#"><img class="ico-planifica" src="img/planifica-visita.svg" alt="Planifica tu visita al Km/h"></a>
            </div>
            <!--planifica-tu-visita-->

            <div class="elemento">
                    <div class="social">
                        <div class="fb-share-button" data-href="https://developers.facebook.com/docs/plugins/" data-layout="button_count" 
                        data-size="small" data-mobile-iframe="true">
                        <a target="_blank" href=
                        "https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fdevelopers.facebook.com%2Fdocs%2Fplugins%2F&amp;src=sdkpreparse"
                        class="fb-xfbml-parse-ignore">Compartir</a></div>
                    </div>
                    <!--facebook-->
                    
                    <div class="social">
                        <a href="https://twitter.com/share?ref_src=twsrc%5Etfw" class="twitter-share-button" data-show-count="false">Tweet</a>
                        <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
                    </div>
            </div>
            <!--compartir-redes-sociales-->
        </div>
    </div>
	<!--Sala-2-->
    
    <div class="mod-sala-1">
    	<h2 class="item-sala-a">Carrocería</h2>
        <div id="img-sala">
        	<img class="ilustra-sala-item-a" src="img/salas/carroceria.jpg" alt="Carrocería">
        </div>
        <div class="link-descrip-sala">
        	<a class="link-sala-a" href="#">
            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever 
            since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.            
            </a>
        </div>
    </div>
    <!--item-1-->

    <div class="mod-sala-2">
    	<h2 class="item-sala-a">Las partes móviles</h2>
        <div id="img-sala">
        	<img class="ilustra-sala-item-a" src="img/salas/partes-moviles.jpg" alt="Las partes móviles">
        </div>
        <div class="link-descrip-sala">
        	<a class="link-sala-a" href="#">
            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever 
            since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.            
            </a>
        </div>
    </div>
    <!--item-2-->

    <div class="mod-sala-3">
    	<h2 class="item-sala-b">Equipamiento y confort</h2>
        <div id="img-sala">
        	<img class="ilustra-sala-item-b" src="img/salas/equipamiento-confort.jpg" alt="Equipamiento y confort">
        </div>
        <div class="link-descrip-sala">
        	<a class="link-sala-b" href="#">
            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever 
            since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.            
            </a>
        </div>
    </div>
    <!--item-3-->

    <div class="mod-sala-4">
    	<h2 class="item-sala-b">Seguridad</h2>
        <div id="img-sala">
        	<img class="ilustra-sala-item-b" src="img/salas/seguridad.jpg" alt="Seguridad">
        </div>
        <div class="link-descrip-sala">
        	<a class="link-sala-b" href="#">
            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever 
            since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.            
            </a>
        </div>
    </div>
    <!--item-4-->

     <footer>
     	<div class="contenido">
        	<div class="km-h">
            	<img src="img/km-h-museo.svg" alt="Museo Km/h">
            </div>
            <!--Logotipo Km/h-->
            
            <div class="col-4">
            	<h2>Salas de Exhibición</h2>
                <ul class="submenu">
                	<li><a href="#">Autocinema</a></li>
                    <li><a href="#">Anatomía del  automóvil</a></li>
                    <li><a href="#">Cadena de producción</a></li>
                    <li><a href="#">Galería de autos</a></li>
                    <li><a href="#">El futuro</a></li>
                </ul>
            </div>
            <!--submenú-1 Km/h-->

            <div class="col-4">
            	<h2>Información para el Visitante</h2>
                <ul class="submenu">
                	<li><a href="#">Como llegar</a></li>
                    <li><a href="#">Costos</a></li>
                    <li><a href="#">Mapa del Museo</a></li>
                    <li><a href="#">Planifica tu visita</a></li>
                    <li><a href="#">Grupos y excursiones</a></li>
                </ul>
            </div>
            <!--submenú-2 Km/h-->

            <div class="col-4">
            	<h2>Ubicación y horarios</h2>
                <div class="dato">
                	<img class="ico-dato" src="img/address-f.svg"> Carretera de Cuota Silao-Gto. Km 3.8, Los Rodríguez, 36270 Silao, Gto.
                </div>
                
                <div class="dato">
                	<img class="ico-dato" src="img/phone-f.svg"> <a class="link-footer" href="#">01 472 + 0 000 000</a>
                </div>
                
                <div class="dato">
                    Abierto: 10:00 a 19:00 hrs.<br>
                    Martes a Domingo                
                </div>
            </div>
            <!--dirceción Km/h-->
        </div>
     </footer>
     <!--Datos del Museo Km/h-->
     
	<div class="derechos"> 
    	Copyright ©  <?php include ("copy-date.php")?> <img class="logo-museo" src="img/km-h-gto.svg" alt="Museo Km/h">
    </div>
    <!--copyright © 2018 Km/h-->     
    
    <section class="to-top">
            <div class="row">
                <div class="to-top-wrap">
                    <a href="#top" class="top"><span class="arriba">&#710;</span></i></a>
                </div>
            </div>
        </div>
    </section>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.min.js"><\/script>')</script>
    <script src="js/jquery.fancybox.pack.js"></script>
    <script src="js/vendor/bootstrap.min.js"></script>
    <script src="js/scripts.js"></script>
    <script src="js/jquery.flexslider-min.js"></script>
    <script src="bower_components/classie/classie.js"></script>
    <script src="bower_components/jquery-waypoints/lib/jquery.waypoints.min.js"></script>
</body>
</html>
